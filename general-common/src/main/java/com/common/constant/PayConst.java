/*
 * Copy Right@
 */
package com.common.constant;

/**
 * <pre>
 * </pre>
 *
 * @author ZhangZhenJie
 * @date 2018年03月14日
 */
public interface PayConst {
    String TRIGGER_ID = "triggerById";
    String TRIGGER_NAME = "triggerByName";
    String PAY_WAY = "payWay";
    String PAY_TYPE = "payType";
    String PAY_WAY_NAME = "payWayName";
    String PAY_TYPE_NAME = "payTypeName";
    String SOURCE_SERVICE = "sourceService";
    String TARGET_SERVICE = "targetService";

    public interface Refund {
        String REFUND_LOG_ID = "refundLogId";
        String PAY_ID = "payIds";
        String REFUND_FORM_ID = "refundFormId";
        String REFUND_FORM_NO = "refundFormNo";
        String FORM_ID = "formId";
        String FORM_NO = "formNo";
        String FORM_TYPE = "formType";
        String FORM_TYPE_NAME = "formTypeName";
        String REFUND_AMOUNT = "refundAmount";
        String OFFSET_PLATFORM_COUPON_AMOUNT = "offsetPlatformCouponAmount";
        String OFFSET_PLATFORM_SERVICE_FEE = "offsetPlatformServiceFee";
    }

    public interface Pay {
        String ORDERS = "orders";
        String PAY_AMOUNT = "payAmount";
        String PAY_COMMISSION = "payCommission";
        String IP = "ip";
        String ORDER_PAY_ID = "orderPayId";
        String ORDER_ID = "orderId";
        String ORDER_TYPE = "orderType";
        String ORDER_TYPE_NAME = "orderTypeName";
        String ORDER_NO = "orderNo";
    }
}