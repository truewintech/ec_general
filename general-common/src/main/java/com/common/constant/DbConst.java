/*
 * Copy Right@
 */
package com.common.constant;

import com.framework.util.DataUtil;

import java.util.EnumMap;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public class DbConst extends DataUtil {

    /**
     * 用户
     */
    public interface User {

        /**
         * 主键
         */
        interface Id {

            /**
             * 超级管理员
             */
            Integer ADMIN = 1;
        }
    }

    /**
     * 菜单
     */
    public interface Resource {

        /**
         * 类型
         */
        interface Type {

            /**
             * 菜单
             */
            String MENU = "MENU";

            /**
             * 地址
             */
            String URL = "URL";

            /**
             * 按钮
             */
            String BUTTON = "BUTTON";
        }

        /**
         * 使用范围
         */
        enum Range {

            /**
             * 管理后台，商家中心
             */
            SYSTEM, SELLER_CENTER;
            public static final String SYSTEM_NAME = "管理后台";
            public static final String SELLER_CENTER_NAME = "商家中心";
            public static EnumMap<Range, String> MAP = new EnumMap<>(Range.class);

            static {
                MAP.put(SYSTEM, SYSTEM_NAME);
                MAP.put(SELLER_CENTER, SELLER_CENTER_NAME);
            }
        }

    }

    /**
     * 集成事件
     */
    public interface Event {

        /**
         * 发送者或者接收者
         */
        interface SenderOrReceiver {

            /**
             * 用户服务
             */
            String USER_SERVICE = "user_service";

            /**
             * 消息服务
             */
            String MESSAGE_SERVICE = "message_service";

            /**
             * 主数据服务
             */
            String DATA_SERVICE = "data_service";

            /**
             * 文档服务
             */
            String DOCUMENT_SERVICE = "document_service";

            /**
             * 日志服务
             */
            String LOG_SERVICE = "log_service";

            /**
             * 支付服务
             */
            String PAY_SERVICE = "pay_service";

            /**
             * 搜索服务
             */
            String ES_SERVICE = "es_service";

            /**
             * tcc事务服务
             */
            String TCC_SERVICE = "tcc_service";

            /**
             * 卖家服务
             */
            String EC_SELLER_SERVICE = "ec_seller_service";

            /**
             * 买家服务
             */
            String EC_BUYER_SERVICE = "ec_buyer_service";

        }

        /**
         * 事件类型
         */
        interface EventType {

            /**
             * 发送给搜索服务
             */
            interface ToEs {

                /**
                 * 更新产品
                 */
                String UPDATE_PRODUCT = "update_product";

                /**
                 * 删除产品
                 */
                String DELETE_PRODUCT = "delete_product";

                /**
                 * 更新订单
                 */
                String UPDATE_ORDER = "update_order";

                /**
                 * 删除订单
                 */
                String DELETE_ORDER = "delete_order";
            }

            /**
             * 发送给日志服务
             */
            interface ToLog {

                /**
                 * 记录登录日志
                 */
                String RECORD_LOGIN_LOG = "record_login_log";
            }

            /**
             * 发送给Tcc服务
             */
            interface ToTcc {

                /**
                 * 取消Tcc事务
                 */
                String CANCEL_TCC = "cancel_tcc";
            }

            /**
             * 发送给卖家服务
             */
            interface ToEcSeller {

                /**
                 * 卖家新增管理人员与店铺关联
                 */
                String ADD_SHOP_IDS = "add_shop_ids";
            }

            /**
             * 发送给买家服务
             */
            interface ToEcBuyer {

                /**
                 * 微信登录新增buyer
                 */
                String ADD_BUYER_FROM_WECHAT = "add_buyer_from_wechat";
            }
        }
    }

    /**
     * 消息模板
     */
    public interface MessageTemplate {

        /**
         * 模板类型
         */
        interface TemplateType {

            /**
             * 短信
             */
            String SMS = "SMS";

            /**
             * 微信
             */
            String WX = "WX";

            /**
             * APP
             */
            String APP = "APP";

        }

        /**
         * 模板id
         */
        interface Id {

            /**
             * 注册短信模板id
             */
            Integer SMS_REGISTER_VALID_ID = 1;

            /**
             * 修改手机号短信模板id
             */
            Integer SMS_UPDATE_MOBILE_VALID_ID = 2;

            /**
             * 忘记密码短信模板id
             */
            Integer SMS_FORGET_PASSWORD_VALID_ID = 3;
        }
    }

    /**
     * tcc事件
     */
    public interface TccEvent {

        /**
         * 类型
         */
        interface TccType {

            /**
             * 新增用户
             */
            String ADD_USER = "ADD_USER";


            /**
             * 修改用户
             */
            String UPDATE_USER = "UPDATE_USER";

            /**
             * 门店注册
             */
            String STORE_REGISTER = "STORE_REGISTER";
        }

    }

    /**
     * 文档
     */
    public interface Document {

        /**
         * 类型
         */
        enum Type {

            /*
             * 商品图片
             */
            PRODUCT,
            /*
             * 订单评价图片
             */
            COMMENT;
            public static final String PRODUCT_NAME = "商品图片";
            public static final String COMMENT_NAME = "订单评价图片";
            public static EnumMap<Document.Type, String> MAP = new EnumMap<>(Document.Type.class);

            static {
                MAP.put(PRODUCT, PRODUCT_NAME);
                MAP.put(COMMENT, COMMENT_NAME);
            }
        }

        /**
         * 导出文件状态
         */
        interface ExportStatus{
        /**
         * 新建
         */
        String NEW = "new";

        /**
         * 完成
         */
        String FINISH = "finish";
        }
    }

    /**
     * 系统参数
     */
    public interface SystemParameter {

        /**
         * 主键
         */
        interface Id {

            /**
             * 发送短信是否需要验证码
             */
            String SEND_SMS_NEED_VALID = "send_sms_need_valid";

            /**
             * 推荐层级限制
             */
            String SHARE_LEVEL_LIMIT = "share_level_limit";
        }

    }
}
