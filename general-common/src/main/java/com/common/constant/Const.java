/*
 * Copy Right@
 */
package com.common.constant;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface Const {

    /**
     * 请求头
     */
    interface Header {

        /**
         * token名
         */
        String TOKEN = "X-Auth-Token";

        /**
         * 当前用户ID
         */
        String CURRENT_USER_ID = "X-CURRENT-USER-ID";

        /**
         * 访客ID
         */
        String VISITOR_ID = "Visitor-Id";

        /**
         * 返回类型
         */
        String CONTENT_TYPE = "Content-Type";

        /**
         * 当前微信 open ID
         */
        String CURRENT_OPEN_ID = "X-CURRENT-OPEN-ID";

        /**
         * 当前微信 app ID
         */
        String CURRENT_APP_ID = "X-CURRENT-APP-ID";

        /**
         * rsa签名
         */
        String RSA_SIGN = "RSA-SIGN";

        /**
         * rsa密文
         */
        String RSA_ENCRYPT = "RSA-ENCRYPT";

        /**
         * 当前微信 open ID
         */
        String REQUEST_TYPE = "REQUEST_TYPE";

        /**
         * 微信小程序请求
         */
        String WEIXIN_MINIAPP_REQUEST = "miniProgram";
    }

    /**
     * 验证码
     */
    interface RegistrationCode {

        /**
         * 禁止重复发送时间（毫秒）
         */
        long FORBIDDEN_TIME = 60 * 1000;

        /**
         * 验证码有效时间（秒）
         */
        int OVERTIME = 60 * 60;

        /**
         * 超时时间
         */
        String VALID_CODE_KEY = "validCode";

        /**
         * 发送时间
         */
        String SEND_TIME_KEY = "sendTime";
    }

    /**
     * 缓存key
     */
    interface CacheKey {

        /**
         * 注册短信验证码缓存
         */
        String SMS_VERIFICATION_CODE_REGISTER_PRE = "SMS_VERIFICATION_CODE_REGISTER_PRE";

        /**
         * 修改验证手机号短信验证码缓存
         */
        String SMS_VERIFICATION_CODE_UPDATE_MOBILE_PRE = "SMS_VERIFICATION_CODE_UPDATE_MOBILE_PRE";

        /**
         * 忘记密码短信验证码缓存
         */
        String SMS_VERIFICATION_CODE_FORGET_PASSWORD_PRE = "SMS_VERIFICATION_CODE_FORGET_PASSWORD_PRE";

        /**
         * 极光推送注册ID缓存
         */
        String AJPUSH_REGISTRATION_ID_PRE = "AJPUSH_REGISTRATION_ID_PRE";


        /**
         * 注册图形验证码缓存
         */
        String VERIFICATION_CODE_REGISTER_PRE = "VERIFICATION_CODE_REGISTER_PRE";

        /**
         * 修改验证手机号图形验证码缓存
         */
        String VERIFICATION_CODE_UPDATE_MOBILE_PRE = "VERIFICATION_CODE_UPDATE_MOBILE_PRE";

        /**
         * 忘记密码图形验证码缓存
         */
        String VERIFICATION_CODE_FORGET_PASSWORD_PRE = "VERIFICATION_CODE_FORGET_PASSWORD_PRE";

        /**
         * 登录图形验证码缓存
         */
        String VERIFICATION_CODE_LOGIN_PRE = "VERIFICATION_CODE_LOGIN_PRE";

        /**
         * 微信公众号accessToken缓存
         */
        String WECHAT_ACCESS_TOKEN_PRE = "WECHAT_ACCESS_TOKEN_PRE";

        /**
         * 微信token在redis缓存中的前缀
         */
        String WECHAT_LOGIN_TOKEN_PRE = "WECHAT_AUTH_TOKEN:";

        /**
         * 微信token在redis缓存中的前缀
         */
        String WECHAT_LOGIN_USER_INFO_PRE = "WECHAT_USER_INFO:";
    }

    /**
     * 集成网关
     */
    interface IntegrationGateway {

        /**
         * 参数名称
         */
        interface ParamName {

            /**
             * json参数名
             */
            String JSON = "jsondata";

            /**
             * 签名参数名
             */
            String SIGN = "signature";

            /**
             * 时间参数名
             */
            String TIME = "producetime";

        }
    }

    /**
     * 通用clause
     */
    interface IsDel {

        /**
         * 是否删除条件
         */
        String CLAUSE = "is_delete = 0";
    }
}
