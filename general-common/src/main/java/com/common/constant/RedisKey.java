/*
 * Copy Right@
 */
package com.common.constant;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface RedisKey {

    /**
     * 基础数据服务
     */
    interface DataService {
        /**
         * 数据字典
         */
        interface Dictionary {

            /**
             * 系统参数服务实现类
             */
            String IMPL = "'com.data.service.impl.DictionaryServiceImpl'+";

            /**
             * 实现方法
             */
            interface Method {

                /**
                 * 获取全部
                 */
                String FIND_ALL = "'findAll'";

                /**
                 * 根据类型获取
                 */
                String FIND_BY_TYPE = "'findByType-'+";
            }

        }

        /**
         * 系统参数
         */
        interface SystemParameter {

            /**
             * 系统参数服务实现类
             */
            String IMPL = "'com.data.service.impl.SystemParameterServiceImpl'+";

            /**
             * 系统参数方法名称
             */
            interface Method {

                /**
                 * 根据key获取系统参数
                 */
                String FIND_SYSTEM_PARAMETER = "'-findSystemParameter-'+";

            }

        }

        /**
         * 区域
         */
        interface Area {

            /**
             * 地址服务实现类
             */
            String IMPL = "'com.data.service.impl.AreaServiceImpl'+";

            /**
             * 区域方法名称
             */
            interface Method {

                /**
                 * 根据父获取所有区域
                 */
                String FIND_ALL_BY_PARENT_ID = "'-findAllByParentId-'+";

                /**
                 * 根据父获取有效区域
                 */
                String FIND_ACTIVE_AREA_LIST_BY_PARENT_ID = "'-findActiveAreaListByParentId-'+";

                /**
                 * 根据父获取区域
                 */
                String FIND_AREA_LIST_BY_PARENT_ID = "'-findAreaListByParentId-'+";

            }

        }

    }


}