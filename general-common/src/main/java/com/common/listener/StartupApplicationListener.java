/*
 * Copy Right@
 */
package com.common.listener;

import com.framework.listener.RedisKeyListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Slf4j
public class StartupApplicationListener extends RedisKeyListener {

    /**
     * 首次执行
     */
    private static boolean FIRST = true;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (!FIRST) {
            return;
        }

        super.onApplicationEvent(event);

        FIRST = false;
    }
}
