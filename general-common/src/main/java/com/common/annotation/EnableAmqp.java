/*
 * Copy Right@
 */
package com.common.annotation;

import com.common.amqp.AmqpConfig;
import com.framework.amqp.EventSender;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Configuration
@EnableRabbit
@Import({AmqpConfig.class, EventSender.class})
public @interface EnableAmqp {
}
