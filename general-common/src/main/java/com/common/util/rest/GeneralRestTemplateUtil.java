/*
 * Copy Right@
 */
package com.common.util.rest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.framework.constant.HttpResponse;
import com.framework.exception.MessageException;
import com.framework.mvc.result.Result;
import com.framework.util.RestTemplateUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Slf4j
public class GeneralRestTemplateUtil {

    /**
     * 数据项目
     */
    public static class Data {

        /**
         * 获取系统参数
         *
         * @param serviceUrl 服务地址
         * @param id         参数id
         * @return 系统参数
         */
        public static String getParam(String serviceUrl, String id) {
            Result result = RestTemplateUtil.getForObject(serviceUrl
                    + "systemParameter/findSystemParameter?id={id}", id);
            if (result == null || result.getCode() != HttpResponse.SUCCESS) {
                throw new MessageException("获取系统参数异常，参数ID：" + id);
            }

            Object data = result.getData();
            if (data == null) {
                return null;
            }

            String json = data.toString();
            JSONObject obj = JSON.parseObject(json);
            return obj.getString("value");
        }

    }

    /**
     * 用户项目
     */
    public static class User {

        /**
         * 查找所有菜单
         *
         * @param serviceUrl 服务地址
         * @return 结果
         */
        public static Result findAllMenus(String serviceUrl) {
            return RestTemplateUtil.getForObject(
                    serviceUrl + "resource/findAllMenus");
        }

        /**
         * 通过用户id查找菜单
         *
         * @param serviceUrl 服务地址
         * @param userId     用户id
         * @return 结果
         */
        public static Result findMenuByUserId(String serviceUrl, Integer userId) {
            return RestTemplateUtil.getForObject(
                    serviceUrl + "resource/findUserMenus?userId={userId}"
                    , userId);
        }

        /**
         * 通过id获取用户
         *
         * @param serviceUrl 服务地址
         * @param id         用户id
         * @return 结果
         */
        public static Result findUserById(String serviceUrl, Integer id) {
            return RestTemplateUtil.getForObject(serviceUrl
                    + "user/findByIdForLogin?id={id}", id);
        }

        /**
         * 通过账户获取用户
         *
         * @param serviceUrl 服务地址
         * @param account    账户
         * @return 结果
         */
        public static Result findUserByAccount(String serviceUrl, String account) {
            return RestTemplateUtil.getForObject(serviceUrl
                    + "user/findByAccountForLogin?account={account}", account);
        }

        /**
         * 通过手机获取用户
         *
         * @param serviceUrl 服务地址
         * @param mobile     手机
         * @return 结果
         */
        public static Result findUserByMobile(String serviceUrl, String mobile) {
            return RestTemplateUtil.getForObject(serviceUrl
                    + "user/findByMobileForLogin?mobile={mobile}", mobile);
        }

    }

    /**
     * 日志项目
     */
    public static class Log {

        /**
         * 添加登录日志
         *
         * @param serviceUrl 服务地址
         * @param userId     用户id
         * @param account    账号
         * @return 是否成功
         */
        public static boolean addLoginLog(String serviceUrl, Integer userId, String account) {
            return RestTemplateUtil.get(serviceUrl
                            + "loginLog/addLoginLog?userId={userId}&account={account}"
                    , userId, account);
        }

        /**
         * 根据用户ID获取上一次登录记录
         *
         * @param userId 用户ID
         * @return 上次登录记录
         */
        public static JSONObject findLastLoginLogByUserId(String serviceUrl, Integer userId) {
            Result result = RestTemplateUtil.getForObject(serviceUrl
                            + "loginLog/findLastLoginLogByUserId?userId={userId}"
                    , userId);
            if (result == null || result.getCode() != HttpResponse.SUCCESS) {
                log.error("根据用户ID获取上一次登录记录，id：{}", userId);
                throw new MessageException("操作失败，请稍后重试");
            }
            return dataToJsonObject(result.getData());
        }

    }

    /**
     * 将Result中的data转换为JSONObject
     *
     * @param data Result中的data
     * @return JSONObject类型的data
     */
    private static JSONObject dataToJsonObject(Object data) {
        if (data == null) {
            return null;
        }
        return JSON.parseObject(data.toString());
    }
}
