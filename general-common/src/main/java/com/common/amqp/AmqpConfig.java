/*
 * Copy Right@
 */
package com.common.amqp;

import com.common.constant.DbConst;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Configuration
public class AmqpConfig {

    /**
     * 消息通知的消息队列名称
     */
    public static final String MESSAGE_QUEUE_NAME = "message_notification_service";

    /**
     * 消息通知的消息队列
     *
     * @return 消息队列
     */
    @Bean
    public Queue messageNotificationQueue() {
        return new Queue(MESSAGE_QUEUE_NAME);
    }

    /**
     * 用户行为日志的消息队列名称
     */
    public static final String LOG_BEHAVIOR_QUEUE_NAME = "log_behavior_receive_service";

    /**
     * 用户行为日志的消息队列
     *
     * @return 消息队列
     */
    @Bean
    public Queue logBehaviorReceiveQueue() {
        return new Queue(LOG_BEHAVIOR_QUEUE_NAME);
    }

    /**
     * 登录日志的消息队列名称
     */
    public static final String LOG_LOGIN_QUEUE_NAME = "log_login_receive_service";

    /**
     * 登录日志的消息队列
     *
     * @return 消息队列
     */
    @Bean
    public Queue logLoginReceiveQueue() {
        return new Queue(LOG_LOGIN_QUEUE_NAME);
    }

    /**
     * 文档服务消息队列
     *
     * @return 消息队列
     */
    @Bean
    public Queue documentQueue() {
        return new Queue(DbConst.Event.SenderOrReceiver.DOCUMENT_SERVICE);
    }

    /**
     * 主数据服务消息队列
     *
     * @return 消息队列
     */
    @Bean
    public Queue dataQueue() {
        return new Queue(DbConst.Event.SenderOrReceiver.DATA_SERVICE);
    }

    /**
     * 消息服务消息队列
     *
     * @return 消息队列
     */
    @Bean
    public Queue messageQueue() {
        return new Queue(DbConst.Event.SenderOrReceiver.MESSAGE_SERVICE);
    }

    /**
     * 用户服务消息队列
     *
     * @return 消息队列
     */
    @Bean
    public Queue userQueue() {
        return new Queue(DbConst.Event.SenderOrReceiver.USER_SERVICE);
    }

    /**
     * 日志服务消息队列
     *
     * @return 消息队列
     */
    @Bean
    public Queue logQueue() {
        return new Queue(DbConst.Event.SenderOrReceiver.LOG_SERVICE);
    }

    /**
     * 搜索服务消息队列
     *
     * @return 消息队列
     */
    @Bean
    public Queue esQueue() {
        return new Queue(DbConst.Event.SenderOrReceiver.ES_SERVICE);
    }

    /**
     * gener_pay 消息队列
     *
     * @return 消息队列
     */
    @Bean
    public Queue payQueue() {
        return new Queue(DbConst.Event.SenderOrReceiver.PAY_SERVICE);
    }

    /**
     * 推荐关系的消息队列
     */
    public static final String SHARING_QUEUE_NAME = "sharing_receive_service";

    /**
     * 推荐关系的消息队列
     *
     * @return 消息队列
     */
    @Bean
    public Queue sharingReceiveQueue() {
        return new Queue(SHARING_QUEUE_NAME);
    }

    /**
     * 实例化消息模板，初始化消息转换器
     *
     * @param connectionFactory 连接工厂类
     * @return 消息模板
     */
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(new Jackson2JsonMessageConverter());
        return template;
    }

    /**
     * 实例化消息监听类，初始化消息转换器
     *
     * @param connectionFactory 连接工厂类
     * @return 消息监听器
     */
    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(new Jackson2JsonMessageConverter());
        return factory;
    }
}
