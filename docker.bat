call mvn clean package

echo "start to server"
cd registry-server
call docker.bat
cd ../turbine-service
call docker.bat
cd ../zipkin-server
call docker.bat
echo "end to server"

echo "start to service"
cd ../general-parent-service/data-service/
call docker.bat
cd ../document-service
call docker.bat
cd ../es-service
call docker.bat
cd ../log-service
call docker.bat
cd ../message-service
call docker.bat
cd ../pay-service
call docker.bat
cd ../tcc-service
call docker.bat
cd ../user-service
call docker.bat
echo "end to service"

echo "start to gateway"
cd ../../general-parent-gateway/api-gateway
call docker.bat
cd ../integration-gateway
call docker.bat
echo "end to gateway"

pause