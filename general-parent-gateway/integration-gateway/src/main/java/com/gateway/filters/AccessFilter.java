/*
 * Copy Right@
 */
package com.gateway.filters;

import com.alibaba.fastjson.JSON;
import com.common.constant.Const;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.framework.constant.HttpResponse;
import com.framework.mvc.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
@Slf4j
public class AccessFilter extends ZuulFilter {

    /**
     * 请求签名加密串
     */
    private String key = "rabg4^BViYd!*qdQ";

    /**
     * 过期时间（毫秒）
     */
    private int expirationTime = 5 * 60 * 1000;

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        if (!validToken(ctx)) {
            ctx.setSendZuulResponse(false);
            ctx.addZuulResponseHeader(Const.Header.CONTENT_TYPE,
                    MediaType.APPLICATION_JSON_UTF8_VALUE);
        }

        return null;
    }

    /**
     * @param ctx 请求上下文
     */
    private boolean validToken(RequestContext ctx) {
        HttpServletRequest request = ctx.getRequest();
        //为调试输出crm请求的内容
        Enumeration<String> names = request.getParameterNames();
        while (names.hasMoreElements()) {
            String name = names.nextElement();
            log.info("request ParamName = " + name);
            log.info("\t\t\t\t paramValue=" + request.getParameter(name));
        }

        String signature = request.getParameter(Const.IntegrationGateway.ParamName.SIGN);
        String jsonData = request.getParameter(Const.IntegrationGateway.ParamName.JSON);
        String produceTime = request.getParameter(Const.IntegrationGateway.ParamName.TIME);
        if (StringUtils.isBlank(signature) || StringUtils.isBlank(jsonData) || StringUtils.isBlank(produceTime)) {
            log.warn("参数为空");
            ctx.setResponseBody(toResult(HttpResponse.EMPTY, "参数为空"));
            return false;
        }

        try {
            long time = Long.parseLong(produceTime);
            if (System.currentTimeMillis() - time > expirationTime) {
                // 5分钟后过期
                log.warn("请求过期");
                ctx.setResponseBody(toResult(HttpResponse.URL_TIME_OUT, "请求过期"));
                return false;
            }
        } catch (Exception ex) {
            log.warn("时间参数格式异常");
            ctx.setResponseBody(toResult(HttpResponse.INVALID_PARAM, "时间参数格式异常"));
            return false;
        }

        String md5 = DigestUtils.md5Hex(jsonData + produceTime + key);
        //校验串忽略大小写
        if (!md5.equalsIgnoreCase(signature)) {
            log.warn("请求非法");
            ctx.setResponseBody(toResult(HttpResponse.FAIL, "请求非法"));
            return false;
        }

        return true;
    }

    /**
     * JSON Result
     *
     * @param code    状态码
     * @param message 返回提示
     * @return result
     */
    protected String toResult(int code, String message) {
        return JSON.toJSONString(new Result(code, message));
    }

}
