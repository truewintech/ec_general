/*
 * Copy Right@
 */
package com.gateway;

import com.framework.annotation.*;
import com.framework.mvc.fallback.JsonFallbackProvider;
import com.framework.util.YamlUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.netflix.zuul.filters.Route;
import org.springframework.cloud.netflix.zuul.filters.SimpleRouteLocator;
import org.springframework.cloud.netflix.zuul.filters.discovery.PatternServiceRouteMapper;
import org.springframework.cloud.netflix.zuul.filters.route.ZuulFallbackProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@EnableZuulProxy
@SpringCloudApplication
@EnableRedis
@EnableFastJson
@EnableStatelessShiro
@EnableRedisManager
@EnableRestTemplateUtil
@Import({YamlUtil.class})
public class ApiGatewayApplication {

    /**
     * 启动Ribbon 负载均衡能力
     *
     * @return restTemplate
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(
            HttpMessageConverters fastJsonHttpMessageConverters) {
        return new RestTemplate(
                fastJsonHttpMessageConverters.getConverters());
    }

    /**
     * 服务路由匹配器
     *
     * @return 路由匹配器
     */
    @Bean
    public PatternServiceRouteMapper serviceRouteMapper() {
        return new PatternServiceRouteMapper(
                "(?<name>^.+)-(?<version>v.+$)",
                "${version}/${name}");
    }

    /**
     * zuul断路器配置
     *
     * @param simpleRouteLocator 路由参数
     * @return zuulFallbackProviders
     */
    @Bean
    public Set<ZuulFallbackProvider> zuulFallbackProviders(
            SimpleRouteLocator simpleRouteLocator) {
        Set<ZuulFallbackProvider> zuulFallbackProviders = new HashSet<>();
        List<Route> routes = simpleRouteLocator.getRoutes();
        routes.forEach(zuulRoute -> zuulFallbackProviders
                .add(new JsonFallbackProvider(zuulRoute.getLocation())));
        return zuulFallbackProviders;
    }

    /**
     * 程序入口
     *
     * @param args 启动参数
     */
    public static void main(String[] args) {
        SpringApplication.run(ApiGatewayApplication.class, args);
    }

}