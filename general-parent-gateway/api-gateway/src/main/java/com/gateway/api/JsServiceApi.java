/*
 * Copy Right@
 */
package com.gateway.api;

import com.framework.mvc.api.BaseApi;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <pre>
 * </pre>
 *
 * @author ChenChao
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("/js")
public class JsServiceApi extends BaseApi {

    /**
     * 生成constants.js
     *
     * @return constants.js
     */
    @GetMapping(value = "/constant.js", produces = "application/javascript;charset=UTF-8")
    @ResponseBody
    public String constantJs() {
        //TODO 返回静态变量
        String content = "(function(window) {window.HTTP = {SUCCESS:0,FAIL:999,URL_TIME_OUT:2,AUTO_LOGIN_FAILED:3}})(window);";
        return content;
    }
}
