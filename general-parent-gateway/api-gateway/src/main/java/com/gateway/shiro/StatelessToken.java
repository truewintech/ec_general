/*
 * Copy Right@
 */
package com.gateway.shiro;

import com.gateway.auth.AuthContext;
import lombok.Getter;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Getter
public class StatelessToken implements AuthenticationToken {

    /**
     * token 操作令牌
     */
    private String token;

    /**
     * 当前用户
     */
    private User user;

    public StatelessToken(String token, AuthContext authContext) {
        this.token = token;
        this.user = authContext.getUser();
    }

    @Override
    public Object getPrincipal() {
        return user.getId();
    }

    @Override
    public Object getCredentials() {
        return token;
    }

}
