/*
 * Copy Right@
 */

package com.gateway.shiro;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.framework.constant.HttpResponse;
import com.framework.mvc.result.Result;
import com.framework.shiro.AbstractAuthorizingRealm;
import com.framework.shiro.app.AppToken;
import com.gateway.auth.AuthTokenUtil;
import com.gateway.service.ResourceService;
import com.gateway.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class StatelessRealm extends AbstractAuthorizingRealm {

    /**
     * 支持那些token
     *
     * @param token 认证token
     * @return 是否支持
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof StatelessToken
                || token instanceof UsernamePasswordToken;
    }

    /**
     * 用户服务
     */
    @Autowired
    private UserService userService;

    /**
     * 资源服务
     */
    @Autowired
    private ResourceService resourceService;

    /**
     * 删除字符
     */
    private String deleteStr = "isDelete";

    /**
     * 有效字符
     */
    private String activeStr = "isActive";

    /**
     * 授权
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(
            PrincipalCollection principals) {
        User user = (User) principals.getPrimaryPrincipal();
        Integer userId = (Integer) user.getId();
        // 查询账号
        Result result = userService.findById(userId);
        if (HttpResponse.SUCCESS != result.getCode()) {
            throw new RuntimeException("连接服务器异常");
        }

        if (result.getData() == null) {
            AuthTokenUtil.logout(userId);
            return null;
        }

        user = result.toObject(User.class);
        if (Boolean.TRUE.equals(user.getBoolean(deleteStr))
                || Boolean.FALSE.equals(user.getBoolean("deleteStr"))) {
            AuthTokenUtil.logout(userId);
            return null;
        }

        // Shiro权限认证，获取角色相关的权限信息
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        // url权限暂时未实现过滤
        result = resourceService.findMenuByUserId(userId);
        if (HttpResponse.SUCCESS == result.getCode()) {
            if (result.getData() == null) {
                AuthTokenUtil.logout(userId);
                return null;
            }

            // 设置用户权限
            JSONArray permissions = result.getJSONArray();
            for (int i = 0; i < permissions.size(); i++) {
                JSONObject permission = permissions.getJSONObject(i);
                String code = permission.getString("code");
                if (StringUtils.isNotBlank(code)) {
                    info.addStringPermission(code);
                }
            }
        }
        return info;
    }

    /**
     * 登录验证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken authenticationToken)
            throws AuthenticationException {
        // TOKEN 验证
        if (authenticationToken instanceof StatelessToken) {
            StatelessToken token = (StatelessToken) authenticationToken;
            User user = token.getUser();
            return new SimpleAuthenticationInfo(user, token.getToken(), getName());
        } else if (authenticationToken instanceof AppToken) {
            //app登录验证
            AppToken token = (AppToken) authenticationToken;
            Integer userId = token.getUserId();
            Result result = userService.findById(userId);
            if (HttpResponse.SUCCESS == result.getCode()) {
                if (result.getData() == null) {
                    // 没找到帐号
                    throw new UnknownAccountException();
                } else {
                    User user = result.toObject(User.class);
                    return getAuthenticationInfoFromUser(user);
                }
            } else {
                throw new RuntimeException("连接服务器异常");
            }
        } else {
            // 用户名密码验证
            Object objectUsername = authenticationToken.getPrincipal();
            Result result = userService
                    .findByMobile((String) objectUsername);
            if (HttpResponse.SUCCESS != result.getCode()) {
                throw new RuntimeException("连接服务器异常");
            }

            //将输入的账号作为手机号能找到用户
            JSONObject data = (JSONObject) result.getData();
            if (data != null) {
                User user = result.toObject(User.class);
                return getAuthenticationInfoFromUser(user);
            }

            result = userService
                    .findByAccount((String) objectUsername);
            if (HttpResponse.SUCCESS != result.getCode()) {
                throw new RuntimeException("连接服务器异常");
            }

            //将输入的账号能找到用户
            data = (JSONObject) result.getData();
            if (data != null) {
                User user = result.toObject(User.class);
                return getAuthenticationInfoFromUser(user);
            }

            // 没找到帐号
            throw new UnknownAccountException();
        }
    }

    /**
     * 删除特殊信息
     *
     * @param user 用户信息
     */
    private void deleteSaltAndPassword(User user) {
        user.remove("salt");
        user.remove("password");
    }

    /**
     * 判断用户是否可用，并组织成校验数据
     *
     * @param user user对象
     * @return 权限信息
     * @throws AuthenticationException 异常
     */
    private AuthenticationInfo getAuthenticationInfoFromUser(User user) throws AuthenticationException {
        {
            if (Boolean.TRUE.equals(user.getBoolean(deleteStr))) {
                // 没找到帐号
                throw new UnknownAccountException();
            }
            if (Boolean.FALSE.equals(user.getBoolean(activeStr))) {
                // 账号停用
                throw new LockedAccountException("账号已停用");
            }
            String password = user.getPassword();
            String account = user.getAccount();
            String salt = user.getSalt();
            deleteSaltAndPassword(user);

            // 获取用户的组织
            return new SimpleAuthenticationInfo(user, password,
                    ByteSource.Util.bytes(account + salt), getName());
        }
    }

}
