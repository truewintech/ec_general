/*
 * Copy Right@
 */
package com.gateway.shiro.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


/**
 * <pre>
 * </pre>
 *
 * @author ChenChao
 * @date 2017年09月04日
 */
@Service
public class AppDeviceId {

    /**
     * 缓存管理器
     */
    @Autowired
    private ValueOperations<String, Object> valueOperations;

    /**
     * 缓存管理器
     */
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * key前缀
     */
    private static final String CACHE_KEY = "APP_DEVICE_ID:";

    /**
     * 设置key
     *
     * @param key   用户id
     * @param value token加密串
     */
    public void set(Serializable key, String value) {
        valueOperations.set(CACHE_KEY + key, value);
    }

    /**
     * 取得key
     *
     * @param key 用户id
     * @return 缓存中的key
     */
    public String get(Serializable key) {
        return (String) valueOperations.get(CACHE_KEY + key);
    }

    /**
     * 删除缓存
     *
     * @param keys 用户ids
     */
    public void delete(Serializable... keys) {
        Set<String> strings = new HashSet<>();
        for (Serializable obj : keys) {
            if (obj != null) {
                strings.add(CACHE_KEY + obj);
            }
        }
        redisTemplate.delete(strings);
    }

}
