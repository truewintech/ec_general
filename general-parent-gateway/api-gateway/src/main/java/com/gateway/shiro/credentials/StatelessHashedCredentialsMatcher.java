/*
 * Copy Right@
 */

package com.gateway.shiro.credentials;

import com.framework.shiro.app.AppToken;
import com.gateway.shiro.StatelessToken;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class StatelessHashedCredentialsMatcher
        extends HashedCredentialsMatcher {

    /**
     * 默认构造 设置加密算法
     */
    public StatelessHashedCredentialsMatcher() {
        // 指定hash算法为MD5
        super("md5");
        // 指定散列次数为2次
        super.setHashIterations(2);
        // true指定Hash散列值使用Hex加密存储
        // false表明hash散列值用用Base64-encoded存储
        super.setStoredCredentialsHexEncoded(true);
    }

    @Override
    public boolean doCredentialsMatch(AuthenticationToken authenticationToken,
                                      AuthenticationInfo info) {
        if (authenticationToken instanceof StatelessToken || authenticationToken instanceof AppToken) {
            return true;
        }
        return super.doCredentialsMatch(authenticationToken, info);
    }
}
