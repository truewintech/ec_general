/*
 * Copy Right@
 */
package com.gateway.shiro;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.framework.shiro.ShiroUser;

import java.io.Serializable;

/**
 * <pre>
 * </pre>
 *
 * @author ChenChao
 * @date 2017年09月04日
 */
public class User extends JSONObject implements ShiroUser {

    @Override
    @JSONField(serialize = false)
    public String getAccount() {
        return this.getString("account");
    }

    @Override
    @JSONField(serialize = false)
    public String getPassword() {
        return this.getString("password");
    }

    @Override
    @JSONField(serialize = false)
    public String getName() {
        return this.getString("name");
    }

    @Override
    @JSONField(serialize = false)
    public Serializable getId() {
        return this.getInteger("id");
    }

    @Override
    @JSONField(serialize = false)
    public String getSalt() {
        return this.getString("salt");
    }

}
