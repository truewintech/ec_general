/*
 * Copy Right@
 */
package com.gateway.service;

import com.framework.mvc.result.Result;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface UserService {

    /**
     * 查找账户
     *
     * @param id 用户id
     * @return 返回结果
     */
    Result findById(Integer id);

    /**
     * 查找账户
     *
     * @param account 账号
     * @return 返回结果
     */
    Result findByAccount(String account);

    /**
     * 查找账户
     *
     * @param mobile 手机号
     * @return 返回结果
     */
    Result findByMobile(String mobile);
}
