/*
 * Copy Right@
 */
package com.gateway.service;

import com.framework.mvc.result.Result;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface ResourceService {

    /**
     * 查询所有菜单
     *
     * @return 菜单
     */
    Result findAllMenus();

    /**
     * 根据 用户ID 查询菜单
     *
     * @param userId 用户ID
     * @return 菜单权限
     */
    Result findMenuByUserId(Integer userId);
}
