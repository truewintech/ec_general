/*
 * Copy Right@
 */
package com.gateway.service.impl;

import com.common.util.rest.GeneralRestTemplateUtil;
import com.framework.mvc.result.Result;
import com.gateway.filters.LocationUtil;
import com.gateway.service.ResourceService;
import org.springframework.stereotype.Service;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
public class ResourceServiceImpl implements ResourceService {

    @Override
    public Result findAllMenus() {
        return GeneralRestTemplateUtil.User.findAllMenus(LocationUtil.getLocation());
    }

    @Override
    public Result findMenuByUserId(Integer userId) {
        return GeneralRestTemplateUtil.User.findMenuByUserId(LocationUtil.getLocation(), userId);
    }

}
