/*
 * Copy Right@
 */
package com.gateway.service.impl;

import com.common.util.rest.GeneralRestTemplateUtil;
import com.framework.mvc.result.Result;
import com.gateway.filters.LocationUtil;
import com.gateway.service.UserService;
import org.springframework.stereotype.Service;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
public class UserServiceImpl implements UserService {

    @Override
    public Result findById(Integer id) {
        return GeneralRestTemplateUtil.User.findUserById(LocationUtil.getLocation(), id);
    }

    @Override
    public Result findByAccount(String account) {
        return GeneralRestTemplateUtil.User.findUserByAccount(LocationUtil.getLocation(), account);
    }

    @Override
    public Result findByMobile(String mobile) {
        return GeneralRestTemplateUtil.User.findUserByMobile(LocationUtil.getLocation(), mobile);
    }

}
