/*
 * Copy Right@
 */
package com.gateway.service;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface LoginLogService {

    /**
     * 添加登录日志
     *
     * @param userId  用户id
     * @param account 账号
     * @return 是否成功
     */
    boolean addLoginLog(Integer userId, String account);

}
