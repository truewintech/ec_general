/*
 * Copy Right@
 */
package com.gateway.service.impl;

import com.common.util.rest.GeneralRestTemplateUtil;
import com.gateway.filters.LocationUtil;
import com.gateway.service.LoginLogService;
import org.springframework.stereotype.Service;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
public class LoginLogServiceImpl implements LoginLogService {

    @Override
    public boolean addLoginLog(Integer userId, String account) {
        return GeneralRestTemplateUtil.Log.addLoginLog(LocationUtil.getLocation(), userId, account);
    }
}
