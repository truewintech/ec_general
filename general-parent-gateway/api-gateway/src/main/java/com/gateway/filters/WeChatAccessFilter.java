/*
 * Copy Right@
 */
package com.gateway.filters;

import com.alibaba.fastjson.JSON;
import com.common.constant.Const;
import com.framework.constant.ConstList;
import com.framework.constant.HttpResponse;
import com.framework.exception.MessageException;
import com.framework.filters.AbstractCrossFilter;
import com.framework.mvc.result.Result;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author chenchao
 * @date 2017年09月04日
 */
@Component
@Slf4j
public class WeChatAccessFilter extends AbstractCrossFilter {

    /**
     * 不过滤的地址
     */
    @Value("${filter.weChat.excludeUrls}")
    private String[] excludeUrls;

    @Value("${weChat.appId}")
    private String appId;

    /**
     * 缓存管理器
     */
    @Autowired
    private ValueOperations<String, Object> valueOperations;

    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        //判断是否是微信小程序请求，是则不走过滤器
        if (Const.Header.WEIXIN_MINIAPP_REQUEST.equals(request.getHeader(Const.Header.REQUEST_TYPE))) {
            return false;
        }
        if (!matchWeChat(request.getHeader(ConstList.USER_AGENT))) {
            return false;
        }

        HttpServletResponse response = ctx.getResponse();
        onCrossDomain(request, response);
        // 检查是否是OPTIONS请求 如果是直接跳过
        String method = request.getMethod();
        if (method.equalsIgnoreCase(RequestMethod.OPTIONS.name())) {
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(HttpStatus.OK.value());
            return false;
        }

        // 请求URL
        String uri = request.getRequestURI();
        log.info("请求url:{}", uri);
        //排除的url不校验token
        // 是否是排除的url
        for (String excludeUrl : excludeUrls) {
            if (StringUtils.contains(uri, excludeUrl)) {
                // 匹配成功，成功返回
                return false;
            }
        }

        //校验token
        try {
            validToken(request);
        } catch (MessageException e) {
            ctx.addZuulResponseHeader(Const.Header.CONTENT_TYPE,
                    MediaType.APPLICATION_JSON_UTF8_VALUE);
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(HttpStatus.OK.value());
            ctx.setResponseBody(JSON.toJSONString(new Result(e.getCode(), e.getMessage())));
            return false;
        }
        return true;
    }

    /**
     * 校验token
     *
     * @param request 请求体
     */
    private void validToken(HttpServletRequest request) {
        String openId = request.getHeader(Const.Header.CURRENT_OPEN_ID);
        String token = request.getHeader(Const.Header.TOKEN);
        if (StringUtils.isBlank(openId)) {
            log.error("校验token失败，openId为空,appId={}", appId);
            throw new MessageException(HttpResponse.LOGIN_TIMEOUT,"请先登录");
        }

        if (StringUtils.isBlank(token)) {
            log.error("校验token失败，token为空,appId={},openId={}", appId, openId);
            throw new MessageException(HttpResponse.LOGIN_TIMEOUT,"请先登录");
        }

        String tokenInCache = (String) valueOperations.get(Const.CacheKey.WECHAT_LOGIN_TOKEN_PRE + appId + "_" + openId);

        if (StringUtils.isBlank(tokenInCache)) {
            log.error("校验token失败，缓存中的token为空,appId={},openId={}", appId, openId);
            throw new MessageException(HttpResponse.LOGIN_TIMEOUT, "登录已超时，请重新登录");
        }

        //比较传入的token与缓存中的token是否一致
        if (!StringUtils.equalsIgnoreCase(token, tokenInCache)) {
            log.error("校验token失败，token不一致，缓存中token:{},提交的token:{},appId:{},openId:{}", token, tokenInCache,
                    appId, openId);
            throw new MessageException(HttpResponse.LOGIN_TIMEOUT, "登录已超时，请重新登录");
        }

        //依据token获取用户信息
        Map<String, Object> userInfo = (Map<String, Object>) valueOperations.get(
                Const.CacheKey.WECHAT_LOGIN_USER_INFO_PRE + token);
        if (userInfo == null) {
            log.error("校验token失败，依据token查找不到用户信息，token:{},appId:{},openId:{}", token,
                    appId, openId);
            throw new MessageException(HttpResponse.LOGIN_TIMEOUT, "登录已超时，请重新登录");
        }
        Date expiredTime = (Date) userInfo.get("expiredTime");

        //比较超时时间
        Date now = new Date();
        if (now.after(expiredTime)) {
            log.error("token已失效，token:{}, 超时时间：{}， openId:{}, appId:{}", token, expiredTime, openId, appId);
            throw new MessageException(HttpResponse.LOGIN_TIMEOUT, "登录已超时，请重新登录");
        }
    }

    /**
     * 是否微信访问
     *
     * @param userAgent 请求头
     * @return 是否标记
     */
    private boolean matchWeChat(String userAgent) {
        if (StringUtils.isBlank(userAgent)) {
            return false;
        }

        // 是微信浏览器
        return userAgent.toLowerCase().indexOf(ConstList.USER_AGENT_WE_CHAT_KEY) > 0;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();

        String userId = getUserIdByOpenId(request.getHeader(Const.Header.CURRENT_OPEN_ID),
                request.getHeader(Const.Header.TOKEN));

        ctx.addZuulRequestHeader(Const.Header.CURRENT_USER_ID, userId);
        return null;
    }

    /**
     * 依据openId获取userId
     *
     * @param openId 微信openId
     * @return userId 用户id
     */
    private String getUserIdByOpenId(String openId, String token) {
        if (StringUtils.isBlank(openId)) {
            log.error("依据openId获取用户Id失败，openId为空");
            throw new MessageException(HttpResponse.LOGIN_TIMEOUT,"请先登录");
        }

        if (StringUtils.isBlank(token)) {
            log.error("依据openId获取用户Id失败，token为空，appId:{},openId:{}", appId, openId);
            throw new MessageException(HttpResponse.LOGIN_TIMEOUT,"请先登录");
        }

        //依据token获取用户信息
        Map<String, Object> userInfo = (Map<String, Object>) valueOperations.get(
                Const.CacheKey.WECHAT_LOGIN_USER_INFO_PRE + token);
        if (userInfo == null) {
            log.error("依据token:{}，获取用户信息失败", token);
            throw new MessageException(HttpResponse.LOGIN_TIMEOUT, "登录已超时，请重新登录");
        }

        Integer userId = (Integer) userInfo.get("userId");
        if (userId == null) {
            log.error("依据token:{}，获取用户信息失败,用户id为空，用户信息：{}", JSON.toJSON(userInfo));
            throw new MessageException("系统异常");
        }
        return String.valueOf(userId);
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 2;
    }
}
