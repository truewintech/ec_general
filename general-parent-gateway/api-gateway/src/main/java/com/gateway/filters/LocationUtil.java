/*
 * Copy Right@
 */
package com.gateway.filters;


import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Slf4j
public class LocationUtil {

    /**
     * LOCATION线程局部变量 处理当前请求
     */
    private static ThreadLocal<String> LOCATION = new ThreadLocal<>();

    /**
     * 设置当前地址
     *
     * @param location 地址
     */
    public static void setLocation(String location) {
        LOCATION.set("http://" + location.toUpperCase() + "/");
    }

    /**
     * 取得当前地址
     *
     * @return 地址
     */
    public static String getLocation() {
        return LOCATION.get();
    }

    /**
     * 销毁当前地址
     */
    public static void removeLocation() {
        LOCATION.remove();
    }

}
