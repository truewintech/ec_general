/*
 * Copy Right@
 */
package com.gateway.filters;

import com.alibaba.fastjson.JSON;
import com.common.constant.Const;
import com.framework.exception.MessageException;
import com.framework.filters.AbstractCrossFilter;
import com.framework.mvc.result.Result;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <pre>
 * </pre>
 *
 * @author dengyalong
 * @date 2018年05月04日
 */
@Component
@Slf4j
public class WeChatMiniappAccessFilter extends AbstractCrossFilter {

    /**
     * 不过滤的地址
     */
    @Value("${filter.weChatMiniapp.excludeUrls}")
    private String[] excludeUrls;

    @Value("${weChat.ma.appId}")
    private String appId;

    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        if (!matchWeChat(request.getHeader(Const.Header.REQUEST_TYPE))) {
            return false;
        }

        HttpServletResponse response = ctx.getResponse();
        onCrossDomain(request, response);
        // 检查是否是OPTIONS请求 如果是直接跳过
        String method = request.getMethod();
        if (method.equalsIgnoreCase(RequestMethod.OPTIONS.name())) {
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(HttpStatus.OK.value());
            return false;
        }

        // 请求URL
        String uri = request.getRequestURI();
        log.info("请求url:{}", uri);
        //排除的url不校验token
        // 是否是排除的url
        for (String excludeUrl : excludeUrls) {
            if (StringUtils.contains(uri, excludeUrl)) {
                // 匹配成功，成功返回
                return false;
            }
        }

        //校验token
        try {
            validOpenId(request);
        } catch (MessageException e) {
            ctx.addZuulResponseHeader(Const.Header.CONTENT_TYPE,
                    MediaType.APPLICATION_JSON_UTF8_VALUE);
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(HttpStatus.OK.value());
            ctx.setResponseBody(JSON.toJSONString(new Result(e.getCode(), e.getMessage())));
            return false;
        }
        return true;
    }

    /**
     * 校验用户openId
     *
     * @param request 请求体
     */
    private void validOpenId(HttpServletRequest request) {
        String openId = request.getHeader(Const.Header.CURRENT_OPEN_ID);
        if (StringUtils.isBlank(openId)) {
            log.error("openId为空,appId={}", appId);
            throw new MessageException("系统异常");
        }
    }

    /**
     * 是否微信小程序访问
     *
     * @param requestType 请求头
     * @return 是否标记
     */
    private boolean matchWeChat(String requestType) {
        if (StringUtils.isBlank(requestType)) {
            return false;
        }

        return Const.Header.WEIXIN_MINIAPP_REQUEST.equals(requestType);
    }

    @Override
    public Object run() {
        return null;
    }


    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }
}
