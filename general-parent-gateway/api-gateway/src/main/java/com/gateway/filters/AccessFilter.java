/*
 * Copy Right@
 */
package com.gateway.filters;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.common.constant.Const;
import com.common.constant.DbConst;
import com.framework.constant.ConstList;
import com.framework.constant.HttpResponse;
import com.framework.filters.AbstractCrossFilter;
import com.framework.mvc.result.Result;
import com.framework.shiro.ShiroUser;
import com.framework.shiro.util.ShiroUtil;
import com.gateway.auth.AuthContext;
import com.gateway.auth.AuthTokenUtil;
import com.gateway.auth.CreateTokenException;
import com.gateway.auth.TokenOccupyException;
import com.gateway.service.LoginLogService;
import com.gateway.service.ResourceService;
import com.gateway.shiro.StatelessToken;
import com.gateway.shiro.User;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.zuul.filters.Route;
import org.springframework.cloud.netflix.zuul.filters.SimpleRouteLocator;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年3月30日
 */
@Component
@Slf4j
public class AccessFilter extends AbstractCrossFilter {

    /**
     * 排除不需要当前用户id的URL
     */
    @Value("${filter.web.excludeUrls}")
    private String[] excludeUrls;

    /**
     * 资源服务
     */
    @Autowired
    private ResourceService resourceService;

    /**
     * 登录日志服务
     */
    @Autowired
    private LoginLogService loginLogService;

    /**
     * 登录URL
     */
    @Value("${filter.web.loginUrl}")
    private String loginUrl;

    /**
     * 登出URL
     */
    @Value("${filter.web.logoutUrl}")
    private String logoutUrl;

    /**
     * 菜单URL
     */
    @Value("${filter.web.menuUrl}")
    private String menuUrl;

    /**
     * 路由
     */
    @Autowired
    private SimpleRouteLocator simpleRouteLocator;

    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        // 请求URL
        String url = request.getRequestURI();
        // 为了手机端网页访问，注释下列内容 ZhangZhenJie 2018-04-03
        /*if (PatternUtil.matcherMobile(request.getHeader(ConstList.USER_AGENT))) {
            // 不是PC则不过滤
            return false;
        }*/
        if (matchWeChat(request.getHeader(ConstList.USER_AGENT))) {
            return false;
        }
        HttpServletResponse response = ctx.getResponse();
        onCrossDomain(request, response);
        // 检查是否是OPTIONS请求 如果是直接跳过
        String method = request.getMethod();
        if (method.equalsIgnoreCase(RequestMethod.OPTIONS.name())) {
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(HttpStatus.OK.value());
            return false;
        }

        // 请求URL
        if (url.contains(loginUrl)
                && method.equalsIgnoreCase(RequestMethod.POST.name())) {
            // 设置地址
            Route route = simpleRouteLocator.getMatchingRoute(url);
            if (route != null) {
                LocationUtil.setLocation(route.getLocation());
            }
            // 登录
            return onLogin(request, response, ctx);
        }

        // 是否是排除的url
        for (String excludeUrl : excludeUrls) {
            if (StringUtils.contains(url, excludeUrl)) {
                // 匹配成功，成功返回
                return true;
            }
        }

        // 校验Token
        boolean b = validToken(request, response, ctx);
        if (!b) {
            return false;
        }

        // 判断是否登出
        if (url.contains(logoutUrl)
                && method.equalsIgnoreCase(RequestMethod.GET.name())) {
            // 登出
            onLogout(ctx);
            return false;
        }

        // 获取菜单
        if (url.contains(menuUrl)
                && method.equalsIgnoreCase(RequestMethod.GET.name())) {
            // 设置地址
            Route route = simpleRouteLocator.getMatchingRoute(url);
            if (route != null) {
                LocationUtil.setLocation(route.getLocation());
            }
            // 菜单
            onMenus(ctx);
            return false;
        }

        // url权限
//        String permission = method + ConstList.COLON + uri;
//        if (!DList.User.Id.ADMIN.equals(ShiroUtil.getUser().getId())
//                && !ShiroUtil.isPermitted(permission)) {
//            onErrorPermission(ctx);
//            return false;
//        }
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        ShiroUser shiroUser = ShiroUtil.getUser();
        if (shiroUser != null) {
            ctx.addZuulRequestHeader(Const.Header.CURRENT_USER_ID,
                    String.valueOf(shiroUser.getId()));
        }
        return null;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 3;
    }

    /**
     * 登录方法
     *
     * @param request  请求
     * @param response 返回
     * @param ctx      当前请求参数
     * @return 是否成功
     */
    private boolean onLogin(HttpServletRequest request,
                            HttpServletResponse response, RequestContext ctx) {
        ctx.setSendZuulResponse(false);
        ctx.addZuulResponseHeader(Const.Header.CONTENT_TYPE,
                MediaType.APPLICATION_JSON_UTF8_VALUE);
        try {
            String s = IOUtils.toString(request.getInputStream(), "UTF-8");
            JSONObject model = JSON.parseObject(s);
            String username = model.getString("username");
            String password = model.getString("password");
            if (StringUtils.isNotBlank(username)
                    && StringUtils.isNotBlank(password)) {
                UsernamePasswordToken token = new UsernamePasswordToken(
                        username, password);
                ShiroUtil.login(token);
                User user = (User) ShiroUtil.getUser();
                // 下发Token
                createToken(user, response);
                ctx.setResponseBody(toResult(HttpResponse.SUCCESS, user));
                try {
                    // 录登录日志
                    loginLogService.addLoginLog(user.getInteger("id"), username);
                } catch (Exception ex) {
                    log.warn("记录登录日志异常", ex);
                }
                return true;
            } else {
                ctx.setResponseBody(toResult(HttpResponse.FAIL, "账号或密码不能为空"));
            }
        } catch (UnknownAccountException e) {
            ctx.setResponseBody(toResult(HttpResponse.FAIL, ("账号不存在")));
        } catch (LockedAccountException e) {
            String message = e.getMessage();
            if (StringUtils.isNotBlank(message)) {
                ctx.setResponseBody(toResult(HttpResponse.FAIL, (message)));
            } else {
                ctx.setResponseBody(toResult(HttpResponse.FAIL, ("账号已停用")));
            }
        } catch (ConcurrentAccessException e) {
            ctx.setResponseBody(toResult(HttpResponse.FAIL, ("你没有登录权限")));
        } catch (AuthenticationException e) {
            ctx.setResponseBody(toResult(HttpResponse.FAIL, ("账号或密码错误")));
        } catch (Exception e) {
            ctx.setResponseBody(toResult(HttpResponse.FAIL, ("连接服务器异常")));
        }
        return false;
    }

    /**
     * 创建Token
     *
     * @param user     用户信息
     * @param response 返回流
     */
    private void createToken(User user, HttpServletResponse response)
            throws CreateTokenException {
        AuthContext authContext = new AuthContext();
        authContext.setId(user.getId());
        authContext.setUser(user);
        authContext.setTimestamp(System.currentTimeMillis());
        response.addHeader(Const.Header.TOKEN,
                AuthTokenUtil.createToken(authContext));
    }

    /**
     * 登出方法
     *
     * @param ctx 当前请求参数
     */
    private void onLogout(RequestContext ctx) {
        ctx.addZuulResponseHeader(Const.Header.CONTENT_TYPE,
                MediaType.APPLICATION_JSON_UTF8_VALUE);
        ShiroUser user = ShiroUtil.getUser();
        AuthTokenUtil.logout(user.getId());
        ctx.setSendZuulResponse(false);
        ctx.setResponseStatusCode(HttpStatus.OK.value());
        ctx.setResponseBody(toResult(HttpResponse.SUCCESS));
    }

    /**
     * 菜单方法
     *
     * @param ctx 当前请求参数
     */
    private void onMenus(RequestContext ctx) {
        ctx.addZuulResponseHeader(Const.Header.CONTENT_TYPE,
                MediaType.APPLICATION_JSON_UTF8_VALUE);
        ctx.setSendZuulResponse(false);
        ctx.setResponseStatusCode(HttpStatus.OK.value());
        Result result = resourceService.findAllMenus();
        if (HttpResponse.SUCCESS == result.getCode()) {
            if (result.getData() == null) {
                onLoginFail(ctx, ConstList.ErrorResult.ERROR_RESPONSE);
            } else {
                JSONArray menus = result.getJSONArray();
                checkPermission(menus);
                ctx.setResponseBody(toResult(HttpResponse.SUCCESS, menus));
            }
        } else {
            onLoginFail(ctx, ConstList.ErrorResult.ERROR_RESPONSE);
        }
    }

    /**
     * 检验菜单权限
     *
     * @param menus 菜单
     */
    private void checkPermission(JSONArray menus) {
        if (menus != null) {
            boolean admin = DbConst.User.Id.ADMIN.equals(ShiroUtil.getUser().getId());
            for (int i = 0; i < menus.size(); i++) {
                JSONObject menu = menus.getJSONObject(i);
                if (menu != null) {
                    String code = menu.getString("code");
                    boolean noPermission = !admin && !ShiroUtil.isPermitted(code);
                    if (StringUtils.isBlank(code)
                            || !Boolean.TRUE.equals(menu.getBoolean("isActive"))
                            || noPermission) {
                        menus.remove(i);
                        i--;
                        continue;
                    }
                    JSONArray children = menu.getJSONArray("children");
                    checkPermission(children);
                }
            }
        }
    }

    /**
     * 校验Token
     *
     * @param request  请求
     * @param response 返回
     * @param ctx      当前请求参数
     * @return 是否成功
     */
    private boolean validToken(HttpServletRequest request,
                               HttpServletResponse response, RequestContext ctx) {
        // 校验TOKEN
        String token = request.getHeader(Const.Header.TOKEN);
        if (StringUtils.isNotBlank(token)) {
            try {
                AuthContext authContext = AuthTokenUtil
                        .parseTokenAndValid(token, response);
                StatelessToken statelessToken = new StatelessToken(token,
                        authContext);
                ShiroUtil.login(statelessToken);
                return true;
            } catch (TokenOccupyException e) {
                onLoginFail(ctx, ConstList.ErrorResult.ERROR_LOGIN_OCCUPY);
            } catch (Exception e) {
                onLoginFail(ctx, ConstList.ErrorResult.ERROR_LOGIN_OVER_TIME);
            }
        } else {
            onLoginFail(ctx, ConstList.ErrorResult.ERROR_REQUEST);
        }
        return false;
    }

    /**
     * 登录失败时默认返回401状态码
     *
     * @param ctx    当前请求参数
     * @param result 提示信息
     */
    private void onLoginFail(RequestContext ctx, String result) {
        ctx.addZuulResponseHeader(Const.Header.CONTENT_TYPE,
                MediaType.APPLICATION_JSON_UTF8_VALUE);
        ctx.setSendZuulResponse(false);
        ctx.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());
        ctx.setResponseBody(result);
    }

    /**
     * 权限不足
     *
     * @param ctx 当前请求参数
     */
    private void onErrorPermission(RequestContext ctx) {
        ctx.addZuulResponseHeader(Const.Header.CONTENT_TYPE,
                MediaType.APPLICATION_JSON_UTF8_VALUE);
        ctx.setSendZuulResponse(false);
        ctx.setResponseStatusCode(HttpStatus.FORBIDDEN.value());
        ctx.setResponseBody(ConstList.ErrorResult.ERROR_PERMISSION);
    }

    /**
     * 是否微信访问
     *
     * @param userAgent 请求头
     * @return 是否标记
     */
    private boolean matchWeChat(String userAgent) {
        if (StringUtils.isBlank(userAgent)) {
            return false;
        }

        // 是微信浏览器
        return userAgent.toLowerCase().indexOf(ConstList.USER_AGENT_WE_CHAT_KEY) > 0;
    }
}
