/*
 * Copy Right@
 */
package com.gateway.auth;

import com.alibaba.fastjson.annotation.JSONField;
import com.gateway.shiro.User;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Getter
@Setter
public class AuthContext implements Serializable {

    /**
     * 用户id
     */
    private Serializable id;

    /**
     * token下发时间
     */
    private Long timestamp;

    /**
     * 用户信息
     */
    @JSONField(serialize = false)
    private User user;

    /**
     * token
     */
    @JSONField(serialize = false)
    private String token;

}
