/*
 * Copy Right@
 */
package com.gateway.auth;

import com.alibaba.fastjson.JSON;
import com.common.constant.Const;
import com.framework.shiro.util.ShiroUtil;
import com.framework.util.security.DesEdeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
@Slf4j
public class AuthTokenUtil implements BeanFactoryAware, EnvironmentAware {

    /**
     * token信息缓存前缀
     */
    private static final String TOKEN_KEY = "AUTH_TOKEN:";

    /**
     * 解密缓存前缀
     */
    private static final String TOKEN_SECRET_KEY = "AUTH_SECRET_TOKEN:";

    /**
     * token更新时间 60 分钟
     */
    private static final long EXPIRATION_TIME = 60 * 60 * 1000;

    /**
     * token有效时间
     */
    private static final long EFFECTIVE_TIME = 120 * 60 * 1000;

    /**
     * 解密缓存有效时间 60 分钟
     */
    private static final long TOKEN_SECRET_END_TIME = 60;

    /**
     * token 缓存时间 120分钟
     */
    private static final long TOKEN_END_TIME = 120;

    /**
     * 缓存工具类
     */
    private static ValueOperations<String, Object> valueOperations;

    /**
     * 缓存工具类
     */
    private static RedisTemplate<String, Object> redisTemplate;

    /**
     * 加密解密串
     */
    private static byte[] SECRET_KEY;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        setValueOperations((ValueOperations<String, Object>) beanFactory
                .getBean("valueOperations"));
        setRedisTemplate((RedisTemplate<String, Object>) beanFactory
                .getBean("redisTemplate"));
    }

    @Override
    public void setEnvironment(Environment environment) {
        setSecretKey(environment.getProperty("auth.secretKey").getBytes());
    }

    /**
     * 创建Token
     *
     * @param authContext 认证参数
     * @return 加密token
     */
    public static String createToken(AuthContext authContext)
            throws CreateTokenException {
        String param = JSON.toJSONString(authContext);
        try {
            String token = DesEdeUtil.encrypt(SECRET_KEY, param);
            authContext.setToken(token);
            valueOperations.set(TOKEN_KEY + authContext.getId(), authContext,
                    TOKEN_END_TIME, TimeUnit.MINUTES);
            return token;
        } catch (Exception e) {
            log.error("创建Token失败，参数{}", param, e);
            throw new CreateTokenException();
        }
    }

    /**
     * 解析Token (token获取缓存的用户id，用户id获取缓存的用户信息，实现用户踢出机制)
     *
     * @param token token
     * @return AuthContext 认证参数
     * @throws InvalidTokenException  解析token失败
     * @throws AuthorizedException    token失效
     * @throws TokenOverTimeException token超时失效
     * @throws TokenOccupyException   token占用
     */
    public static AuthContext parseTokenAndValid(String token,
                                                 HttpServletResponse response) throws InvalidTokenException,
            TokenOverTimeException, AuthorizedException, TokenOccupyException {
        if (token != null) {
            // 通过token获取用户信息
            AuthContext authContext;
            try {
                // 从缓存中获取用户信息
                authContext = (AuthContext) valueOperations
                        .get(TOKEN_SECRET_KEY + token);
                if (authContext == null) {
                    // 解密token获取用户信息
                    String data = new String(
                            DesEdeUtil.decrypt(SECRET_KEY, token));
                    authContext = JSON.parseObject(data, AuthContext.class);
                    // 缓存token对应的用户信息
                    valueOperations.set(TOKEN_SECRET_KEY + token, authContext,
                            TOKEN_SECRET_END_TIME, TimeUnit.MINUTES);
                }
            } catch (Exception e) {
                log.error("解析TOKEN异常", e);
                throw new InvalidTokenException();
            }

            if (authContext != null) {
                // 通过用户id获取带时间和token的 用户信息
                authContext = (AuthContext) valueOperations
                        .get(TOKEN_KEY + authContext.getId());
                if (authContext != null) {
                    // 校验token时间，并下发新的token
                    if (token.equals(authContext.getToken())) {
                        if (isAuthorized(authContext, response)) {
                                return authContext;
                        }
                    }else{
                        throw new TokenOccupyException();
                    }
                    throw new AuthorizedException();
                }
            }
        }
        throw new InvalidTokenException();
    }

    /**
     * 校验token是否有效 如果快失效 则更新token
     *
     * @param authContext token
     * @param response    返回流
     * @return 是否有效
     * @throws TokenOverTimeException token失效
     */
    private static boolean isAuthorized(AuthContext authContext,
                                        HttpServletResponse response) throws TokenOverTimeException {
        try {
            if (authContext != null) {
                long date = authContext.getTimestamp();
                long currentDate = System.currentTimeMillis();
                long diff = currentDate - date;
                if (diff > EXPIRATION_TIME && diff < EFFECTIVE_TIME) {
                    authContext.setTimestamp(currentDate);
                    response.addHeader(Const.Header.TOKEN,
                            createToken(authContext));
                } else if (diff > EFFECTIVE_TIME) {
                    // token失效
                    throw new TokenOverTimeException();
                }
                return true;
            }
            return false;
        } catch (CreateTokenException e) {
            return false;
        }
    }

    /**
     * 删除Token
     *
     * @param id 用户id
     */
    public static void logout(Serializable id) {
        if (id != null) {
            AuthContext authContext = (AuthContext) valueOperations
                    .get(TOKEN_KEY + id);
            if (authContext != null) {
                redisTemplate.delete(TOKEN_KEY + authContext.getId());
                redisTemplate.delete(TOKEN_SECRET_KEY + authContext.getToken());
            }
        }
        ShiroUtil.logout();
    }

    private static void setValueOperations(
            ValueOperations<String, Object> valueOperations) {
        AuthTokenUtil.valueOperations = valueOperations;
    }

    private static void setRedisTemplate(
            RedisTemplate<String, Object> redisTemplate) {
        AuthTokenUtil.redisTemplate = redisTemplate;
    }

    private static void setSecretKey(byte[] secretKey) {
        SECRET_KEY = secretKey;
    }

}
