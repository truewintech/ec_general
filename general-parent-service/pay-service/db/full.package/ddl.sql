drop table if exists pay_log;

drop table if exists refund_log;

/*==============================================================*/
/* Table: pay_log                                               */
/*==============================================================*/
create table pay_log
(
   id                   int not null auto_increment comment '主键',
   pay_platform_no      varchar(32) not null comment '支付平台的流水号',
   pay_time             datetime not null comment '支付时间',
   amount               decimal(12,4) comment '支付金额',
   pay_way              varchar(32) comment '支付渠道(举例,微信,支付宝)',
   pay_way_name         varchar(32) comment '支付渠道名称',
   pay_type             varchar(32) comment '支付类型(举例,微信分公众号,wap,app几种类型)',
   pay_type_name        varchar(32) comment '支付类型名称',
   commission           decimal(12,4) not null comment '支付平台手续费',
   ip                   varchar(32) comment '发起支付的ip',
   source_service       varchar(32) comment '来源服务',
   target_service       varchar(32) comment '转发服务',
   pay_params           TEXT comment '接收支付平台参数',
   self_params          text comment '发起的支付信息',
   primary key (id)
);

alter table pay_log comment '平台上的支付凭证,在收款成功后才会产生记录';

/*==============================================================*/
/* Index: idx_o_p_pay_platform_no                               */
/*==============================================================*/
create index idx_o_p_pay_platform_no on pay_log
(
   pay_platform_no
);

/*==============================================================*/
/* Index: idx_o_p_pay_time                                      */
/*==============================================================*/
create index idx_o_p_pay_time on pay_log
(
   pay_time
);


create table refund_log
(
   id                   int not null auto_increment,
   pay_platform_no      varchar(32) comment '支付流水号',
   refund_no            varchar(32) not null comment '退款流水号',
   refund_time          datetime not null comment '退款时间',
   buyer_id             int not null comment '买家id',
   buyer_name           varchar(64) comment '买家名称',
   seller_id            int not null comment '卖家id',
   seller_name          varchar(64) comment '卖家名称',
   business_type        varchar(32) not null comment '业务类型',
   form_id              int comment '业务单据id',
   form_no              varchar(32) comment '业务单据号',
   amount               decimal(12,4) not null comment '退款金额',
   pay_way              varchar(32) comment '支付渠道',
   pay_way_name         varchar(32) comment '支付渠道名称',
   commission           decimal(12,4) not null comment '抵扣支付平台手续费',
   primary key (id)
);

alter table refund_log comment '退款记录,每个周期内每个企业一条记录';

/*==============================================================*/
/* Index: idx_r_i_pay_platform_no                               */
/*==============================================================*/
create index idx_r_i_pay_platform_no on refund_log
(
   pay_platform_no
);

/*==============================================================*/
/* Index: idx_r_i_refund_no                                     */
/*==============================================================*/
create index idx_r_i_refund_no on refund_log
(
   refund_no
);

/*==============================================================*/
/* Index: idx_r_i_create_time                                   */
/*==============================================================*/
create index idx_r_i_create_time on refund_log
(
   refund_time
);

CREATE TABLE `single_record` (
  `order_no` varchar(30) NOT NULL COMMENT '流水号',
  `payload` varchar(255) NOT NULL COMMENT '内容',
  `id` int(11) NOT NULL COMMENT '主键',
  PRIMARY KEY (`id`),
  KEY `idx_s_r_order_no` (`order_no`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='下单记录';



