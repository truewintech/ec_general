/*
 *
 * Copy Right@
 */
package com.pay.bean;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * <pre>
 *     支付请求参数,一个支付请求中包含多个订单支付请求
 *     所有字段名称不可以随意修改,影响回调时穿透数据的解析
 *     包括orderPayInfo 中的字段也不可修改.
 * </pre>
 *
 * @author wjz
 * @date 2017年12月08日
 */
@Data
public class PayInfo {

    /**
     * 支付渠道
     */
    @NotBlank(message = "支付渠道必填")
    private String payWay;

    /**
     * 支付渠道名称
     */
    private String payWayName;

    /**
     * 支付类型
     */
    @NotBlank(message = "支付类型必填")
    private String payType;

    /**
     * 支付类型名称
     */
    private String payTypeName;

    /**
     * 具体订单的支付信息
     */
    private List<OrderPayInfo> orders;

    /**
     * 调用支付的服务名(由调用支付的服务填写)
     * 如果不需要自己接受处理业务,可以不填
     */
    private String sourceService;

    /**
     * 回调成功后处理业务的服务(由调用支付的服务填写)
     */
    private String targetService;

    /**
     * 合计支付金额(general-pay项目会在合计一次)
     */
    @NotNull(message = "合计支付金额必填")
    private BigDecimal payAmount;

    /**
     * 合计支付需要提点的费用(general-pay项目会在合计一次)
     */
    @NotNull(message = "支付平台提点的费用")
    private BigDecimal payCommission;

    /**
     * 网页请求的ip
     */
    private String ip;

    /**
     * 用户id,触发者的id
     */
    private Integer triggerById;

    /**
     * 用户名,触发者的名称
     */
    private Integer triggerByName;

    /**
     * openid 供微信支付使用
     */
    private String openId;

    /**
     * 支付还是退款 供回调使用
     */
    private Boolean isPay;
}
