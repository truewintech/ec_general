/*
 *
 * Copy Right@
 */
package com.pay.bean;

import lombok.Data;

import java.math.BigDecimal;

/**
 * <pre>
 *     订单支付参数,被PayInfo支付参数包含
 * </pre>
 *
 * @author wjz
 * @date 2017年12月08日
 */
@Data
public class OrderPayInfo {
    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 订单类型
     */
    private String orderType;

    /**
     * 订单类型名称
     */
    private String orderTypeName;

    /**
     * 订单编码
     */
    private String orderNo;

    /**
     * 订单支付金额(包含了手续费)
     */
    private BigDecimal payAmount;

    /**
     * 订单手续费(这个主要在穿透之后记录用)
     */
    private BigDecimal payCommission;
}
