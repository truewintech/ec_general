/*
 * Copy Right@
 */
package com.pay.bean;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * <pre>
 *     退款提交的参数
 * </pre>
 *
 * @author wjz
 * @date 2017年12月19日
 */
@Data
public class RefundInfo {

    /**
     * 付款支付id
     */
    @NotNull(message = "原支付记录id必填")
    private List<Integer> payIds;

    /**
     * 退款单id
     */
    @NotNull(message = "退款单id必填")
    private Integer refundFormId;

    /**
     * 退款单编号
     */
    @NotBlank(message = "退款单编号必填")
    private String refundFormNo;

    /**
     * 退款相关订单id
     */
    private String formId;

    /**
     * 退款相关订单编号
     */
    private String formNo;

    /**
     * 单据类型
     */
    private String formType;

    /**
     * 退款类型名
     */
    private String formTypeName;

    /**
     * 退款金额
     */
    @NotNull(message = "退款金额必填")
    private BigDecimal refundAmount;

    /**
     * 冲抵支付的手续费
     */
    @NotNull(message = "退冲抵支付的手续费必填")
    private BigDecimal offsetPayCommission;

    /**
     * 调用支付的服务名(由调用支付的服务填写)
     * 如果不需要自己接受处理业务,可以不填
     */
    private String sourceService;

    /**
     * 回调成功后处理业务的服务(由调用支付的服务填写)
     */
    private String targetService;

    /**
     * 用户id,触发者的id
     */
    private Integer triggerBy;

    /**
     * 用户名,触发者的名称
     */
    private Integer triggerByName;
}
