/*
 *
 * Copy Right@
 */
package com.pay.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.common.constant.PayConst;
import com.framework.constant.DbConstList;
import com.framework.data.redis.RedisIdGenerator;
import com.framework.exception.MessageException;
import com.jpay.ext.kit.DateKit;
import com.jpay.unionpay.*;
import com.pay.bean.OrderPayInfo;
import com.pay.bean.PayInfo;
import com.pay.bean.RefundInfo;
import com.pay.entity.PayLog;
import com.pay.repository.RefundLogRepository;
import com.pay.service.CallBackService;
import com.pay.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author wjz
 * @date 2017年12月09日
 */
@Service
@Slf4j
public class UnionPayServiceImpl implements PayService {

    /**
     * id生成器
     */
    @Autowired
    private RedisIdGenerator payNoGenerator;

    /**
     * 回调业务处理服务
     */
    @Autowired
    private CallBackService callBackService;

    /**
     * 退款记录仓库
     */
    @Autowired
    private RefundLogRepository refundLogRepository;

    /**
     * 银联回调中的支付成功状态
     */
    private static final String CHECK_OK = "00";

    /**
     * 消费
     */
    private static final String CONSUMER = "01";

    /**
     * 退款
     */
    private static final String REFUND = "04";

    @Override
    public String pay(PayInfo payInfo) {
        String payType = payInfo.getPayType();
        if (StringUtils.isBlank(payType)) {
            log.error("支付类型是空的,见DbConstList.PayType.UNION");
            throw new MessageException("支付类型是空的");
        }

        List<OrderPayInfo> orderList = payInfo.getOrders();
        if (orderList == null || orderList.isEmpty()) {
            throw new MessageException("未包含订单支付信息");
        }

        DbConstList.PayType.Union unionType = DbConstList.PayType.Union.valueOf(payType);
        if (DbConstList.PayType.Union.GATEWAY.equals(unionType)) {
            return pcWapPay(payInfo, "000201", "07");
        } else if (DbConstList.PayType.Union.WAP.equals(unionType)) {
            return pcWapPay(payInfo, "000201", "08");
        } else if (DbConstList.PayType.Union.B2B.equals(unionType)) {
            return pcWapPay(payInfo, "000202", "07");
        } else if (DbConstList.PayType.Union.APP.equals(unionType)) {
            return appPay(payInfo, "08");
        } else {
            log.error("此支付渠道,支付方式" + unionType + "未实现");
            throw new MessageException("支付失败");
        }

    }

    /**
     * pc h5 页面支付
     * B2B业务没有手机支付
     *
     * @param payInfo     支付对象
     * @param bizType     业务类型
     * @param channelType 渠道类型
     * @return 提交的表单字符串
     */
    private String pcWapPay(PayInfo payInfo, String bizType, String channelType) {
        //组织页面提交的表单
        BigDecimal payAmount = payInfo.getPayAmount();
        //以分为单位的金额
        long pay = payAmount.multiply(new BigDecimal(100)).longValue();
        String payInfoStr = JSON.toJSONString(payInfo);
        String payInfoBase64Str = Base64Utils.encodeToString(payInfoStr.getBytes());
        Map<String, String> reqDataMap = UnionPayApiConfig.builder()
                .setOrderId(payNoGenerator.generateByPrefix("FY"))
                .setChannelType(channelType)
                .setBizType(bizType)
                // 支付金额
                .setTxnAmt(String.valueOf(pay))
                //穿透信息
                .setReqReserved(payInfoBase64Str)
                .createMap();
        return UnionPayApi.frontRequest(reqDataMap);
    }

    /**
     * apple pay 支付方法(仅适用苹果手机)
     *
     * @param payInfo 支付对象
     * @return 银联的流水号
     */
    private String appPay(PayInfo payInfo, String channelType) {
        try {
            BigDecimal payAmount = payInfo.getPayAmount();
            //以分为单位的金额
            long pay = payAmount.multiply(new BigDecimal(100)).longValue();
            String payInfoStr = JSON.toJSONString(payInfo);
            String payInfoBase64Str = Base64Utils.encodeToString(payInfoStr.getBytes());
            Map<String, String> reqData = UnionPayApiConfig.builder()
                    .setOrderId(payNoGenerator.generateByPrefix("FY"))
                    .setChannelType(channelType)
                    // 支付金额
                    .setTxnAmt(String.valueOf(pay))
                    //穿透信息
                    .setReqReserved(payInfoBase64Str)
                    .setBackUrl(SDKConfig.getConfig().getBackUrl())
                    .createMap();
            Map<String, String> rspData = UnionPayApi.AppConsumeByMap(reqData);
            // 应答码规范参考open.unionpay.com帮助中心 下载 产品接口规范 《平台接入接口规范-第5部分-附录》
            if (!rspData.isEmpty()) {
                if (AcpService.validate(rspData, SDKConstants.UTF_8_ENCODING)) {
                    log.info("验证签名成功");
                    String respCode = rspData.get("respCode");
                    if (CHECK_OK.equals(respCode)) {
                        // 成功,获取tn号
                        return rspData.get("tn");
                    } else {
                        log.error("银联app支付返回其他应答码为失败请排查原因,应答码:" + respCode);
                        throw new MessageException("支付失败");
                    }
                } else {
                    log.error("银联app支付验证签名失败");
                    // 检查验证签名失败的原因
                    throw new MessageException("支付失败");
                }
            } else {
                // 未返回正确的http状态
                log.error("未获取到返回报文或返回http状态码非200");
                throw new MessageException("支付失败");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public String callBack(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        LogUtil.writeLog("BackRcvResponse接收后台通知开始");
        String encoding = httpServletRequest.getParameter(SDKConstants.param_encoding);
        // 获取银联通知服务器发送的后台通知参数
        Map<String, String> reqParam = getAllRequestParamStream(httpServletRequest, encoding);
        LogUtil.printRequestLog(reqParam);
        Boolean status = false;
        //重要！验证签名前不要修改reqParam中的键值对的内容，否则会验签不过
        if (!AcpService.validate(reqParam, encoding)) {
            LogUtil.writeLog("验证签名结果[失败].");
        } else {
            LogUtil.writeLog("验证签名结果[成功].");
            //银联的流水号
            String payNo = reqParam.get(SDKConstants.param_queryId);
            String orderId = reqParam.get(SDKConstants.param_orderId);
            String txnType = reqParam.get(SDKConstants.param_txnType);
            String respCode = reqParam.get(SDKConstants.param_respCode);
            //判断respCode=00、A6后，对涉及资金类的交易，请再发起查询接口查询，确定交易成功后更新数据库。
            if (CHECK_OK.equals(respCode)) {
                try {
                    boolean isExist = false;
                    if (CONSUMER.equals(txnType)) {
                        isExist = callBackService.checkPayExist(payNo, true, DbConstList.PayWay.UNION.name());
                    } else if (REFUND.equals(txnType)) {
                        isExist = callBackService.checkPayExist(payNo, false, DbConstList.PayWay.UNION.name());
                    }

                    if (isExist) {
                        log.error("该回调已经处理过了,payNo={},orderId={}", payNo, orderId);
                        throw new MessageException("该回调已经处理过了");
                    }
                    //获取保留内容
                    String base64ReservedStr = reqParam.get(SDKConstants.param_reqReserved);
                    byte[] reservedByte = Base64Utils.decodeFromString(base64ReservedStr);
                    if (reservedByte == null) {
                        log.error("保留字段内容不存在");
                        throw new MessageException("保留字段内容不存在");
                    }
                    String reservedStr = new String(reservedByte);
                    if (StringUtils.isBlank(reservedStr)) {
                        log.error("保留字段内容不存在");
                        throw new MessageException("保留字段内容不存在");
                    }
                    Map<String, String> platMapParam = new HashMap<>(16);
                    //交易类型
                    platMapParam.put(SDKConstants.param_txnType, reqParam.get(SDKConstants.param_txnType));
                    //交易子类
                    platMapParam.put(SDKConstants.param_txnSubType, reqParam.get(SDKConstants.param_txnSubType));
                    //产品类型
                    platMapParam.put(SDKConstants.param_bizType, reqParam.get(SDKConstants.param_bizType));
                    //接入类型
                    platMapParam.put(SDKConstants.param_accessType, reqParam.get(SDKConstants.param_accessType));
                    //商户订单号(自己平台的流水号)
                    platMapParam.put(SDKConstants.param_orderId, reqParam.get(SDKConstants.param_orderId));
                    //订单发送时间
                    platMapParam.put(SDKConstants.param_txnTime, reqParam.get(SDKConstants.param_txnTime));
                    //交易金额
                    platMapParam.put(SDKConstants.param_txnAmt, reqParam.get(SDKConstants.param_txnAmt));
                    //银联交易流水号
                    platMapParam.put(SDKConstants.param_queryId, reqParam.get(SDKConstants.param_queryId));
                    //如果是退款就有原始交易流水
                    platMapParam.put(SDKConstants.param_origQryId, reqParam.get(SDKConstants.param_origQryId));
                    //交易币种
                    platMapParam.put(SDKConstants.param_currencyCode, reqParam.get(SDKConstants.param_currencyCode));
                    //结算金额
                    platMapParam.put(SDKConstants.param_settleAmt, reqParam.get(SDKConstants.param_settleAmt));
                    //结算币种
                    platMapParam.put(SDKConstants.param_settleCurrencyCode, reqParam.get(SDKConstants.param_settleCurrencyCode));
                    //结算日期
                    platMapParam.put(SDKConstants.param_settleDate, reqParam.get(SDKConstants.param_settleDate));
                    //系统跟踪号
                    platMapParam.put(SDKConstants.param_traceNo, reqParam.get(SDKConstants.param_traceNo));
                    //结算日期
                    platMapParam.put(SDKConstants.param_traceTime, reqParam.get(SDKConstants.param_traceTime));

                    if (CONSUMER.equals(txnType)) {
                        callBackService.gatheringAmount(reservedStr, platMapParam, payNo);
                    } else if (REFUND.equals(txnType)) {
                        callBackService.refundAmount(reservedStr, platMapParam, payNo);
                    }

                    status = true;
                } catch (Exception e) {
                    status = false;
                    e.printStackTrace();
                }
            } else {
                status = false;
                String respMsg = reqParam.get(SDKConstants.param_respMsg);
                log.error("银联应答消息:" + respMsg);
            }
        }
        LogUtil.writeLog("BackRcvResponse接收后台通知结束");
        //返回给银联服务器http 200  状态码
        if (status) {
            httpServletResponse.setStatus(200);
            return "ok";
        } else {
            httpServletResponse.setStatus(500);
            return "ERROR";
        }
    }

    @Override
    public int refundAmount(PayLog payLog, RefundInfo refundInfo) {
        if (payLog == null) {
            log.error("支付记录为空");
            throw new MessageException("退款失败");
        }

        String payParams = payLog.getPayParams();
        JSONObject payJson = JSON.parseObject(payParams);
        String txnSubType = payJson.getString(SDKConstants.param_txnSubType);
        String bizType = payJson.getString(SDKConstants.param_bizType);
        String dbPayType = payLog.getPayType();
        String payPlatformNo = payLog.getPayPlatformNo();
        BigDecimal payAmount = payLog.getAmount();
        //修改穿透的退款信息为本次退款的金额
        refundInfo.setRefundAmount(payAmount);
        DbConstList.PayType.Union payType;
        try {
            payType = DbConstList.PayType.Union.valueOf(dbPayType);
        } catch (IllegalArgumentException e) {
            log.error("支付类型不存在,payType=" + dbPayType);
            throw new MessageException("退款失败");
        }
        String channelType = null;
        if (DbConstList.PayType.Union.GATEWAY.equals(payType) || DbConstList.PayType.Union.B2B.equals(payType)) {
            channelType = "07";
        } else if (DbConstList.PayType.Union.APP.equals(payType) || DbConstList.PayType.Union.WAP.equals(payType)) {
            channelType = "08";
        }
        //以分为单位
        long re = payAmount.multiply(new BigDecimal(100)).longValue();

        JSONObject jsonObject = JSON.parseObject(JSON.toJSONString(refundInfo));
        jsonObject.put(PayConst.PAY_WAY, DbConstList.PayWay.UNION.name());
        jsonObject.put(PayConst.PAY_WAY_NAME, DbConstList.PayWay.UNION_NAME);
        jsonObject.put(PayConst.PAY_TYPE, payLog.getPayType());
        jsonObject.put(PayConst.PAY_TYPE_NAME, payLog.getPayTypeName());
        //获取保留内容
        String refundStr = jsonObject.toJSONString();
        String reservedQueryStr = Base64Utils.encodeToString(refundStr.getBytes());

        Map<String, String> reqData = UnionPayApiConfig.builder()
                //交易类型 04-退货,31-消费撤销
                .setTxnType(REFUND)
                //交易子类型  默认00
                .setTxnSubType(txnSubType)
                //业务类型
                .setBizType(bizType)
                //渠道类型
                .setChannelType(channelType)
                //商户退款流水号,不同于原消费
                .setOrderId(payNoGenerator.generateByPrefix("RT"))
                //订单发送时间，格式为YYYYMMDDhhmmss，必须取当前时间，否则会报txnTime无效
                .setTxnTime(DateKit.toStr(new Date(), DateKit.UnionTimeStampPattern))
                .setTxnAmt(String.valueOf(re))
                .setReqReserved(reservedQueryStr)
                //****原消费交易返回的的queryId，可以从消费交易后台通知接口中或者交易状态查询接口中获取
                .setOrigQryId(payPlatformNo)
                .createMap();
        Map<String, String> rspData = UnionPayApi.backRequestByMap(reqData);
        if (!rspData.isEmpty()) {
            if (AcpService.validate(rspData, SDKConstants.UTF_8_ENCODING)) {
                log.info("验证签名成功");
                String respCode = rspData.get("respCode");
                if (CHECK_OK.equals(respCode)) {
                    //交易已受理，等待接收后台通知更新订单状态,也可以主动发起 查询交易确定交易状态。
                    String payNo = rspData.get(SDKConstants.param_queryId);
                    String base64ReservedStr = rspData.get(SDKConstants.param_reqReserved);
                    byte[] reservedByte = Base64Utils.decodeFromString(base64ReservedStr);
                    if (reservedByte == null) {
                        log.error("保留字段内容不存在");
                        throw new MessageException("保留字段内容不存在");
                    }
                    String reservedStr = new String(reservedByte);
                    if (StringUtils.isBlank(reservedStr)) {
                        log.error("保留字段内容不存在");
                        throw new MessageException("保留字段内容不存在");
                    }
                    callBackService.refundAmount(reservedStr, rspData, payNo);
                } else {
                    //其他应答码为失败请排查原因
                    String respMsg = rspData.get(SDKConstants.param_respMsg);
                    log.error("respCode={},respMsg={}", respCode, respMsg);
                }
            } else {
                log.error("验证签名失败");
            }
        } else {
            log.error("未获取到返回报文或返回http状态码非200");
        }

        return 0;
    }

    /**
     * 获取请求参数中所有的信息。
     *
     * @param request http请求
     * @return 参数的map集合
     */
    private Map<String, String> getAllRequestParamStream(
            final HttpServletRequest request, String encoding) {
        Map<String, String> res = new HashMap<>(16);
        try {
            String notifyStr = new String(IOUtils.toByteArray(request.getInputStream()), encoding);
            LogUtil.writeLog("收到通知报文：" + notifyStr);
            String[] kvs = notifyStr.split("&");
            for (String kv : kvs) {
                String[] tmp = kv.split("=");
                if (tmp.length >= 2) {
                    String key = tmp[0];
                    String value = URLDecoder.decode(tmp[1], encoding);
                    res.put(key, value);
                }
            }
        } catch (UnsupportedEncodingException e) {
            LogUtil.writeLog("getAllRequestParamStream.UnsupportedEncodingException error: " + e.getClass() + ":" + e.getMessage());
        } catch (IOException e) {
            LogUtil.writeLog("getAllRequestParamStream.IOException error: " + e.getClass() + ":" + e.getMessage());
        }
        return res;
    }
}
