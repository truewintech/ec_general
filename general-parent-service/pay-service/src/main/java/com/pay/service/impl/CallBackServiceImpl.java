/*
 *
 * Copy Right@
 */
package com.pay.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.common.constant.DbConst;
import com.common.constant.PayConst;
import com.framework.constant.DbConstList;
import com.framework.entity.Event;
import com.framework.exception.MessageException;
import com.framework.repository.EventRepository;
import com.jpay.util.StringUtils;
import com.pay.entity.PayLog;
import com.pay.entity.RefundLog;
import com.pay.repository.PayLogRepository;
import com.pay.repository.RefundLogRepository;
import com.pay.service.CallBackService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author wjz
 * @date 2017年12月09日
 */
@Service
@Slf4j
public class CallBackServiceImpl implements CallBackService {

    /**
     * 事件仓库
     */
    @Autowired
    private EventRepository eventRepository;

    /**
     * 支付记录仓库
     */
    @Autowired
    private PayLogRepository payLogRepository;

    /**
     * 退款记录仓库
     */
    @Autowired
    private RefundLogRepository refundLogRepository;

    @Override
    public boolean checkPayExist(String payNo, boolean txnType, String payWay) {
        if (txnType) {
            PayLog payLog = payLogRepository.findByPayPlatformNoAndPayWay(payNo, payWay);
            return payLog != null;
        } else {
            RefundLog refundLog = refundLogRepository.findByRefundNoAndPayWay(payNo, payWay);
            return refundLog != null;
        }
    }

    @Override
    @Transactional
    public void gatheringAmount(String reservedStr, Map<String, String> platMapParam, String payNo) {

        JSONObject jsonReserved = JSON.parseObject(reservedStr);
        if (jsonReserved.get(PayConst.Pay.ORDERS) == null) {
            log.error("保留字段内容,未包含订单信息,支付流水号: " + payNo);
            throw new MessageException("保留字段内容,未包含订单信息");
        }
        //保存支付记录
        PayLog payLog = new PayLog();

        payLog.setPayPlatformNo(payNo);
        payLog.setPayWay(jsonReserved.getString(PayConst.PAY_WAY));
        payLog.setPayWayName(jsonReserved.getString(PayConst.PAY_WAY_NAME));
        payLog.setPayType(jsonReserved.getString(PayConst.PAY_TYPE));
        payLog.setPayTypeName(jsonReserved.getString(PayConst.PAY_TYPE_NAME));
        payLog.setAmount(jsonReserved.getBigDecimal(PayConst.Pay.PAY_AMOUNT));
        payLog.setCommission(jsonReserved.getBigDecimal(PayConst.Pay.PAY_COMMISSION));
        String sourceService = jsonReserved.getString(PayConst.SOURCE_SERVICE);
        payLog.setSourceService(sourceService);
        String targetService = jsonReserved.getString(PayConst.TARGET_SERVICE);
        payLog.setTargetService(targetService);
        //发起支付的参数
        payLog.setSelfParams(reservedStr);
        //支付平台返回的参数
        payLog.setPayParams(JSON.toJSONString(platMapParam));
        payLog.setIp(jsonReserved.getString(PayConst.Pay.IP));
        payLog = payLogRepository.save(payLog);

        //将支付记录的id加入保留内容,待转发
        jsonReserved.put(PayConst.Pay.ORDER_PAY_ID, payLog.getId());
        String newJsonStr = jsonReserved.toJSONString();
        //收款事件
        String eventType = DbConstList.PayEventType.ORDER_PAY_CONFIRM;
        //给来源服务和目标服务各发送一个event
        if (StringUtils.isNotBlank(targetService)) {
            String[] targets = targetService.split(",");
            for(String target : targets){
                sendEvent(target, newJsonStr, eventType);
            }
//            sendEvent(targetService, newJsonStr, eventType);
        }
        if (StringUtils.isNotBlank(sourceService)) {
            sendEvent(sourceService, newJsonStr, eventType);
        }

    }

    @Override
    @Transactional
    public void refundAmount(String reservedStr, Map<String, String> platMapParam, String refundNo) {
        JSONObject refundJson = JSON.parseObject(reservedStr);
        String sourceService = refundJson.getString(PayConst.SOURCE_SERVICE);
        String targetService = refundJson.getString(PayConst.TARGET_SERVICE);
        RefundLog refundLog = new RefundLog();
        refundLog.setAmount(refundJson.getBigDecimal(PayConst.Refund.REFUND_AMOUNT));
        refundLog.setRefundNo(refundNo);
        refundLog.setPayWay(refundJson.getString(PayConst.PAY_WAY));
        refundLog.setPayWayName(refundJson.getString(PayConst.PAY_WAY_NAME));
        refundLog.setAmount(refundJson.getBigDecimal(PayConst.Refund.REFUND_AMOUNT));
        refundLog.setSourceService(sourceService);
        refundLog.setTargetService(targetService);

        refundLog = refundLogRepository.save(refundLog);

        //将支付记录的id加入保留内容,待转发
        refundJson.put(PayConst.Refund.REFUND_LOG_ID, refundLog.getId());
        String newJsonStr = refundJson.toJSONString();
        //退款事件
        String eventType = DbConstList.PayEventType.REFUND_ORDER_PAY;
        //给来源服务和目标服务各发送一个event
        if (StringUtils.isNotBlank(targetService)) {
            sendEvent(targetService, newJsonStr, eventType);
        }
        if (StringUtils.isNotBlank(sourceService)) {
            sendEvent(sourceService, newJsonStr, eventType);
        }
    }

    /**
     * 转发事件
     *
     * @param service   目标服务名
     * @param jsonStr   转发内容
     * @param eventType 事件类型
     */
    private void sendEvent(String service, String jsonStr, String eventType) {
        Event eventOrder = new Event();
        eventOrder.setSender(DbConst.Event.SenderOrReceiver.PAY_SERVICE);
        eventOrder.setReceiver(service);
        eventOrder.setEventType(eventType);
        eventOrder.setStatus(DbConstList.Event.Status.NEW);
        eventOrder.setPayload(jsonStr);
        eventRepository.save(eventOrder);
    }

}
