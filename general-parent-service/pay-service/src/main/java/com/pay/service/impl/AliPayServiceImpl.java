/*
 *
 * Copy Right@
 */
package com.pay.service.impl;

import com.pay.bean.PayInfo;
import com.pay.entity.PayLog;
import com.pay.bean.RefundInfo;
import com.pay.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <pre>
 * </pre>
 *
 * @author wjz
 * @date 2017年12月09日
 */
@Service
@Slf4j
public class AliPayServiceImpl implements PayService {
    @Override
    public String pay(PayInfo payInfo) {
        return null;
    }

    @Override
    public String callBack(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        return null;
    }

    @Override
    public int refundAmount(PayLog payLog, RefundInfo refundInfo) {
        return 0;
    }

}
