/*
 *
 * Copy Right@
 */
package com.pay.service;

import java.util.Map;

/**
 * <pre>
 *    回调验证签名过后的处理过程,在这个服务实现
 * </pre>
 *
 * @author wjz
 * @date 2017年12月09日
 */
public interface CallBackService {

    /**
     * 检查回调内容是否已经处理
     *
     * @param reservedStr 保留字段的json格式
     * @param txnType     交易类型(收款为:true,退款:false)
     * @param payNo       支付流水号
     * @return 如果payLog已经存在返回真
     */
    boolean checkPayExist(String reservedStr, boolean txnType, String payNo);

    /**
     * 处理回调支付内容,并转发给相关的服务
     *
     * @param jsonReserved 保留字段
     * @param platMapParam 支付平台的参数
     * @param payNo        支付平台的流水号
     */
    void gatheringAmount(String jsonReserved, Map<String, String> platMapParam, String payNo);


    /**
     * 处理回调退款内容,并转发给相关的服务
     *
     * @param reservedStr  保留字段
     * @param platMapParam 支付平台的参数
     * @param refundNo     支付平台的退款流水号
     */
    void refundAmount(String reservedStr, Map<String, String> platMapParam, String refundNo);
}
