/*
 *
 * Copy Right@
 */
package com.pay.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.framework.constant.DbConstList;
import com.framework.data.redis.RedisIdGenerator;
import com.framework.exception.MessageException;
import com.jpay.ext.kit.HttpKit;
import com.jpay.ext.kit.PaymentKit;
import com.jpay.weixin.api.WxPayApi;
import com.jpay.weixin.api.WxPayApiConfigKit;
import com.pay.bean.OrderPayInfo;
import com.pay.bean.PayInfo;
import com.pay.bean.RefundInfo;
import com.pay.entity.PayLog;
import com.pay.entity.SingleRecord;
import com.pay.repository.SingleRecordRepository;
import com.pay.service.CallBackService;
import com.pay.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author wjz
 * @date 2017年12月09日
 */
@Service
@Slf4j
public class WxPayServiceImpl implements PayService {


    /**
     * id生成器
     */
    @Autowired
    private RedisIdGenerator payNoGenerator;

    /**
     * 回调业务处理服务
     */
    @Autowired
    private CallBackService callBackService;


    /**
     * 支付参数记录仓库
     */
    @Autowired
    private SingleRecordRepository singleRecordRepository;


    @Value("${pay.wxPayBody}")
    private String WxPayBody;


    /**
     * 微信支付 @author lwb  @date 2018年10月18日
     *
     * @param payInfo 支付信息
     * @return
     */
    @Override
    @Transactional
    public String pay(PayInfo payInfo) {
        //支付类型
        String payType = payInfo.getPayType();
        //openid
        String openId = payInfo.getOpenId();
        if (StringUtils.isBlank(openId)) {
            log.error("发起微信支付失败，openid为空");
            throw new MessageException("支付失败");
        }
        if (StringUtils.isBlank(payType)) {
            log.error("支付类型是空的,见DbConstList.PayType.WxPay");
            throw new MessageException("支付类型是空的");
        }

        //支付详情
        List<OrderPayInfo> orderList = payInfo.getOrders();
        if (orderList == null || orderList.isEmpty()) {
            throw new MessageException("未包含订单支付信息");
        }
        //获取支付类型
        DbConstList.PayType.WxPay wxPayType = DbConstList.PayType.WxPay.valueOf(payType);
        //写入payInfo 未支付 回调以此字段判断
        payInfo.setIsPay(true);
        //公众号支付
        if (DbConstList.PayType.WxPay.PUBLIC.equals(wxPayType)) {
            return wxPlulicPay(payInfo);
        } else {
            log.error("此支付渠道,支付方式" + wxPayType + "未实现");
            throw new MessageException("支付失败");
        }
    }

    /**
     * 微信支付 回调  @author lwb  @date 2018年10月18日 Todo 未测试
     * 支付结果通用通知文档: https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_7
     *
     * @param httpServletRequest  http请求
     * @param httpServletResponse http响应
     * @return
     */
    @Override
    public String callBack(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        try {
            log.info("微信 BackRcvResponse 接收后台通知开始");
            //获取回调参数 xml
            String xmlMsg = HttpKit.readData(httpServletRequest);
            log.info("微信 BackRcvResponse 参数：{}", xmlMsg);
            //XML转成MAP
            Map<String, String> params = PaymentKit.xmlToMap(xmlMsg);
            //回调状态
            String return_code = params.get("return_code");
            //回调状态名
            String return_msg = params.get("return_msg");
            if (!PaymentKit.codeIsOK(return_code)) {
                log.error("微信支付回调失败：return_code：{},return_msg:{}", return_code, return_msg);
                throw new MessageException("微信支付回调异常");
            }
            //微信订单编号
            String transactionId = params.get("transaction_id");
            //商户订单号
            String orderNo = params.get("out_trade_no");
            //查询微信支付参数
            SingleRecord singleRecord = singleRecordRepository.findOne(orderNo);
            if (singleRecord == null) {
                log.error("微信支付回调异常，支付参数 SingleRecord 为空");
                throw new MessageException("微信支付回调异常");
            }
            //下单时的PayInfo
            String payload = singleRecord.getPayload();
            PayInfo payInfo = JSONObject.parseObject(payload, PayInfo.class);
            Boolean isPay = payInfo.getIsPay();
            //验证此回调是否已经处理过
            Boolean isExist = callBackService.checkPayExist(transactionId, isPay, DbConstList.PayWay.WX_PAY.name());
            //构建微信处理反馈参数
            Map<String, String> xml = new HashMap<String, String>();
            xml.put("return_code", "SUCCESS");
            xml.put("return_msg", "OK");
            //处理过的告知微信服务
            if (isExist) {
                log.error("微信订单号：{} 的支付订单已处理", transactionId);
                return PaymentKit.toXml(xml);
            }
            //写入回调日志，并转发给相关服务
            if (isPay) {
                //支付回调
                callBackService.gatheringAmount(payload, params, transactionId);
            } else {
                //退款回调
                callBackService.refundAmount(payload, params, transactionId);
            }
            log.info("微信 BackRcvResponse接收后台通知结束");
            httpServletResponse.setStatus(200);
            return PaymentKit.toXml(xml);
        } catch (Exception e) {
            log.error("微信 BackRcvResponse 处理异常", e);
            httpServletResponse.setStatus(500);
            return "ERROR";
        }

    }

    @Override
    public int refundAmount(PayLog payLog, RefundInfo refundInfo) {
        return 0;
    }

    /**
     * 微信公众号支付
     *
     * @param payInfo
     * @return
     */
    private String wxPlulicPay(PayInfo payInfo) {
        //组织页面提交的表单
        BigDecimal payAmount = payInfo.getPayAmount();
        //以分为单位的金额
        long pay = payAmount.multiply(new BigDecimal(100)).longValue();
        //交易流水号
        String outTradeNo = payNoGenerator.generateByPrefix("LJPZ");
        //构建支付参数
        Map<String, String> params = WxPayApiConfigKit.getWxPayApiConfig()
                .setBody(WxPayBody)
                //openId
                .setOpenId(payInfo.getOpenId())
                //IP
                .setSpbillCreateIp(payInfo.getIp())
                //支付金额
                .setTotalFee(String.valueOf(pay))
                //支付类型
                .setTradeType(WxPayApi.TradeType.JSAPI)
                //交易流水号
                .setOutTradeNo(outTradeNo)
                .build();
        //统一下单 获取微信返回参数
        String xmlResult = WxPayApi.pushOrder(false, params);
        //xml转map
        Map<String, String> resultMap = PaymentKit.xmlToMap(xmlResult);
        //统一下单状态
        String return_code = resultMap.get("return_code");
        String return_msg = resultMap.get("return_msg");
        if (!PaymentKit.codeIsOK(return_code)) {
            log.error("微信统一下单失败：return_code：{},return_msg:{}", return_code, return_msg);
            throw new MessageException("发起支付失败");
        }
        //构建前端发起支付参数
        String prepay_id = resultMap.get("prepay_id");
        Map<String, String> packageParams = PaymentKit.prepayIdCreateSign(prepay_id);
        String jsonStr = JSON.toJSONString(packageParams);
        //构建下单记录
        String payInfoStr = JSON.toJSONString(payInfo);
        SingleRecord singleRecord = new SingleRecord();
        singleRecord.setOrderNo(outTradeNo);
        singleRecord.setPayload(payInfoStr);
        singleRecordRepository.save(singleRecord);
        return jsonStr;
    }


}
