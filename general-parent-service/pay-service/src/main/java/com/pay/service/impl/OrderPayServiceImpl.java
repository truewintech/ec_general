/*
 *
 * Copy Right@
 */
package com.pay.service.impl;

import com.framework.constant.DbConstList;
import com.framework.exception.MessageException;
import com.pay.bean.OrderPayInfo;
import com.pay.bean.PayInfo;
import com.pay.entity.PayLog;
import com.pay.bean.RefundInfo;
import com.pay.entity.RefundLog;
import com.pay.repository.PayLogRepository;
import com.pay.repository.RefundLogRepository;
import com.pay.service.OrderPayService;
import com.pay.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author wjz
 * @date 2017年12月09日
 */
@Service
@Slf4j
public class OrderPayServiceImpl implements OrderPayService {

    /**
     * 银联支付服务
     */
    @Autowired
    @Qualifier("unionPayServiceImpl")
    private PayService unionPayService;

    /**
     * 支付宝支付服务
     */
    @Autowired
    @Qualifier("aliPayServiceImpl")
    private PayService aliPayService;

    /**
     * 微信支付服务
     */
    @Autowired
    @Qualifier("wxPayServiceImpl")
    private PayService wxPayService;

    /**
     * 支付记录仓库
     */
    @Autowired
    private PayLogRepository payLogRepository;

    /**
     * 退款记录仓库
     */
    @Autowired
    private RefundLogRepository refundLogRepository;

    @Override
    public String orderPay(PayInfo payInfo) {
        if (payInfo == null) {
            log.error("支付信息:payInfo是空的");
            throw new MessageException("支付失败");
        }

        List<OrderPayInfo> orderList = payInfo.getOrders();
        if (orderList == null || orderList.isEmpty()) {
            throw new MessageException("未包含订单支付信息");
        }
        //计算合计支付的金额
        BigDecimal payAmount = new BigDecimal("0");
        for (OrderPayInfo orderpayInfo : orderList) {
            payAmount = payAmount.add(orderpayInfo.getPayAmount());
        }
        payInfo.setPayAmount(payAmount);

        DbConstList.PayWay payWay;
        try {
            payWay = DbConstList.PayWay.valueOf(payInfo.getPayWay());
        } catch (IllegalArgumentException e) {
            log.error("支付渠道[{}]不存在,见DbConstList.PayWay", payInfo);
            throw new MessageException("支付失败");
        }

        if (DbConstList.PayWay.WX_PAY.equals(payWay)) {
            return wxPayService.pay(payInfo);
        } else if (DbConstList.PayWay.ALI_PAY.equals(payWay)) {
            return aliPayService.pay(payInfo);
        } else if (DbConstList.PayWay.UNION.equals(payWay)) {
            return unionPayService.pay(payInfo);
        }
        return null;
    }

    @Override
    public int refund(RefundInfo refundInfo) {
        if (refundInfo == null) {
            log.error("refundInfo 参数信息为空");
            throw new MessageException("退款失败");
        }

        List<Integer> payIds = refundInfo.getPayIds();
        //查找相关的支付记录
        List<PayLog> payLogs = payLogRepository.findByIdIsIn(payIds);
        //判断对应支付和已经发生的退款进行合并,决定使用的退款记录
        mergePayInfo(payLogs, refundInfo);
        if (CollectionUtils.isEmpty(payLogs)) {
            log.error("对应的支付记录不存在,payId=" + payIds);
            throw new MessageException("退款失败");
        }
        payLogs.forEach(payLog -> {
            String dbPayWay = payLog.getPayWay();
            try {
                DbConstList.PayWay payWay = DbConstList.PayWay.valueOf(dbPayWay);
                if (DbConstList.PayWay.ALI_PAY.equals(payWay)) {
                    aliPayService.refundAmount(payLog, refundInfo);
                } else if (DbConstList.PayWay.UNION.equals(payWay)) {
                    unionPayService.refundAmount(payLog, refundInfo);
                } else if (DbConstList.PayWay.WX_PAY.equals(payWay)) {
                    wxPayService.refundAmount(payLog, refundInfo);
                }
            } catch (IllegalArgumentException e) {
                log.error("对应的支付渠道不存在,payWay=" + dbPayWay);
                throw new MessageException("退款失败");
            }
        });
        return 0;
    }


    /**
     * 把对应支付的退款金额合并到支付记录中
     *
     * @param payLogs    支付记录
     * @param refundInfo 退款信息
     */
    private void mergePayInfo(List<PayLog> payLogs, final RefundInfo refundInfo) {

        //处理支付记录中的退货金额
        BigDecimal refundAmount = refundInfo.getRefundAmount();
        BigDecimal alreadyCompute = BigDecimal.ZERO;
        List<PayLog> removePayLogs = new ArrayList<>();
        for (PayLog payLog : payLogs) {
            BigDecimal payAmount = payLog.getAmount();
            /* START 对应payLog发生过退款,需要将已经退款的金额减去 */
            List<RefundLog> refundLogs = refundLogRepository.findByPayPlatformNo(payLog.getPayPlatformNo());
            if (!CollectionUtils.isEmpty(refundLogs)) {
                for (RefundLog refund : refundLogs) {
                    payAmount = payAmount.subtract(refund.getAmount());
                }
                if (payAmount.compareTo(BigDecimal.ZERO) > 0) {
                    payLog.setAmount(payAmount);
                } else {
                    log.warn("支付流水[{}],已经退款完毕,不可在继续退款");
                    //加入移除列表
                    removePayLogs.add(payLog);
                    continue;
                }
            }
            /* END 对应payLog发生过退款,需要将已经退款的金额减去 */

            /*START 每个payLog应该退多少钱,不需要退款的payLog从列表移除 */
            BigDecimal currentAlreadyCompute = alreadyCompute.add(payAmount);
            if (alreadyCompute.compareTo(refundAmount) < 0) {
                if (currentAlreadyCompute.compareTo(refundAmount) > 0) {
                    //当前这前支付记录超过了剩下应退金额
                    BigDecimal tmp = refundAmount.subtract(alreadyCompute);
                    payLog.setAmount(tmp);
                    alreadyCompute = alreadyCompute.add(tmp);
                } else {
                    payLog.setAmount(payAmount);
                    alreadyCompute = alreadyCompute.add(payAmount);
                }
            } else {
                //加入移除列表
                removePayLogs.add(payLog);
            }
             /*END 每个payLog应该退多少钱,不需要退款的payLog从列表移除 */
        }

        //移除不需要做退款的记录
        for (PayLog removePayLog : removePayLogs) {
            payLogs.remove(removePayLog);
        }
    }
}
