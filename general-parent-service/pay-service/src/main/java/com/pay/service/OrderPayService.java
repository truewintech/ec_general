/*
 *
 * Copy Right@
 */
package com.pay.service;

import com.pay.bean.PayInfo;
import com.pay.bean.RefundInfo;

/**
 * <pre>
 * </pre>
 *
 * @author wjz
 * @date 2017年12月09日
 */
public interface OrderPayService {

    /**
     * 订单支付入口,根据payInfo中的信息调用不同平台的接口
     *
     * @param payInfo 支付信息
     * @return 支付所需的字符串
     */
    String orderPay(PayInfo payInfo);

    /**
     * 订单退款
     *
     * @param refundInfo 退款信息
     * @return 成功返回0 ,不成返回1
     */
    int refund(RefundInfo refundInfo);
}
