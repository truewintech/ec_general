/*
 * Copy Right@
 */
package com.pay.service;

/**
 * <pre>
 *
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface RestService {

    /**
     * 获取系统参数
     *
     * @param id 系统参数id
     * @return 系统参数值
     */
    String getSystemParameter(String id);
}
