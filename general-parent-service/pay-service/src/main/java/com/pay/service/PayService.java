/*
 *
 * Copy Right@
 */
package com.pay.service;

import com.pay.bean.PayInfo;
import com.pay.entity.PayLog;
import com.pay.bean.RefundInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <pre>
 * </pre>
 *
 * @author wjz
 * @date 2017年12月09日
 */
public interface PayService {

    /**
     * 订单支付入口,根据payInfo中的信息调用不同类型的支付方式接口
     *
     * @param payInfo 支付信息
     * @return 支付所需的字符串
     */
    String pay(PayInfo payInfo);

    /**
     * 处理支付平台的回调
     *
     * @param httpServletRequest  http请求
     * @param httpServletResponse http响应
     * @return 支付平台成功标记
     */
    String callBack(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse);

    /**
     * 订单退款
     *
     * @param payLog     支付记录
     * @param refundInfo 退款信息
     * @return 成功返回:0 ,不成返回:1
     */
    int refundAmount(PayLog payLog, RefundInfo refundInfo);
}
