/*
 *
 * Copy Right@
 */
package com.pay.api;

import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.pay.bean.PayInfo;
import com.pay.bean.RefundInfo;
import com.pay.service.OrderPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <pre>
 * </pre>
 *
 * @author wjz
 * @date 2017年12月08日
 */
@RestController
@RequestMapping("pay")
@Api("支付")
public class PayApi extends BaseApi {

    /**
     * 订单支付服务
     */
    @Autowired
    private OrderPayService orderPayService;


    /**
     * 网页需要微信签名时调用
     */
    @GetMapping("getWXPageSign")
    @ApiOperation("getWXPageSign")
    public Result getWXPageSign() {
        return success();
    }

    /**
     * 支付内部服务调用接口
     *
     * @param payInfo 请求支付信息
     * @return 页面, app所需的信息
     */
    @PostMapping("orderPay")
    @ApiOperation("订单支付(内部)")
    public Result orderPay(@RequestBody @Validated PayInfo payInfo) {
        return success("ok", orderPayService.orderPay(payInfo));
    }

    /**
     * 退款内部服务调用接口
     *
     * @param refundInfo 请求支付信息
     * @return 页面, app所需的信息
     */
    @PostMapping("refundPay")
    @ApiOperation("订单退款(内部)")
    public Result refund(@RequestBody @Validated RefundInfo refundInfo) {
        return success("ok", orderPayService.refund(refundInfo));
    }
}
