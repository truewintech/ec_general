/*
 *
 * Copy Right@
 */
package com.pay.api;

import com.framework.constant.DbConstList;
import com.pay.service.PayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * <pre>
 *     支付平台回调通知专用,不接受前台提交的业务请求
 * </pre>
 *
 * @author wjz
 * @date 2017年12月08日
 */
@RestController
@RequestMapping("response")
@Api("支付回调")
public class PayResponse {
    /**
     * 银联支付服务
     */
    @Autowired
    @Qualifier("unionPayServiceImpl")
    private PayService unionPayService;

    /**
     * 支付宝支付服务
     */

    @Autowired
    @Qualifier("aliPayServiceImpl")
    private PayService aliPayService;

    /**
     * 微信支付服务
     */
    @Autowired
    @Qualifier("wxPayServiceImpl")
    private PayService wxPayService;

    /**
     * 支付回调的入口(post,get都从这里走)
     *
     * @param platform           支付渠道
     * @param httpServletRequest http请求对象
     */
    @ApiOperation("支付回调处理")
    @RequestMapping("{platform}/cb")
    public void responseApp(@PathVariable String platform, HttpServletRequest httpServletRequest,
                            HttpServletResponse httpServletResponse) {

        httpServletResponse.setCharacterEncoding("UTF-8");
        String payWay = platform.toUpperCase();
        String result;
        try {
            DbConstList.PayWay way = DbConstList.PayWay.valueOf(payWay);

            switch (way){
                case UNION:
                    result = unionPayService.callBack(httpServletRequest, httpServletResponse);
                    break;
                case WX_PAY:
                    result = wxPayService.callBack(httpServletRequest, httpServletResponse);
                    break;
                case ALI_PAY:
                    result = aliPayService.callBack(httpServletRequest, httpServletResponse);
                    break;
                default:
                    httpServletResponse.setStatus(500);
                    result = "ERROR";
            }
            PrintWriter outPut = httpServletResponse.getWriter();
            outPut.write(result);
        } catch (IllegalArgumentException | IOException e) {
            e.printStackTrace();
        }
    }
}
