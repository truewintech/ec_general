/*
 * Copy Right@
 */
package com.pay.amqp;

import com.common.constant.DbConst;
import com.framework.amqp.EventReceiver;
import com.framework.entity.Event;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class Receiver extends EventReceiver {

    /**
     * 接收服务事件
     *
     * @param event 事件
     * @return 处理是否成功
     */
    @RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = DbConst.Event.SenderOrReceiver.PAY_SERVICE)
    @RabbitHandler
    public boolean receiveEvent(Event event) {
        return saveEvent(event);
    }
}
