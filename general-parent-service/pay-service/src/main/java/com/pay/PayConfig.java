/*
 *
 * Copy Right@
 */
package com.pay;

import com.framework.data.redis.RedisIdGenerator;
import com.jpay.unionpay.SDKConfig;
import com.jpay.weixin.api.WxPayApiConfig;
import com.jpay.weixin.api.WxPayApiConfigKit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * <pre>
 *     预加载支付配置
 * </pre>
 *
 * @author wjz
 * @date 2017年12月08日
 */
@Configuration
@Slf4j
public class PayConfig {

    /**
     * 银联支付配置文件
     */
    @Value("${pay.unionPay}")
    private String unionPayFile;

    /**
     * 微信支付配置文件
     */
    @Value("${pay.weixin}")
    private String wxPayFile;

    @Bean
    public PayConfig unionConfig() {
        SDKConfig.getConfig().loadPropertiesFromSrc(unionPayFile);
        return new PayConfig();
    }

    @Bean
    public WxPayApiConfigKit wxPayApiConfig() {
        WxPayApiConfigKit.putApiConfig(getApiConfig());
        return new WxPayApiConfigKit();
    }

    /**
     * 写入微信支付配置文件 TODO 回调地址待填
     *
     * @return
     */
    private WxPayApiConfig getApiConfig() {
        InputStream in = SDKConfig.class.getClassLoader().getResourceAsStream(wxPayFile);

        Properties properties = new Properties();
        try {
            properties.load(in);
        } catch (IOException var14) {
            log.error("加载微信支付配置文件失败", var14);
        }
        log.info("开始加载微信配置文件");
        String appId = properties.getProperty("appId");
        String appSecret = properties.getProperty("appSecret");
        String mchId = properties.getProperty("mchId");
        String partnerKey = properties.getProperty("partnerKey");
        String certPath = properties.getProperty("certPath");
        String domain = properties.getProperty("domain");
        if (StringUtils.isBlank(appId))
            log.error("微信支付配置文件=========> appId <========= 读取失败");
        else
            log.info("微信支付配置文件=========> appId <========= 加载完成");
        if (StringUtils.isBlank(appSecret))
            log.error("微信支付配置文件=========> appSecret <========= 读取失败");
        else
            log.info("微信支付配置文件=========> appSecret <========= 加载完成");
        if (StringUtils.isBlank(mchId))
            log.error("微信支付配置文件=========> mchId <========= 读取失败");
        else
            log.info("微信支付配置文件=========> mchId <========= 加载完成");
        if (StringUtils.isBlank(partnerKey))
            log.error("微信支付配置文件=========> certPath <========= 读取失败");
        else
            log.info("微信支付配置文件=========> certPath <========= 加载完成");
        if (StringUtils.isBlank(certPath))
            log.error("微信支付配置文件=========> certPath <========= 读取失败");
        else
            log.info("微信支付配置文件=========> certPath <========= 加载完成");
        if (StringUtils.isBlank(domain))
            log.error("微信支付配置文件=========> domain <========= 读取失败");
        else
            log.info("微信支付配置文件=========> domain <========= 加载完成");
        WxPayApiConfig apiConfig = WxPayApiConfig.New()
                .setAppId(appId)
                .setMchId(mchId)
                .setPaternerKey(partnerKey)
                .setNotifyUrl(domain)
                .setPayModel(WxPayApiConfig.PayModel.BUSINESSMODEL);
        log.info("微信支付配置加载完成");
        return apiConfig;
    }

    /**
     * 支付流水号
     *
     * @return id生成器
     */
    @Bean
    public RedisIdGenerator payNoGenerator() {
        return new RedisIdGenerator("GENERAL_PAY_SERIAL_NUMBER");
    }

}
