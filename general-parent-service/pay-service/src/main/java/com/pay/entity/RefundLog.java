/*
 * Copy Right@
 */
package com.pay.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Entity
@Table(name = "refund_log")
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Getter
@Setter
public class RefundLog {

    /**
     * 主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 支付流水号
     */
    @Column(name = "pay_platform_no")
    private String payPlatformNo;

    /**
     * 退款流水号
     */
    @Column(name = "refund_no")
    private String refundNo;

    /**
     * 退款时间
     */
    @Column(name = "refund_time")
    private Date refundTime;

    /**
     * 退款金额
     */
    @Column(name = "amount")
    private BigDecimal amount;

    /**
     * 支付渠道
     */
    @Column(name = "pay_way")
    private String payWay;

    /**
     * 支付渠道名称
     */
    @Column(name = "pay_way_name")
    private String payWayName;

    /**
     * 手续费
     */
    @Column(name = "commission")
    private BigDecimal commission;

    /**
     * 来源服务
     */
    @NotBlank(message = "来源服务")
    @Column(name = "source_service")
    private String sourceService;

    /**
     * 转发服务
     */
    @NotBlank(message = "转发服务")
    @Column(name = "target_service")
    private String targetService;

}
