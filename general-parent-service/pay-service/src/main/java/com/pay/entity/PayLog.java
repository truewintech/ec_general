/*
 * Copy Right@
 */
package com.pay.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Entity
@Table(name = "pay_log")
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Getter
@Setter
public class PayLog {

    /**
     * 主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 支付平台的流水号
     */
    @NotBlank(message = "支付平台的流水号不可以为空")
    @Column(name = "pay_platform_no")
    private String payPlatformNo;

    /**
     * 支付时间
     */
    @NotNull(message = "支付时间不可以为空")
    @Column(name = "pay_time")
    private Date payTime;

    /**
     * 支付金额
     */
    @Column(name = "amount")
    @NotNull(message = "支付金额不可以为空")
    private BigDecimal amount;

    /**
     * 支付渠道,例如,微信支付,支付宝,银联
     */
    @Column(name = "pay_way")
    @NotBlank(message = "支付渠道不可以为空")
    private String payWay;

    /**
     * 支付渠道名称
     */
    @Column(name = "pay_way_name")
    private String payWayName;

    /**
     * 支付类型,举例:手机支付,pc网关,收银宝等等
     */
    @Column(name = "pay_type")
    @NotBlank(message = "支付类型不可以为空")
    private String payType;

    /**
     * 支付类型名称
     */
    @Column(name = "pay_type_name")
    private String payTypeName;

    /**
     * 手续费
     */
    @NotNull(message = "手续费不可以为空")
    @Column(name = "commission")
    private BigDecimal commission;

    /**
     * 来源服务
     */
    @NotBlank(message = "来源服务")
    @Column(name = "source_service")
    private String sourceService;

    /**
     * 转发服务
     */
    @NotBlank(message = "转发服务")
    @Column(name = "target_service")
    private String targetService;

    /**
     * 发起ip
     */
    @Column(name = "ip")
    private String ip;

    /**
     * 发起的支付信息
     */
    @Column(name = "self_params")
    private String selfParams;

    /**
     * 接收支付平台参数
     */
    @Column(name = "pay_params")
    private String payParams;

    @PrePersist
    void prePersist() {
        payTime = new Date();
    }
}
