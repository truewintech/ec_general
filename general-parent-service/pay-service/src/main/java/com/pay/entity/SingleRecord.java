package com.pay.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author Liwenbi
 * @Date 2018/10/19 10:33\
 * 下单记录
 */
@Entity
@Data
@Table(name = "single_record")
public class SingleRecord {

    /**
     * 主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @Column(name = "order_no")
    private String orderNo;

    @Column(name = "payload")
    private String payload;
}
