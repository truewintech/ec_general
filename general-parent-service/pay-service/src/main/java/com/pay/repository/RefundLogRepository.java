/*
 *
 * Copy Right@
 */
package com.pay.repository;

import com.pay.entity.RefundLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author wjz
 * @date 2017年12月08日
 */
@Repository
public interface RefundLogRepository extends JpaRepository<RefundLog, Integer> {

    /**
     * 根据支付流水查找对应的退款记录
     *
     * @param payPlatformNo 支付流水号
     * @param payWay        支付渠道
     * @return 退款记录列表
     */
    List<RefundLog> findByPayPlatformNoAndPayWay(String payPlatformNo, String payWay);

    /**
     * 根据退款流水查找对应的退款记录
     *
     * @param refundNo 支付流水号
     * @param payWay   支付渠道
     * @return 退款记录
     */
    RefundLog findByRefundNoAndPayWay(String refundNo, String payWay);

    /**
     * 根据支付流水查询退款记录
     *
     * @param payPlatformNo 支付流水
     * @return 退款记录
     */
    List<RefundLog> findByPayPlatformNo(String payPlatformNo);
}
