/*
 *
 * Copy Right@
 */
package com.pay.repository;

import com.pay.entity.PayLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author wjz
 * @date 2017年12月08日
 */
public interface PayLogRepository extends JpaRepository<PayLog, Integer> {

    /**
     * 根据支付流水,支付渠道找实体
     *
     * @param payPlatformNo 支付流水
     * @param payWay        支付渠道
     * @return 支付的实体
     */
    PayLog findByPayPlatformNoAndPayWay(String payPlatformNo, String payWay);

    /**
     * 根据id列表查询支付记录
     *
     * @param ids 主键列表
     * @return 支付近路
     */
    List<PayLog> findByIdIsIn(List<Integer> ids);
}
