package com.pay.repository;

import com.pay.entity.SingleRecord;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author Liwenbi
 * @Date 2018/10/19 10:35
 */
public interface SingleRecordRepository extends JpaRepository<SingleRecord, String> {
}
