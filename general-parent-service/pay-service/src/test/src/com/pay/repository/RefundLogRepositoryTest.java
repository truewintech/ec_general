package com.pay.repository;

import com.framework.constant.DbConstList;
import com.pay.entity.RefundLog;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <pre>
 * 修改版本: 0.9
 * 修改日期: 2017年12月08日
 * 修改人 :  wjz
 * 修改说明:
 * 复审人 ：
 * </pre>
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class RefundLogRepositoryTest {

    @Autowired
    private RefundLogRepository refundLogRepository;

    @Test
    public void saveTest(){
        RefundLog refundLog = new RefundLog();

        refundLog.setPayPlatformNo("platform");
        refundLog.setRefundNo("31233");
        refundLog.setRefundTime(new Date());
        refundLog.setAmount(new BigDecimal(0.9));
        refundLog.setPayWay(DbConstList.PayWay.ALI_PAY.toString());
        refundLog.setPayWayName(DbConstList.PayWay.ALI_PAY_NAME);
        refundLog.setCommission(new BigDecimal(0.9));
        refundLogRepository.save(refundLog);

    }
}