package com.pay.repository;

import com.framework.constant.DbConstList;
import com.jpay.unionpay.SDKConfig;
import com.pay.entity.PayLog;
import io.swagger.annotations.ApiOperation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * <pre>
 * 修改版本: 0.9
 * 修改日期: 2017年12月08日
 * 修改人 :  wjz
 * 修改说明:
 * 复审人 ：
 * </pre>
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class PayLogRepositoryTest {

    @Autowired
    PayLogRepository payLogRepository;

    @Test
    public void saveTest(){

        PayLog payLog = new PayLog();
        payLog.setPayPlatformNo("11111");
        payLog.setAmount(new BigDecimal(88.09));
        payLog.setPayWay(DbConstList.PayWay.ALI_PAY.toString());
        payLog.setPayWayName(DbConstList.PayWay.ALI_PAY_NAME);
        payLog.setPayType(DbConstList.PayType.AliPay.PC.toString());
        payLog.setPayTypeName(DbConstList.PayType.AliPay.PC_NAME);
        payLog.setCommission(new BigDecimal(.99) );
        payLog.setIp("112.923.32.33");
        payLog.setPayParams("remark");

        payLogRepository.save(payLog);

    }

    @Test
    public void findpayids(){
        List<Integer> ids  = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        List<PayLog> payLogs = payLogRepository.findByIdIsIn(ids);
    }
}