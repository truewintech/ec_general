/*
 *
 * Copy Right@
 */
package com.pay;

import com.alibaba.fastjson.JSON;
import com.framework.constant.DbConstList;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

/**
 * <pre>
 * 修改版本: 0.9
 * 修改日期: 2017年10月30日
 * 修改人 :  wjz
 * 修改说明:
 * 复审人 ：
 * </pre>
 */
public class Test {

    public static void main(String[] args) {

        List<Integer> ids = new ArrayList<>();
        ids.add(1);
        ids.add(2);
        ids.add(3);
        ids.add(4);
        ids.add(5);
        ids.add(6);
        ids.add(7);
        aaa(ids);

//        Class cls = DList.getEnumClass("DList.PayType");
//
//        String json =  DList.getEnumJson("DList.PayType");
//
//        Class cls2 =  DList.getEnumClass("DList.Document.Type");
//
//        String json2 =  DList.getEnumJson("DList.Document.Type");

//        DList.PayType[] a =  DataList.PayType.values();
//        DList.PayType  b =   DList.PayType.valueOf(DList.PayType.class,"ALI");
        DbConstList.PayWay payWay = DbConstList.PayWay.valueOf(DbConstList.PayWay.class, "ALIPAY");

        DbConstList.PayWay payWay2 = DbConstList.PayWay.valueOf(DbConstList.PayWay.class, "ALIPAY2");

        String c = DbConstList.getEnumJson(DbConstList.PayType.class, "WXPAY");
        EnumMap em = null;
        try {
            em = (EnumMap) DbConstList.PayType.class.getDeclaredField("MAP").get(EnumMap.class);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }


        String aaa = JSON.toJSONString(em);

    }

    public static void aaa(List<Integer> ids) {
        List<Integer> rids = new ArrayList<>();
        for (Integer id : ids) {
            if (id > 5) {
                rids.add(id);
            }
        }

        for (Integer id : rids) {
            ids.remove(id);
        }
    }
}
