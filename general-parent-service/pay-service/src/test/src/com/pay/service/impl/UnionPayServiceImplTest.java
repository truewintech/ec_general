package com.pay.service.impl;

import com.jpay.unionpay.LogUtil;
import com.pay.service.PayService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * <pre>
 * 修改版本: 0.9
 * 修改日期: 2017年12月12日
 * 修改人 :  wjz
 * 修改说明:
 * 复审人 ：
 * </pre>
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class UnionPayServiceImplTest {


    private PayService unionPayServiceImpl;

    @Autowired
    @Qualifier("unionPayServiceImpl")
    public void setUnionPayServiceImpl(PayService unionPayService){
        this.unionPayServiceImpl = unionPayService;
    }

    @Test
    public void test(){
        LogUtil.writeLog("aaaaaa");
        LogUtil.writeErrorLog("bbbb");
        LogUtil.debug("ccccc");
    }
}