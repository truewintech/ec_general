package com.pay.entity;

import com.alibaba.fastjson.JSON;
import com.pay.bean.OrderPayInfo;
import com.pay.bean.PayInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * 修改版本: 0.9
 * 修改日期: 2017年12月09日
 * 修改人 :  wjz
 * 修改说明:
 * 复审人 ：
 * </pre>
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class PayInfoTest {

    @Test
    public void payInfoTest(){
        PayInfo payInfo = new PayInfo();
        payInfo.setPayAmount(new BigDecimal(3000));
        payInfo.setPayType("WAP");
        payInfo.setPayTypeName("手机支付");
        payInfo.setPayWay("UNION");
        payInfo.setPayWayName("银联支付");
        OrderPayInfo orderPayInfo = new OrderPayInfo();
        orderPayInfo.setOrderId(1);
        orderPayInfo.setOrderNo("so12321");
        orderPayInfo.setPayAmount(new BigDecimal(3000));
        orderPayInfo.setPayCommission(new BigDecimal(30));
        List<OrderPayInfo> orders =  new ArrayList<OrderPayInfo>();
        orders.add(orderPayInfo);
        payInfo.setOrders(orders);

        System.out.println(JSON.toJSONString(payInfo));
    }
}