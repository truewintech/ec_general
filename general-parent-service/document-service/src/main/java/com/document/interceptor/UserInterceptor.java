/*
 * Copy Right@
 */
package com.document.interceptor;

import com.common.constant.Const;
import com.framework.util.UserUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class UserInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        // 当前用户ID
        String currentUserId = request.getHeader(Const.Header.CURRENT_USER_ID);
        if (StringUtils.isBlank(currentUserId)) {
            currentUserId = request.getParameter(Const.Header.CURRENT_USER_ID);
        }
        if (StringUtils.isNotBlank(currentUserId)) {
            // 设置进线程局部变量
            UserUtil.setCurrentUserId(Integer.parseInt(currentUserId));
        }

        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        UserUtil.removeCurrentUserId();
    }
}
