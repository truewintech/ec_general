/*
 * Copy Right@
 */
package com.document.repository;

import com.document.entity.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Repository
public interface DocumentRepository extends JpaRepository<Document, Integer> {
}
