/*
 * Copy Right@
 */
package com.document.entity;

import com.framework.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Data
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Entity
@EqualsAndHashCode
@Table(name = "document")
public class Document extends BaseEntity {

    /**
     * 主表主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 文件类型
     */
    @Column(name = "type")
    private String type;

    /**
     * 类型名称
     */
    @Column(name = "type_name")
    private String typeName;

    /**
     * 文件格式
     */
    @Column(name = "format")
    private String format;

    /**
     * 文件名称
     */
    @Column(name = "name")
    private String name;

    /**
     * 文件大小
     */
    @Column(name = "size")
    private Long size;

    /**
     * 访问文件URL
     */
    @Column(name = "access_url")
    private String accessUrl;

}