/*
 * Copy Right@
 */
package com.document;

import com.common.annotation.EnableAmqp;
import com.common.listener.StartupApplicationListener;
import com.framework.annotation.*;
import com.framework.util.YamlUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@SpringCloudApplication
@EnableJpaRedis
@EnableJpaFastJson
@EnableMessageException
@EnableOSS
@EnableRedisManager
@EnableAmqp
@EnableRestTemplateUtil
@Import({YamlUtil.class})
@EnableCircuitBreaker
@EnableSwagger
@EnableQuartz
public class DocumentApplication {

    /**
     * 启动Ribbon 负载均衡能力
     *
     * @param fastJsonHttpMessageConverters json转换器
     * @return restTemplate
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(
            HttpMessageConverters fastJsonHttpMessageConverters) {
        return new RestTemplate(
                fastJsonHttpMessageConverters.getConverters());
    }

    /**
     * 程序入口
     *
     * @param args 启动参数
     */
    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(DocumentApplication.class);
        springApplication.addListeners(new StartupApplicationListener());

        springApplication.run(args);
    }

}