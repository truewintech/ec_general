/*
 * Copy Right@
 */
package com.document.api;

import com.document.entity.Document;
import com.document.service.DocumentService;
import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("document")
@Api(value = "文档服务API")
@Slf4j
public class DocumentServiceApi extends BaseApi {

    /**
     * 文档服务
     */
    @Autowired
    private DocumentService documentService;

    /**
     * 获取文档信息
     *
     * @param documentId 文档ID
     * @return 文档信息
     */
    @ApiOperation(value = "根据ID获取文档信息")
    @GetMapping("getDocumentById")
    public Result getDocumentById(@RequestParam int documentId) {
        Document document = documentService.findById(documentId);

        return success(document);
    }

    /**
     * 获取文档信息集合
     *
     * @param documentIds 文档ID集合
     * @return 文档信息集合
     */
    @ApiOperation(value = "根据多个ID获取文档信息")
    @GetMapping("getDocumentsByIds")
    public Result getDocumentsByIds(@RequestParam int[] documentIds) {
        List<Document> documents = documentService.findByIds(documentIds);

        return success(documents);
    }

    /**
     * 上传一个文档
     *
     * @param file 文件流
     * @param type 文件类型
     * @return 文档信息
     * @throws IOException 异常
     */
    @ApiOperation(value = "上传一个文档")
    @PostMapping("uploadDocument")
    public Result uploadDocument(@RequestParam(value = "file") MultipartFile file, String type) throws IOException {
        Document doc = documentService.uploadDocument(file, type);

        return success(doc);
    }

    /**
     * 不需要登录，上传一个文档
     *
     * @param file 文件流
     * @param type 文件类型
     * @return 文档信息
     * @throws IOException 异常
     */
    @ApiOperation(value = "不需要登录，上传一个文档")
    @PostMapping("view/uploadDocument")
    public Result viewUploadDocument(@RequestParam(value = "file") MultipartFile file, String type) throws IOException {
        Document doc = documentService.uploadDocument(file, type);

        return success(doc);
    }

    /**
     * 删除一个文档
     *
     * @param documentId 文档ID
     * @return 文档信息
     * @throws IOException 异常
     */
    @ApiOperation(value = "删除一个文档")
    @GetMapping("deleteDocument")
    public Result deleteDocument(@RequestParam int documentId) throws IOException {
        documentService.deleteDocument(documentId);
        return success();
    }

    /**
     * 上传一个文档
     *
     * @param base64Code 文件流
     * @return 文档信息
     */
    @ApiOperation("上传一个文档")
    @PostMapping("uploadDocumentFromBase64Code")
    public Result uploadDocumentFromBase64Code(@RequestBody String base64Code) {
        return success(documentService.uploadDocumentFromBase64Code(base64Code));
    }

    /**
     * 上传一个文档
     *
     * @param url 文件地址
     * @return 文档信息
     */
    @ApiOperation("上传一个文档")
    @GetMapping("uploadDocumentByUrl")
    public Result uploadDocumentByUrl(@RequestParam String url) {
        return success(documentService.uploadDocumentByUrl(url));
    }
}
