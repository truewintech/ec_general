/*
 * Copy Right@
 */
package com.document.api;

import com.common.constant.Const;
import com.document.service.VerificationService;
import com.framework.constant.HttpResponse;
import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("verificationCode")
@Api(value = "验证码服务API")
@Slf4j
public class VerificationCodeServiceApi extends BaseApi {

    /**
     * 图形校验码服务
     */
    @Autowired
    private VerificationService verificationService;

    /**
     * 获取注册的图形验证码信息
     *
     * @param mobile 手机号
     * @return 图形验证码信息
     */
    @ApiOperation(value = "获取注册的图形验证码信息")
    @GetMapping
    public Result getVerificationCode(@RequestParam("mobile") String mobile) {
        return success(HttpResponse.SUCCESS, verificationService.getBase64Img(Const.CacheKey.VERIFICATION_CODE_REGISTER_PRE, mobile));
    }

    /**
     * 获取修改验证手机号的图形验证码信息
     *
     * @param mobile 手机号
     * @return 图形验证码信息
     */
    @ApiOperation(value = "获取修改验证手机号的图形验证码信息")
    @GetMapping(value = "getVerificationCodeForUpdateMobile")
    public Result getVerificationCodeForUpdateMobile(@RequestParam("mobile") String mobile) {
        return success(HttpResponse.SUCCESS, verificationService.getBase64Img(Const.CacheKey.VERIFICATION_CODE_UPDATE_MOBILE_PRE, mobile));
    }

    /**
     * 获取忘记密码的图形验证码信息
     *
     * @param mobile 手机号
     * @return 图形验证码信息
     */
    @ApiOperation(value = "获取忘记密码的图形验证码信息")
    @GetMapping(value = "view/getVerificationCodeForForgetPassword")
    public Result getVerificationCodeForForgetPassword(@RequestParam("mobile") String mobile) {
        return success(HttpResponse.SUCCESS, verificationService.getBase64Img(Const.CacheKey.VERIFICATION_CODE_FORGET_PASSWORD_PRE, mobile));
    }
}
