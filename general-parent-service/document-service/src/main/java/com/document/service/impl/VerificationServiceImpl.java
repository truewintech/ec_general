/*
 * Copy Right@
 */
package com.document.service.impl;

import com.document.service.VerificationService;
import com.framework.data.redis.RedisManager;
import com.framework.exception.MessageException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class VerificationServiceImpl implements VerificationService {

    /**
     * 缓存服务
     */
    @Autowired
    private RedisManager redisManager;

    /**
     * 图片宽度
     */
    private static int IMG_WIDTH = 78;

    /**
     * 图片高度
     */
    private static int IMG_HEIGHT = 30;

    /**
     * 干扰线数量
     */
    private static int LINE_SIZE = 40;

    /**
     * 字体离左边距离
     */
    private static int FONT_LEFT_PADDING = 14;

    /**
     * 字体离上边距离
     */
    private static int FONT_TOP_PADDING = 25;

    /**
     * 字体大小
     */
    private static int FONT_SIZE = 24;

    /**
     * 随机数
     */
    private Random random = new Random();

    @Override
    public Map<String, Object> getBase64Img(String key, String mobile) {
        Map<String, Object> result = new HashMap<>(20);
        try {
            String verificationCode = RandomStringUtils.random(4, "123456789");
            String verificationCodeKey = key + mobile;
            redisManager.setCache(verificationCodeKey, verificationCode, 3600);
            result.put("verificationCodeImg", getVerificationCodeBase64Str(verificationCode));
        } catch (IOException e) {
            throw new MessageException("获取图形验证码失败");
        }
        return result;
    }

    /**
     * 依据验证码获取验证码图片Base64字符串
     *
     * @param code 验证码
     * @return 验证码图片Base64字符串
     * @throws IOException io异常
     */
    private String getVerificationCodeBase64Str(String code) throws IOException {
        BufferedImage verificationCodeImg = getVerificationCodeImg(code);
        //io流
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        //写入流中
        ImageIO.write(verificationCodeImg, "JPEG", baos);
        //转换成字节
        byte[] bytes = baos.toByteArray();
        Base64 encoder = new Base64();
        //转换成base64串
        String verificationCodeBase64Img = encoder.encodeAsString(bytes).trim();
        //删除 \r\n
        return verificationCodeBase64Img.replaceAll("\n", "").replaceAll("\r", "");
    }

    /**
     * 依据验证码获取验证码图片
     *
     * @param code 验证码
     * @return 验证码图片
     */
    private BufferedImage getVerificationCodeImg(String code) {
        BufferedImage image = new BufferedImage(IMG_WIDTH, IMG_HEIGHT,
                BufferedImage.TYPE_INT_BGR);
        // 产生Image对象的Graphics对象,改对象可以在图像上进行各种绘制操作
        Graphics g = image.getGraphics();
        g.fillRect(0, 0, IMG_WIDTH, IMG_HEIGHT);
        g.setFont(new Font("Times New Roman", Font.PLAIN, 18));
        g.setColor(getRandColor(110, 133));
        // 绘制干扰线
        for (int i = 0; i <= LINE_SIZE; i++) {
            drawLine(g);
        }

        // 绘制随机字符
        drawRandCode(g, code);
        return image;
    }

    /**
     * 获取随机颜色
     *
     * @param fc 随机参数
     * @param bc 随机参数
     * @return 颜色
     */
    private Color getRandColor(int fc, int bc) {
        int max = 255;
        if (fc > max) {
            fc = max;
        }
        if (bc > max) {
            bc = max;
        }
        int r = fc + random.nextInt(bc - fc - 16);
        int g = fc + random.nextInt(bc - fc - 14);
        int b = fc + random.nextInt(bc - fc - 18);
        return new Color(r, g, b);
    }

    /**
     * 绘制干扰线
     *
     * @param g 图片
     */
    private void drawLine(Graphics g) {
        int x = random.nextInt(IMG_WIDTH);
        int y = random.nextInt(IMG_HEIGHT);
        int xl = random.nextInt(13);
        int yl = random.nextInt(15);
        g.drawLine(x, y, x + xl, y + yl);
    }

    /**
     * 绘制验证码
     *
     * @param g    图片
     * @param code 验证码
     */
    private void drawRandCode(Graphics g, String code) {
        g.setFont(getFont());
        g.setColor(new Color(random.nextInt(101), random.nextInt(111),
                random.nextInt(121)));
        g.translate(random.nextInt(3), random.nextInt(3));
        g.drawString(code, FONT_LEFT_PADDING, FONT_TOP_PADDING);
    }

    /**
     * 获取字体
     *
     * @return 字体
     */
    private Font getFont() {
        return new Font("Fixedsys", Font.BOLD, FONT_SIZE);
    }

}
