/*
 * Copy Right@
 */
package com.document.service.impl;

import com.aliyun.oss.OSSClient;
import com.document.entity.Document;
import com.document.repository.DocumentRepository;
import com.document.service.DocumentService;
import com.framework.constant.HttpResponse;
import com.framework.exception.MessageException;
import com.framework.oss.OssConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class DocumentServiceImpl implements DocumentService {

    /**
     * 文档数据仓库
     */
    @Autowired
    private DocumentRepository documentRepository;

    /**
     * oss客户端
     */
    @Autowired
    private OSSClient ossClient;

    @Override
    public Document findById(final Integer id) {
        if (id == 0) {
            throw new MessageException("ID值不能为零或空");
        } else {
            Document document = documentRepository.findOne(id);
            if (document != null) {
                return document;
            } else {
                throw new MessageException("该ID找不到实体");
            }
        }
    }

    @Override
    public List<Document> findByIds(final int[] ids) {
        if (ids.length <= 0) {
            throw new MessageException("ID值不能为零或空");
        } else {
            List<Document> documents = new ArrayList<>();
            for (int id : ids) {
                Document document = documentRepository.findOne(id);
                if (document != null) {
                    documents.add(document);
                }
            }
            return documents;
        }
    }

    @Override
    public Document uploadDocument(MultipartFile file, String type) throws IOException {
        if (file == null) {
            throw new MessageException("上传的文件为空");
        }

        // 文件名称和格式
        String name = file.getOriginalFilename();
        String format = "jpg";
        int idx = name.lastIndexOf(".");
        if (idx > 0) {
            format = name.substring(idx + 1);
        }
        String key = OssConfig.getKey(format);

        try {
            ossClient.putObject(OssConfig.bucket, key, file.getInputStream());
        } catch (Exception ex) {
            log.error("上传文件失败", ex);
            throw new MessageException(HttpResponse.FAIL, "上传文件失败");
        }

        Document doc = new Document();
        doc.setFormat(format);
        doc.setAccessUrl(OssConfig.getUrl(key));
        doc.setName(key);
        doc.setType(type);
        doc.setSize(file.getSize());
        try {
            doc = documentRepository.save(doc);
        } catch (Exception ex) {
            // 保存失败
            if (doc.getId() == null) {
                try {
                    ossClient.deleteObject(OssConfig.bucket, key);
                } catch (Exception e) {
                    log.error("删除临时文件失败", e);
                }
            }

            throw new MessageException(HttpResponse.FAIL, "保存文件失败");
        }

        if (doc == null || doc.getId() == null) {
            try {
                ossClient.deleteObject(OssConfig.bucket, key);
            } catch (Exception e) {
                log.error("删除临时文件失败", e);
            }
        }

        return doc;
    }

    @Override
    public void deleteDocument(int id) {
        Document doc = documentRepository.findOne(id);
        if (doc == null) {
            throw new MessageException(HttpResponse.FAIL, "删除的文档不存在");
        }

        String key = doc.getName();
        documentRepository.delete(doc);
        ossClient.deleteObject(OssConfig.bucket, key);
    }

    @Override
    public Document uploadDocumentFromBase64Code(String base64code) {

        if (StringUtils.isBlank(base64code)) {
            throw new MessageException("上传的文件为空");
        }

        String base64Str = "base64";
        if (!base64code.contains(base64Str)) {
            throw new MessageException("上传文件格式错误");
        }

        int idx = base64code.indexOf("/");
        if (idx < 0) {
            try {
                base64code = base64code.replaceAll("\\+", "%2B");
                base64code = URLDecoder.decode(base64code, "utf-8");
                idx = base64code.indexOf("/");
            } catch (Exception e) {
                throw new MessageException("文件转码错误");
            }
        }

        int index = base64code.indexOf(",");
        if (idx < 0 || index < 0) {
            throw new MessageException("上传文件内容错误");
        }

        // 获取图片的后缀
        String format = base64code.substring(idx + 1, index);
        // 去掉插件自带的前缀
        base64code = base64code.replace(base64code.substring(0, index + 1), "");

        // Base64解码
        InputStream inputStream;
        try {
            Base64 decoder = new Base64();
            byte[] b = decoder.decode(base64code);
            for (int i = 0; i < b.length; ++i) {
                // 调整异常数据
                if (b[i] < 0) {
                    b[i] += 256;
                }
            }
            inputStream = new ByteArrayInputStream(b);

        } catch (Exception e) {
            log.error("Base64解码失败", e);
            throw new MessageException(HttpResponse.FAIL, "Base64解码失败");
        }

        // 拼接图片名
        String key = OssConfig.getKey(format);
        try {
            ossClient.putObject(OssConfig.bucket, key, inputStream);
        } catch (Exception ex) {
            log.error("上传文件失败", ex);
            throw new MessageException(HttpResponse.FAIL, "上传文件失败");
        }

        Document doc = new Document();
        doc.setFormat(format);
        doc.setAccessUrl(OssConfig.getUrl(key));
        doc.setName(key);
        try {
            doc = documentRepository.save(doc);
        } catch (Exception ex) {
            // 保存失败
            if (doc.getId() == null) {
                try {
                    ossClient.deleteObject(OssConfig.bucket, key);
                } catch (Exception e) {
                    log.error("删除临时文件失败", e);
                }
            }
            throw new MessageException("保存文件失败");
        }

        if (doc == null || doc.getId() == 0) {
            try {
                ossClient.deleteObject(OssConfig.bucket, key);
            } catch (Exception e) {
                log.error("删除临时文件失败", e);
            }
        }
        return doc;
    }

    @Override
    public Document uploadDocumentByUrl(String url) {
        if (StringUtils.isBlank(url)) {
            throw new MessageException("上传的文件为空");
        }

        // 文件名称和格式
        String format = "jpg";
        int idx = url.lastIndexOf(".");
        if (idx > 0) {
            format = url.substring(idx + 1);
        }
        String key = OssConfig.getKey(format);

        try {
            URL u = new URL(url);
            ossClient.putObject(OssConfig.bucket, key, u.openStream());
        } catch (Exception ex) {
            log.error("上传文件失败", ex);
            throw new MessageException(HttpResponse.FAIL, "上传文件失败");
        }

        Document doc = new Document();
        doc.setFormat(format);
        doc.setAccessUrl(OssConfig.getUrl(key));
        doc.setName(key);
        try {
            doc = documentRepository.save(doc);
        } catch (Exception ex) {
            // 保存失败
            if (doc.getId() == null) {
                try {
                    ossClient.deleteObject(OssConfig.bucket, key);
                } catch (Exception e) {
                    log.error("删除临时文件失败", e);
                }
            }

            throw new MessageException(HttpResponse.FAIL, "保存文件失败");
        }

        if (doc == null || doc.getId() == null) {
            try {
                ossClient.deleteObject(OssConfig.bucket, key);
            } catch (Exception e) {
                log.error("删除临时文件失败", e);
            }
        }

        return doc;
    }
}
