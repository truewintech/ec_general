/*
 * Copy Right@
 */
package com.document.service;

import com.document.entity.Document;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface DocumentService {

    /**
     * 根据文档ID返回文档实体
     *
     * @param id
     * @return 返回文档实体
     */
    Document findById(final Integer id);

    /**
     * 根据多个文档ID返回文档实体列表
     *
     * @param ids
     * @return 回文档实体列表
     */
    List<Document> findByIds(final int[] ids);

    /**
     * 上传文档
     *
     * @param file 文件流
     * @param type 文件类型
     * @return 文档
     * @throws IOException 异常
     */
    Document uploadDocument(MultipartFile file, String type) throws IOException;

    /**
     * 删除文档
     *
     * @param id 文档id
     */
    void deleteDocument(int id);

    /**
     * 上传base64格式文件
     *
     * @param base64code base64字符串
     * @return 文档
     */
    Document uploadDocumentFromBase64Code(String base64code);

    /**
     * 通过url上传文件
     *
     * @param url 文件地址
     * @return 文件
     */
    Document uploadDocumentByUrl(String url);
}
