/*
 * Copy Right@
 */
package com.document.service.impl;

import com.document.service.EventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class EventServiceImpl implements EventService {
}
