/*
 * Copy Right@
 */
package com.document.service;

import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface VerificationService {

    /**
     * 获取base64格式图形验证码
     *
     * @param key    关键字（用于区分不同页面请求的验证码）
     * @param mobile 手机号
     * @return 图形验证码
     */
    Map<String, Object> getBase64Img(String key, String mobile);
}
