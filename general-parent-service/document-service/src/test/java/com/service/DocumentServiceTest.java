/*
 * Copy Right@
 */
package com.service;

import com.document.service.DocumentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class DocumentServiceTest {

    @Autowired
    DocumentService documentService;

    @Test
    public void documentTest() {
        documentService.deleteDocument(1);
    }
}
