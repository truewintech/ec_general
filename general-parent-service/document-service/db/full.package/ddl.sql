drop table if exists document;

/*==============================================================*/
/* Table: document                                              */
/*==============================================================*/
create table document
(
   id                   int not null auto_increment comment '主键',
   type                 varchar(32) comment '文件类型',
   type_name            varchar(32) comment '类型名称',
   format               varchar(32) comment '格式',
   name                 varchar(256) comment '名称',
   size                 int comment '大小',
   access_url           varchar(256) comment '文件地址',
   create_user          int comment '创建人',
   create_time          datetime not null comment '创建时间',
   update_user          int comment '更新人',
   update_time          datetime comment '更新时间',
   primary key (id)
);

alter table document comment '文档';
