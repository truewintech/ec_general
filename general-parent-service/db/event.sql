SET FOREIGN_KEY_CHECKS = 0;

drop table if exists event;

drop table if exists event_log;

/*==============================================================*/
/* Table: event                                                 */
/*==============================================================*/
create table event
(
   id                   int not null auto_increment comment '主键',
   event_id             varchar(64) comment '事件标识',
   sender               varchar(32) comment '发送者',
   receiver             varchar(32) comment '接收者',
   status               varchar(32) comment '状态',
   event_type           varchar(256) comment '事件类型',
   payload              text comment '事件内容',
   remark               varchar(256) comment '备注',
   deal_group           int comment '处理分组',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '更新时间',
   primary key (id)
);

alter table event comment '事件表';

/*==============================================================*/
/* Index: idx_event_s_e_t                                       */
/*==============================================================*/
create index idx_event_s_e_t on event
(
   status,
   event_type
);

/*==============================================================*/
/* Index: uk_event                                              */
/*==============================================================*/
create unique index uk_event on event
(
   event_id,
   sender,
   receiver
);

/*==============================================================*/
/* Table: event_log                                             */
/*==============================================================*/
create table event_log
(
   id                   int not null auto_increment comment '主键',
   event_ref            int comment '事件外键',
   is_success           bit not null comment '是否成功',
   code                 int comment '状态码',
   code_name            varchar(64) comment '状态码名称',
   detail               text comment '详情',
   create_time          datetime comment '创建时间',
   try_times            int comment '重试次数',
   primary key (id)
);

alter table event_log comment '事件日志表';

alter table event_log add constraint fk_log_event_r_event foreign key (event_ref)
      references event (id) on delete restrict on update restrict;

SET FOREIGN_KEY_CHECKS = 1;
