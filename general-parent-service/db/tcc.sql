drop table if exists tcc_event;

/*==============================================================*/
/* Table: tcc_event                                             */
/*==============================================================*/
create table tcc_event
(
   id                   int not null auto_increment comment '主键',
   status               varchar(32) comment '状态',
   tcc_type             varchar(32) comment '类型',
   service_id           varchar(256) comment '服务地址',
   payload              text comment '事件内容',
   create_time          datetime comment '创建时间',
   expiration_time      datetime comment '过期时间',
   update_time          datetime comment '更新时间',
   version              int comment '版本号',
   primary key (id)
);

/*==============================================================*/
/* Index: idx_tcc_event_status                                  */
/*==============================================================*/
create index idx_tcc_event_status on tcc_event
(
   status
);
