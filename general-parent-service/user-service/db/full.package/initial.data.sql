-- 后台超级管理员
INSERT INTO `user` VALUES (1, NULL, 'admin', NULL, '6b76a944765fc7c595259523344eca33', 'ff08e675dad2db8974071d2f35fd6cc8', NULL, NOW(), NULL, NULL, 1, 0, 1);

ALTER TABLE `user` AUTO_INCREMENT=1000;

-- 菜单资源
INSERT INTO `resource` VALUES (1, NULL, 'MENU', 'SYSTEM', '管理后台', NULL, '系统管理', '系统管理', '#', 99, NULL, NOW(), NULL, NULL, 1, 0);
INSERT INTO `resource` VALUES (2, 1, 'MENU', 'SYSTEM', '管理后台', NULL, '用户管理', '系统管理|用户管理', '/general/user', 1, NULL, NOW(), NULL, NULL, 1, 0);
INSERT INTO `resource` VALUES (3, 1, 'MENU', 'SYSTEM', '管理后台', NULL, '角色管理', '系统管理|角色管理', '/general/role', 2, NULL, NOW(), NULL, NULL, 1, 0);
INSERT INTO `resource` VALUES (4, 1, 'MENU', 'SYSTEM', '管理后台', NULL, '资源管理', '系统管理|资源管理', '/general/resource', 3, NULL, NOW(), NULL, NULL, 1, 0);
INSERT INTO `resource` VALUES (5, 1, 'MENU', 'SYSTEM', '管理后台', NULL, '职位管理', '系统管理|职位管理', '/general/position', 4, NULL, NOW(), NULL, NULL, 1, 0);
INSERT INTO `resource` VALUES (6, 1, 'MENU', 'SYSTEM', '管理后台', NULL, '地址管理', '系统管理|地址管理', '/general/address', 5, NULL, NOW(), NULL, NULL, 1, 0);

ALTER TABLE `resource` AUTO_INCREMENT=1000;