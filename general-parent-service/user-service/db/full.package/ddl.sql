SET FOREIGN_KEY_CHECKS=0;

drop table if exists position;

drop table if exists position_role;

drop table if exists resource;

drop table if exists role;

drop table if exists role_resource;

drop table if exists user;

drop table if exists user_position;

drop table if exists user_role;

drop table if exists user_wechat;

drop table if exists sharing;

/*==============================================================*/
/* Table: position                                              */
/*==============================================================*/
create table position
(
   id                   int not null auto_increment comment '主键',
   owner_id             int comment '拥有者id',
   name                 varchar(32) comment '名称',
   create_user          int comment '创建人',
   create_time          datetime comment '创建时间',
   update_user          int comment '更新人',
   update_time          datetime comment '更新时间',
   is_active            bit comment '是否有效',
   is_delete            bit comment '是否删除',
   primary key (id)
);

alter table position comment '职位表';

/*==============================================================*/
/* Table: position_role                                         */
/*==============================================================*/
create table position_role
(
   position_id          int comment '职位id',
   role_id              int comment '角色id'
);

alter table position_role comment '职位角色';

/*==============================================================*/
/* Table: resource                                              */
/*==============================================================*/
create table resource
(
   id                   int not null auto_increment comment '主键',
   parent_id            int comment '父id',
   type                 varchar(32) comment '类型',
   `range`              varchar(32) comment '范围：SYSTEM、SELLER_CENTER',
   range_name           varchar(32) comment '范围名称：管理后台、商家中心',
   icon                 varchar(256) comment 'icon',
   name                 varchar(256) comment '名称',
   code                 varchar(256) not null comment '权限编码',
   link                 varchar(512) comment '链接',
   sort                 int comment '排序',
   create_user          int comment '创建人',
   create_time          datetime comment '创建时间',
   update_user          int comment '更新人',
   update_time          datetime comment '更新时间',
   is_active            bit comment '是否有效',
   is_delete            bit comment '是否删除',
   primary key (id),
   key ak_resource_code (code)
)
;

alter table resource comment '资源';

/*==============================================================*/
/* Table: role                                                  */
/*==============================================================*/
create table role
(
   id                   int not null auto_increment comment '主键',
   owner_id             int comment '拥有者id',
   name                 varchar(32) comment '名称',
   detail               varchar(256) comment '描述',
   create_user          int comment '创建人',
   create_time          datetime comment '创建时间',
   update_user          int comment '更新人',
   update_time          datetime comment '更新时间',
   is_active            bit comment '是否有效',
   is_delete            bit comment '是否删除',
   primary key (id)
)
;

alter table role comment '角色';

/*==============================================================*/
/* Table: role_resource                                         */
/*==============================================================*/
create table role_resource
(
   role_id              int comment '角色id',
   resource_id          int comment '资源id'
)
;

alter table role_resource comment '角色资源';

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
create table user
(
   id                   int not null auto_increment comment '主键',
   owner_id             int comment '拥有者id',
   account              varchar(32) not null comment '账号',
   mobile               varchar(32) comment '手机号',
   password             varchar(64) comment '密码',
   salt                 varchar(64) comment '盐值',
   create_user          int comment '创建人',
   create_time          datetime comment '创建时间',
   update_user          int comment '更新人',
   update_time          datetime comment '更新时间',
   is_active            bit comment '是否有效',
   is_delete            bit comment '是否删除',
   is_system            bit default 0 comment '是否系统用户',
   primary key (id)
)
;

alter table user comment '用户';

/*==============================================================*/
/* Table: user_position                                         */
/*==============================================================*/
create table user_position
(
   user_id              int comment '用户id',
   position_id          int comment '职位id'
);

alter table user_position comment '用户职位表';

/*==============================================================*/
/* Table: user_role                                             */
/*==============================================================*/
create table user_role
(
   user_id              int comment '用户id',
   role_id              int comment '角色id'
)
;

alter table user_role comment '用户角色关联表';

/*==============================================================*/
/* Table: user_wechat                                           */
/*==============================================================*/
create table user_wechat
(
   id                   int not null auto_increment comment '主键',
   user_id              int comment '用户id',
   union_id             varchar(32) comment '唯一标识',
   open_id              varchar(32) comment '公众号用户标识',
   app_id               varchar(32) comment '公众号id',
   primary key (id)
)
;

/*==============================================================*/
/* Table: sharing                                           */
/*==============================================================*/
CREATE TABLE sharing (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sharer_id` int(11) NOT NULL COMMENT '分享者的userid',
  `be_shared_id` int(11) NOT NULL COMMENT '被分享者userid',
  `share_level` int(11) NOT NULL COMMENT '推荐层级',
  `registration_time` datetime NOT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`)
);

alter table sharing comment '推荐关系表';


alter table user_wechat comment '微信用户';

alter table position_role add constraint fk_position_role_r_position foreign key (position_id)
      references position (id) on delete restrict on update restrict;

alter table position_role add constraint fk_position_role_r_role foreign key (role_id)
      references role (id) on delete restrict on update restrict;

alter table resource add constraint fk_resource_r_resource foreign key (parent_id)
      references resource (id) on delete restrict on update restrict;

alter table role_resource add constraint fk_role_r_r_resource foreign key (resource_id)
      references resource (id) on delete restrict on update restrict;

alter table role_resource add constraint fk_role_r_r_role foreign key (role_id)
      references role (id) on delete restrict on update restrict;

alter table user_position add constraint fk_user_position_r_position foreign key (position_id)
      references position (id) on delete restrict on update restrict;

alter table user_position add constraint fk_user_position_r_user foreign key (user_id)
      references user (id) on delete restrict on update restrict;

alter table user_role add constraint fk_user_role_r_role foreign key (role_id)
      references role (id) on delete restrict on update restrict;

alter table user_role add constraint fk_user_role_r_user foreign key (user_id)
      references user (id) on delete restrict on update restrict;

alter table user_wechat add constraint fk_user_wechat_r_user foreign key (user_id)
      references user (id) on delete restrict on update restrict;

SET FOREIGN_KEY_CHECKS=1;

ALTER TABLE `user_wechat`
MODIFY COLUMN `nick_name`  varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户的昵称' AFTER `province`;

