/*
 * Copy Right@
 */
package com.user.task;

import com.common.constant.DbConst;
import com.framework.constant.DbConstList;
import com.framework.entity.Event;
import com.framework.task.BaseEventTask;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class EventHandleTask extends BaseEventTask {

    @Override
    public List<Event> findEvents(Integer dealGroup) {
        return eventRepository.findTop100ByDealGroupAndStatusAndReceiverOrderById(dealGroup, DbConstList.Event.Status.NEW
                , DbConst.Event.SenderOrReceiver.USER_SERVICE);
    }

    @Override
    protected boolean dealEvent(Event event) {
        return false;
    }

}
