/*
 * Copy Right@
 */
package com.user.task;

import com.framework.constant.DbConstList;
import com.framework.data.redis.klock.annotation.Klock;
import com.framework.entity.TccEvent;
import com.framework.quartz.AbstractSingleTask;
import com.framework.repository.TccEventRepository;
import com.framework.transaction.TransactionInterceptor;
import com.user.service.TccService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionCallback;

import java.util.Date;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class TccEventCancelTask extends AbstractSingleTask {

    /**
     * tcc事件仓库
     */
    @Autowired
    private TccEventRepository tccEventRepository;

    /**
     * tcc服务
     */
    @Autowired
    private TccService tccService;

    /**
     * 手动事务拦截器
     */
    @Autowired
    private TransactionInterceptor transaction;

    @Override
    @Klock
    public void execute() {
        try {
            log.info("{}开始执行", this.getClass());
            cancelTccEvents();
        } finally {
            log.info("{}执行结束", this.getClass());
        }
    }

    /**
     * 取消事件集合
     */
    private void cancelTccEvents() {
        while (true) {
            List<TccEvent> events = tccEventRepository
                    .findTop100ByStatusOrderById(DbConstList.TccEvent.Status.TRY);
            if (events == null || events.isEmpty()) {
                break;
            }

            events.forEach(event -> {
                try {
                    cancelSingleEvent(event);
                } catch (Throwable throwable) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("处理tcc事件[");
                    sb.append(event.getId());
                    sb.append("]失败");
                    log.error(sb.toString(), throwable);
                }
            });
        }
    }

    /**
     * 取消单个事件
     *
     * @param event 事件
     * @throws Throwable 异常
     */
    private void cancelSingleEvent(TccEvent event) throws Throwable {
        // 启用手动事务
        transaction.doTransaction((TransactionCallback<Void>) status -> {
            // 处理事件
            log.info("开始处理tcc事件，id：{}", event.getId());
            boolean result = cancelTccEvent(event);

            if (result) {
                // 更新事件表
                event.setStatus(DbConstList.TccEvent.Status.CANCEL);
                tccEventRepository.save(event);
            }
            return null;
        });
    }

    /**
     * 取消tcc事件
     *
     * @param event 事件
     * @return 是否成功
     */
    private boolean cancelTccEvent(TccEvent event) {
        Date expirationTime = event.getExpirationTime();
        if (expirationTime != null && expirationTime.after(new Date())) {
            return false;
        }
        String tccType = event.getTccType();
        if (StringUtils.isNotBlank(tccType)) {
            tccService.cancelTcc(event);
            return true;
        }
        return false;
    }
}
