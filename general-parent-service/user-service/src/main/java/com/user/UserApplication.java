/*
 * Copy Right@
 */
package com.user;

import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.common.annotation.EnableAmqp;
import com.common.listener.StartupApplicationListener;
import com.framework.annotation.*;
import com.framework.data.redis.RedisIdGenerator;
import com.framework.util.YamlUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@SpringCloudApplication
@EnableJpaRedis
@EnableJpaFastJson
@EnableMessageException
@EnableRedisManager
@EnableAmqp
@EnableRestTemplateUtil
@Import({YamlUtil.class})
@EnableCircuitBreaker
@EnableSwagger
@EnableQuartz
@EnableWxMp
@EnableWxMa
public class UserApplication {

    /**
     * 启动Ribbon 负载均衡能力
     *
     * @param fastJsonHttpMessageConverters json转换器
     * @return restTemplate
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(
            HttpMessageConverters fastJsonHttpMessageConverters) {
        return new RestTemplate(
                fastJsonHttpMessageConverters.getConverters());
    }

    /**
     * 注入账号id生成器
     *
     * @return id生成器
     */
    @Bean
    public RedisIdGenerator userAccountGenerator() {
        return new RedisIdGenerator("GENERAL_USER_ACCOUNT");
    }

    /**
     * 程序入口
     *
     * @param args 启动参数
     */
    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(UserApplication.class);
        springApplication.addListeners(new StartupApplicationListener());
        springApplication.run(args);
    }

    @Bean
    public HttpMessageConverters fastJsonHttpMessageConverters() {
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        ArrayList supportedMediaTypes = new ArrayList();
        supportedMediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        fastConverter.setSupportedMediaTypes(supportedMediaTypes);
        return new HttpMessageConverters(new HttpMessageConverter[]{fastConverter});
    }

}