package com.user.api;

import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.user.entity.Sharing;
import com.user.service.SharingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("sharing")
@Api(value = "用户推荐")
@Slf4j
public class SharingServiceApi extends BaseApi {

    /**
     * 推荐关系服务
     */
    @Autowired
    private SharingService sharingService;

    /**
     * 查询推荐关系分页列表
     *
     * @param pageable 分页
     * @return 分页数据
     */
    @ApiOperation(value = "查询推荐关系分页列表")
    @GetMapping("findSharingPage")
    public Result findPage(@RequestParam(value = "shareType", required  =  false) String shareType,
            @PageableDefault(sort = {
            "id"}, direction = Sort.Direction.DESC) Pageable pageable){
        Result result = successEager(sharingService.findSharingListByPage(shareType,pageable));
        return result;
    }
}
