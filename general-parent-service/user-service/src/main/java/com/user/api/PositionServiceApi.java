/*
 * Copy Right@
 */
package com.user.api;

import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.user.entity.Position;
import com.user.service.PositionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

/**
 * <pre>
 * 资源接口
 * </pre>
 *
 * @author FengShiWei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("position")
@Api(value = "职位API")
public class PositionServiceApi extends BaseApi {

    /**
     * 职位服务
     */
    @Autowired
    private PositionService positionService;

    /**
     * 分页查询职位列表
     *
     * @param pageable 分页
     * @return 职位列表
     */
    @ApiOperation(value = "分页查询职位列表")
    @GetMapping(value = "findPositionListPage")
    public Result findPositionListPage(@PageableDefault(sort = {
            "id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return successEager(positionService.findPositionListPage(pageable));
    }

    /**
     * 根据ownerId分页查询职位列表
     *
     * @param ownerId  拥有者id
     * @param pageable 分页
     * @return 职位列表
     */
    @ApiOperation(value = "根据ownerId分页查询职位列表")
    @GetMapping(value = "findPositionListPageByOwnerId")
    public Result findPositionListPageByOwnerId(@RequestParam Integer ownerId, @PageableDefault(sort = {
            "id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return successEager(positionService.findPositionListPageByOwnerId(ownerId, pageable));
    }

    /**
     * 查询职位列表
     *
     * @return 职位列表
     */
    @ApiOperation(value = "查询职位列表")
    @GetMapping(value = "findPositionList")
    public Result findPositionList() {
        return success(positionService.findPositionList());
    }

    /**
     * 根据ownerId查询职位列表
     *
     * @param ownerId 拥有者id
     * @return 职位列表
     */
    @ApiOperation(value = "根据ownerId查询职位列表")
    @GetMapping(value = "findPositionListByOwnerId")
    public Result findPositionListByOwnerId(@RequestParam Integer ownerId) {
        return success(positionService.findPositionListByOwnerId(ownerId));
    }

    /**
     * 新增职位
     *
     * @param position 职位
     * @return 是否成功
     */
    @ApiOperation(value = "新增职位")
    @PostMapping(value = "savePosition")
    public Result savePosition(@RequestBody Position position) {
        positionService.savePosition(position);
        return success();
    }

    /**
     * 根据拥有者id新增职位
     *
     * @param position 职位
     * @return 是否成功
     */
    @ApiOperation(value = "根据拥有者id新增职位")
    @PostMapping(value = "savePositionByOwnerId")
    public Result savePositionByOwnerId(@RequestBody Position position) {
        positionService.savePositionByOwnerId(position);
        return success();
    }

    /**
     * 删除职位
     *
     * @param ids 职位ID数组
     * @return 是否成功
     */
    @ApiOperation(value = "删除职位")
    @PostMapping(value = "updatePositionIsDel")
    public Result updatePositionIsDel(@RequestBody Integer[] ids) {
        positionService.updatePositionIsDel(ids);
        return success();
    }

    /**
     * 根据职位id查找职位
     *
     * @param id 职位id
     * @return 职位数据
     */
    @ApiOperation(value = "根据职位id查找职位")
    @GetMapping(value = "findById")
    public Result findById(@RequestParam Integer id) {
        return success(positionService.findById(id));
    }

    /**
     * 修改职位
     *
     * @param position 职位
     * @return 是否成功
     */
    @ApiOperation(value = "修改职位")
    @PostMapping(value = "updatePosition")
    public Result updatePosition(@RequestBody Position position) {
        positionService.updatePosition(position);
        return success();
    }

    /**
     * 绑定职位角色
     *
     * @param id      职位id
     * @param roleIds 角色id数组
     * @return 是否成功
     */
    @ApiOperation(value = "绑定职位角色")
    @PostMapping(value = "bindPositionAndRole")
    public Result bindPositionAndRole(@RequestParam Integer id, @RequestBody Integer[] roleIds) {
        positionService.bindPositionAndRole(id, roleIds);
        return success();
    }

    /**
     * 解绑职位角色
     *
     * @param id      职位id
     * @param roleIds 角色id数组
     * @return 是否成功
     */
    @ApiOperation(value = "解绑职位和角色")
    @PostMapping(value = "unBindPositionAndRole")
    public Result unBindPositionAndRole(@RequestParam Integer id, @RequestBody Integer[] roleIds) {
        positionService.unBindPositionAndRole(id, roleIds);
        return success();
    }

    /**
     * 开启或禁用职位
     *
     * @param ids      职位id数组
     * @param isActive 开启或禁用状态
     * @return 是否成功
     */
    @ApiOperation(value = "开启或禁用职位")
    @PostMapping(value = "changePositionActive")
    public Result changePositionActive(@RequestBody Integer[] ids, @RequestParam Boolean isActive) {
        positionService.changePositionActive(ids, isActive);
        return success();
    }

}
