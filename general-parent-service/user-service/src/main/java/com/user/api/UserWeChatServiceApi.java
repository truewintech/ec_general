/*
 * Copy Right@
 */
package com.user.api;

import com.framework.constant.HttpResponse;
import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.user.service.UserWeChatService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("userWeChat")
@Api(value = "微信用户API")
@Slf4j
public class UserWeChatServiceApi extends BaseApi {

    /**
     * 用户微信服务
     */
    @Autowired
    private UserWeChatService userWeChatService;

    /**
     * 依据微信code获取账号信息
     *
     * @param code 微信code
     * @return 账号信息
     */
    @ApiOperation(value = "依据微信code获取账号信息")
    @GetMapping("getUserByCode")
    public Result getUserByCode(@RequestParam String code) {
        return success(userWeChatService.getUserByCode(code));
    }

    /**
     * 依据微信小程序code获取用户相关信息
     *
     * @param code 微信code
     * @return 账号信息
     */
    @ApiOperation(value = "依据微信小程序code获取用户相关信息")
    @GetMapping("getMiniappInfoByCode")
    public Result getMiniappInfoByCode(@RequestParam String code) {
        return success(userWeChatService.getMiniappInfoByCode(code));
    }

    /**
     * 事件推送处理
     *
     * @param request  请求
     * @param response 响应
     */
    @RequestMapping(value = "/handleEvent", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void handleEvent(HttpServletRequest request, HttpServletResponse response) {
        userWeChatService.handleEvent(request, response);
    }

    /**
     * 绑定客户手机号
     *
     * @param user 客户数据
     * @return 是否成功
     */
    @ApiOperation(value = "绑定客户手机号")
    @PostMapping(value = "bindMobile")
    public Result bindMobile(@RequestBody Map<String, Object> user) {
        return success(userWeChatService.bindMobile(user));
    }

    /**
     * 依据用户ID获取微信信息
     *
     * @param userId 用户ID
     * @return 微信信息
     */
    @ApiOperation("依据用户ID获取微信信息")
    @GetMapping("getWeChatInfoByUserId")
    public Result getWeChatInfoByUserId(@RequestParam Integer userId) {
        return success(userWeChatService.getWeChatInfoByUserId(userId));
    }

    /**
     * 依据appId和token,插入已关注用户信息
     *
     * @param token 微信accessToken
     * @return 成功标记
     */
    @ApiOperation("依据appId和token,插入已关注用户信息")
    @GetMapping("saveSubscribedWechatUser")
    public Result saveSubscribedWechatUser(@RequestParam String token) {
        userWeChatService.saveSubscribedWechatUser(token);
        return success(HttpResponse.SUCCESS);
    }

    /**
     * 微信网页登录  生成user  wechat buyer信息
     * TODO 暂时不使用
     *
     * @param code
     * @return token
     */
    @ApiOperation("微信网页登录")
    @GetMapping("wechatLogin")
    public Result wechatLogin(@RequestParam String code) {
        return success(userWeChatService.wechatLogin(code));
    }

    /**
     * 微信网页登录   生成wechat信息
     *
     * @param code 微信code
     * @return
     */
    @ApiOperation("微信网页登录")
    @GetMapping("wechatLoginByCode")
    public Result wechatLoginByCode(@RequestParam String code) {
        return success(userWeChatService.wechatLoginByCode(code));
    }


    /**
     * 微信网页用户绑定手机号
     *
     * @return
     */
    @ApiOperation("绑定手机号")
    @PostMapping("wechatBindMobile")
    public Result wechatBindMobile(@RequestBody Map<String, Object> userMap) {
        return success(userWeChatService.wechatBindMobile(userMap));
    }


    /**
     * 获取AccessToken
     *
     * @return
     */
    @ApiOperation("获取AccessToken")
    @GetMapping("getAccessToken")
    public Result getAccessToken() {
        return success(userWeChatService.getAccessToken());
    }


}
