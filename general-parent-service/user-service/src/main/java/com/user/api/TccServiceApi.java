/*
 * Copy Right@
 */
package com.user.api;

import com.framework.entity.TccEvent;
import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.user.service.TccService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("tcc")
@Api(value = "用户服务tcc事务api")
public class TccServiceApi extends BaseApi {

    /**
     * tcc服务
     */
    @Autowired
    private TccService tccService;

    /**
     * 确认tcc事务
     *
     * @param tccEvent 事件
     * @return 是否成功
     */
    @ApiOperation(value = "确认tcc事务")
    @PostMapping(value = "confirmTcc")
    public Result confirmTcc(@RequestBody TccEvent tccEvent) {
        tccService.confirmTcc(tccEvent);
        return success();
    }

    /**
     * 取消tcc事务
     *
     * @param tccEvent 事件
     * @return 是否成功
     */
    @ApiOperation(value = "取消tcc事务")
    @PostMapping(value = "cancelTcc")
    public Result cancelTcc(@RequestBody TccEvent tccEvent) {
        tccService.cancelTcc(tccEvent);
        return success();
    }

}
