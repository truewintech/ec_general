package com.user.api;

import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.user.entity.Role;
import com.user.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("role")
@Api(value = "角色api")
public class RoleServiceApi extends BaseApi {

    /**
     * 角色服务
     */
    @Autowired
    private RoleService roleService;

    /**
     * 增加角色
     *
     * @param role   角色
     * @param result 校验
     * @return 是否成功
     */
    @ApiOperation(value = "增加角色")
    @PostMapping(value = "addRole")
    public Result addRole(@RequestBody Role role, BindingResult result) {
        if (result.hasErrors()) {
            return validError(result);
        }
        roleService.saveRole(role);
        return success();
    }

    /**
     * 根据拥有者id增加角色
     *
     * @param role   角色
     * @param result 校验
     * @return 是否成功
     */
    @ApiOperation(value = "根据拥有者id增加角色")
    @PostMapping(value = "addRoleByOwnerId")
    public Result addRoleByOwnerId(@RequestBody Role role, BindingResult result) {
        if (result.hasErrors()) {
            return validError(result);
        }
        roleService.addRoleByOwnerId(role);
        return success();
    }

    /**
     * 删除角色
     *
     * @param ids 角色id的数组
     * @return 是否成功
     */
    @ApiOperation(value = "删除角色")
    @PostMapping(value = "deleteRole")
    public Result deleteRole(@RequestBody Integer[] ids) {
        roleService.deleteRole(ids);
        return success();
    }

    /**
     * 根据id回显操作
     *
     * @param id 角色id
     * @return 回显的数据
     */
    @ApiOperation(value = "根据id回显操作")
    @GetMapping(value = "findById")
    public Result findById(Integer id) {
        Role roleBack = roleService.findById(id);
        return successEager(roleBack);
    }

    /**
     * 修改角色
     *
     * @param role 角色
     * @return 是否成功
     */
    @ApiOperation(value = "修改角色")
    @PostMapping(value = "updateRole")
    public Result updateRole(@RequestBody Role role) {
        roleService.update(role);
        return success();
    }

    /**
     * 分页查询角色列表
     *
     * @param pageable 分页
     * @return 角色列表
     */
    @ApiOperation(value = "分页查询角色列表")
    @GetMapping(value = "findRoleListByPage")
    public Result findRoleListByPage(@PageableDefault(sort = {
            "id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return success(roleService.findRoleListByPage(pageable));
    }

    /**
     * 根据拥有者id分页查询角色列表
     *
     * @param ownerId  拥有者id
     * @param pageable 分页
     * @return 角色列表
     */
    @ApiOperation(value = "根据拥有者id分页查询角色列表")
    @GetMapping(value = "findRolePageByOwnerId")
    public Result findRolePageByOwnerId(@RequestParam Integer ownerId, @PageableDefault(sort = {
            "id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return success(roleService.findRolePageByOwnerId(ownerId, pageable));
    }

    /**
     * 根据拥有者id查询角色列表
     *
     * @param ownerId 拥有者id
     * @return 角色列表
     */
    @ApiOperation(value = "查询角色列表")
    @GetMapping(value = "findRoleListByOwnerId")
    public Result findRoleListByOwnerId(@RequestParam Integer ownerId) {
        return success(roleService.findRoleListByOwnerId(ownerId));
    }

    /**
     * 查询角色列表
     *
     * @return 角色列表
     */
    @ApiOperation(value = "查询角色列表")
    @GetMapping(value = "findRoleList")
    public Result findRoleListByOwnerId() {
        return success(roleService.findRoleList());
    }

    /**
     * 开启或禁用角色
     *
     * @param ids      角色
     * @param isActive 开启或禁用状态
     * @return 是否成功
     */
    @ApiOperation(value = "开启或禁用角色")
    @PostMapping(value = "changeRoleActive")
    public Result changeRoleActive(@RequestBody Integer[] ids, @RequestParam Boolean isActive) {
        roleService.changeRoleActive(ids, isActive);
        return success();
    }

    /**
     * 绑定角色和权限
     *
     * @param id          角色id
     * @param type        类型
     * @param resourceIds 权限id数组
     * @return 是否成功
     */
    @ApiOperation(value = "绑定角色和权限")
    @PostMapping(value = "bindRoleAndResource")
    public Result bindRoleAndResource(@RequestParam Integer id, @RequestParam String type
            , @RequestBody Integer[] resourceIds) {
        roleService.bindRoleAndResource(id, type, resourceIds);
        return success();
    }

    /**
     * 解绑角色和按钮
     *
     * @param id          角色id
     * @param resourceIds 权限id数组
     * @return 是否成功
     */
    @ApiOperation(value = "解绑角色和按钮")
    @PostMapping(value = "unbindRoleAndBtn")
    public Result unbindRoleAndBtn(@RequestParam Integer id
            , @RequestBody Integer[] resourceIds) {
        roleService.unbindRoleAndBtn(id, resourceIds);
        return success();
    }

    /**
     * 根据角色查询button权限并分页
     *
     * @param id 角色id
     * @return 权限列表
     */
    @ApiOperation(value = "根据角色查询button权限并分页")
    @GetMapping(value = "findBtnByRoleIdAndPage")
    public Result findBtnByRoleIdAndPage(@RequestParam Integer id, @PageableDefault(sort = {
            "id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return success(roleService.findBtnByRoleIdAndPage(id, pageable));
    }

    /**
     * 根据角色id查询权限列表
     *
     * @param id   角色id
     * @param type 权限类型
     * @return 权限列表
     */
    @ApiOperation(value = "根据角色id查询权限列表")
    @GetMapping(value = "findResourceByRoleId")
    public Result findResourceByRoleId(@RequestParam Integer id, @RequestParam String type) {
        return success(roleService.findResourceByRoleId(id, type));
    }

    /**
     * 未绑定用户的角色
     *
     * @param id 用户id
     * @return 角色列表
     */
    @ApiOperation(value = "未绑定用户的角色")
    @PostMapping(value = "findUnBindRoleListByUser")
    public Result findUnBindRoleListByUser(@RequestParam Integer id, @PageableDefault(sort = {
            "id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return success(roleService.findUnBindRoleListByUser(id, pageable));
    }

    /**
     * 用户已经绑定了的角色
     *
     * @param id 用户id
     * @return 角色列表
     */
    @ApiOperation(value = "用户已经绑定了的角色")
    @PostMapping(value = "findRoleListByUser")
    public Result findRoleListByUser(@RequestParam Integer id, @PageableDefault(sort = {
            "id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return success(roleService.findRoleListByUser(id, pageable));
    }
}
