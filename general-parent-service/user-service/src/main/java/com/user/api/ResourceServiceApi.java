/*
 * Copy Right@
 */
package com.user.api;

import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.framework.util.UserUtil;
import com.user.entity.Resource;
import com.user.service.ResourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

/**
 * <pre>
 * 资源接口
 *
 *
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("resource")
@Api(value = "资源API")
public class ResourceServiceApi extends BaseApi {

    /**
     * 资源服务
     */
    @Autowired
    private ResourceService resourceService;

    /**
     * 查询所有菜单
     *
     * @return 菜单列表
     */
    @ApiOperation(value = "查询所有菜单")
    @GetMapping(value = "findAllMenus")
    public Result findAllMenus() {
        return successEager(resourceService.findAllMenus());
    }

    /**
     * 根据类型查询所有资源
     *
     * @param range 范围
     * @return 菜单列表
     */
    @ApiOperation(value = "根据类型查询所有资源")
    @GetMapping(value = "findAllMenusByRange")
    public Result findAllMenusByRange(@RequestParam String range) {
        return successEager(resourceService.findAllMenusByRange(range));
    }

    /**
     * 查找用户菜单
     *
     * @param userId 用户id
     * @return 菜单列表
     */
    @ApiOperation(value = "查找用户菜单")
    @GetMapping(value = "findUserMenus")
    public Result findUserMenus(@RequestParam Integer userId) {
        return successEager(resourceService.findUserMenus(userId));
    }

    /**
     * 查询角色绑定的资源
     *
     * @param roleId 用户id
     * @return 资源id列表
     */
    @ApiOperation(value = "查询角色绑定的资源")
    @GetMapping(value = "findBindMenus")
    public Result findBindMenus(@RequestParam Integer roleId) {
        return success(resourceService.findBindMenus(roleId));
    }

    /**
     * 查询button权限并分页
     *
     * @param pageable 分页
     * @return 分页数据
     */
    @ApiOperation(value = "查询button权限并分页")
    @GetMapping(value = "findBtnByPage")
    public Result findBtnByPage(@PageableDefault(sort = {
            "sort"}, direction = Sort.Direction.ASC) Pageable pageable) {
        return success(resourceService.findBtnByPage(pageable));
    }

    /**
     * 查询当前用户按钮权限
     *
     * @return 按钮权限
     */
    @ApiOperation(value = "查询当前用户按钮权限")
    @GetMapping(value = "findUserBtn")
    public Result findUserBtn() {
        Integer userId = UserUtil.getCurrentUserId();
        return success(resourceService.findUserBtn(userId));
    }

    /**
     * 查询角色绑定的按钮
     *
     * @return 按钮权限
     */
    @ApiOperation(value = "查询角色绑定的按钮")
    @GetMapping(value = "findBindBtn")
    public Result findBindBtn(@RequestParam Integer roleId, @PageableDefault(sort = {
            "sort"}, direction = Sort.Direction.ASC) Pageable pageable) {
        return success(resourceService.findBindBtn(roleId, pageable));
    }

    /**
     * 查询角色未绑定的按钮
     *
     * @return 按钮权限
     */
    @ApiOperation(value = "查询角色未绑定的按钮")
    @GetMapping(value = "findUnbindBtn")
    public Result findUnbindBtn(@RequestParam Integer roleId, @PageableDefault(sort = {
            "sort"}, direction = Sort.Direction.ASC) Pageable pageable) {
        return success(resourceService.findUnbindBtn(roleId, pageable));
    }

    /**
     * 新增权限
     *
     * @param resource 权限
     * @return 是否成功
     */
    @ApiOperation(value = "新增权限")
    @PostMapping(value = "saveResource")
    public Result saveResource(@RequestBody Resource resource) {
        resourceService.saveResource(resource);
        return success();
    }

    /**
     * 删除权限
     *
     * @param ids 权限id数组
     * @return 是否成功
     */
    @ApiOperation(value = "删除权限")
    @PostMapping(value = "deleteResource")
    public Result deleteResource(@RequestBody Integer[] ids) {
        resourceService.deleteResource(ids);
        return success();
    }

    /**
     * 更新权限
     *
     * @param resource 权限
     * @return 是否成功
     */
    @ApiOperation(value = "更新权限")
    @PostMapping(value = "updateResource")
    public Result updateResource(@RequestBody Resource resource) {
        resourceService.updateResource(resource);
        return success();
    }

    /**
     * 根据权限id查询权限
     *
     * @param id 权限id
     * @return 权限
     */
    @ApiOperation(value = "根据权限id查询权限")
    @GetMapping(value = "findById")
    public Result findById(@RequestParam Integer id) {
        return success(resourceService.findById(id));
    }

    /**
     * 启用禁用权限
     *
     * @param ids 权限id数组
     * @return 是否成功
     */
    @ApiOperation(value = "启用禁用权限")
    @GetMapping(value = "changeResourceActive")
    public Result changeResourceActive(@RequestBody Integer[] ids) {
        resourceService.changeResourceActive(ids);
        return success();
    }
}
