/*
 * Copy Right@
 */
package com.user.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.framework.constant.HttpResponse;
import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.framework.util.UserUtil;
import com.framework.validator.group.Insert;
import com.user.entity.User;
import com.user.service.TccService;
import com.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("user")
@Api(value = "用户API")
@Slf4j
public class UserServiceApi extends BaseApi {

    /**
     * 用户服务
     */
    @Autowired
    private UserService userService;

    /**
     * tcc服务
     */
    @Autowired
    private TccService tccService;

    /**
     * 根据账号查询用户
     *
     * @param account 用户账号
     * @return 用户数据
     */
    @ApiOperation(value = "根据账号查询用户")
    @GetMapping("findByAccount")
    public Result findByAccount(@RequestParam String account) {
        return successEager(userService.findByAccount(account));
    }

    /**
     * 根据账号查询用户（包括密码信息）
     *
     * @param account 用户账号
     * @return 用户数据
     */
    @ApiOperation(value = "根据账号查询用户密码信息")
    @GetMapping("findByAccountForLogin")
    public Result findByAccountForLogin(@RequestParam String account) {
        User user = userService.findByAccount(account);
        if (user == null) {
            return success();
        }
        return success(user.toLoginUser());
    }

    /**
     * 校验用户是否已登录
     *
     * @return 是否登录结果
     */
    @ApiOperation(value = "校验是否已登录")
    @GetMapping("isLogin")
    public Result isLogin() {
        boolean isLogin = UserUtil.getNullableCurrentUserId() != null;
        return success(isLogin);
    }

    /**
     * 根据ID查询用户
     *
     * @param id 用户id
     * @return 用户数据
     */
    @ApiOperation(value = "根据ID查询用户")
    @GetMapping("findById")
    public Result findById(@RequestParam Integer id) {
        return success(userService.findById(id));
    }

    /**
     * 根据ID查询系统用户密码信息
     *
     * @param id 用户id
     * @return 用户数据
     */
    @ApiOperation(value = "根据ID查询系统用户密码信息")
    @GetMapping("findByIdForLogin")
    public Result findByIdForLogin(@RequestParam Integer id) {
        User user = userService.findById(id);
        if (user == null) {
            return success();
        }
        return success(user.toLoginUser());
    }

    /**
     * 依据手机号查询账户
     *
     * @param mobile 手机号
     * @return 账户数据
     */
    @ApiOperation(value = "依据手机号查询账户")
    @GetMapping("findByMobile")
    public Result findByMobile(@RequestParam String mobile) {
        return success(userService.findByMobile(mobile));
    }

    /**
     * 依据手机号查询账户
     *
     * @param mobile 手机号
     * @return 账户数据
     */
    @ApiOperation(value = "依据手机号查询账户密码信息")
    @GetMapping("findByMobileForLogin")
    public Result findByMobileForLogin(@RequestParam String mobile) {
        User user = userService.findByMobile(mobile);
        if (user == null) {
            return success();
        }
        return success(user.toLoginUser());
    }

    /**
     * 根据手机号查询用户是否存在
     *
     * @return 用户是否存在
     */
    @ApiOperation(value = "根据手机号查询账户是否存在")
    @GetMapping(value = "view/isUserExistByMobile")
    public Result isUserExistByMobile(@RequestParam String mobile) {
        return success(userService.isUserExistByMobile(mobile));
    }

    /**
     * 分页查询用户
     *
     * @param account  账号
     * @param pageable 分页
     * @return 分页数据
     */
    @ApiOperation(value = "分页查询用户")
    @GetMapping(value = "findUserByPage")
    public Result findUserByPage(String account, @PageableDefault(sort = {
            "id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return successEager(userService.findUserByPage(account, pageable));
    }

    /**
     * 根据ownerId分页查询用户
     *
     * @param ownerId  拥有者id
     * @param pageable 分页
     * @return 分页数据
     */
    @ApiOperation(value = "根据ownerId分页查询用户")
    @GetMapping(value = "findUserPageByOwnerId")
    public Result findUserPageByOwnerId(@RequestParam Integer ownerId, @PageableDefault(sort = {
            "id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return successEager(userService.findUserPageByOwnerId(ownerId, pageable));
    }

    /**
     * 新增用户
     *
     * @param user   用户
     * @param result 参数验证
     * @return 是否新增成功
     */
    @ApiOperation(value = "新增用户")
    @PostMapping(value = "addUser")
    public Result addUser(@Validated({Insert.class})
                          @RequestBody User user, BindingResult result) {
        if (result.hasErrors()) {
            return validError(result);
        }
        userService.addUser(user);
        return success();
    }

    /**
     * 新增系统用户
     *
     * @param user   用户
     * @param result 参数验证
     * @return 是否新增成功
     */
    @ApiOperation(value = "新增系统用户")
    @PostMapping(value = "addSystemUser")
    public Result addSystemUser(@Validated({Insert.class})
                                @RequestBody User user, BindingResult result) {
        if (result.hasErrors()) {
            return validError(result);
        }
        user.setIsSystem(true);
        userService.addUser(user);
        return success();
    }

    /**
     * 尝试新增用户
     *
     * @param userJson 用户Json
     * @param result   参数验证
     * @return TccEvent
     */
    @ApiOperation(value = "新增用户")
    @PostMapping(value = "tryAddUser")
    public Result tryAddUser(@RequestBody String userJson, BindingResult result) {
        if (result.hasErrors()) {
            return validError(result);
        }
        return success(tccService.tryAddUser(userJson));
    }

    /**
     * 尝试修改用户
     *
     * @param userJson 用户Json
     * @param result   参数验证
     * @return TccEvent
     */
    @ApiOperation(value = "尝试修改用户")
    @PostMapping(value = "tryUpdateUser")
    public Result tryUpdateUser(@RequestBody String userJson, BindingResult result) {
        if (result.hasErrors()) {
            return validError(result);
        }
        return success(tccService.tryUpdateUser(JSON.toJavaObject(JSONObject.parseObject(userJson), User.class)));
    }

    /**
     * 批量操作用户是否删除
     *
     * @param ids      用户主键列表，
     * @param isDelete 是否删除标识
     * @return 是否成功
     */
    @ApiOperation(value = "批量操作用户是否删除")
    @PostMapping("updateUserIsDelete")
    public Result updateUserIsDelete(@RequestBody Integer[] ids, @RequestParam Boolean isDelete) {
        userService.updateUserIsDelete(ids, isDelete);
        return success();
    }

    /**
     * 批量操作用户开启禁用
     *
     * @param ids      用户主键列表，
     * @param isActive 开启禁用标识
     * @return 是否成功
     */
    @ApiOperation(value = "批量操作用户开启禁用")
    @PostMapping("updateUserIsActive")
    public Result updateUserIsActive(@RequestBody Integer[] ids, @RequestParam Boolean isActive) {
        userService.updateUserIsActive(ids, isActive);
        return success();
    }

    /**
     * 修改密码
     *
     * @param map 参数（原始密码,新密码）
     * @return 是否修改成功
     */
    @ApiOperation(value = "修改密码")
    @PostMapping(value = "updatePassword")
    public Result updatePassword(@RequestBody Map<String, String> map) {
        userService.updatePassword(map.get("oldPassword"), map.get("newPassword"));
        return success();
    }

    /**
     * 绑定用户和角色
     *
     * @param id      用户id
     * @param roleIds 角色id数组
     * @return 是否成功
     */
    @ApiOperation(value = "绑定用户和角色")
    @PostMapping(value = "bindUserAndRole")
    public Result bindUserAndRole(@RequestParam Integer id, @RequestBody Integer[] roleIds) {
        userService.bindUserAndRole(id, roleIds);
        return success();
    }

    /**
     * 解绑用户和角色
     *
     * @param userId  用户id
     * @param roleIds 角色ids
     * @return 是否成功
     */
    @ApiOperation(value = "解绑用户和角色")
    @PostMapping(value = "unBindRoleAndUser")
    public Result unBindRoleAndUser(@RequestParam Integer userId, @RequestBody Integer[] roleIds) {
        userService.unBindRoleAndUser(userId, roleIds);
        return success();
    }

    /**
     * 绑定用户和职位
     *
     * @param userId      用户id
     * @param positionIds 职位id数组
     * @return 是否成功
     */
    @ApiOperation(value = "绑定用户和职位")
    @PostMapping(value = "bindUserAndPosition")
    public Result bindUserAndPosition(@RequestParam Integer userId, @RequestBody Integer[] positionIds) {
        userService.bindUserAndPosition(userId, positionIds);
        return success();
    }

    /**
     * 解绑用户和职位
     *
     * @param userId      用户id
     * @param positionIds 职位数组
     * @return 是否成功
     */
    @ApiOperation(value = "解绑用户和职位")
    @PostMapping(value = "unBindUserAndPosition")
    public Result unBindUserAndPosition(@RequestParam Integer userId, @RequestBody Integer[] positionIds) {
        userService.unBindUserAndPosition(userId, positionIds);
        return success();
    }

    /**
     * 修改验证手机号
     *
     * @param oldMobile    老手机号
     * @param newMobile    新手机号
     * @param validateCode 短信验证码
     */
    @ApiOperation(value = "修改验证手机号")
    @GetMapping(value = "updateMobile")
    public Result updateMobile(@RequestParam String oldMobile, @RequestParam String newMobile, @RequestParam String validateCode) {
        userService.updateMobile(oldMobile, newMobile, validateCode);
        return success();
    }

    /**
     * 忘记密码
     *
     * @param mobile 已验证手机号
     * @return 是否验证通过
     */
    @ApiOperation(value = "忘记密码")
    @GetMapping(value = "view/forgetPassword")
    public Result forgetPassword(@RequestParam String mobile, @RequestParam String validateCode, @RequestParam String newPassword) {
        userService.forgetPassword(mobile, validateCode, newPassword);
        return success();
    }

    /**
     * 校验忘记密码短信验证码是否正确
     *
     * @param mobile       已验证手机号
     * @param validateCode 忘记密码短信验证码
     * @return 是否正确
     */
    @ApiOperation(value = "校验忘记密码短信验证码是否正确")
    @GetMapping(value = "view/checkForgetPasswordValidateCode")
    public Result checkForgetPasswordValidateCode(@RequestParam String mobile, @RequestParam String validateCode) {
        userService.checkForgetPasswordValidateCode(mobile, validateCode);
        return success();
    }

    /**
     * 查询当前用户信息及上次登录时间
     *
     * @return 当前用户信息及上次登录时间
     */
    @ApiOperation(value = "查询当前用户信息及上次登录时间")
    @GetMapping(value = "getCurrentUserInfoAndLastLoginTime")
    public Result getCurrentUserInfoAndLastLoginTime() {
        return success(userService.getCurrentUserInfoAndLastLoginTime());
    }

    /**
     * 根据ID查询用户子账号及其职位
     *
     * @param pageable 分页条件
     * @param userId   用户ID
     * @return 用户子账号及其职位
     */
    @ApiOperation(value = "根据ID查询用户子账号")
    @PostMapping(value = "findSubAccountByUserId")
    public Result findSubAccountByUserId(@RequestParam Integer userId, @PageableDefault(sort = {
            "id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return successEager(userService.findSubAccountByUserId(userId, pageable));
    }

    /**
     * 系统账户登录
     *
     * @return 登录系统管理员
     */
    @ApiOperation("系统账户登录")
    @PostMapping(value = "systemLogin")
    public Result login() {
        Boolean isSystem = userService.login();
        return success(isSystem);

    }

    /**
     * 门店注册，创建user用户
     *
     * @param jsonObject 门店注册参数
     * @return 是否成功
     */
    @ApiOperation("门店注册,创建user用户")
    @PostMapping(value = "createUser")
    public Result createUser(@RequestBody JSONObject jsonObject) {
        return success(userService.createUser(jsonObject));
    }

}
