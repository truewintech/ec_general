/*
 * Copy Right@
 */
package com.user.api;

import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.framework.util.ServletUtil;
import com.user.amqp.Sender;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("loginLog")
@Api(value = "登录日志服务API")
@Slf4j
public class LoginLogServiceApi extends BaseApi {

    /**
     * 发送工具
     */
    @Autowired
    private Sender sender;

    /**
     * 新增一个登录日志
     *
     * @param userId  用户id
     * @param account 账号
     * @param request http请求
     * @return 是否成功
     */
    @ApiOperation(value = "新增一个登录日志")
    @GetMapping("addLoginLog")
    public Result addLoginLog(@RequestParam Integer userId, @RequestParam String account
            , HttpServletRequest request) {
        String ip = ServletUtil.getRealIp(request);
        Map<String, String> loginLog = new HashMap<>(3);
        loginLog.put("account", account);
        loginLog.put("accountId", String.valueOf(userId));
        loginLog.put("ip", ip);

        sender.addLoginLog(loginLog);
        return success();
    }

}
