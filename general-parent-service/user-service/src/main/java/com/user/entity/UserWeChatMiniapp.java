/*
 * Copy Right@
 */
package com.user.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Entity
@Table(name = "user_wechat_miniapp")
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Getter
@Setter
public class UserWeChatMiniapp {

    /**
     * 主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 用戶
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    /**
     * 小程序用户标识
     */
    @Column(name = "open_id")
    private String openId;

    /**
     * 唯一标识
     */
    @Column(name = "union_id")
    private String unionId;

    /**
     * 小程序标识
     */
    @Column(name = "app_id")
    private String appId;

}
