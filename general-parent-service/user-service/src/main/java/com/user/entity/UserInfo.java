/*
 * Copy Right@
 */
package com.user.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * <pre>
 *     用于卖家新增人员时提交数据
 * </pre>
 *
 * @author dengyalong
 * @date 2017年12月29日
 */
@Setter
@Getter
public class UserInfo {

    /**
     * 账号id
     */
    private Integer id;

    /**
     * 账号
     */
    @NotBlank(message = "账号不能为空")
    @Length(min = 1, max = 32, message = "名称的长度只能在1-32之内")
    private String account;

    /**
     * 密码
     */
    @Pattern(regexp = "^.{6,20}|\\s*$", message = "密码的长度只能在6-20之内")
    private String password;

    /**
     * 店铺id集合
     */
    private List<Integer> shopIds;

    /**
     * 职位id
     */
    private Integer positionId;

}
