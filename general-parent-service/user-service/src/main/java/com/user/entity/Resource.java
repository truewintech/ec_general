/*
 * Copy Right@
 */
package com.user.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.common.constant.Const;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.framework.entity.IsActiveAndIsDeleteEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Entity
@Table(name = "resource")
@DynamicUpdate
@DynamicInsert
@Getter
@Setter
@Where(clause = Const.IsDel.CLAUSE)
public class Resource extends IsActiveAndIsDeleteEntity {

    /**
     * 主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 父ID
     */
    @Column(name = "parent_id", insertable = false, updatable = false)
    private Integer parentId;

    /**
     * 父
     */
    @ManyToOne
    @JoinColumn(name = "parent_id")
    @JSONField(serialize = false)
    @JsonIgnore
    private Resource parent;

    /**
     * 类型
     */
    @Column(name = "type")
    private String type;

    /**
     * 使用范围：SYSTEM，SELLER_CENTER
     */
    @Column(name = "`range`")
    private String range;

    /**
     * 使用范围名称：管理后台，商家中心
     */
    @Column(name = "range_name")
    private String rangeName;

    /**
     * 图标
     */
    @Column(name = "icon")
    private String icon;

    /**
     * 资源名称
     */
    @Column(name = "name")
    private String name;

    /**
     * 权限标识（父name|子name）
     */
    @Column(name = "code")
    private String code;

    /**
     * 链接
     */
    @Column(name = "link")
    private String link;

    /**
     * 排序
     */
    @Column(name = "sort")
    private Integer sort;

    /**
     * 角色集合
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @ManyToMany(targetEntity = Role.class)
    @JoinTable(name = "role_resource", joinColumns = {@JoinColumn(name = "resource_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> roles;

    /**
     * 子资源
     */
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "parent")
    @OrderBy("sort")
    @Where(clause = Const.IsDel.CLAUSE)
    private List<Resource> children;

    /**
     * 是否绑定
     */
    @Transient
    private boolean hasBind;

}
