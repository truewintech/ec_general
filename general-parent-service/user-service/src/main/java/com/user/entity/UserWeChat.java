/*
 * Copy Right@
 */
package com.user.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Entity
@Table(name = "user_wechat")
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Getter
@Setter
public class UserWeChat {

    /**
     * 主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 用戶
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    /**
     * 公众号用户标识
     */
    @Column(name = "open_id")
    private String openId;

    /**
     * 唯一标识
     */
    @Column(name = "union_id")
    private String unionId;

    /**
     * 公众号标识
     */
    @Column(name = "app_id")
    private String appId;

    /**
     * 用户所在国家
     */
    @Column(name = "country")
    private String country;

    /**
     * 用户所在城市
     */
    @Column(name = "city")
    private String city;

    /**
     * 用户被打上的标签ID列表
     */
    @Column(name = "tag_id_list")
    private String tagIdList;

    /**
     * 用户的性别
     */
    @Column(name = "sex")
    private Integer sex;

    /**
     * 用户所在的分组ID
     */
    @Column(name = "group_id")
    private Integer groupId;

    /**
     * 用户的语言
     */
    @Column(name = "language")
    private String language;

    /**
     * 公众号运营者对粉丝的备注
     */
    @Column(name = "remark")
    private String remark;

    /**
     * 用户是否订阅该公众号标识
     */
    @Column(name = "subscribe")
    private Integer subscribe;

    /**
     * 用户关注时间
     */
    @Column(name = "subscribe_time")
    private Integer subscribeTime;

    /**
     * 用户所在省份
     */
    @Column(name = "province")
    private String province;

    /**
     * 用户的昵称
     */
    @Column(name = "nick_name")
    private String nickName;

    /**
     * 用户头像
     */
    @Column(name = "head_image_url")
    private String headImageUrl;


}
