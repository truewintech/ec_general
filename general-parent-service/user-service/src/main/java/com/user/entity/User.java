/*
 * Copy Right@
 */
package com.user.entity;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.common.constant.Const;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.framework.entity.IsActiveAndIsDeleteEntity;
import com.framework.validator.group.Insert;
import com.framework.validator.group.Update;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Entity
@Table(name = "user")
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Getter
@Setter
@Where(clause = Const.IsDel.CLAUSE)
public class User extends IsActiveAndIsDeleteEntity {

    /**
     * 主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 拥有者id(不是父id，可能是组织id，可能是卖家id，可能是买家id)
     */
    @Column(name = "owner_id")
    private Integer ownerId;

    /**
     * 角色集合
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @ManyToMany(targetEntity = Role.class)
    @JoinTable(name = "user_role", joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    @Where(clause = Const.IsDel.CLAUSE)
    private List<Role> roles;

    /**
     * 账号(后台生成)
     */
    @Column(name = "account", unique = true)
    private String account;

    /**
     * 手机号
     */
    @Column(name = "mobile")
    private String mobile;

    /**
     * 密码
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @Column(name = "password", nullable = false)
    @Pattern(regexp = "^.{6,20}|\\s*$", message = "密码的长度只能在6-20之内", groups = {Insert.class, Update.class})
    private String password;

    /**
     * 盐值
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @Column(name = "salt", nullable = false)
    private String salt;

    /**
     * 是否系统角色
     */
    @Column(name = "is_system")
    private Boolean isSystem = false;

    /**
     * 用户微信信息
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "user")
    private List<UserWeChat> userWeChats;

    /**
     * 职位集合
     */
    @ManyToMany(targetEntity = Position.class)
    @JoinTable(name = "user_position", joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "position_id")})
    @Where(clause = Const.IsDel.CLAUSE)
    private List<Position> positions;

    /**
     * 推荐列表
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "sharer")
    private List<Sharing> shareList;

    /**
     * 推荐列表
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "shared")
    private List<Sharing> sharedList;

    /**
     * 获取登录用户信息
     *
     * @return 登录用户信息
     */
    public JSONObject toLoginUser() {
        JSONObject user = (JSONObject) JSONObject.toJSON(this);
        user.put("password", this.getPassword());
        user.put("salt", this.getSalt());
        return user;
    }
}
