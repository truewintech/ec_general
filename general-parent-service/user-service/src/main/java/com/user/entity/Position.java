/*
 * Copy Right@
 */
package com.user.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.common.constant.Const;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.framework.entity.IsActiveAndIsDeleteEntity;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Entity
@Table(name = "position")
@DynamicUpdate
@DynamicInsert
@Getter
@Setter
@Where(clause = Const.IsDel.CLAUSE)
public class Position extends IsActiveAndIsDeleteEntity {

    /**
     * 主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 拥有者id(不是父id，可能是组织id，可能是卖家id，可能是买家id)
     */
    @Column(name = "owner_id")
    private Integer ownerId;

    /**
     * 名称
     */
    @Column(name = "name")
    private String name;

    /**
     * 用户集合
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @ManyToMany(targetEntity = User.class)
    @JoinTable(name = "user_position", joinColumns = {@JoinColumn(name = "position_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")})
    @Where(clause = Const.IsDel.CLAUSE)
    private List<User> users;

    /**
     * 角色集合
     */
    @ManyToMany(targetEntity = Role.class)
    @JoinTable(name = "position_role", joinColumns = {@JoinColumn(name = "position_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    @Where(clause = Const.IsDel.CLAUSE)
    private List<Role> roles = new ArrayList<>();

}
