/*
 * Copy Right@
 */
package com.user.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.common.constant.Const;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.framework.entity.IsActiveAndIsDeleteEntity;
import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Entity
@Table(name = "role")
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Getter
@Setter
@Where(clause = Const.IsDel.CLAUSE)
public class Role extends IsActiveAndIsDeleteEntity {

    /**
     * 主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 拥有者id(不是父id，可能是组织id，可能是卖家id，可能是买家id)
     */
    @Column(name = "owner_id")
    private Integer ownerId;

    /**
     * 角色名
     */
    @Column(name = "name", nullable = false)
    @NotBlank(message = "角色名不能为空")
    @Length(min = 1, max = 10, message = "角色名的长度只能在1-10之内")
    private String name;

    /**
     * 角色详情
     */
    @Column(name = "detail", nullable = false)
    @NotBlank(message = "角色详情不能为空")
    @Length(min = 1, max = 20, message = "角色详情的长度只能在1-20之内")
    private String detail;

    /**
     * 用户集合
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @ManyToMany(targetEntity = User.class)
    @JoinTable(name = "user_role", joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")})
    @Where(clause = Const.IsDel.CLAUSE)
    private List<User> users;

    /**
     * 资源集合
     */
    @ManyToMany(targetEntity = Resource.class)
    @JoinTable(name = "role_resource", joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = {@JoinColumn(name = "resource_id")})
    @Where(clause = Const.IsDel.CLAUSE)
    private List<Resource> resources;

    /**
     * 职位集合
     */
    @JsonIgnore
    @JSONField(serialize = false)
    @ManyToMany(targetEntity = Position.class)
    @JoinTable(name = "position_role", joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = {@JoinColumn(name = "position_id")})
    @Where(clause = Const.IsDel.CLAUSE)
    private List<Position> positions;

}
