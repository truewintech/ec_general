package com.user.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.common.constant.Const;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.framework.util.DateUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

/**
 * <pre>
 * </pre>
 *
 * @author zhanglong
 * @date 2018年07月30日
 */
@Entity
@Table(name="sharing")
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Getter
@Setter
public class Sharing {
    /**
     * 主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 推荐者ID
     */
    @Column(name = "sharer_id", nullable = false)
    private Integer sharerId;

    /**
     * 被推荐者ID
     */
    @Column(name = "be_shared_id", nullable = false)
    private Integer beSharedId;

    /**
     * 推荐层级
     */
    @Column(name = "share_Level", nullable = false)
    private Integer shareLevel;

    /**
     * 注册时间
     */
    @Column(name = "registration_time")
    private Date registrationTime;

    /**
     * 推荐人
     */
    @ManyToOne
    @JoinColumn(name = "sharer_id", insertable = false, updatable = false)
    private User sharer;

    /**
     * 被推荐人
     */
    @ManyToOne
    @JoinColumn(name = "be_shared_id", insertable = false, updatable = false)
    private User shared;

    public String getRegistrationTimeStr(){
        return DateUtil.formatDateTime(this.registrationTime);
    }
}
