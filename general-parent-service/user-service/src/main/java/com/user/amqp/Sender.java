/*
 * Copy Right@
 */
package com.user.amqp;

import com.common.amqp.AmqpConfig;
import com.user.entity.Sharing;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class Sender {

    /**
     * 消息模板
     */
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 新增登录日志
     *
     * @param loginLog 登录日志
     */
    public void addLoginLog(Map<String, String> loginLog) {
        rabbitTemplate.convertAndSend(AmqpConfig.LOG_LOGIN_QUEUE_NAME, loginLog);
    }

    /**
     * 新增推荐关系
     *
     * @param sharing 推荐关系
     */
    public void addSharing(Sharing sharing) {
        rabbitTemplate.convertAndSend(AmqpConfig.SHARING_QUEUE_NAME, sharing);
    }


}
