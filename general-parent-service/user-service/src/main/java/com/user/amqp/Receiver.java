/*
 * Copy Right@
 */
package com.user.amqp;

import com.common.amqp.AmqpConfig;
import com.common.constant.DbConst;
import com.common.util.rest.GeneralRestTemplateUtil;
import com.framework.amqp.EventReceiver;
import com.framework.entity.Event;
import com.user.entity.Sharing;
import com.user.repository.SharingRespository;
import com.user.service.RestService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class Receiver extends EventReceiver {

    /**
     * 推荐关系数据仓库
     */
    @Autowired
    private SharingRespository sharingRespository;

    /**
     * 远程服务
     */
    @Autowired
    private RestService restService;

    /**
     * 接收服务事件
     *
     * @param event 事件
     * @return 处理是否成功
     */
    @RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = DbConst.Event.SenderOrReceiver.USER_SERVICE)
    @RabbitHandler
    public boolean receiveEvent(Event event) {
        return saveEvent(event);
    }

    /**
     * 处理推荐关系事件
     * @param sharing
     * @return
     */
    @RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = AmqpConfig.SHARING_QUEUE_NAME)
    @RabbitHandler
    public void receiveSharingEvent(Sharing sharing){
        if(sharing != null){
            Date now = new Date();
            sharing.setRegistrationTime(new Date());
            sharing.setShareLevel(1);
            sharingRespository.save(sharing);
            //查询所有被分享ID为当前分享者的记录
            List<Sharing> sharings = sharingRespository.findByBeSharedIdOrderByShareLevelAsc(sharing.getSharerId());
            if(sharings != null && sharings.size() > 0){
                //说明当前的分享者是被其他人推荐的，需要添加关联的推荐层级关系
                //查询推荐关系层级数限制
                String levelLimit = restService.getSystemParameter(DbConst.SystemParameter.Id.SHARE_LEVEL_LIMIT);
                //当层级关系>=2时，建立推荐关联关系层级
                if(StringUtils.isNotEmpty(levelLimit) && Integer.valueOf(levelLimit) >= 2){
                    if(sharings.size() == Integer.valueOf(levelLimit)){
                        //当层级关系达到上限，去除最顶级的关系层级
                        sharings.remove(sharings.size()-1);
                    }
                    //添加父级推荐关系
                    List<Sharing> sharingLevelList = new ArrayList<>();
                    for(Sharing s : sharings){
                        Sharing sharing1 = new Sharing();
                        sharing1.setSharerId(s.getSharerId());
                        sharing1.setBeSharedId(sharing.getBeSharedId());
                        sharing1.setShareLevel(s.getShareLevel()+1);
                        sharing1.setRegistrationTime(now);

                        sharingLevelList.add(sharing1);
                    }

                    sharingRespository.save(sharingLevelList);
                }

            }
        }
    }
}
