/*
 * Copy Right@
 */
package com.user.repository;

import com.user.entity.Position;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author FengShiWei
 * @date 2017年09月04日
 */
public interface PositionRepository extends JpaRepository<Position, Integer>,
        JpaSpecificationExecutor<Position> {

    /**
     * 根据职位名称和拥有者id查询职位
     *
     * @param name    职位名称
     * @param ownerId 拥有者id
     * @return 职位对象
     */
    Position findByNameAndOwnerId(String name, Integer ownerId);

    /**
     * 根据职位名称查询职位
     *
     * @param name 职位名称
     * @return 职位对象
     */
    Position findByNameAndOwnerIdIsNull(String name);

    /**
     * 查询用户未绑定的职位
     *
     * @param id       用户id
     * @param pageable 分页
     * @return 职位列表
     */
    @Query("select p from Position p where p.isActive = true and not exists " +
            "(select 1 from User u join u.positions up where up.id = p.id and u.id = :id)")
    Page<Position> findUnBindPositionByUser(@Param("id") Integer id, Pageable pageable);

    /**
     * 根据职位id查询职位
     *
     * @param ids 职位id数组
     * @return 职位列表
     */
    List<Position> findByIdIn(Integer[] ids);

    /**
     * 分页查询所有职位
     *
     * @param pageable 分页
     * @return 职位
     */
    Page<Position> findByOwnerIdIsNull(Pageable pageable);

    /**
     * 查询职位集合
     *
     * @return 职位集合
     */
    List<Position> findByOwnerIdIsNull();

    /**
     * 根据拥有者id分页查询所有职位
     *
     * @param ownerId  拥有者id
     * @param pageable 分页
     * @return 职位
     */
    Page<Position> findByOwnerId(Integer ownerId, Pageable pageable);

    /**
     * 根据拥有者id查询所有职位
     *
     * @param ownerId 拥有者id
     * @return 职位集合
     */
    List<Position> findByOwnerId(Integer ownerId);


}
