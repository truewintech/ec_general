/*
 * Copy Right@
 */
package com.user.repository;

import com.user.entity.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface ResourceRepository extends JpaRepository<Resource, Integer>,
        JpaSpecificationExecutor<Resource> {

    /**
     * 通过类型查找资源
     *
     * @param type 类型
     * @return 资源
     */
    List<Resource> findAllByTypeAndParentIdIsNullOrderBySort(String type);

    /**
     * 通过类型与范围查找资源
     *
     * @param type  类型
     * @param range 范围
     * @return 资源
     */
    List<Resource> findAllByTypeAndRangeAndParentIdIsNullOrderBySort(String type, String range);

    /**
     * 分页查询权限资源
     *
     * @param type     类型
     * @param pageable 分页
     * @return 权限列表
     */
    Page<Resource> findByType(String type, Pageable pageable);

    /**
     * 通过用户id和类型查找资源
     *
     * @param userId 用户id
     * @param type   类型
     * @return 资源
     */
    @Query(value = "select r from Role role join role.users u join role.resources r " +
            "where u.id=?1 and r.type=?2")
    List<Resource> findAllByUserIdAndType(Integer userId, String type);

    /**
     * 通过用户类型查找资源
     *
     * @param type 类型
     * @return 资源
     */
    List<Resource> findAllByType(String type);

    /**
     * 通过用户id和类型查找资源
     *
     * @param userId   用户id
     * @param type     类型
     * @param pageable 分页
     * @return 资源
     */
    @Query(value = "select r from Resource r where r.type=?2 and " +
            "exists (select 1 from Role role join role.users u join role.resources rs where u.id=?1 and rs.id=r.id)")
    Page<Resource> findAllByUserIdAndType(Integer userId, String type, Pageable pageable);

    /**
     * 通过用户id和类型查找资源id
     *
     * @param userId 用户id
     * @param type   类型
     * @return 资源id
     */
    @Query(value = "select r.id from Role role join role.users u join role.resources r " +
            "where u.id=?1 and r.type=?2")
    List<Integer> findIdByUserIdAndType(Integer userId, String type);

    /**
     * 通过角色和类型查找资源id
     *
     * @param roleId 角色id
     * @param type   类型
     * @return 资源id列表
     */
    @Query(value = "select r.id from Resource r join r.roles role " +
            "where role.id=?1 and r.type=?2")
    List<Integer> findIdByRoleIdAndType(Integer roleId, String type);

    /**
     * 获取绑定的资源
     *
     * @param roleId   角色id
     * @param type     类型
     * @param pageable 分页
     * @return 绑定资源
     */
    @Query(value = "select r from Resource r join r.roles role " +
            "where role.id=?1 and r.type=?2")
    Page<Resource> findBindByRoleIdAndType(Integer roleId, String type, Pageable pageable);

    /**
     * 获取未绑定的资源
     *
     * @param roleId   角色id
     * @param type     类型
     * @param pageable 分页
     * @return 未绑定资源
     */
    @Query(value = "select r from Resource r where r.type = ?2 and " +
            "not exists (select 1 from Role role join role.resources rs where role.id=?1 and rs.id=r.id)")
    Page<Resource> findUnbindByRoleIdAndType(Integer roleId, String type, Pageable pageable);

    /**
     * 获取操作用户未绑定的资源
     *
     * @param userId   用户id
     * @param roleId   角色id
     * @param type     类型
     * @param pageable 分页
     * @return 未绑定资源
     */
    @Query(value = "select r from Resource r where r.type = ?3 and " +
            "not exists (select 1 from Role role join role.resources rs where role.id=?2 and rs.id=r.id) and " +
            "exists (select 1 from Role role join role.users u join role.resources rs where u.id=?1 and rs.id=r.id)")
    Page<Resource> findUnbindByUserRoleAndType(Integer userId, Integer roleId, String type, Pageable pageable);

    /**
     * 根据权限id查询权限资源
     *
     * @param type 类型
     * @param ids  权限id数组
     * @return 权限集合
     */
    List<Resource> findByTypeAndIdIn(String type, Integer[] ids);

    /**
     * 根据权限id查询权限资源id
     *
     * @param type 类型
     * @param ids  权限id数组
     * @return 权限集合
     */
    @Query("select r.id from Resource r where r.type=?1 and id in ?2")
    List<Integer> findIdByTypeAndIdIn(String type, Integer[] ids);

    /**
     * 根据角色id查询按钮权限并分页
     *
     * @param id       角色id
     * @param type     按钮类型
     * @param pageable 分页
     * @return 按钮权限列表
     */
    @Query("select rr from Role r join r.resources rr where r.id = :id and r.isActive = true and rr.type = :type")
    Page<Resource> findByRoleIdAndType(@Param("id") Integer id, @Param("type") String type, Pageable pageable);

    /**
     * 根据角色id和类型查询权限
     *
     * @param id   角色id
     * @param type 类型
     * @return 权限列表
     */
    @Query("select re from Role r join r.resources re where r.id = :id and re.type = :type")
    List<Resource> findResourceByRoleIdAndType(@Param("id") Integer id, @Param("type") String type);
}