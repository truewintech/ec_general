package com.user.repository;

import com.user.entity.Sharing;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author zhanglong
 * @date 2018年07月30日
 */
public interface SharingRespository extends JpaRepository<Sharing,Integer>,JpaSpecificationExecutor<Sharing>{

    List<Sharing> findByBeSharedIdOrderByShareLevelAsc(Integer beSharedId);


}
