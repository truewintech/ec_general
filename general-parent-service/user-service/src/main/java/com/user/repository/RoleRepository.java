/*
 * Copy Right@
 */
package com.user.repository;

import com.user.entity.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface RoleRepository
        extends JpaRepository<Role, Integer>, JpaSpecificationExecutor<Role> {

    /**
     * 根据id查询角色
     *
     * @param id 角色id
     * @return 角色对象
     */
    Role findById(Integer id);

    /**
     * 查询角色
     *
     * @param ids 角色id数组
     * @return 角色集合
     */
    List<Role> findByIdIn(Integer[] ids);

    /**
     * 查询用户未绑定的角色
     *
     * @param id       用户id
     * @param ownerId  主账号
     * @param pageable 分页
     * @return 角色列表
     */
    @Query("select r from Role r where r.ownerId = :ownerId and r.isActive = true and not exists " +
            "(select 1 from User u join u.roles ur where ur.id = r.id and u.id = :id)")
    Page<Role> findUnBindRoleListByUser(@Param("id") Integer id, @Param("ownerId") Integer ownerId
            , Pageable pageable);

    /**
     * 查询用户未绑定的角色
     *
     * @param id       用户id
     * @param pageable 分页
     * @return 角色列表
     */
    @Query("select r from Role r where r.ownerId is null and r.isActive = true and not exists " +
            "(select 1 from User u join u.roles ur where ur.id = r.id and u.id = :id)")
    Page<Role> findUnBindRoleListByUser(@Param("id") Integer id
            , Pageable pageable);

    /**
     * 查询用户已绑定的角色
     *
     * @param id       用户id
     * @param pageable 分页
     * @return 角色列表
     */
    @Query("select r from User u join u.roles r where u.id = :id and r.isActive = true")
    Page<Role> findRoleListByUser(@Param("id") Integer id, Pageable pageable);

    /**
     * 分页查询角色列表
     *
     * @param pageable 分页
     * @return 角色列表
     */
    Page<Role> findByOwnerIdIsNull(Pageable pageable);

    /**
     * 查询角色列表
     *
     * @return 角色列表
     */
    List<Role> findByOwnerIdIsNullAndIsActiveIsTrue();

    /**
     * 根据ownerId分页查询角色列表
     *
     * @param ownerId  主账号
     * @param pageable 分页
     * @return 角色列表
     */
    Page<Role> findByOwnerId(Integer ownerId, Pageable pageable);

    /**
     * 根据ownerId查询角色列表
     *
     * @param ownerId 主账号
     * @return 角色列表
     */
    List<Role> findByOwnerIdAndIsActiveIsTrue(Integer ownerId);

    /**
     * 根据拥有者id和角色名称查询角色
     *
     * @param ownerId 拥有者id
     * @param name    角色名称
     * @return 角色集合
     */
    List<Role> findByOwnerIdAndNameLike(Integer ownerId, String name);
}
