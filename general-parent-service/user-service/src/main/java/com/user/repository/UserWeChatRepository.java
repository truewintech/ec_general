/*
 * Copy Right@
 */
package com.user.repository;

import com.user.entity.User;
import com.user.entity.UserWeChat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface UserWeChatRepository extends JpaRepository<UserWeChat, Integer>,
        JpaSpecificationExecutor<UserWeChat> {
    /**
     * 依据微信appId和openId获取用户微信信息
     * @param appId appId
     * @param openId openId
     * @return 用户微信信息
     */
    UserWeChat findByAppIdAndOpenId(String appId, String openId);

    /**
     * 依据用户信息获取用户微信信息
     * @param user 用户信息
     * @return 用户微信信息
     */
    UserWeChat findByUser(User user);
}