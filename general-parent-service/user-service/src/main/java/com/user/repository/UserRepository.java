/*
 * Copy Right@
 */
package com.user.repository;

import com.user.entity.Position;
import com.user.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface UserRepository extends JpaRepository<User, Integer>,
        JpaSpecificationExecutor<User> {

    /**
     * 通过账号获取账户
     *
     * @param account 账号
     * @return 账户
     */
    User findByAccount(String account);

    /**
     * 通过手机号获取账户
     *
     * @param mobile 手机号
     * @return 账户
     */
    User findByMobile(String mobile);

    /**
     * 分页查询数据
     *
     * @param pageable 分页
     * @return 分页数据
     */
    Page<User> findByIsSystemIsTrue(Pageable pageable);

    /**
     * 分页查询数据
     *
     * @param account  账户
     * @param pageable 分页
     * @return 分页数据
     */
    Page<User> findByAccountLikeAndIsSystemIsTrue(String account, Pageable pageable);

    /**
     * 分页查询非系统用户数据
     *
     * @param ownerId  主账号id
     * @param pageable 分页
     * @return 分页数据
     */
    Page<User> findByOwnerIdAndIsSystemIsTrue(Integer ownerId, Pageable pageable);

    /**
     * 分页查询操作人员数据
     *
     * @param ownerId  主账号id
     * @param pageable 分页
     * @return 分页数据
     */
    Page<User> findByOwnerId(Integer ownerId, Pageable pageable);

    /**
     * 分页查询非系统数据
     *
     * @param ownerId  主账号id
     * @param account  账户
     * @param pageable 分页
     * @return 分页数据
     */
    Page<User> findByOwnerIdAndAccountLikeAndIsSystemIsTrue(Integer ownerId
            , String account, Pageable pageable);

    /**
     * 查询用户已经绑定的职位
     *
     * @param id       用户id
     * @param pageable 分页
     * @return 职位列表
     */
    @Query("select p from User u join u.positions p where p.isActive = true and u.id = :id")
    Page<Position> findBindPositionByUser(@Param("id") Integer id, Pageable pageable);
}