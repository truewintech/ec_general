/*
 * Copy Right@
 */
package com.user.repository;

import com.user.entity.User;
import com.user.entity.UserWeChatMiniapp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author dengyalong
 * @date 2018年05月04日
 */
public interface UserWeChatMiniappRepository extends JpaRepository<UserWeChatMiniapp, Integer>,
        JpaSpecificationExecutor<UserWeChatMiniapp> {
    /**
     * 依据微信appId和openId获取用户微信信息
     * @param appId appId
     * @param openId openId
     * @return 用户微信信息
     */
    List<UserWeChatMiniapp> findByAppIdAndOpenId(String appId, String openId);

}