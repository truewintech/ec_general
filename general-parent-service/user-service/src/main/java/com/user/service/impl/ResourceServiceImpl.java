/*
 * Copy Right@
 */
package com.user.service.impl;

import com.common.constant.DbConst;
import com.framework.exception.MessageException;
import com.framework.util.UserUtil;
import com.user.entity.Resource;
import com.user.entity.User;
import com.user.repository.ResourceRepository;
import com.user.repository.UserRepository;
import com.user.service.ResourceService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
@CacheConfig(cacheNames = "ResourceServiceImpl")
public class ResourceServiceImpl implements ResourceService {

    /**
     * 资源数据仓库
     */
    @Autowired
    private ResourceRepository resourceRepository;

    /**
     * 用户仓库
     */
    @Autowired
    private UserRepository userRepository;

    @Override
    @Cacheable
    public List<Resource> findAllMenus() {
        return resourceRepository.findAllByTypeAndParentIdIsNullOrderBySort(DbConst.Resource.Type.MENU);
    }

    @Override
    @Cacheable
    public List<Resource> findAllMenusByRange(String range) {
        return resourceRepository.findAllByTypeAndRangeAndParentIdIsNullOrderBySort(DbConst.Resource.Type.MENU, range);
    }

    @Override
    public List<Resource> findUserMenus(Integer userId) {
        return resourceRepository.findAllByUserIdAndType(userId, DbConst.Resource.Type.MENU);
    }

    @Override
    public List<Resource> findBindMenus(Integer roleId) {
        Integer userId = UserUtil.getCurrentUserId();
        boolean isSystemUser = isSystemUser(userId);
        String range = DbConst.Resource.Range.SELLER_CENTER.toString();
        if (isSystemUser) {
            range = DbConst.Resource.Range.SYSTEM.toString();
        }
        List<Integer> userMenuIds = resourceRepository.findIdByUserIdAndType(userId, DbConst.Resource.Type.MENU);
        List<Integer> bindMenuIds = resourceRepository.findIdByRoleIdAndType(roleId, DbConst.Resource.Type.MENU);
        List<Resource> tops = resourceRepository.findAllByTypeAndRangeAndParentIdIsNullOrderBySort(DbConst.Resource.Type.MENU, range);
        checkPermission(tops, userMenuIds, bindMenuIds);
        return tops;
    }

    @Override
    public Page<Resource> findBtnByPage(Pageable pageable) {
        Integer userId = UserUtil.getCurrentUserId();
        if (DbConst.User.Id.ADMIN.equals(userId)) {
            return resourceRepository.findByType(DbConst.Resource.Type.BUTTON, pageable);
        }

        return resourceRepository.findAllByUserIdAndType(userId, DbConst.Resource.Type.BUTTON, pageable);
    }

    @Override
    public Map<String, List<String>> findUserBtn(Integer userId) {
        List<Resource> resources;
        if (DbConst.User.Id.ADMIN.equals(userId)) {
            // 管理员拥有所有权限
            resources = resourceRepository.findAllByType(DbConst.Resource.Type.BUTTON);
        } else {
            resources = resourceRepository.findAllByUserIdAndType(userId, DbConst.Resource.Type.BUTTON);
        }

        Map<String, List<String>> map = new HashMap<>(20);
        for (Resource resource : resources) {
            String code = resource.getCode();
            if (StringUtils.isBlank(code)) {
                continue;
            }
            int index = code.indexOf("|");
            if (index < 0) {
                continue;
            }

            String key = code.substring(0, index);
            code = code.substring(index + 1, code.length());
            List<String> codes = map.get(key);
            if (codes == null) {
                codes = new ArrayList<>();
            }
            codes.add(code);
            map.put(key, codes);
        }

        return map;
    }

    @Override
    public Page<Resource> findBindBtn(Integer roleId, Pageable pageable) {
        return resourceRepository.findBindByRoleIdAndType(roleId, DbConst.Resource.Type.BUTTON, pageable);
    }

    @Override
    public Page<Resource> findUnbindBtn(Integer roleId, Pageable pageable) {
        Integer userId = UserUtil.getCurrentUserId();
        if (DbConst.User.Id.ADMIN.equals(userId)) {
            return resourceRepository.findUnbindByRoleIdAndType(roleId, DbConst.Resource.Type.BUTTON, pageable);
        }
        return resourceRepository.findUnbindByUserRoleAndType(userId, roleId, DbConst.Resource.Type.BUTTON, pageable);
    }

    /**
     * 检验菜单权限
     *
     * @param menus       菜单
     * @param userMenuIds 用户菜单id
     * @param bindMenuIds 绑定菜单id
     */
    private void checkPermission(List<Resource> menus
            , List<Integer> userMenuIds, List<Integer> bindMenuIds) {
        for (int i = 0; i < menus.size(); i++) {
            Resource menu = menus.get(i);
            Integer menuId = menu.getId();
            boolean noPermission = !DbConst.User.Id.ADMIN.equals(UserUtil.getCurrentUserId())
                    && (!Boolean.TRUE.equals(menu.getIsActive()) || !userMenuIds.contains(menuId));
            if (noPermission) {
                menus.remove(i--);
                continue;
            }

            if (bindMenuIds.contains(menuId)) {
                menu.setHasBind(true);
            }
            List<Resource> children = menu.getChildren();
            if (children != null && !children.isEmpty()) {
                checkPermission(children, userMenuIds, bindMenuIds);
            }
        }
    }

    @Override
    @Transactional
    @CacheEvict(allEntries = true)
    public void saveResource(Resource resource) {
        if (resource == null) {
            log.error("新增权限失败");
            throw new MessageException("新增权限失败");
        }
        String sep = "|";
        String type = resource.getType();
        if (DbConst.Resource.Type.MENU.equals(type)) {
            resource.setCode(resource.getName());

            Integer parentId = resource.getParentId();
            if (parentId != null) {
                Resource parent = resourceRepository.findOne(parentId);
                if (parent != null) {
                    resource.setCode(parent.getName() + sep + resource.getName());
                    resource.setParent(parent);
                }
            }
        } else if (DbConst.Resource.Type.BUTTON.equals(type)) {
            String code = resource.getCode();
            if (code == null || !code.contains(sep)) {
                throw new MessageException("按钮标识必须包含|");
            }
        }

        if (isSystemUser(UserUtil.getCurrentUserId())) {
            resource.setRange(DbConst.Resource.Range.SYSTEM.toString());
            resource.setRangeName(DbConst.Resource.Range.SYSTEM_NAME);
        } else {
            resource.setRange(DbConst.Resource.Range.SELLER_CENTER.toString());
            resource.setRangeName(DbConst.Resource.Range.SELLER_CENTER_NAME);
        }
        resourceRepository.save(resource);
    }

    @Override
    @Transactional
    @CacheEvict(allEntries = true)
    public void deleteResource(Integer[] ids) {
        for (Integer id : ids) {
            deleteResource(id);
        }
    }

    /**
     * 删除资源
     *
     * @param id 资源id
     */
    private void deleteResource(Integer id) {
        Resource resource = resourceRepository.findOne(id);
        if (resource == null) {
            log.error("权限不存在,权限id为：{}", id);
            throw new MessageException("权限不存在");
        }
        if (!resource.getChildren().isEmpty()) {
            log.error("无法删除的菜单:" + resource.getName() + ",请先删除子菜单");
            throw new MessageException("无法删除的菜单:" + resource.getName() + ",请先删除子菜单");
        }

        resource.setIsDelete(true);
        resourceRepository.save(resource);
    }

    @Override
    @Transactional
    @CacheEvict(allEntries = true)
    public void updateResource(Resource resource) {
        if (resource == null) {
            log.error("权限不存在");
            throw new MessageException("权限不存在");
        }
        Integer id = resource.getId();
        Resource dbResource = resourceRepository.findOne(id);
        if (dbResource == null) {
            log.error("权限不存在");
            throw new MessageException("权限不存在");
        }
        dbResource.setName(resource.getName());
        dbResource.setIcon(resource.getIcon());
        dbResource.setLink(resource.getLink());
        dbResource.setSort(resource.getSort());
        dbResource.setIsActive(resource.getIsActive());

        resourceRepository.save(dbResource);
    }

    @Override
    public Resource findById(Integer id) {
        if (id == null) {
            log.error("权限不存在");
            throw new MessageException("权限不存在");
        }
        return resourceRepository.findOne(id);
    }

    @Override
    @Transactional
    @CacheEvict(allEntries = true)
    public void changeResourceActive(Integer[] ids) {
        for (Integer id : ids) {
            Resource resource = resourceRepository.findOne(id);
            Boolean isActive = resource.getIsActive();
            if (isActive) {
                resource.setIsActive(false);
            } else {
                resource.setIsActive(true);
            }
            resourceRepository.save(resource);
        }
    }

    /**
     * 判断当前用户是否是系统用户
     *
     * @param userId 用户id
     * @return 是否标志
     */
    private boolean isSystemUser(Integer userId) {
        User user = userRepository.findOne(userId);
        if (user == null) {
            throw new MessageException("用户信息不存在");
        }
        return user.getIsSystem();
    }
}
