/*
 * Copy Right@
 */
package com.user.service.impl;

import com.framework.exception.MessageException;
import com.framework.util.UserUtil;
import com.user.entity.Position;
import com.user.entity.Role;
import com.user.entity.User;
import com.user.repository.PositionRepository;
import com.user.repository.RoleRepository;
import com.user.repository.UserRepository;
import com.user.service.PositionService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author FengShiWei
 * @date 2017年09月04日
 */
@Service
@Slf4j
@CacheConfig(cacheNames = "PositionServiceImpl")
public class PositionServiceImpl implements PositionService {

    /**
     * 职位仓库
     */
    @Autowired
    private PositionRepository positionRepository;

    /**
     * 角色仓库
     */
    @Autowired
    private RoleRepository roleRepository;

    /**
     * 用户仓库
     */
    @Autowired
    private UserRepository userRepository;

    @Override
    public Page<Position> findPositionListPage(Pageable pageable) {
        Integer userId = UserUtil.getNullableCurrentUserId();
        Integer ownerId = null;
        if (userId != null) {
            User user = userRepository.findOne(userId);
            if (user != null) {
                ownerId = user.getOwnerId();
            }
        }
        if (ownerId != null) {
            return positionRepository.findByOwnerId(ownerId, pageable);
        }
        return positionRepository.findByOwnerIdIsNull(pageable);
    }

    @Override
    public Page<Position> findPositionListPageByOwnerId(Integer ownerId, Pageable pageable) {
        return positionRepository.findByOwnerId(ownerId, pageable);
    }

    @Override
    public List<Position> findPositionList() {
        Integer userId = UserUtil.getNullableCurrentUserId();
        Integer ownerId = null;
        if (userId != null) {
            User user = userRepository.findOne(userId);
            if (user != null) {
                ownerId = user.getOwnerId();
            }
        }
        if (ownerId != null) {
            return positionRepository.findByOwnerId(ownerId);
        }
        return positionRepository.findByOwnerIdIsNull();
    }

    @Override
    public List<Position> findPositionListByOwnerId(Integer ownerId) {
        return positionRepository.findByOwnerId(ownerId);
    }

    @Override
    @Transactional
    @Modifying
    public void savePosition(Position position) {
        if (position == null) {
            log.error("新增职位失败");
            throw new MessageException("新增职位失败");
        }
        String name = position.getName();
        if (name == null) {
            throw new MessageException("请填写职位名称");
        }
        Integer userId = UserUtil.getNullableCurrentUserId();
        Integer ownerId = null;
        if (userId != null) {
            User user = userRepository.findOne(userId);
            if (user != null) {
                ownerId = user.getOwnerId();
            }
        }
        position.setOwnerId(ownerId);
        positionRepository.save(position);
    }

    @Override
    public void savePositionByOwnerId(Position position) {
        Integer ownerId = position.getOwnerId();
        if (ownerId == null) {
            throw new MessageException("拥有者id为空");
        }
        List<Position> positionList = positionRepository.findByOwnerId(ownerId);
        if (positionList != null && positionList.size() > 0) {
            for (Position oldPosition : positionList) {
                String name = oldPosition.getName();
                if (StringUtils.isNotBlank(name) && name.equals(position.getName())) {
                    log.error("职位名称已经存在,userId={}", UserUtil.getCurrentUserId());
                    throw new MessageException("职位名称已经存在，请重新填写");
                }
            }
        }
        positionRepository.save(position);
    }

    @Override
    @Transactional
    public void updatePositionIsDel(Integer[] ids) {
        if (ids == null || ids.length == 0) {
            log.error("删除失败");
            throw new MessageException("删除失败");
        }

        for (Integer id : ids) {
            Position position = positionRepository.findOne(id);
            if (position == null) {
                throw new MessageException("编辑的职位不存在");
            }
            position.setIsDelete(true);
            positionRepository.save(position);
        }
    }

    @Override
    public Position findById(Integer id) {
        if (id == null) {
            log.error("职位不存在");
            throw new MessageException("职位不存在");
        }
        return positionRepository.findOne(id);
    }

    @Override
    @Modifying
    @Transactional
    public void updatePosition(Position position) {
        if (position == null) {
            log.error("职位不存在");
            throw new MessageException("职位不存在");
        }
        Integer id = position.getId();
        if (id == null) {
            log.error("职位不存在");
            throw new MessageException("职位不存在");
        }
        Integer userId = UserUtil.getCurrentUserId();
        if (!userId.equals(position.getOwnerId())) {
            throw new MessageException("您无权操作该职位");
        }
        String name = position.getName();
        Position byName = positionRepository.findByNameAndOwnerId(name, userId);
        if (byName != null && !id.equals(byName.getId())) {
            log.error("职位名已存在");
            throw new MessageException("职位名已存在");
        }
        positionRepository.save(position);
    }

    @Override
    @Transactional
    public void bindPositionAndRole(Integer id, Integer[] roleIds) {
        if (roleIds == null || roleIds.length == 0) {
            log.error("绑定角色失败");
            throw new MessageException("绑定角色失败");
        }
        if (id == null) {
            log.error("职位不存在");
            throw new MessageException("职位不存在");
        }
        List<Role> roles = roleRepository.findByIdIn(roleIds);
        Position position = positionRepository.findOne(id);
        if (position == null) {
            log.error("职位不存在");
            throw new MessageException("职位不存在");
        }
        List<Role> roleList = position.getRoles();
        roleList.addAll(roles);
        position.setRoles(roleList);
        Position savePosition = positionRepository.save(position);

        //给绑定了此职位的用户绑定角色
        List<User> users = savePosition.getUsers();
        if (users != null && users.size() > 0) {
            users.forEach(user -> {
                List<Role> userRoles = user.getRoles();
                userRoles.addAll(roles);
                userRepository.save(user);
            });
        }
    }

    @Override
    @Transactional
    public void unBindPositionAndRole(Integer id, Integer[] roleIds) {
        if (roleIds == null || roleIds.length == 0) {
            log.error("绑定角色失败");
            throw new MessageException("绑定角色失败");
        }
        if (id == null) {
            log.error("职位不存在");
            throw new MessageException("职位不存在");
        }
        List<Role> roles = roleRepository.findByIdIn(roleIds);
        Position position = positionRepository.findOne(id);
        if (position == null) {
            log.error("职位不存在");
            throw new MessageException("职位不存在");
        }
        List<Role> positionRoles = position.getRoles();
        boolean flag = positionRoles.removeAll(roles);
        if (!flag) {
            log.error("解绑失败");
            throw new MessageException("解绑失败");
        }
        position.setRoles(positionRoles);
        Position savePosition = positionRepository.save(position);

        List<User> users = savePosition.getUsers();
        if (users != null && users.size() > 0) {
            users.forEach(user -> {
                List<Role> userRoles = user.getRoles();
                userRoles.removeAll(roles);
                userRepository.save(user);
            });
        }
    }

    @Override
    public Page<Position> findBindPositionByUser(Integer id, Pageable pageable) {
        if (id == null) {
            log.error("用户不存在");
            throw new MessageException("用户不存在");
        }
        return userRepository.findBindPositionByUser(id, pageable);
    }

    @Override
    public Page<Position> findUnBindPositionByUser(Integer id, Pageable pageable) {
        if (id == null) {
            log.error("用户不存在");
            throw new MessageException("用户不存在");
        }
        return positionRepository.findUnBindPositionByUser(id, pageable);
    }

    @Override
    @Transactional
    public void changePositionActive(Integer[] ids, Boolean isActive) {
        for (Integer id : ids) {
            changePositionActive(id, isActive);
        }
    }

    /**
     * 修改职位是否有效
     *
     * @param id       职位id
     * @param isActive 是否禁用
     */
    private void changePositionActive(Integer id, Boolean isActive) {
        if (id == null) {
            throw new MessageException("id不能为空");
        }
        Position position = positionRepository.findOne(id);
        if (position == null) {
            throw new MessageException("职位不存在");
        }
        position.setIsActive(isActive);
        positionRepository.save(position);
    }

}
