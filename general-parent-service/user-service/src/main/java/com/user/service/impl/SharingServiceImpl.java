package com.user.service.impl;

import com.framework.util.UserUtil;
import com.user.entity.Sharing;
import com.user.entity.User;
import com.user.repository.SharingRespository;
import com.user.repository.UserRepository;
import com.user.service.SharingService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SharingServiceImpl implements SharingService {
    /**
     * 用户数据仓库
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * 推荐数据仓库
     */
    @Autowired
    private SharingRespository sharingRespository;

    /**
     * 分页查询推荐列表
     *
     * @param pageable 分页
     * @return  Page<Sharing>   推荐分页数据
     */
    @Override
    public Page<Sharing> findSharingListByPage(String shareType, Pageable pageable) {
        Integer userId = UserUtil.getCurrentUserId();
        User user = userRepository.findOne(userId);
        Specification querySpecifi = new Specification<Sharing>(){
            @Override
            public Predicate toPredicate(Root<Sharing> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<Predicate>();
                //如果是系统用户，查询所有推荐关系,非系统用户查询自己推荐的人
                if(user.getIsSystem() != true){
                    predicates.add(criteriaBuilder.equal(root.get("sharerId"),userId));
                }
                criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
                return null;
            }
        };
        Page<Sharing> pageresult =sharingRespository.findAll(querySpecifi,pageable);
        return pageresult;
    }
}
