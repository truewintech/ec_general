/*
 * Copy Right@
 */
package com.user.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.common.util.rest.GeneralRestTemplateUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.user.service.RestService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * <pre>
 *
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
public class RestServiceImpl implements RestService {

    /**
     * 数据服务地址
     */
    @Value("${serviceUrl.general.data}")
    private String dataServiceUrl;

    /**
     * 卖家服务地址
     */
    @Value("${serviceUrl.ec.seller}")
    private String ecSellerServiceUrl;

    /**
     * 买家服务地址
     */
    @Value("${serviceUrl.ec.buyer}")
    private String ecBuyerServiceUrl;

    /**
     * 日志服务地址
     */
    @Value("${serviceUrl.general.log}")
    private String logServiceUrl;

    @Override
    @HystrixCommand(fallbackMethod = "getSystemParameterFallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000")
    })
    public String getSystemParameter(String id) {
        return GeneralRestTemplateUtil.Data.getParam(dataServiceUrl, id);
    }

    private String getSystemParameterFallback(String id) {
        return null;
    }

    @Override
    public JSONObject findLastLoginLogByUserId(Integer userId) {
        return GeneralRestTemplateUtil.Log.findLastLoginLogByUserId(logServiceUrl, userId);
    }
}
