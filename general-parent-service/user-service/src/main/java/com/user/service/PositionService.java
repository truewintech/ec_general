/*
 * Copy Right@
 */
package com.user.service;

import com.user.entity.Position;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author FengShiWei
 * @date 2017年09月04日
 */
public interface PositionService {

    /**
     * 分页查询职位列表
     *
     * @param pageable 分页
     * @return 职位列表
     */
    Page<Position> findPositionListPage(Pageable pageable);

    /**
     * 根据ownerId分页查询职位列表
     *
     * @param ownerId  拥有者id
     * @param pageable 分页
     * @return 职位列表
     */
    Page<Position> findPositionListPageByOwnerId(Integer ownerId, Pageable pageable);

    /**
     * 查询职位列表
     *
     * @return 职位列表
     */
    List<Position> findPositionList();

    /**
     * 查询职位列表
     *
     * @param ownerId 拥有者id
     * @return 职位列表
     */
    List<Position> findPositionListByOwnerId(Integer ownerId);

    /**
     * 新增职位
     *
     * @param position 职位对象
     */
    void savePosition(Position position);

    /**
     * 根据拥有者id新增职位
     *
     * @param position 职位
     */
    void savePositionByOwnerId(Position position);

    /**
     * 删除职位
     *
     * @param ids 职位数组
     */
    void updatePositionIsDel(Integer[] ids);

    /**
     * 根据职位id查询职位
     *
     * @param id 职位id
     * @return 职位
     */
    Position findById(Integer id);

    /**
     * 更新职位
     *
     * @param position 职位对象
     */
    void updatePosition(Position position);

    /**
     * 绑定职位和角色
     *
     * @param id      职位id
     * @param roleIds 角色id数组
     */
    void bindPositionAndRole(Integer id, Integer[] roleIds);

    /**
     * 解绑职位和角色
     *
     * @param id      职位id
     * @param roleIds 角色id数组
     */
    void unBindPositionAndRole(Integer id, Integer[] roleIds);

    /**
     * 查询用户已经绑定了的职位
     *
     * @param id       用户id
     * @param pageable 分页
     * @return 职位列表
     */
    Page<Position> findBindPositionByUser(Integer id, Pageable pageable);

    /**
     * 查询用户未绑定的职位
     *
     * @param id       用户id
     * @param pageable 分页
     * @return 职位列表
     */
    Page<Position> findUnBindPositionByUser(Integer id, Pageable pageable);

    /**
     * 开启或禁用职位
     *
     * @param ids      职位id数组
     * @param isActive 开启或禁用状态
     */
    void changePositionActive(Integer[] ids, Boolean isActive);
}
