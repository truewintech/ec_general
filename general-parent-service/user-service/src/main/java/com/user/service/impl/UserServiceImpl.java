/*
 * Copy Right@
 */
package com.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.common.constant.Const;
import com.common.constant.DbConst;
import com.framework.constant.DbConstList;
import com.framework.data.redis.RedisIdGenerator;
import com.framework.data.redis.RedisManager;
import com.framework.entity.TccEvent;
import com.framework.exception.MessageException;
import com.framework.repository.TccEventRepository;
import com.framework.shiro.Salt;
import com.framework.shiro.util.ShiroUtil;
import com.framework.util.DateUtil;
import com.framework.util.UserUtil;
import com.framework.util.pattern.PatternUtil;
import com.user.entity.Position;
import com.user.entity.Role;
import com.user.entity.User;
import com.user.entity.UserWeChat;
import com.user.repository.PositionRepository;
import com.user.repository.RoleRepository;
import com.user.repository.UserRepository;
import com.user.repository.UserWeChatRepository;
import com.user.service.RestService;
import com.user.service.UserService;
import com.user.service.UserWeChatService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springfox.documentation.spring.web.json.Json;

import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    /**
     * 用户数据仓库
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * 角色数据仓库
     */
    @Autowired
    private RoleRepository roleRepository;

    /**
     * 职位仓库
     */
    @Autowired
    private PositionRepository positionRepository;

    /**
     * id生成器
     */
    @Autowired
    private RedisIdGenerator userAccountGenerator;

    /**
     * 缓存服务
     */
    @Autowired
    private RedisManager redisManager;

    /**
     * 事件服务
     */
    @Autowired
    private EventServiceImpl eventService;

    /**
     * rest服务
     */
    @Autowired
    private RestService restService;

    /**
     * 微信用户服务
     */
    @Autowired
    private UserWeChatService userWeChatService;

    @Autowired
    private UserWeChatRepository userWeChatRepository;

    /**
     * appId
     */
    @Value("${weChat.appId}")
    private String appId;

    /**
     * seller服务地址
     */
    @Value("${serviceUrl.ec.seller}")
    private String ecSellerServiceUrl;

    /**
     * buyer服务地址
     */
    @Value("${serviceUrl.ec.buyer}")
    private String ecBuyerServiceUrl;

    @Autowired
    private TccEventRepository tccEventRepository;

    @Value("${serviceUrl.general.user}")
    private String generalUserServiceUrl;


    @Override
    public User findById(Integer id) {
        return userRepository.findOne(id);
    }

    @Override
    public User findByAccount(String account) {
        return userRepository.findByAccount(account);
    }

    @Override
    public User findByMobile(String mobile) {
        return userRepository.findByMobile(mobile);
    }

    @Override
    public Page<User> findUserByPage(String account, Pageable pageable) {
        Integer ownerId = null;
        Integer userId = UserUtil.getCurrentUserId();
        User user = userRepository.findOne(userId);
        if (user != null) {
            ownerId = user.getOwnerId();
        }
        if (ownerId == null) {
            if (StringUtils.isBlank(account)) {
                return userRepository.findByIsSystemIsTrue(pageable);
            }
            return userRepository.findByAccountLikeAndIsSystemIsTrue("%" + account + "%", pageable);
        }
        if (StringUtils.isBlank(account)) {
            return userRepository.findByOwnerIdAndIsSystemIsTrue(ownerId, pageable);
        }
        return userRepository.findByOwnerIdAndAccountLikeAndIsSystemIsTrue(ownerId, account, pageable);
    }

    @Override
    public Page<User> findUserPageByOwnerId(Integer ownerId, Pageable pageable) {
        return userRepository.findByOwnerId(ownerId, pageable);
    }

    @Override
    @Transactional
    public User addUser(User user) {
        if (user == null) {
            throw new MessageException("用户信息为空");
        }
        // 判断手机号及账号是否均为空
        String mobile = user.getMobile();
        String account = user.getAccount();
        if (StringUtils.isBlank(mobile) && StringUtils.isBlank(account)) {
            throw new MessageException("账号和手机信息为空");
        }
        User oldUser;
        if (StringUtils.isNotBlank(mobile)) {
            // 用手机号注册
            oldUser = userRepository.findByMobile(mobile);
            if (oldUser != null) {
                throw new MessageException("手机已注册");
            }
            // 检测手机号是否和账号重复
            oldUser = userRepository.findByAccount(mobile);
            if (oldUser != null) {
                throw new MessageException("用户已存在");
            }
            // 自动生成账号
            if (StringUtils.isBlank(account)) {
                account = userAccountGenerator.generateByPrefix("U");
                user.setAccount(account);
            }
        } else {
            // 用账号注册，检测账号是否重复
            oldUser = userRepository.findByAccount(account);
            if (oldUser != null) {
                throw new MessageException("用户已存在");
            }
            user.setMobile(null);
        }
        String password = user.getPassword();
        if (StringUtils.isNotBlank(password)) {
            Salt salt = ShiroUtil.encryptPassword(account, password);
            user.setSalt(salt.getSalt());
            user.setPassword(salt.getNewPassword());
        }
        if (user.getIsActive() == null) {
            user.setIsActive(false);
        }
        // 取当前操作用户的ownerId并保存
        Integer userId = UserUtil.getNullableCurrentUserId();
        if (userId != null) {
            User u = userRepository.findOne(userId);
            if (u != null) {
                Integer ownerId = u.getOwnerId();
                if (ownerId != null) {
                    user.setOwnerId(ownerId);
                }
            }
        }
        return userRepository.save(user);
    }

    @Override
    public User updateUser(User user) {
        if (user == null) {
            throw new MessageException("用户信息为空");
        }
        Integer userId = user.getId();
        if (userId == null) {
            throw new MessageException("用户id为空");
        }
        User oldUser = userRepository.findOne(userId);
        if (oldUser == null) {
            log.error("根据用户id查询用户信息失败，用户id={}", userId);
            throw new MessageException("用户信息为空");
        }
        String password = user.getPassword();
        if (StringUtils.isNotBlank(password)) {
            Salt salt = ShiroUtil.encryptPassword(oldUser.getAccount(), password);
            oldUser.setSalt(salt.getSalt());
            oldUser.setPassword(salt.getNewPassword());
        }
        return userRepository.save(oldUser);
    }

    @Override
    @Transactional
    public void updateUserIsDelete(Integer[] ids, boolean isDelete) {
        if (ids == null || ids.length == 0) {
            throw new MessageException("获取数据失败");
        }
        for (Integer id : ids) {
            updateUserIsDelete(id, isDelete);
        }
    }

    @Override
    @Transactional
    public void updateUserIsDelete(Integer id, boolean isDelete) {
        User user = userRepository.findOne(id);
        if (user == null) {
            throw new MessageException("用户不存在");
        }
        user.setIsDelete(isDelete);
        userRepository.save(user);
    }

    @Override
    public void deleteUserById(Integer id) {
        if (id == null) {
            log.error("删除用户时参数错误，id为空");
            throw new MessageException("操作失败，请稍后重试");
        }
        userRepository.delete(id);
    }

    @Override
    @Transactional
    public void updateUserIsActive(Integer[] ids, boolean isActive) {
        if (ids == null || ids.length == 0) {
            throw new MessageException("获取数据失败");
        }
        for (Integer id : ids) {
            updateUserIsActive(id, isActive);
        }
    }

    @Override
    @Transactional
    public void updateUserIsActive(Integer id, boolean isActive) {
        User user = userRepository.findOne(id);
        if (user == null) {
            throw new MessageException("用户不存在");
        }
        user.setIsActive(isActive);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void updatePassword(String oldPassword, String newPassword) {
        if (StringUtils.isBlank(oldPassword) || StringUtils.isBlank(newPassword)) {
            throw new MessageException("密码不能为空");
        }
        Integer userId = UserUtil.getCurrentUserId();
        User user = userRepository.findOne(userId);
        if (user == null) {
            log.error("尝试修改密码时根据当前用户ID无法找到对应用户，userId：{}", userId);
            throw new MessageException("操作失败，请稍后重试");
        }
        String password = user.getPassword();
        String salt = user.getSalt();
        String account = user.getAccount();
        if (!ShiroUtil.checkPassword(account, salt, oldPassword, password)) {
            throw new MessageException("原始密码错误");
        }
        Salt newSalt = ShiroUtil.encryptPassword(account, newPassword);
        user.setSalt(newSalt.getSalt());
        user.setPassword(newSalt.getNewPassword());
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void bindUserAndRole(Integer id, Integer[] roleIds) {
        User user = userRepository.findOne(id);
        List<Role> userRoles = user.getRoles();
        List<Role> roleList = roleRepository.findByIdIn(roleIds);
        boolean flag = userRoles.addAll(roleList);
        if (!flag) {
            log.error("绑定角色失败");
            throw new MessageException("绑定角色失败");
        }
        user.setRoles(userRoles);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void unBindRoleAndUser(Integer userId, Integer[] roleIds) {
        List<Role> roleList = roleRepository.findByIdIn(roleIds);
        User user = userRepository.findOne(userId);
        if (user == null) {
            throw new MessageException("用户不存在");
        }
        List<Role> userRoles = user.getRoles();
        boolean flag = userRoles.removeAll(roleList);
        if (!flag) {
            log.error("解绑失败");
            throw new MessageException("解绑失败");
        }
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void bindUserAndPosition(Integer userId, Integer[] positionIds) {
        if (positionIds == null || positionIds.length == 0) {
            log.error("绑定失败");
            throw new MessageException("绑定失败");
        }
        List<Position> positions = positionRepository.findByIdIn(positionIds);
        if (userId == null) {
            log.error("用户不存在");
            throw new MessageException("用户不存在");
        }
        User user = userRepository.findOne(userId);
        if (user == null) {
            log.error("用户不存在");
            throw new MessageException("用户不存在");
        }
        user.setPositions(positions);
        //用户绑定职位对应的角色
        List<Role> userRoles = user.getRoles();
        List<Role> roleList = new ArrayList<>();
        positions.forEach(position -> roleList.addAll(position.getRoles()));
        roleList.stream().distinct().forEach(userRoles::add);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void unBindUserAndPosition(Integer userId, Integer[] positionIds) {
        if (userId == null) {
            log.error("用户不存在");
            throw new MessageException("用户不存在");
        }
        if (positionIds == null || positionIds.length == 0) {
            log.error("职位不存在");
            throw new MessageException("职位不存在");
        }
        List<Position> positionList = positionRepository.findByIdIn(positionIds);
        User user = userRepository.findOne(userId);
        if (user == null) {
            log.error("用户不存在");
            throw new MessageException("用户不存在");
        }
        List<Position> positions = user.getPositions();
        boolean flag = positions.removeAll(positionList);
        if (!flag) {
            log.error("解绑失败");
            throw new MessageException("解绑失败");
        }
        user.setPositions(positions);
        //解绑职位同时 删除职位对应的角色
        List<Role> roleList = new ArrayList<>();
        positionList.forEach(position -> roleList.addAll(position.getRoles()));
        List<Role> userRoles = user.getRoles();
        userRoles.removeAll(roleList);
        //添加剩余绑定的职位对应的角色
        List<Role> bindRoleList = new ArrayList<>();
        positions.forEach(position -> bindRoleList.addAll(position.getRoles()));
        user.setRoles(bindRoleList);
        userRepository.save(user);
    }

    @Override
    public boolean isUserExistByMobile(String mobile) {
        // 调用公用方法检验手机号格式
        if (!PatternUtil.checkMobile(mobile)) {
            throw new MessageException("手机号格式非法");
        }
        User user = userRepository.findByMobile(mobile);
        return (user != null);
    }

    @Override
    public void updateMobile(String oldMobile, String newMobile, String validateCode) {
        Integer userId = UserUtil.getCurrentUserId();
        if (!PatternUtil.checkMobile(newMobile)) {
            log.error("尝试修改验证手机号时手机号格式不正确");
            throw new MessageException("手机号格式不正确，请检查");
        }
        // 判断短信验证码是否正确
        Map<String, Object> validateCodeInCache = redisManager.getCache(Const.CacheKey.SMS_VERIFICATION_CODE_UPDATE_MOBILE_PRE + oldMobile);
        if (validateCodeInCache == null || validateCodeInCache.size() == 0) {
            log.warn("尝试修改已验证手机号时短信验证码已失效,原手机号{}", oldMobile);
            throw new MessageException("短信验证码已失效");
        } else {
            String correctValidateCode = (String) validateCodeInCache.get(Const.RegistrationCode.VALID_CODE_KEY);
            if (StringUtils.isBlank(correctValidateCode)) {
                log.error("尝试修改已验证手机号时从缓存中获取的信息中缺少正确短信验证码,原手机号{}", oldMobile);
                throw new MessageException("短信验证码已失效");
            }
            if (!StringUtils.equalsIgnoreCase(validateCode, correctValidateCode)) {
                log.warn("尝试修改已验证手机号时短信验证码输入不正确，原手机号：{}", oldMobile);
                throw new MessageException("短信验证不正确，请重试");
            } else {
                // 验证码输入正确，为防止重复提交，将改验证码从缓存中移除
                redisManager.delCache(Const.CacheKey.SMS_VERIFICATION_CODE_UPDATE_MOBILE_PRE + oldMobile);
            }
        }
        User user = userRepository.findOne(userId);
        user.setMobile(newMobile);
        userRepository.save(user);
    }

    @Override
    public void forgetPassword(String mobile, String validateCode, String newPassword) {
        User user = userRepository.findByMobile(mobile);
        if (user == null) {
            log.warn("用户请求通过忘记密码接口修改密码时根据手机号未能查找对应用户，手机号：{}", mobile);
            throw new MessageException("操作失败，未找到对应用户，请检查");
        }
        // 判断短信验证码是否正确
        Map<String, Object> validateCodeInCache = redisManager.getCache(Const.CacheKey.SMS_VERIFICATION_CODE_FORGET_PASSWORD_PRE + mobile);
        if (validateCodeInCache == null || validateCodeInCache.size() == 0) {
            log.warn("尝试通过忘记密码接口修改密码时短信验证码已失效,手机号{}", mobile);
            throw new MessageException("短信验证码已失效");
        } else {
            String correctValidateCode = (String) validateCodeInCache.get(Const.RegistrationCode.VALID_CODE_KEY);
            if (StringUtils.isBlank(correctValidateCode)) {
                log.error("尝试通过忘记密码接口修改密码时从缓存中获取的信息中缺少正确短信验证码,手机号{}", mobile);
                throw new MessageException("短信验证码已失效");
            }
            if (!StringUtils.equalsIgnoreCase(validateCode, correctValidateCode)) {
                log.warn("尝试通过忘记密码接口修改密码时短信验证码输入不正确，手机号：{}", mobile);
                throw new MessageException("短信验证不正确，请重试");
            } else {
                // 验证码输入正确，为防止重复提交，将改验证码从缓存中移除
                redisManager.delCache(Const.CacheKey.SMS_VERIFICATION_CODE_FORGET_PASSWORD_PRE + mobile);
            }
        }
        String account = user.getAccount();
        Salt newSalt = ShiroUtil.encryptPassword(account, newPassword);
        user.setSalt(newSalt.getSalt());
        user.setPassword(newSalt.getNewPassword());
        userRepository.save(user);
    }

    @Override
    public void checkForgetPasswordValidateCode(String mobile, String validateCode) {
        // 判断短信验证码是否正确
        Map<String, Object> validateCodeInCache = redisManager.getCache(Const.CacheKey.SMS_VERIFICATION_CODE_FORGET_PASSWORD_PRE + mobile);
        if (validateCodeInCache == null || validateCodeInCache.size() == 0) {
            log.warn("尝试通过忘记密码接口修改密码时短信验证码已失效,手机号{}", mobile);
            throw new MessageException("短信验证码已失效");
        } else {
            String correctValidateCode = (String) validateCodeInCache.get(Const.RegistrationCode.VALID_CODE_KEY);
            if (StringUtils.isBlank(correctValidateCode)) {
                log.error("尝试通过忘记密码接口修改密码时从缓存中获取的信息中缺少正确短信验证码,手机号{}", mobile);
                throw new MessageException("短信验证码已失效");
            }
            if (!StringUtils.equalsIgnoreCase(validateCode, correctValidateCode)) {
                log.warn("尝试通过忘记密码接口修改密码时短信验证码输入不正确，手机号：{}", mobile);
                throw new MessageException("短信验证不正确，请重试");
            }
        }
    }

    @Override
    public Map<String, Object> getCurrentUserInfoAndLastLoginTime() {
        Map<String, Object> currentUserInfoAndLastLoginTime = new HashMap<>(16);
        Integer userId = UserUtil.getCurrentUserId();
        User user = userRepository.findOne(userId);
        if (user == null) {
            log.warn("数据错误，未能根据当前用户Id获取用户信息");
            throw new MessageException("未能根据当前用户Id获取用户信息，请稍后重试");
        }
        String account = user.getAccount();
        currentUserInfoAndLastLoginTime.put("account", account);
        List<Position> positions = user.getPositions();
        currentUserInfoAndLastLoginTime.put("positions", positions);
        JSONObject lastLoginLog = restService.findLastLoginLogByUserId(userId);
        if (lastLoginLog != null && lastLoginLog.containsKey("loginTime")){
            Date lastLoginLogDate = lastLoginLog.getDate("loginTime");
            currentUserInfoAndLastLoginTime.put("lastLoginLogDate", DateUtil.formatDateTime(lastLoginLogDate));
            return currentUserInfoAndLastLoginTime;
        } else {
            currentUserInfoAndLastLoginTime.put("lastLoginLogDate", null);
        }
        return currentUserInfoAndLastLoginTime;
    }

    @Override
    public Page<User> findSubAccountByUserId(Integer userId, Pageable pageable) {
        return userRepository.findByOwnerId(userId, pageable);
    }

    @Override
    public Boolean login() {
        Integer userId = UserUtil.getCurrentUserId();
        User user = userRepository.findOne(userId);
        if (user == null) {
            log.error("平台登录时参数错误，获取到的参数user为空");
            throw new MessageException("登录失败，请稍后重试");
        }
        // 判断是否为系统用户
        if (Boolean.TRUE.equals(user.getIsSystem())) {
            return true;
        }else{
            log.warn("尝试平台登录被阻止，登录账号为非系统用户，尝试登陆userId：{}", userId);
            throw new MessageException("您使用的账号为非系统用户账号，无法进行平台登录");
        }
    }

    @Override
    @Transactional
    public JSONObject createUser(JSONObject jsonObject) {
        if (jsonObject.size() < 1) {
            log.error("门店注册，创建用户，参数为空，jsonObject= {}", jsonObject);
            throw new MessageException("请填写完整资料后再申请");
        }
        String openId = jsonObject.getString("openId");
        if (StringUtils.isBlank(openId)) {
            log.error("门店注册，创建用户，获取的openId不正确,openId={}", openId);
            throw new MessageException("网络延迟超时，请刷新后重试");
        }
        // 获取用户输入的手机号
        String mobile = jsonObject.getString("mobile");
        if (StringUtils.isBlank(mobile)) {
            log.error("门店注册，创建用户，获取到的mobile不正确，mobile= {}", mobile);
            throw new MessageException("手机号码不能为空，请输入手机号码");
        }
        // 获取 userWeChat
        UserWeChat userWeChat = userWeChatRepository.findByAppIdAndOpenId(appId, openId);
        if (userWeChat == null) {
            log.error("门店注册，创建用户，未获取到当前的微信用户信息");
            throw new MessageException("网络延迟超时，请刷新后重试");
        }
        User user = userWeChat.getUser();
        // 创建 JSONObject 用于记录payload
        JSONObject payload = new JSONObject();
        // 判断是否生成user  没有则添加user
        if (user == null) {
            // 判断该手机号是否已经被绑定
            User user1 = userRepository.findByMobile(mobile);
            if (user1 != null) {
                log.error("门店注册，该手机号已被注册，无法重复注册，mobile：{}",mobile);
                throw new MessageException("该手机号已被注册，请更换手机号码");
            }
            user = new User();
            user.setAccount(mobile);
            user.setMobile(mobile);
            // 先设置新用户状态为不可用
            user.setIsActive(false);
            user.setIsDelete(false);
            userRepository.save(user);

            // 保存userId
            userWeChat.setUser(user);
            userWeChat = userWeChatRepository.save(userWeChat);

            // 设置用户为新建状态
            payload.put("type",DbConstList.Event.Status.NEW);
        } else {
            String userMobile = user.getMobile();
            if (!mobile.equals(userMobile)) {
                // 判断该手机号是否已经被绑定
                User user1 = userRepository.findByMobile(mobile);
                if (user1 != null) {
                    log.error("门店注册，该手机号已被注册，无法重复注册，mobile：{}",mobile);
                    throw new MessageException("该手机号已被注册，请更换手机号码");
                }
            }
        }
        JSONObject uuu = JSONObject.parseObject(JSON.toJSONString(userWeChat));
        uuu.put("userId",userWeChat.getUser().getId());
        jsonObject.put("userWeChat", uuu);
        // 创建tcc
        payload.put("user",user);
        TccEvent tccEvent = new TccEvent();
        tccEvent.setServiceId(generalUserServiceUrl);
        tccEvent.setStatus(DbConstList.TccEvent.Status.TRY);
        tccEvent.setTccType(DbConst.TccEvent.TccType.STORE_REGISTER);
        tccEvent.setPayload(JSON.toJSONString(payload));
        TccEvent saveTccEvent = tccEventRepository.save(tccEvent);
        jsonObject.put("tccEvent",saveTccEvent);
        // 创建买家账号
        return jsonObject;
    }

}
