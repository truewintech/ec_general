/*
 * Copy Right@
 */
package com.user.service;

import com.alibaba.fastjson.JSONObject;
import com.framework.entity.TccEvent;

/**
 * <pre>
 *
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface RestService {

    /**
     * 获取系统参数
     *
     * @param id 系统参数id
     * @return 系统参数值
     */
    String getSystemParameter(String id);


    /**
     * 根据用户ID获取上次登录记录
     *
     * @param userId 用户ID
     * @return 上次登录记录
     */
    JSONObject findLastLoginLogByUserId(Integer userId);
}
