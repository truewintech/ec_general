/*
 * Copy Right@
 */
package com.user.service;

import com.user.entity.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface ResourceService {

    /**
     * 获取所有菜单
     *
     * @return 菜单列表
     */
    List<Resource> findAllMenus();

    /**
     * 根据类型查询所有资源
     *
     * @param range 范围
     * @return 菜单列表
     */
    List<Resource> findAllMenusByRange(String range);

    /**
     * 获取用户菜单
     *
     * @param userId 用户id
     * @return 资源列表
     */
    List<Resource> findUserMenus(Integer userId);

    /**
     * 获取绑定菜单
     *
     * @param roleId 角色id
     * @return 菜单列表
     */
    List<Resource> findBindMenus(Integer roleId);

    /**
     * 分页查询按钮权限
     *
     * @param pageable 分页
     * @return 分页数据
     */
    Page<Resource> findBtnByPage(Pageable pageable);

    /**
     * 查找当前用户按钮权限
     *
     * @param userId 用户id
     * @return 按钮权限
     */
    Map<String, List<String>> findUserBtn(Integer userId);

    /**
     * 获取角色绑定的按钮
     *
     * @param roleId   角色id
     * @param pageable 分页
     * @return 按钮列表
     */
    Page<Resource> findBindBtn(Integer roleId, Pageable pageable);

    /**
     * 获取角色未绑定的按钮
     *
     * @param roleId   角色id
     * @param pageable 分页
     * @return 按钮列表
     */
    Page<Resource> findUnbindBtn(Integer roleId, Pageable pageable);

    /**
     * 新增权限
     *
     * @param resource 权限
     */
    void saveResource(Resource resource);

    /**
     * 删除权限
     *
     * @param ids 权限id数组
     */
    void deleteResource(Integer[] ids);

    /**
     * 更新权限
     *
     * @param resource 权限
     */
    void updateResource(Resource resource);

    /**
     * 根据权限id查询权限
     *
     * @param id 权限id
     * @return 权限
     */
    Resource findById(Integer id);

    /**
     * 启用禁用权限
     *
     * @param ids 权限id数组
     */
    void changeResourceActive(Integer[] ids);
}
