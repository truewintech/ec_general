/*
 * Copy Right@
 */
package com.user.service;

import com.alibaba.fastjson.JSONObject;
import com.common.constant.DbConst;
import com.framework.entity.TccEvent;
import com.user.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface UserService {

    /**
     * 通过id查找用户
     *
     * @param id 主键id
     * @return 用户
     */
    User findById(Integer id);

    /**
     * 通过账号查找用户
     *
     * @param account 账号
     * @return 用户
     */
    User findByAccount(String account);

    /**
     * 通过手机号查找用户
     *
     * @param mobile 手机号
     * @return 账户
     */
    User findByMobile(String mobile);

    /**
     * 分页查询用户
     *
     * @param account  账号
     * @param pageable 分页
     * @return 分页数据
     */
    Page<User> findUserByPage(String account, Pageable pageable);

    /**
     * 根据ownerId分页查询用户
     *
     * @param ownerId  拥有者id
     * @param pageable 分页
     * @return 分页数据
     */
    Page<User> findUserPageByOwnerId(Integer ownerId, Pageable pageable);

    /**
     * 新增用户
     *
     * @param user 用户信息
     * @return 用户
     */
    User addUser(User user);

    /**
     * 修改用户
     *
     * @param user 用户信息
     * @return 用户
     */
    User updateUser(User user);

    /**
     * 批量修改用户是否删除状态（软删除）
     *
     * @param ids      用户ID列表
     * @param isDelete 是否删除标识
     */
    void updateUserIsDelete(Integer[] ids, boolean isDelete);

    /**
     * 单个修改用户是否删除状态（软删除）
     *
     * @param id       用户ID
     * @param isDelete 是否删除标识
     */
    void updateUserIsDelete(Integer id, boolean isDelete);


    /**
     * 单个删除用户家（硬删除）
     *
     * @param id 用户ID
     */
    void deleteUserById(Integer id);

    /**
     * 批量操作用户开启禁用
     *
     * @param ids      用户ID列表
     * @param isActive 开启禁用标识
     */
    void updateUserIsActive(Integer[] ids, boolean isActive);

    /**
     * 单个操作用户开启禁用
     *
     * @param id       用户ID
     * @param isActive 开启禁用标识
     */
    void updateUserIsActive(Integer id, boolean isActive);

    /**
     * 修改密码
     *
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     */
    void updatePassword(String oldPassword, String newPassword);

    /**
     * 绑定用户和角色
     *
     * @param id      用户id
     * @param roleIds 角色id数组
     */
    void bindUserAndRole(Integer id, Integer[] roleIds);

    /**
     * 解绑用户和角色
     *
     * @param userId  用户id
     * @param roleIds 角色id数组
     */
    void unBindRoleAndUser(Integer userId, Integer[] roleIds);

    /**
     * 绑定用户和职位
     *
     * @param userId      用户id
     * @param positionIds 职位id数组
     */
    void bindUserAndPosition(Integer userId, Integer[] positionIds);

    /**
     * 根据手机号查询用户是否存在
     *
     * @param mobile 手机号
     * @return 是否存在的布尔值
     */
    boolean isUserExistByMobile(String mobile);

    /**
     * 解绑用户和职位
     *
     * @param userId      用户id
     * @param positionIds 职位id数组
     */
    void unBindUserAndPosition(Integer userId, Integer[] positionIds);

    /**
     * 修改验证手机号
     *
     * @param oldMobile    老手机号
     * @param newMobile    新手机号
     * @param validateCode 短信验证码
     */
    void updateMobile(String oldMobile, String newMobile, String validateCode);

    /**
     * 忘记密码
     *
     * @param mobile       手机号
     * @param validateCode 验证码
     * @param newPassword  新密码
     */
    void forgetPassword(String mobile, String validateCode, String newPassword);

    /**
     * 校验忘记密码短信验证码是否正确
     *
     * @param mobile       手机号
     * @param validateCode 验证码
     */
    void checkForgetPasswordValidateCode(String mobile, String validateCode);

    /**
     * 查询当前用户信息及上次登录时间
     *
     * @return 当前用户信息及上次登录时间
     */
    Map<String, Object> getCurrentUserInfoAndLastLoginTime();

    /**
     * 根据ID查询用户子账号及其职位
     *
     * @param userId   用户ID
     * @param pageable 分页条件
     * @return 查询用户子账号及其职位
     */
    Page<User> findSubAccountByUserId(Integer userId, Pageable pageable);

    /**
     * 系统用户登录
     *
     * @return 是否是系统用户
     */
    public Boolean login();

    /**
     * 门店注册
     *
     * @param jsonObject 门店注册参数
     * @return 是否成功
     */
    JSONObject createUser(JSONObject jsonObject);
}
