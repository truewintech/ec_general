/*
 * Copy Right@
 */
package com.user.service;

import com.alibaba.fastjson.JSONObject;
import com.framework.entity.TccEvent;
import com.user.entity.User;

/**
 * <pre>
 *
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface TccService {


    /**
     * 确认tcc事务
     *
     * @param tccEvent tcc事件
     */
    void confirmTcc(TccEvent tccEvent);

    /**
     * 取消tcc事务
     *
     * @param tccEvent tcc事件
     */
    void cancelTcc(TccEvent tccEvent);

    /**
     * 尝试新增用户
     *
     * @param userStr 用户信息
     * @return tcc事件
     */
    TccEvent tryAddUser(String userStr);

    /**
     * 尝试修改用户
     *
     * @param user 用户
     * @return tcc事件
     */
    TccEvent tryUpdateUser(User user);

}
