/*
 * Copy Right@
 */
package com.user.service;

import com.user.entity.Resource;
import com.user.entity.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface RoleService {
    /**
     * 增加角色
     *
     * @param role 角色
     */
    void saveRole(Role role);

    /**
     * 根据拥有者id增加角色
     *
     * @param role 角色
     */
    void addRoleByOwnerId(Role role);

    /**
     * 删除角色
     *
     * @param ids 角色id数组
     */
    void deleteRole(Integer[] ids);

    /**
     * 查询角色
     *
     * @param id 角色id
     * @return 角色对象
     */
    Role findById(Integer id);

    /**
     * 更新角色
     *
     * @param role 角色对象
     */
    void update(Role role);

    /**
     * 开启或禁用角色
     *
     * @param ids      角色id数组
     * @param isActive 开启或禁用状态
     */
    void changeRoleActive(Integer[] ids, Boolean isActive);

    /**
     * 分页查询角色列表
     *
     * @param pageable 分页
     * @return 角色列表
     */
    Page<Role> findRoleListByPage(Pageable pageable);

    /**
     * 根据拥有者id分页查询角色列表
     *
     * @param ownerId  拥有者id
     * @param pageable 分页
     * @return 角色列表
     */
    Page<Role> findRolePageByOwnerId(Integer ownerId, Pageable pageable);

    /**
     * 查询角色列表
     *
     * @return 角色列表
     */
    List<Role> findRoleList();

    /**
     * 查询角色列表
     *
     * @param ownerId 拥有者id
     * @return 角色列表
     */
    List<Role> findRoleListByOwnerId(Integer ownerId);

    /**
     * 绑定角色和权限
     *
     * @param id          角色id
     * @param type        类型
     * @param resourceIds 权限id数组
     */
    void bindRoleAndResource(Integer id, String type, Integer[] resourceIds);

    /**
     * 解绑角色和按钮
     *
     * @param id          角色id
     * @param resourceIds 权限id数组
     */
    void unbindRoleAndBtn(Integer id, Integer[] resourceIds);

    /**
     * 根据角色查询button权限并分页
     *
     * @param id       角色id
     * @param pageable 分页
     * @return 权限列表
     */
    Page<Resource> findBtnByRoleIdAndPage(Integer id, Pageable pageable);

    /**
     * 根据角色id和权限类型查询角色的权限
     *
     * @param id   角色id
     * @param type 类型
     * @return 权限列表
     */
    List<Resource> findResourceByRoleId(Integer id, String type);

    /**
     * 未绑定用户的角色
     *
     * @param id       用户id
     * @param pageable 分页
     * @return 角色列表
     */
    Page<Role> findUnBindRoleListByUser(Integer id, Pageable pageable);

    /**
     * 用户已经绑定了的角色
     *
     * @param id       用户id
     * @param pageable 分页
     * @return 角色列表
     */
    Page<Role> findRoleListByUser(Integer id, Pageable pageable);
}
