/*
 * Copy Right@
 */
package com.user.service.impl;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.common.constant.Const;
import com.common.constant.DbConst;
import com.framework.constant.DbConstList;
import com.framework.data.redis.RedisManager;
import com.framework.entity.Event;
import com.framework.exception.MessageException;
import com.framework.repository.EventRepository;
import com.framework.util.security.DesEdeUtil;
import com.user.amqp.Sender;
import com.user.entity.Sharing;
import com.user.entity.User;
import com.user.entity.UserWeChat;
import com.user.repository.UserWeChatRepository;
import com.user.service.UserService;
import com.user.service.UserWeChatService;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import me.chanjar.weixin.mp.bean.result.WxMpUserList;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class UserWeChatServiceImpl implements UserWeChatService {

    /**
     * 缓存服务
     */
    @Autowired
    private RedisManager redisManager;

    /**
     * 微信id
     */
    @Value("${weChat.appId}")
    private String appId;

    /**
     * 微信密码
     */
    @Value("${weChat.appSecret}")
    private String appSecret;

    /**
     * token校验key
     */
    @Value("${auth.secretKey}")
    private String authSecretKey;

    /**
     * token有效时长(天)
     */
    @Value("${auth.expiredTime}")
    private Integer authExpiredTime;

    /**
     * 微信首页
     */
    @Value("${weChat.redirectUrl}")
    private String redirectUrl;

    /**
     * 缓存管理器
     */
    @Autowired
    private ValueOperations<String, Object> valueOperations;

    /**
     * 账户数据仓库
     */
    @Autowired
    private UserWeChatRepository userWeChatRepository;

    /**
     * 用户服务
     */
    @Autowired
    private UserService userService;

    /**
     * 微信服务
     */
    @Autowired
    private WxMpService wxMpService;

    /**
     * 微信小程序服务
     */
    @Autowired
    private WxMaService wxMaService;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private Sender sender;


    @Override
    public Map<String, Object> getUserByCode(String code) {
        Map<String, Object> result = new HashMap<>(20);

        if (StringUtils.isBlank(code)) {
            log.error("依据code获取用户信息失败");
            throw new MessageException("系统异常");
        }
        //依据code调用微信接口获取openId
        String openId = getOpenIdByCode(code);

        //依据appId和openId获取用户信息
        User user = getUserByOpenId(openId);

        //用户不为空时，将用户信息及生成的token传到前端
        if (user != null) {
            //获取token中需存储的用户信息
            String token = getToken(user, openId);
            result.put("token", token);
            result.put("user", getUserInfoForPage(user));
        }

        result.put("openId", openId);
        result.put("appId", appId);
        return result;
    }

    @Override
    public Map<String, Object> getMiniappInfoByCode(String code) {
        Map<String, Object> result = new HashMap<>(10);
        if (StringUtils.isBlank(code)) {
            log.error("依据code获取用户信息失败");
            throw new MessageException("系统异常");
        }
        //微信小程序依据code调用微信接口获取openId
        try {
            // 获取openId和token，scope为snsapi_base即可
            WxMaJscode2SessionResult sessionResult = wxMaService.getUserService().getSessionInfo(code);
            // 获取用户信息，前台取code时scope需要设置为snsapi_userinfo（需要用户授权）
            String openId = sessionResult.getOpenid();
            if (StringUtils.isBlank(openId)) {
                log.error("依据code:{},获取微信用户信息失败, openId为空，返回数据为：{}", code, JSON.toJSONString(sessionResult));
                throw new MessageException("系统异常");
            }
            result.put("openId", openId);
        } catch (WxErrorException e) {
            log.error("依据code:{},获取微信用户信息失败", code, e);
            throw new MessageException("系统异常");
        }
        return result;
    }

    /**
     * 拼装需要存入token中的信息
     *
     * @param user   用户信息
     * @param openId 微信openId
     * @return 用户信息
     */
    private Map<String, Object> getTokenInfo(User user, String openId) {
        if (user == null) {
            log.error("获取用户Token信息失败，用户为空");
            throw new MessageException("系统异常");
        }
        //存储部分用户信息到token中
        Map<String, Object> tokenInfo = new HashMap<>(20);
        tokenInfo.put("userId", user.getId());
        tokenInfo.put("mobile", user.getMobile());
        tokenInfo.put("appId", appId);
        tokenInfo.put("openId", openId);

        //设置token超时时间
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(Calendar.DATE, authExpiredTime);
        tokenInfo.put("expiredTime", nowTime.getTime());
        return tokenInfo;
    }

    /**
     * 依据用户信息生成token
     *
     * @param tokenInfo 用户信息
     * @return 生成的token
     */
    private String getToken(Map<String, Object> tokenInfo) {
        //设置token超时时间
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(Calendar.DATE, authExpiredTime);
        tokenInfo.put("expiredTime", nowTime.getTime());
        String tokenInfoJson = JSON.toJSONString(tokenInfo);
        try {
            return DesEdeUtil.encrypt(authSecretKey.getBytes(), tokenInfoJson);
        } catch (Exception e) {
            log.error("创建Token失败，参数{}", tokenInfoJson);
            throw new MessageException("系统异常");
        }
    }

    /**
     * 依据openId获取用户信息
     *
     * @param openId openId
     * @return 用户信息
     */
    private User getUserByOpenId(String openId) {
        if (StringUtils.isBlank(openId)) {
            log.error("依据openId及appId获取用户信息失败，openId为空");
            throw new MessageException("系统异常");
        }

        UserWeChat userWeChat = userWeChatRepository.findByAppIdAndOpenId(appId, openId);
        //没找到用户微信信息，证明没关注过
        if (userWeChat == null) {
            return null;
        }

        User user = userWeChat.getUser();
        //没找到用户信息，证明关注了，没绑定手机号
        if (user == null) {
            return null;
        }

        return user;
    }

    /**
     * 筛选需要传到前端的用户信息
     *
     * @param user 用户
     * @return 部分用户信息
     */
    private Map<String, Object> getUserInfoForPage(User user) {
        if (user == null) {
            log.error("获取前端用户信息失败，用户为空");
            throw new MessageException("系统异常");
        }
        Map<String, Object> userInfo = new HashMap<>(20);
        userInfo.put("mobile", user.getMobile());
     //   userInfo.put("id", user.getId());
        return userInfo;
    }

    /**
     * code获取openId
     *
     * @param code 微信code
     * @return 微信openId
     */
    private String getOpenIdByCode(String code) {
        if (StringUtils.isBlank(code)) {
            log.error("依据code获取openId失败，code为空");
            throw new MessageException("系统异常");
        }
        try {
            // 获取openId和token，scope为snsapi_base即可
            WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
            // 获取用户信息，前台取code时scope需要设置为snsapi_userinfo（需要用户授权）
            String openId = wxMpOAuth2AccessToken.getOpenId();
            if (StringUtils.isBlank(openId)) {
                log.error("依据code:{},获取微信用户信息失败, openId为空，返回数据为：{}", code, JSON.toJSONString(wxMpOAuth2AccessToken));
                throw new MessageException("系统异常");
            }
            return openId;
        } catch (WxErrorException e) {
            log.error("依据code:{},获取微信用户信息失败", code, e);
            throw new MessageException("系统异常");
        }
    }

    @Override
    public void handleEvent(HttpServletRequest request, HttpServletResponse response) {
        boolean isGet = "get".equals(request.getMethod().toLowerCase());
        if (isGet) {
            String echoStr = request.getParameter("echostr");
            try {
                response.getWriter().write(echoStr);
            } catch (IOException e) {
                log.error("接收处理微信事件推送失败:{}", e);
            }
        } else {
            // 进入POST时间处理
            try {
                // 接收消息并返回消息
                acceptMessage(request);
            } catch (Exception e) {
                log.error("接收处理微信事件推送失败:{}", e);
                throw new MessageException("接收处理微信事件推送失败");
            }
        }
    }

    @Override
    @Transactional
    public Map<String, Object> bindMobile(Map<String, Object> userMap) {
        //校验
        validBindMobile(userMap);
        String openId = (String) userMap.get("openId");
        String mobile = (String) userMap.get("mobile");
        if (StringUtils.isBlank(mobile)) {
            log.error("绑定手机号失败，手机号为空");
            throw new MessageException("绑定手机号失败，请重试");
        }

        //依据手机号查询用户，如果查询不到则插入用户信息
        User user = userService.findByMobile(mobile);
        if (user == null) {
            user = new User();
            user.setAccount(mobile);
            user.setMobile(mobile);
            user.setIsActive(true);
            user = userService.addUser(user);
        }

        //appId和openId都存在时，更新微信信息中的用户Id
        if (StringUtils.isNotBlank(openId)) {
            //依据openId及appId查找微信信息记录
            UserWeChat userWeChat = userWeChatRepository.findByAppIdAndOpenId(appId, openId);
            //找不到微信信息时，认为是注册，保存微信信息
            if (userWeChat == null) {
                throw new MessageException("绑定手机号失败，请重新关注公众号");
            }

            userWeChat.setUser(user);
            userWeChatRepository.save(userWeChat);
        } else {
            //openId不存在时，抛出异常
            log.error("微信openId为空，手机号：{}", mobile);
            throw new MessageException("绑定手机号失败，请重试");
        }

        Map<String, Object> result = new HashMap<>(20);
        result.put("openId", openId);
        result.put("appId", appId);

        //用户不为空时，将用户信息及生成的token传到前端
        if (user != null) {
            String token = getToken(user, openId);
            result.put("token", token);
            result.put("user", getUserInfoForPage(user));
        }

        //清除短信缓存
        String key = Const.CacheKey.SMS_VERIFICATION_CODE_REGISTER_PRE + mobile;
        redisManager.delCache(key);
        return result;
    }

    @Override
    public UserWeChat getWeChatInfoByUserId(Integer userId) {
        User user = new User();
        user.setId(userId);
        return userWeChatRepository.findByUser(user);
    }

    @Override
    public void saveSubscribedWechatUser(String token) {
        //递归查询已关注用户
        saveSubscribedWechatUserByNextOpenId(token);
        log.info("保存已关注用户信息执行完成");
    }

    /**
     * 保存已关注用户信息
     *
     * @param token 微信access token
     */
    private void saveSubscribedWechatUserByNextOpenId(String token) {
        if (StringUtils.isBlank(token)) {
            log.error("保存已关注用户信息失败，token为空");
            return;
        }
        try {
            WxMpUserList wxMpUserList = wxMpService.getUserService().userList(null);
            List<String> openIds = wxMpUserList.getOpenids();
            if (openIds != null) {
                for (String openId : openIds) {
                    WxMpUser wxMpUser = wxMpService.getUserService().userInfo(openId, "zh_CN");
                    if (wxMpUser == null) {
                        log.error("openId:{}，获取微信用户信息失败", openId);
                        return;
                    }
                    UserWeChat userWeChat = userWeChatRepository.findByAppIdAndOpenId(appId, openId);
                    //调用获取微信用户信息接口保存或更新微信信息
                    if (userWeChat == null) {
                        userWeChat = new UserWeChat();
                    }
                    userWeChat.setAppId(appId);
                    userWeChat.setSubscribe(wxMpUser.getSubscribe() ? 1 : 0);
                    userWeChat.setOpenId(wxMpUser.getOpenId());
                    userWeChat.setNickName(wxMpUser.getNickname());
                    userWeChat.setSex(wxMpUser.getSex());
                    userWeChat.setCity(wxMpUser.getCity());
                    userWeChat.setCountry(wxMpUser.getCountry());
                    userWeChat.setProvince(wxMpUser.getProvince());
                    userWeChat.setLanguage(wxMpUser.getLanguage());
                    userWeChat.setHeadImageUrl(wxMpUser.getHeadImgUrl());
                    userWeChat.setSubscribeTime(wxMpUser.getSubscribeTime().intValue());
                    userWeChat.setUnionId(wxMpUser.getUnionId());
                    userWeChat.setRemark(wxMpUser.getRemark());
                    userWeChat.setGroupId(wxMpUser.getGroupId());
                    userWeChat.setTagIdList(JSONArray.toJSONString(wxMpUser.getTagIds()));
                    userWeChatRepository.save(userWeChat);
                }
            }
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public Map<String, Object> wechatLogin(String code) {
        if (StringUtils.isBlank(code)) {
            log.error("微信登录失败，code为空");
            throw new MessageException("系统异常");
        }
        try {
            // 获取openId和token，scope为snsapi_base即可
            WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
            // 获取用户信息，前台取code时scope需要设置为snsapi_userinfo（需要用户授权）
            String openId = wxMpOAuth2AccessToken.getOpenId();
            if (StringUtils.isBlank(openId)) {
                log.error("依据code:{},获取微信用户信息失败, openId为空，返回数据为：{}", code, JSON.toJSONString(wxMpOAuth2AccessToken));
                throw new MessageException("系统异常");
            }
            //查询当前用户是否已存在
            UserWeChat userWeChat = userWeChatRepository.findByAppIdAndOpenId(appId, openId);
            //构建返回参数
            Map<String, Object> map = new HashMap<>();
            //如果当前用户第一次微信授权
            if (userWeChat == null) {
                //生成user表数据
                User user = new User();
                user.setAccount(openId);
                user.setIsActive(true);
                user = userService.addUser(user);
                //插入微信信息
                WxMpUser wxMpUser = wxMpService.getUserService().userInfo(openId, "zh_CN");
                userWeChat = new UserWeChat();
                userWeChat.setUser(user);
                userWeChat.setAppId(appId);
                userWeChat.setSubscribe(wxMpUser.getSubscribe() ? 1 : 0);
                userWeChat.setOpenId(wxMpUser.getOpenId());
                userWeChat.setNickName(wxMpUser.getNickname());
                userWeChat.setSex(wxMpUser.getSex());
                userWeChat.setCity(wxMpUser.getCity());
                userWeChat.setCountry(wxMpUser.getCountry());
                userWeChat.setProvince(wxMpUser.getProvince());
                userWeChat.setLanguage(wxMpUser.getLanguage());
                userWeChat.setHeadImageUrl(wxMpUser.getHeadImgUrl());
                userWeChat.setSubscribeTime(wxMpUser.getSubscribeTime().intValue());
                userWeChat.setUnionId(wxMpUser.getUnionId());
                userWeChat.setRemark(wxMpUser.getRemark());
                userWeChat.setGroupId(wxMpUser.getGroupId());
                userWeChat.setTagIdList(JSONArray.toJSONString(wxMpUser.getTagIds()));
                userWeChat = userWeChatRepository.save(userWeChat);
                //生成token
                String token = getToken(user, openId);
                map.put("token", token);
                map.put("user", getUserInfoForPage(user));
                //构建 同步参数
                JSONObject eventDate = new JSONObject();
                eventDate.put("id", user.getId());
                eventDate.put("herdUrl", wxMpUser.getHeadImgUrl());
                eventDate.put("name", wxMpUser.getNickname());
                //构建 event
                Event event = new Event();
                //发送给buyer服务
                event.setSender(DbConst.Event.SenderOrReceiver.USER_SERVICE);
                event.setEventType(DbConst.Event.EventType.ToEcBuyer.ADD_BUYER_FROM_WECHAT);
                event.setReceiver(DbConst.Event.SenderOrReceiver.EC_BUYER_SERVICE);
                event.setStatus(DbConstList.Event.Status.NEW);
                event.setPayload(JSON.toJSONString(eventDate));
                eventRepository.save(event);
            } else {
                String token = getToken(userWeChat.getUser(), openId);
                map.put("token", token);
                map.put("user", getUserInfoForPage(userWeChat.getUser()));
            }
            WxJsapiSignature jsapiSignature = wxMpService.createJsapiSignature(redirectUrl);
            map.put("openid", openId);
            map.put("access_token", wxMpOAuth2AccessToken.getAccessToken());
            map.put("jsapiSignature", jsapiSignature);
            map.put("userWeChat", userWeChat);
            return map;
        } catch (WxErrorException e) {
            log.error("依据code:{},获取微信用户信息失败", code, e);
            throw new MessageException("系统异常");
        }
    }

    @Override
    @Transactional
    public Map<String, Object> wechatLoginByCode(String code) {
        if (StringUtils.isBlank(code)) {
            log.error("微信登录失败，code为空");
            throw new MessageException("系统异常");
        }
        try {
            // 获取openId和token，scope为snsapi_base即可
            WxMpOAuth2AccessToken wxMpOAuth2AccessToken = wxMpService.oauth2getAccessToken(code);
            // 获取用户信息，前台取code时scope需要设置为snsapi_userinfo（需要用户授权）
            String openId = wxMpOAuth2AccessToken.getOpenId();
            if (StringUtils.isBlank(openId)) {
                log.error("依据code:{},获取微信用户信息失败, openId为空，返回数据为：{}", code, JSON.toJSONString(wxMpOAuth2AccessToken));
                throw new MessageException("系统异常");
            }
            //查询当前用户是否已存在
            UserWeChat userWeChat = userWeChatRepository.findByAppIdAndOpenId(appId, openId);
            //构建返回参数
            Map<String, Object> map = new HashMap<>();
            map.put("isMoblie", false);
            if (userWeChat == null) {
                //插入微信信息
                WxMpUser wxMpUser = wxMpService.getUserService().userInfo(openId, "zh_CN");
                userWeChat = new UserWeChat();
                userWeChat.setAppId(appId);
                userWeChat.setSubscribe(wxMpUser.getSubscribe() ? 1 : 0);
                userWeChat.setOpenId(wxMpUser.getOpenId());
                userWeChat.setNickName(wxMpUser.getNickname());
                userWeChat.setSex(wxMpUser.getSex());
                userWeChat.setCity(wxMpUser.getCity());
                userWeChat.setCountry(wxMpUser.getCountry());
                userWeChat.setProvince(wxMpUser.getProvince());
                userWeChat.setLanguage(wxMpUser.getLanguage());
                userWeChat.setHeadImageUrl(wxMpUser.getHeadImgUrl());
                userWeChat.setSubscribeTime(wxMpUser.getSubscribeTime().intValue());
                userWeChat.setUnionId(wxMpUser.getUnionId());
                userWeChat.setRemark(wxMpUser.getRemark());
                userWeChat.setGroupId(wxMpUser.getGroupId());
                userWeChat.setTagIdList(JSONArray.toJSONString(wxMpUser.getTagIds()));
                userWeChat = userWeChatRepository.save(userWeChat);
            } else {
                User user = userWeChat.getUser();
                //以手机号为依据 判断是否下发token  下发分享
                if (user != null && StringUtils.isNotBlank(user.getMobile())) {
                    map.put("token", getToken(user, openId));
                    map.put("user", getUserInfoForPage(userWeChat.getUser()));
                    map.put("isMoblie", true);
                }
            }
            WxJsapiSignature jsapiSignature = wxMpService.createJsapiSignature(redirectUrl);
            map.put("jsapiSignature", jsapiSignature);
            map.put("openid", openId);
            map.put("access_token", wxMpOAuth2AccessToken.getAccessToken());
            map.put("userWeChat", userWeChat);
            return map;
        } catch (WxErrorException e) {
            log.error("依据code:{},获取微信用户信息失败", code, e);
            throw new MessageException("系统异常");
        }
    }

    @Override
    @Transactional
    public Map<String, Object> wechatBindMobile(Map<String, Object> map) {
        //验证手机号
        validBindMobile(map);
        String openId = (String) map.get("openId");
        String mobile = (String) map.get("mobile");
        String refereeOpenid = map.containsKey("refereeOpenid") ? ((String) map.get("refereeOpenid")) : null;
        if (StringUtils.isBlank(mobile)) {
            log.error("绑定手机号失败，手机号为空");
            throw new MessageException("绑定手机号失败，请重试");
        }
        if (StringUtils.isBlank(openId)) {
            log.error("绑定手机号失败，openid为空");
            throw new MessageException("绑定手机号失败，请重试");
        }
        UserWeChat userWeChat = userWeChatRepository.findByAppIdAndOpenId(appId, openId);
        if (userWeChat == null) {
            log.error("绑定手机号失败：未查找到 openid：{} 的wechat信息 ");
            throw new MessageException("绑定手机号失败，请重试");
        }
        //先查看用户是否已经绑定过
        User user = userWeChat.getUser();
        if (user != null && StringUtils.isNotBlank(user.getMobile())) {
            log.error("绑定手机号失败，openid：{} 的用户已绑定手机号");
            throw new MessageException("您已绑定手机号，请不要重复操作");
        }
        //再查看当前手机号是否有用户数据
        user = userService.findByMobile(mobile);
        if (user == null) {
            user = new User();
            user.setAccount(mobile);
            user.setIsActive(true);
            user.setMobile(mobile);
            userService.addUser(user);
        }
        userWeChat.setUser(user);
        userWeChat = userWeChatRepository.save(userWeChat);
        //判断是否有推荐人信息
        if (StringUtils.isNotBlank(refereeOpenid)) {
            UserWeChat refereeUserWechat = userWeChatRepository.findByAppIdAndOpenId(appId, refereeOpenid);
            //获取推荐人信息
            if (refereeUserWechat == null || refereeUserWechat.getUser() == null) {
                log.error("获取推荐人信息失败 推荐人openid:{}", refereeOpenid);
            } else {
                //保存推荐信息
                Sharing sharing = new Sharing();
                sharing.setSharerId(refereeUserWechat.getUser().getId());
                sharing.setBeSharedId(user.getId());
                sender.addSharing(sharing);
                // sharingRespository.save(sharing);
            }
        }
        //构建 同步参数
        JSONObject eventDate = new JSONObject();
        eventDate.put("id", user.getId());
        eventDate.put("herdUrl", userWeChat.getHeadImageUrl());
        eventDate.put("name", userWeChat.getNickName());
        eventDate.put("mobile", mobile);
        //构建 event
        Event event = new Event();
        //发送给buyer服务
        event.setSender(DbConst.Event.SenderOrReceiver.USER_SERVICE);
        event.setEventType(DbConst.Event.EventType.ToEcBuyer.ADD_BUYER_FROM_WECHAT);
        event.setReceiver(DbConst.Event.SenderOrReceiver.EC_BUYER_SERVICE);
        event.setStatus(DbConstList.Event.Status.NEW);
        event.setPayload(JSON.toJSONString(eventDate));
        eventRepository.save(event);
        Map<String, Object> returnMap = new HashMap<>();
        String token = getToken(userWeChat.getUser(), openId);
        returnMap.put("token", token);
        returnMap.put("user",

                getUserInfoForPage(user));
        returnMap.put("openid", openId);
        try {
            WxJsapiSignature jsapiSignature = wxMpService.createJsapiSignature(redirectUrl);
            returnMap.put("jsapiSignature", jsapiSignature);
        } catch (
                WxErrorException e) {
            log.error("绑定手机号后获取分享签名失败", e);
        } finally {
            return returnMap;
        }

    }

    @Override
    public Map<String, String> getAccessToken() {
        Map<String, String> map = new HashMap<>();
        try {
            String token = wxMpService.getAccessToken();
            map.put("accessToken", token);
            return map;
        } catch (Exception e) {
            log.error("获取AccessToken失败", e);
            return null;
        }
    }

    /**
     * 依据用户信息及openI获取token
     *
     * @param user   用户信息
     * @param openId 微信openId
     * @return token
     */
    private String getToken(User user, String openId) {
        //获取token中需存储的用户信息
        Map<String, Object> tokenInfo = getTokenInfo(user, openId);
        //获取token
        String token = getToken(tokenInfo);
        //token存入缓存（用appId和openId来查找）
        valueOperations.set(Const.CacheKey.WECHAT_LOGIN_TOKEN_PRE + appId + "_" + openId, token,
                authExpiredTime, TimeUnit.DAYS);
        //以token为key,将用户信息存入缓存
        valueOperations.set(Const.CacheKey.WECHAT_LOGIN_USER_INFO_PRE + token, tokenInfo,
                authExpiredTime, TimeUnit.DAYS);
        return token;
    }

    /**
     * 校验绑定手机号
     *
     * @param user 客户信息
     */
    private void validBindMobile(Map<String, Object> user) {
        String mobile = (String) user.get("mobile");
        if (StringUtils.isBlank(mobile)) {
            log.error("绑定手机号失败，手机号为空");
            throw new MessageException("绑定手机号失败，请重试");
        }
        String msgCode = (String) user.get("msgCode");
        if (StringUtils.isBlank(msgCode)) {
            log.error("绑定手机号失败，短信验证码为空");
            throw new MessageException("绑定手机号失败，请重试");
        }

        //校验短信验证码
        String key = Const.CacheKey.SMS_VERIFICATION_CODE_REGISTER_PRE + mobile;
        Map<String, Object> registerValidCodeMap = redisManager.getCache(key);

        if (registerValidCodeMap == null) {
            throw new MessageException("验证码校验失败，请重新获取");
        }
        Object object = registerValidCodeMap.get(Const.RegistrationCode.SEND_TIME_KEY);
        if (object == null) {
            throw new MessageException("验证码校验失败，请重新获取");
        }
        Date sendTime = (Date) object;
        int millis = 1000;
        if (System.currentTimeMillis() - sendTime.getTime() > millis * Const.RegistrationCode.OVERTIME) {
            throw new MessageException("验证码已失效，请重新获取");
        }

        String registerValidCode = (String) registerValidCodeMap.get("validCode");
        if (!StringUtils.equalsIgnoreCase(registerValidCode, msgCode)) {
            throw new MessageException("请输入正确的验证码");
        }
    }

    /**
     * 接收消息处理消息
     *
     * @param request 请求
     * @throws Exception 异常
     */
    private void acceptMessage(HttpServletRequest request) throws Exception {
        // xml请求解析
        WxMpXmlMessage message = WxMpXmlMessage.fromXml(request.getInputStream());
        String event = message.getEvent();
        if (StringUtils.isBlank(event)) {
//            handleUserMessage(message);
        } else if (StringUtils.equals(WxConsts.EventType.SUBSCRIBE, event)) {
            handleSubscribeEvent(message);
        } else if (StringUtils.equals(WxConsts.EventType.CLICK, event)) {
//            handlerClickEvent(message);
        } else if (StringUtils.equals(WxConsts.EventType.SCAN, event)) {
//            handlerScanEvent(message);
        }
    }

    /**
     * 处理关注事件
     *
     * @param message 事件
     */
    private void handleSubscribeEvent(WxMpXmlMessage message) {
        if (message == null) {
            log.error("处理微信关注事件失败，请求体为空");
            return;
        }
        String openId = message.getFromUser();
        String weChatCode = message.getToUser();
        if (StringUtils.isBlank(weChatCode)) {
            log.error("处理微信关注事件失败，微信号为空");
            return;
        }

        //依据appId和openId查找是否已保存微信信息
        UserWeChat userWeChat = userWeChatRepository.findByAppIdAndOpenId(appId, openId);

        //调用获取微信用户信息接口保存或更新微信信息
        if (userWeChat == null) {
            userWeChat = new UserWeChat();
        }

        String accessToken = redisManager.getCache(Const.CacheKey.WECHAT_ACCESS_TOKEN_PRE + appId);
        if (StringUtils.isBlank(accessToken)) {
            log.error("依据appId:{}，从缓存中获取微信accessToken失败", appId);
            return;
        }

        try {
            WxMpUser user = wxMpService.getUserService().userInfo(openId, "zh_CN");
            if (user == null) {
                log.error("openId:{}，获取微信用户信息失败", openId);
                return;
            }
            userWeChat.setAppId(appId);
            userWeChat.setSubscribe(user.getSubscribe() ? 1 : 0);
            userWeChat.setOpenId(user.getOpenId());
            userWeChat.setNickName(user.getNickname());
            userWeChat.setSex(user.getSex());
            userWeChat.setCity(user.getCity());
            userWeChat.setCountry(user.getCountry());
            userWeChat.setProvince(user.getProvince());
            userWeChat.setLanguage(user.getLanguage());
            userWeChat.setHeadImageUrl(user.getHeadImgUrl());
            userWeChat.setSubscribeTime(user.getSubscribeTime().intValue());
            userWeChat.setUnionId(user.getUnionId());
            userWeChat.setRemark(user.getRemark());
            userWeChat.setGroupId(user.getGroupId());
            userWeChat.setTagIdList(JSONArray.toJSONString(user.getTagIds()));
            userWeChatRepository.save(userWeChat);
        } catch (WxErrorException e) {
            log.error("依据openId:{}获取用户信息失败", openId, e);
            throw new MessageException("系统异常");
        }
    }
}
