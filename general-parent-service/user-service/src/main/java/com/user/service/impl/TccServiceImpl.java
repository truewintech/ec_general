/*
 * Copy Right@
 */
package com.user.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.common.constant.DbConst;
import com.framework.constant.DbConstList;
import com.framework.entity.TccEvent;
import com.framework.exception.MessageException;
import com.framework.repository.TccEventRepository;
import com.user.amqp.Sender;
import com.user.entity.Sharing;
import com.user.entity.User;
import com.user.entity.UserWeChat;
import com.user.repository.UserRepository;
import com.user.repository.UserWeChatRepository;
import com.user.service.TccService;
import com.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class TccServiceImpl implements TccService {

    /**
     * 用户服务
     */
    @Autowired
    private UserService userService;

    /**
     * 用户仓库
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * tcc事件仓库
     */
    @Autowired
    private TccEventRepository tccEventRepository;

    /**
     * 用户服务地址
     */
    @Value("${serviceUrl.general.user}")
    private String userServiceUrl;

    @Autowired
    private Sender sender;

    /**
     * 微信用户仓库
     */
    @Autowired
    private UserWeChatRepository userWeChatRepository;

    @Override
    @Transactional
    public void confirmTcc(TccEvent tccEvent) {
        if (tccEvent == null) {
            log.error("confirmTcc时参数错误，tccEvent为空");
            throw new MessageException("操作失败，请重试");
        }
        // 判断Tcc事件类型
        String tccType = tccEvent.getTccType();
        if (StringUtils.isBlank(tccType)) {
            log.error("confirmTcc时参数错误，tccEvent对象中tccType为空");
            throw new MessageException("操作失败，请重试");
        }
        if (DbConst.TccEvent.TccType.ADD_USER.equals(tccType)) {
            confirmAddUser(tccEvent);
        } else if (DbConst.TccEvent.TccType.UPDATE_USER.equals(tccType)) {
            confirmUpdateUser(tccEvent);
        } else if (DbConst.TccEvent.TccType.STORE_REGISTER.equals(tccType)) {
            confirmUserStoreRegister(tccEvent);
        } else {
            log.error("confirmTcc时参数错误，Tcc类型不存在于当前预置类型中，无法处理。TccEventId：{}", tccEvent.getId());
            throw new MessageException("操作失败，请重试");
        }
    }


    @Override
    @Transactional
    public void cancelTcc(TccEvent tccEvent) {
        if (tccEvent == null) {
            log.error("cancelTcc时参数错误，tccEvent为空");
            throw new MessageException("操作失败，请重试");
        }
        // 判断Tcc事件类型
        String tccType = tccEvent.getTccType();
        if (StringUtils.isBlank(tccType)) {
            log.error("cancelTcc时参数错误，tccEvent对象中tccType为空");
            throw new MessageException("操作失败，请重试");
        }
        if (DbConst.TccEvent.TccType.ADD_USER.equals(tccType)) {
            cancelAddUser(tccEvent);
        } else if (DbConst.TccEvent.TccType.UPDATE_USER.equals(tccType)) {
            cancelUpdateUser(tccEvent);
        } else if (DbConst.TccEvent.TccType.STORE_REGISTER.equals(tccType)) {
           cancelUserStoreRegister(tccEvent);
        } else {
            log.error("cancelTcc时参数错误，Tcc类型不存在于当前预置类型中，无法处理。TccEventId：{}", tccEvent.getId());
            throw new MessageException("操作失败，请重试");
        }
    }

    @Override
    @Transactional
    public TccEvent tryAddUser(String userStr) {
        JSONObject userJson = JSONObject.parseObject(userStr);
        User user = JSON.toJavaObject(userJson, User.class);
        if (user == null) {
            log.error("tryAddUser时参数错误，user为空");
            throw new MessageException("操作失败，请稍后重试");
        }
        user = userService.addUser(user);
        Integer userId = user.getId();
        if (userId == null) {
            log.error("tryAddUser时参数错误，userId为空");
            throw new MessageException("操作失败，请稍后重试");
        }

        //try add user时添加推荐关系记录到tccEvent中
        String sharerId = userJson.getString("sharerId");
        JSONObject tccparam = new JSONObject();
        tccparam.put("sharerId",sharerId);
        tccparam.put("userId",userId);

        TccEvent tccEvent = new TccEvent();
        // 记录tcc事件
        tccEvent.setPayload(tccparam.toJSONString());
        tccEvent.setServiceId(userServiceUrl);
        tccEvent.setStatus(DbConstList.TccEvent.Status.TRY);
        tccEvent.setTccType(DbConst.TccEvent.TccType.ADD_USER);
        return tccEventRepository.save(tccEvent);
    }

    @Override
    @Transactional
    public TccEvent tryUpdateUser(User user) {
        if (user == null) {
            log.error("tryUpdateUser时参数错误，user为空");
            throw new MessageException("操作失败，请稍后重试");
        }
        Integer userId = user.getId();
        if (userId == null) {
            log.error("tryUpdateUser时参数错误，userId为空");
            throw new MessageException("操作失败，请稍后重试");
        }
        User oldUser = userService.findById(userId);
        userService.updateUser(user);
        TccEvent tccEvent = new TccEvent();
        // 记录tcc事件
        tccEvent.setPayload(JSONObject.toJSONString(oldUser));
        tccEvent.setServiceId(userServiceUrl);
        tccEvent.setStatus(DbConstList.TccEvent.Status.TRY);
        tccEvent.setTccType(DbConst.TccEvent.TccType.UPDATE_USER);
        return tccEventRepository.save(tccEvent);
    }


    /**
     * 确认新增用户
     *
     * @param tccEvent tccEvent实体
     */
    private void confirmAddUser(TccEvent tccEvent) {
        if (tccEvent == null) {
            log.error("confirmAddUser时参数错误，tccEvent为空");
            throw new MessageException("操作失败，请稍后重试");
        }
        Integer tccEventId = tccEvent.getId();
        if (tccEventId == null) {
            log.error("confirmAddUser时参数错误，tccEventId为空");
            throw new MessageException("操作失败，请稍后重试");
        }
        TccEvent completeTccEvent = tccEventRepository.findOne(tccEventId);
        if (completeTccEvent == null) {
            log.error("confirmAddUser时参数错误，tccEvent不存在，尝试查找的tccEvent：{}", tccEventId);
            throw new MessageException("操作失败，请稍后重试");
        }
        // 只处理状态为TRY的TCC事件
        String tccEventStatus = completeTccEvent.getStatus();
        if (StringUtils.isBlank(tccEventStatus)) {
            log.error("confirmAddUser时参数错误，tccEvent中tccEventStatus为空，tccEventId:{}", tccEventId);
            throw new MessageException("操作失败，请稍后重试");
        }
        if (DbConstList.TccEvent.Status.TRY.equals(tccEventStatus)) {
            // 将用户设为有效
            String payload = completeTccEvent.getPayload();
            if (StringUtils.isBlank(payload)) {
                log.error("confirmAddUser时参数错误，payload为空");
                throw new MessageException("操作失败，请稍后重试");
            }
            JSONObject userShareInfo = JSONObject.parseObject(payload);
            Integer userId = userShareInfo.getInteger("userId");
            userService.updateUserIsActive(userId, true);

            //添加推荐关系消息通知
            String sharerId = userShareInfo.getString("sharerId");
            if(StringUtils.isNotEmpty(sharerId)){
                User share = userRepository.findByAccount(sharerId);
                if(share != null){
                    Sharing sharing = new Sharing();
                    sharing.setSharerId(share.getId());
                    sharing.setBeSharedId(userId);

                    sender.addSharing(sharing);
                }
            }

            // 记录confirm事件
            completeTccEvent.setStatus(DbConstList.TccEvent.Status.CONFIRM);
            tccEventRepository.save(completeTccEvent);
        }
    }

    /**
     * 确认修改用户
     *
     * @param tccEvent tccEvent实体
     */
    private void confirmUpdateUser(TccEvent tccEvent) {
        if (tccEvent == null) {
            log.error("confirmUpdateUser时参数错误，tccEvent为空");
            throw new MessageException("操作失败，请稍后重试");
        }
        Integer tccEventId = tccEvent.getId();
        if (tccEventId == null) {
            log.error("confirmUpdateUser时参数错误，tccEventId为空");
            throw new MessageException("操作失败，请稍后重试");
        }
        TccEvent completeTccEvent = tccEventRepository.findOne(tccEventId);
        if (completeTccEvent == null) {
            log.error("confirmUpdateUser时参数错误，tccEvent不存在，尝试查找的tccEvent：{}", tccEventId);
            throw new MessageException("操作失败，请稍后重试");
        }
        // 只处理状态为TRY的TCC事件
        String tccEventStatus = completeTccEvent.getStatus();
        if (StringUtils.isBlank(tccEventStatus)) {
            log.error("confirmUpdateUser时参数错误，tccEvent中tccEventStatus为空，tccEventId:{}", tccEventId);
            throw new MessageException("操作失败，请稍后重试");
        }
        if (DbConstList.TccEvent.Status.TRY.equals(tccEventStatus)) {
            // 记录confirm事件
            completeTccEvent.setStatus(DbConstList.TccEvent.Status.CONFIRM);
            tccEventRepository.save(completeTccEvent);
        }
    }

    /**
     * 取消新增用户
     *
     * @param tccEvent tccEvent实体
     */
    private void cancelAddUser(TccEvent tccEvent) {
        if (tccEvent == null) {
            log.error("cancelAddUser时参数错误，tccEvent为空");
            throw new MessageException("操作失败，请重试");
        }
        Integer tccEventId = tccEvent.getId();
        if (tccEventId == null) {
            log.error("参数错误，tccEventId为空");
            throw new MessageException("操作失败，请重试");
        }
        TccEvent completeTccEvent = tccEventRepository.findOne(tccEventId);
        if (completeTccEvent == null) {
            log.error("cancelAddUser时参数错误，tccEvent不存在，尝试查找的tccEventId：{}", tccEventId);
            throw new MessageException("操作失败，请稍后重试");
        }
        // 只处理状态为不为CANCEL的事件
        String tccEventStatus = completeTccEvent.getStatus();
        if (StringUtils.isBlank(tccEventStatus)) {
            log.error("cancelAddUser时参数错误，tccEvent中状态字段为空，tccEventId:{}", tccEventId);
            throw new MessageException("操作失败，请稍后重试");
        }
        if (!DbConstList.TccEvent.Status.CANCEL.equals(tccEventStatus)) {
            // 删除用户
            String payload = completeTccEvent.getPayload();
            if (StringUtils.isBlank(payload)) {
                log.error("参数错误，payload为空");
                throw new MessageException("操作失败，请稍后重试");
            }
            JSONObject userShareInfo = JSONObject.parseObject(payload);
            Integer userId = userShareInfo.getInteger("userId");
            userService.deleteUserById(userId);
            // 记录cancel事件
            completeTccEvent.setStatus(DbConstList.TccEvent.Status.CANCEL);
            tccEventRepository.save(completeTccEvent);
        }
    }

    /**
     * 取消修改用户
     *
     * @param tccEvent tccEvent实体
     */
    private void cancelUpdateUser(TccEvent tccEvent) {
        if (tccEvent == null) {
            log.error("cancelUpdateUser时参数错误，tccEvent为空");
            throw new MessageException("操作失败，请重试");
        }
        Integer tccEventId = tccEvent.getId();
        if (tccEventId == null) {
            log.error("参数错误，tccEventId为空");
            throw new MessageException("操作失败，请重试");
        }
        TccEvent completeTccEvent = tccEventRepository.findOne(tccEventId);
        if (completeTccEvent == null) {
            log.error("cancelUpdateUser时参数错误，tccEvent不存在，尝试查找的tccEventId：{}", tccEventId);
            throw new MessageException("操作失败，请稍后重试");
        }
        // 只处理状态为不为CANCEL的事件
        String tccEventStatus = completeTccEvent.getStatus();
        if (StringUtils.isBlank(tccEventStatus)) {
            log.error("cancelUpdateUser时参数错误，tccEvent中状态字段为空，tccEventId:{}", tccEventId);
            throw new MessageException("操作失败，请稍后重试");
        }
        if (!DbConstList.TccEvent.Status.CANCEL.equals(tccEventStatus)) {
            // 记录cancel事件
            completeTccEvent.setStatus(DbConstList.TccEvent.Status.CANCEL);
            tccEventRepository.save(completeTccEvent);
        }
    }

    /**
     * 门店注册  创建用户
     *
     * @param tccEvent tccEvent实体
     */
    private void confirmUserStoreRegister(TccEvent tccEvent) {
        if (tccEvent == null) {
            log.error("confirmUserStoreRegister时参数错误，tccEvent为空");
            throw new MessageException("操作失败，请重试");
        }
        Integer tccEventId = tccEvent.getId();
        if (tccEventId == null) {
            log.error("参数错误，tccEventId为空");
            throw new MessageException("操作失败，请重试");
        }
        TccEvent completeTccEvent = tccEventRepository.findOne(tccEventId);
        if (completeTccEvent == null) {
            log.error("confirmUserStoreRegister时参数错误，tccEvent不存在，尝试查找的tccEventId：{}", tccEventId);
            throw new MessageException("操作失败，请稍后重试");
        }
        // 只处理状态为TRY的事件
        String tccEventStatus = completeTccEvent.getStatus();
        if (StringUtils.isBlank(tccEventStatus)) {
            log.error("confirmUserStoreRegister时参数错误，tccEvent中状态字段为空，tccEventId:{}", tccEventId);
            throw new MessageException("操作失败，请稍后重试");
        }
        if (DbConstList.TccEvent.Status.TRY.equals(tccEventStatus)) {
            String payload = completeTccEvent.getPayload();
            if (StringUtils.isBlank(payload)) {
                log.error("confirmUserStoreRegister时参数错误，tccEvent中payload字段为空，tccEventId:{}", tccEventId);
                throw new MessageException("操作失败，请稍后重试");
            }
            JSONObject payloadInfo = JSONObject.parseObject(payload);
            Integer userId = payloadInfo.getJSONObject("user").getInteger("id");
            // 设置用户状态可用
            User user = userRepository.findOne(userId);
            user.setIsActive(true);
            // 记录事件
            completeTccEvent.setStatus(DbConstList.TccEvent.Status.CONFIRM);
            tccEventRepository.save(completeTccEvent);
        }
    }

    /**
     * 门店注册 创建用户失败
     *
     * @param tccEvent tccEvent实体
     */
    private void cancelUserStoreRegister(TccEvent tccEvent) {
        if (tccEvent == null) {
            log.error("cancelUserStoreRegister时参数错误，tccEvent为空");
            throw new MessageException("操作失败，请重试");
        }
        Integer tccEventId = tccEvent.getId();
        if (tccEventId == null) {
            log.error("参数错误，tccEventId为空");
            throw new MessageException("操作失败，请重试");
        }
        TccEvent completeTccEvent = tccEventRepository.findOne(tccEventId);
        if (completeTccEvent == null) {
            log.error("cancelUserStoreRegister时参数错误，tccEvent不存在，尝试查找的tccEventId：{}", tccEventId);
            throw new MessageException("操作失败，请稍后重试");
        }
        // 只处理状态不为CANCEL的事件
        String tccEventStatus = completeTccEvent.getStatus();
        if (StringUtils.isBlank(tccEventStatus)) {
            log.error("cancelUserStoreRegister时参数错误，tccEvent中状态字段为空，tccEventId:{}", tccEventId);
            throw new MessageException("操作失败，请稍后重试");
        }
        if (!DbConstList.TccEvent.Status.CANCEL.equals(tccEventStatus)) {
            String payload = completeTccEvent.getPayload();
            if (StringUtils.isBlank(payload)) {
                log.error("confirmUserStoreRegister时参数错误，tccEvent中payload字段为空，tccEventId:{}", tccEventId);
                throw new MessageException("操作失败，请稍后重试");
            }
            // 获取状态
            JSONObject payloadInfo = JSONObject.parseObject(payload);
            String type = payloadInfo.getString("type");
            // 只处理新增状态的用户
            if (DbConstList.Event.Status.NEW.equals(type)) {
                Integer userId = payloadInfo.getJSONObject("user").getInteger("id");
                // 设置微信用户为空
                User user = new User();
                user.setId(userId);
                UserWeChat userWeChat = userWeChatRepository.findByUser(user);
                userWeChat.setUser(null);
                userWeChatRepository.save(userWeChat);
                // 删除用户
                userRepository.delete(userId);
            }
            // 记录事件
            completeTccEvent.setStatus(DbConstList.TccEvent.Status.CANCEL);
            tccEventRepository.save(completeTccEvent);
        }
    }
}
