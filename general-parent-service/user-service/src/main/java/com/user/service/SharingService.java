package com.user.service;

import com.user.entity.Sharing;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * <pre>
 * </pre>
 *
 * @author zhanglong
 * @date 2018年07月30日
 */
public interface SharingService {
    /**
     * 分页查询角色列表
     *
     * @param pageable 分页
     * @return 角色列表
     */
    Page<Sharing> findSharingListByPage(String shareType, Pageable pageable);
}
