/*
 * Copy Right@
 */
package com.user.service.impl;

import com.common.constant.DbConst;
import com.framework.exception.MessageException;
import com.framework.util.UserUtil;
import com.user.entity.Resource;
import com.user.entity.Role;
import com.user.entity.User;
import com.user.repository.ResourceRepository;
import com.user.repository.RoleRepository;
import com.user.repository.UserRepository;
import com.user.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
@CacheConfig(cacheNames = "RoleServiceImpl")
public class RoleServiceImpl implements RoleService {

    /**
     * 角色仓库
     */
    @Autowired
    private RoleRepository roleRepository;

    /**
     * 权限仓库
     */
    @Autowired
    private ResourceRepository resourceRepository;

    /**
     * 用户仓库
     */
    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public void saveRole(Role role) {
        if (role == null) {
            log.error("角色为空");
            throw new MessageException("角色为空");
        }
        String name = role.getName();
        if (name == null) {
            log.error("角色名不能为空");
            throw new MessageException("角色名不能为空");
        }

        Integer userId = UserUtil.getNullableCurrentUserId();
        Integer ownerId = null;
        if (userId != null) {
            User user = userRepository.findOne(userId);
            if (user != null) {
                Integer userOwnerId = user.getOwnerId();
                if (userOwnerId == null) {
                    ownerId = userId;
                } else {
                    ownerId = userOwnerId;
                }
            }
        }
        role.setOwnerId(ownerId);
        roleRepository.save(role);
    }

    @Override
    @Transactional
    public void addRoleByOwnerId(Role role) {
        if (role == null) {
            log.error("角色为空");
            throw new MessageException("角色为空");
        }
        String name = role.getName();
        if (name == null) {
            log.error("角色名不能为空");
            throw new MessageException("角色名不能为空");
        }
        Integer ownerId = role.getOwnerId();
        if (ownerId == null) {
            log.error("拥有者id不能为空");
            throw new MessageException("拥有者id不能为空");
        }
        List<Role> roleList = roleRepository.findByOwnerIdAndNameLike(ownerId, name);
        if (roleList != null && roleList.size() > 0) {
            log.error("角色名称已经存在了");
            throw new MessageException("角色名称已经存在了，请重新填写");
        }
        roleRepository.save(role);
    }

    @Override
    @Transactional
    public void deleteRole(Integer[] ids) {
        for (Integer id : ids) {
            deleteRole(id);
        }
    }

    /**
     * 删除角色
     *
     * @param id 角色id
     */
    private void deleteRole(Integer id) {
        if (id == null) {
            log.error("删除失败");
            throw new MessageException("删除失败");
        }
        Role role = roleRepository.findOne(id);
        if (role == null) {
            log.error("删除失败");
            throw new MessageException("删除失败");
        }
        List<User> users = role.getUsers();
        if (!users.isEmpty()) {
            log.error("已经绑定了用户的角色无法删除角色id为：{}", id);
            throw new MessageException("已经绑定了用户的角色无法删除");
        }

        role.setIsDelete(true);
        roleRepository.save(role);
    }

    @Override
    public Role findById(Integer id) {
        if (id == null) {
            log.error("id不能为空");
            throw new MessageException("id不能为空");
        }
        return roleRepository.findById(id);
    }

    @Override
    @Transactional
    public void update(Role role) {
        if (role == null) {
            log.error("角色信息为空");
            throw new MessageException("角色信息为空");
        }
        Integer roleId = role.getId();
        if (roleId == null) {
            log.error("角色ID为空");
            throw new MessageException("角色ID为空");
        }

        Role beforeRole = roleRepository.findById(roleId);
        if (beforeRole == null) {
            log.error("角色不存在");
            throw new MessageException("角色不存在");
        }
        beforeRole.setName(role.getName());
        beforeRole.setDetail(role.getDetail());
        beforeRole.setIsActive(role.getIsActive());
        roleRepository.save(beforeRole);
    }

    @Override
    @Transactional
    public void changeRoleActive(Integer[] ids, Boolean isActive) {
        for (Integer id : ids) {
            changeRoleActive(id, isActive);
        }
    }

    /**
     * 修改角色是否有效
     *
     * @param id       角色id
     * @param isActive 是否禁用
     */
    private void changeRoleActive(Integer id, Boolean isActive) {
        if (id == null) {
            throw new MessageException("id不能为空");
        }
        Role role = roleRepository.findOne(id);
        if (role == null) {
            throw new MessageException("角色不存在");
        }
        role.setIsActive(isActive);
        roleRepository.save(role);
    }

    @Override
    public Page<Role> findRoleListByPage(Pageable pageable) {
        Integer userId = UserUtil.getNullableCurrentUserId();
        Integer ownerId = null;
        if (userId != null) {
            User user = userRepository.findOne(userId);
            if (user != null) {
                ownerId = user.getOwnerId();
            }
        }

        if (ownerId != null) {
            return roleRepository.findByOwnerId(ownerId, pageable);
        }
        return roleRepository.findByOwnerIdIsNull(pageable);
    }

    @Override
    public Page<Role> findRolePageByOwnerId(Integer ownerId, Pageable pageable) {
        Integer userId = UserUtil.getNullableCurrentUserId();
        if (userId != null) {
            User user = userRepository.findOne(userId);
            if (user != null) {
                Integer userOwnerId = user.getOwnerId();
                if (userOwnerId == null) {
                    ownerId = userId;
                } else {
                    ownerId = userOwnerId;
                }
            }
        }
        return roleRepository.findByOwnerId(ownerId, pageable);
    }

    @Override
    public List<Role> findRoleList() {
        Integer userId = UserUtil.getNullableCurrentUserId();
        Integer ownerId = null;
        if (userId != null) {
            User user = userRepository.findOne(userId);
            if (user != null) {
                ownerId = user.getOwnerId();
            }
        }
        if (ownerId != null) {
            return roleRepository.findByOwnerIdAndIsActiveIsTrue(ownerId);
        }
        return roleRepository.findByOwnerIdIsNullAndIsActiveIsTrue();
    }

    @Override
    public List<Role> findRoleListByOwnerId(Integer ownerId) {
        return roleRepository.findByOwnerIdAndIsActiveIsTrue(ownerId);
    }

    @Override
    @Transactional
    public void bindRoleAndResource(Integer id, String type, Integer[] resourceIds) {
        if (type == null) {
            throw new MessageException("请选择绑定资源类型");
        }
        Role role = roleRepository.findOne(id);
        if (role == null) {
            throw new MessageException("角色不存在");
        }
        List<Resource> resources = role.getResources();
        if (resources == null) {
            resources = new ArrayList<>();
        }
        if (DbConst.Resource.Type.MENU.equals(type)) {
            // 菜单为全量提交，移除原来的菜单
            resources.removeIf(resource -> type.equals(resource.getType()));
        }
        if (resourceIds != null && resourceIds.length > 0) {
            // 绑定新权限列表
            for (Integer resourceId : resourceIds) {
                Resource resource = resourceRepository.findOne(resourceId);
                if (resource != null) {
                    resources.add(resource);
                }
            }
        }
        roleRepository.save(role);
    }

    @Override
    @Transactional
    public void unbindRoleAndBtn(Integer id, Integer[] resourceIds) {
        Role role = roleRepository.findOne(id);
        if (role == null) {
            throw new MessageException("角色不存在");
        }
        List<Resource> resources = role.getResources();
        List<Integer> unbindIds = resourceRepository
                .findIdByTypeAndIdIn(DbConst.Resource.Type.BUTTON, resourceIds);
        resources.removeIf(resource -> unbindIds.contains(resource.getId()));
    }

    @Override
    public Page<Resource> findBtnByRoleIdAndPage(Integer id, Pageable pageable) {
        return resourceRepository.findByRoleIdAndType(id, DbConst.Resource.Type.BUTTON, pageable);
    }

    @Override
    public List<Resource> findResourceByRoleId(Integer id, String type) {
        return resourceRepository.findResourceByRoleIdAndType(id, type);
    }

    @Override
    public Page<Role> findUnBindRoleListByUser(Integer id, Pageable pageable) {
        Integer ownerId = null;
        Integer userId = UserUtil.getCurrentUserId();
        User user = userRepository.findOne(userId);
        if (user != null) {
            ownerId = user.getOwnerId();
        }

        if (ownerId == null) {
            // 平台管理员查平台角色
            return roleRepository.findUnBindRoleListByUser(id, pageable);
        }

        return roleRepository.findUnBindRoleListByUser(id, ownerId, pageable);
    }

    @Override
    public Page<Role> findRoleListByUser(Integer id, Pageable pageable) {
        //查询用户已经绑定了的角色列表
        return roleRepository.findRoleListByUser(id, pageable);
    }

}
