/*
 * Copy Right@
 */
package com.user.service;

import com.user.entity.UserWeChat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface UserWeChatService {
    /**
     * 依据微信code和appId获取微信openId及用户信息
     *
     * @param code 微信code
     * @return 微信openId及用户信息
     */
    Map<String, Object> getUserByCode(String code);

    /**
     * 依据微信小程序code获取用户相关信息
     *
     * @param code 微信code
     * @return 微信openId及用户信息
     */
    Map<String, Object> getMiniappInfoByCode(String code);

    /**
     * 事件推送处理
     *
     * @param request  请求
     * @param response 响应
     */
    void handleEvent(HttpServletRequest request, HttpServletResponse response);

    /**
     * 绑定手机号
     *
     * @param user 绑定手机号参数
     * @return 绑定手机号返回信息
     */
    Map<String, Object> bindMobile(Map<String, Object> user);

    /**
     * 依据用户ID获取微信信息
     *
     * @param userId 用户ID
     * @return 微信信息
     */
    UserWeChat getWeChatInfoByUserId(Integer userId);

    /**
     * 依据appId和token,插入已关注用户信息
     *
     * @param token 微信accessToken
     */
    void saveSubscribedWechatUser(String token);

    /**
     * 微信网页登录
     *
     * @param code
     * @return token
     */
    Map<String, Object> wechatLogin(String code);

    /**
     * 微信网页登录   wechat 信息
     *
     * @param code 微信code
     * @return
     */
    Map<String, Object> wechatLoginByCode(String code);

    /**
     * 微信网页用户绑定手机号
     *
     * @param map
     * @return
     */
    Map<String, Object> wechatBindMobile(Map<String, Object> map);

    /**
     * 获取AccessToken
     *
     * @return
     */
    Map<String, String> getAccessToken();


}
