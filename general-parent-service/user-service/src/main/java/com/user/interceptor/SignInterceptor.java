/*
 * Copy Right@
 */
package com.user.interceptor;

import com.common.constant.Const;
import com.framework.util.security.RsaUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <pre>
 * 验签过滤器：权限相关的请求，客户端需要验证平台的签名
 * 例如菜单权限，按钮权限等
 *

 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
@Slf4j
public class SignInterceptor extends HandlerInterceptorAdapter {

    /**
     * 私钥
     */
    private String privateKey = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAJgshkWIVZfSfE+4f+b9/28RKF8i\n" +
            "dNWkXM4UX50vSuyWaSbcBHm4gZX0YsTmHU4zv4jOblSVuID+Mkuuq9/IqbHDqXlX53UnUgVHWLRt\n" +
            "C+C7F8gMhYsGYJnvE6kGt9NC1ElY6A/XpYJ1BdxOaoWlfI94wQdXAHH67ZNbFVts1Tp/AgMBAAEC\n" +
            "gYBvg6FZ7xCaU3sZiKDiC5xWLgKhxMRNk0cXFcMDkxaazZZ9DZqSAG2mEBMZK3CqUC25+VzMy3ML\n" +
            "xDvjTjtRoOEI5mi0AN9caUnijf6eF4MHR3pfORz+fg+fOUW6iAXNpB882DFYFgLerR613i4p4DfA\n" +
            "PKSGRxC+Oh3vJohTvzgp0QJBAPJwiSfaYnjiXsB6vCEprrcjl++vUyrYLYz2cw2J9NkoScTlxe6x\n" +
            "aPayHxEQcIxmGZbGa38A1tKHCQ2KJY6b3VMCQQCgr3wzD5JJ7zAFX6YGj3vkib7wecTfy/FPJK9m\n" +
            "+VKzwqiDgT9vCobEhY1jif3+aope8R2cPRzPnaO/esJZW5ylAkEA7/ft+pGR6L+ANBsIPfcJJeNI\n" +
            "T4mF06G4rI+AYEVOIuha5Fbe7R2TnKPAVl6kWIPzDieGXWaSPD5G+qzj8d1bWwJBAIR6KduvKtzp\n" +
            "5p2Hjko6Ydq59kItIrOgYQLvK+2qARVTl7tUBAh4lUPRGBDI5C+thwdfoxn9CJPjU+26OCGT9kEC\n" +
            "QQCVuRe37O82GrITQ/cbyb6tqBRlBWemuHAzT9Ao7xQ4wYwS9GjZqLyuYhDeFZ7M3ulTm2jbM4L9\n" +
            "fDUI/IVp9oSX\n";

    /**
     * 公钥（客户端使用）
     */
    private String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCYLIZFiFWX0nxPuH/m/f9vEShfInTVpFzOFF+d\n" +
            "L0rslmkm3AR5uIGV9GLE5h1OM7+Izm5UlbiA/jJLrqvfyKmxw6l5V+d1J1IFR1i0bQvguxfIDIWL\n" +
            "BmCZ7xOpBrfTQtRJWOgP16WCdQXcTmqFpXyPeMEHVwBx+u2TWxVbbNU6fwIDAQAB\n";

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        String enc = RandomStringUtils.random(4, true, true);
        String sign = RsaUtil.sign(enc.getBytes(), privateKey);
        response.setHeader(Const.Header.RSA_ENCRYPT, enc);
        response.setHeader(Const.Header.RSA_SIGN, sign);

        return super.preHandle(request, response, handler);
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
    }

}
