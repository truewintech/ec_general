/*
 * Copy Right@
 */
package com.es;

import com.es.entity.ec.Order;
import com.es.repository.ec.OrderRepository;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Data
@Slf4j
public class OrderServiceTest {

    @Autowired
    OrderRepository orderRepository;

    @Test
    public void add() throws Throwable {

        // 线程池
        ExecutorService executor = Executors.newFixedThreadPool(100);
        for (int group = 0; group < 100; group++) {
            final int g = group;
            final int interval = 1000000 / 100;
            executor.execute(() -> {
                List<Order> orders = new ArrayList<>();
                for (int i = 1 + g * interval; i < (g + 1) * interval + 1; i++) {
                    Order order = orderRepository.findOne(i);
                    if (order != null) {
                        continue;
                    }

                    order = new Order();
                    order.setId(i);
                    order.setOrderId("20171122" + String.format("%06d", i));
                    order.setOrderTime(new Date());
                    order.setPrice(new BigDecimal(i));
                    order.setStatus("payed");
                    orders.add(order);
                    if (orders.size() % 100 == 0) {
                        orderRepository.save(orders);
                        log.info("批量添加订单成功：{}", i);
                    }
                }
                if (!orders.isEmpty()) {
                    orderRepository.save(orders);
                    log.info("批量添加订单成完成");
                }
            });
        }

        executor.shutdown();
        try {
            boolean stop;
            do {
                //等待所有任务完成
                stop = executor.awaitTermination(1, TimeUnit.SECONDS);
            } while (!stop);
        } catch (Exception e) {
        }
    }

}
