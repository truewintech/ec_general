/*
 * Copy Right@
 */
package com.es;

import com.es.entity.ec.Product;
import com.es.repository.ec.ProductRepository;
import lombok.Data;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Data
public class EcProductServiceTest {

    @Autowired
    ProductRepository productRepository;

    @Test
    public void add() throws Throwable {
        List<Integer> categoryIds = new ArrayList<>();
        categoryIds.add(1);
        categoryIds.add(2);

        List<String> attributes = new ArrayList<>();
        attributes.add("属性一");
        attributes.add("属性二");

        Product product = new Product();
        product.setId(3);
        product.setProductName("产品三");
        product.setAttributes(attributes);
        product.setCategoryIds(categoryIds);
        productRepository.save(product);

        product = new Product();
        product.setId(4);
        product.setProductName("产品四");
        product.setAttributes(attributes);
        product.setCategoryIds(categoryIds);
        productRepository.save(product);
    }
}
