/*
 * Copy Right@
 */
package com.es.task;

import com.alibaba.fastjson.JSON;
import com.common.constant.DbConst;
import com.es.entity.ec.Order;
import com.es.repository.ec.OrderRepository;
import com.es.repository.ec.ProductRepository;
import com.es.service.ec.impl.ProductServiceImpl;
import com.framework.constant.DbConstList;
import com.framework.entity.Event;
import com.framework.task.BaseEventTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class EventHandleTask extends BaseEventTask {

    /**
     * 产品服务
     */
    @Autowired
    private ProductRepository productRepository;

    /**
     * 订单服务
     */
    @Autowired
    private OrderRepository orderRepository;

    /**
     * 商品服务
     */
    @Autowired
    private ProductServiceImpl productService;

    @Override
    public List<Event> findEvents(Integer dealGroup) {
        return eventRepository.findTop100ByDealGroupAndStatusAndReceiverOrderById(dealGroup, DbConstList.Event.Status.NEW
                , DbConst.Event.SenderOrReceiver.ES_SERVICE);
    }

    @Override
    protected boolean dealEvent(Event event) {
        String type = event.getEventType();
        String payload = event.getPayload();
        if (DbConst.Event.EventType.ToEs.UPDATE_PRODUCT.equals(type)) {
            productService.addProduct(event);
        } else if (DbConst.Event.EventType.ToEs.DELETE_PRODUCT.equals(type)) {
            productService.deleteProduct(event);
        } else if (DbConst.Event.EventType.ToEs.UPDATE_ORDER.equals(type)) {
            Order order = JSON.toJavaObject(JSON.parseObject(payload), Order.class);
            orderRepository.save(order);
        } else if (DbConst.Event.EventType.ToEs.DELETE_ORDER.equals(type)) {
            Integer id = Integer.parseInt(payload);
            orderRepository.delete(id);
        } else {
            return false;
        }
        return true;
    }

}
