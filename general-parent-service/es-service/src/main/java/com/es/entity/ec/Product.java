/*
 * Copy Right@
 */
package com.es.entity.ec;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Mapping;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Setter
@Getter
@Document(indexName = "ec", type = "Product", shards = 1, replicas = 0, refreshInterval = "-1")
public class Product implements Serializable {

    /**
     * 主表主键
     */
    private Integer id;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品品牌
     */
    private String brandName;

    /**
     * 品牌logo
     */
    private String brandLogoUrl;

    /**
     * 商品品牌ID
     */
    private Integer brandId;

    /**
     * 店铺ID
     */
    private Integer shopId;

    /**
     * 商品图片
     */
    private String imageUrl;

    /**
     * 商品最低价格
     */
    private BigDecimal minSalesPrice;

    /**
     * 商品最高价格
     */
    private BigDecimal maxSalesPrice;

    /**
     * 商品销量
     */
    private Integer salesCount;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 商品类目ID
     */
    private List<Integer> categoryIds;

    /**
     * 商品类目属性ID
     */
    private List<Integer> categoryAttributeIds;

    /**
     * 商品类目属性值ID
     */
    private List<Integer> categoryAttributeValueIds;

    /**
     * 商品店铺类目ID
     */
    private List<Integer> shopCategoryIds;

    /**
     * 商品属性
     */
    private List<String> attributes;

}
