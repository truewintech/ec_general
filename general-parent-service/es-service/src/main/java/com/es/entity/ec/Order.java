/*
 * Copy Right@
 */
package com.es.entity.ec;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Setter
@Getter
@Document(indexName = "ec", type = "Order", shards = 1, replicas = 0, refreshInterval = "-1")
public class Order implements Serializable {

    /**
     * 主表主键
     */
    private Integer id;

    /**
     * 订单号
     */
    private String orderId;

    /**
     * 出售人
     */
    private int sellerId;

    /**
     * 购买人
     */
    private int buyerId;

    /**
     * 订单价格
     */
    private BigDecimal price;

    /**
     * 订单时间
     */
    private Date orderTime;

    /**
     * 订单状态
     */
    private String status;
}
