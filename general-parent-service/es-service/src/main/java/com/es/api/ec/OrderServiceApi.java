/*
 * Copy Right@
 */
package com.es.api.ec;

import com.es.entity.ec.Order;
import com.es.service.ec.OrderService;
import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("order")
@Api("订单API")
@Slf4j
public class OrderServiceApi extends BaseApi {

    /**
     * 订单服务
     */
    @Autowired
    private OrderService orderService;

    /**
     * 批量初始化订单
     *
     * @param orderNum 订单个数
     * @return 是否成功
     */
    @ApiOperation(value = "批量初始化订单")
    @GetMapping(value = "initOrders")
    public Result initOrders(@RequestParam int orderNum) {
        orderService.initOrders(orderNum);
        return success();
    }

    /**
     * 分页查询订单
     *
     * @param pageable 分页
     * @return 订单列表
     */
    @ApiOperation(value = "分页查询订单")
    @GetMapping(value = "findOrderByPage")
    public Result findOrderByPage(String orderId, @PageableDefault(sort = {
            "id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        Page<Order> orders = orderService.findByOrderIdLike(orderId, pageable);
        Map<String, Object> map = new HashMap<>(20);
        map.put("content", orders.getContent());
        map.put("totalElements", orders.getTotalElements());
        map.put("totalPages", orders.getTotalPages());
        map.put("number", orders.getNumber());
        map.put("size", orders.getSize());
        return success(map);
    }

}
