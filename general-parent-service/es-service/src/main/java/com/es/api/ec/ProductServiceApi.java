/*
 * Copy Right@
 */
package com.es.api.ec;

import com.es.entity.ec.Product;
import com.es.service.ec.ProductService;
import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("ec/product")
@Api("商品API")
@Slf4j
public class ProductServiceApi extends BaseApi {

    /**
     * 产品服务
     */
    @Autowired
    private ProductService productService;

    /**
     * 查询产品列表
     *
     * @param condition 查询条件
     * @param pageable  分页
     * @return 产品列表
     */
    @ApiOperation(value = "查询产品列表")
    @GetMapping(value = "view/findProductByPage")
    public Result findProducts(@RequestParam(required = false) Map<String, Object> condition, @PageableDefault Pageable pageable) {
        Page<Product> productPage = productService.findProductPage(condition, pageable);
        Map<String, Object> map = new HashMap<>(20);
        map.put("content", productPage.getContent());
        map.put("totalElements", productPage.getTotalElements());
        map.put("totalPages", productPage.getTotalPages());
        map.put("number", productPage.getNumber());
        map.put("size", productPage.getSize());
        return success(map);
    }

    /**
     * 根据传入的商品名称和分类id获取品牌信息
     *
     * @param productName 商品名称
     * @param categoryId  分类id
     * @return 品牌信息
     */
    @ApiOperation(value = "查询产品列表")
    @GetMapping(value = "view/findBrandList")
    public Result findBrandList(@RequestParam(required = false)String productName, @RequestParam(required = false) Integer categoryId) {
        return success(productService.findBrandList(productName, categoryId));
    }

}
