/*
 * Copy Right@
 */
package com.es.repository.ec;

import com.es.entity.ec.Product;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface ProductRepository extends ElasticsearchRepository<Product, Integer> {
}
