/*
 * Copy Right@
 */
package com.es.repository.ec;

import com.es.entity.ec.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface OrderRepository extends ElasticsearchRepository<Order, Integer> {

    /**
     * 通过订单号查找订单
     *
     * @param orderId  订单号
     * @param pageable 分页
     * @return 订单列表
     */
    Page<Order> findByOrderIdLike(String orderId, Pageable pageable);
}
