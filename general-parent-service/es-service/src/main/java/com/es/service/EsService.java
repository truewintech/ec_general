/*
 * Copy Right@
 */
package com.es.service;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface EsService<T> {

    /**
     * 搜索
     *
     * @param queryString 条件字符串
     * @param clz         搜索的对象类
     * @return 搜索结果列表
     */
    List<T> find(String queryString, Class<T> clz);
}
