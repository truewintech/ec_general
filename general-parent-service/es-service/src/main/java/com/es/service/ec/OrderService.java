/*
 * Copy Right@
 */
package com.es.service.ec;

import com.es.entity.ec.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface OrderService {

    /**
     * 初始化订单
     *
     * @param orderNum 订单数量
     */
    void initOrders(int orderNum);

    /**
     * 分页查询订单
     *
     * @param orderId  订单号
     * @param pageable 分页
     * @return 订单列表
     */
    Page<Order> findByOrderIdLike(String orderId, Pageable pageable);

}
