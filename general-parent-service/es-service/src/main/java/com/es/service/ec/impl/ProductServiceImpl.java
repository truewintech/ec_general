/*
 * Copy Right@
 */
package com.es.service.ec.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.es.entity.ec.Product;
import com.es.repository.ec.ProductRepository;
import com.es.service.ec.ProductService;
import com.framework.entity.Event;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

    /**
     * 产品ES仓库
     */
    @Autowired
    private ProductRepository productRepository;

    /**
     * es工具类
     */
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Override
    public Page<Product> findProductPage(Map<String, Object> condition, Pageable pageable) {
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        if (condition != null) {
            //根据商品名称搜索
            Object productNameObj = condition.get("productName");
            if (productNameObj != null) {
                String productName = productNameObj.toString();
                if (StringUtils.isNotBlank(productName)) {
                    boolQueryBuilder.must(matchQuery("productName", productName).operator(MatchQueryBuilder.Operator.AND));
                }
            }
            //根据商品品牌ID搜索
            Object brandIdObj = condition.get("brandId");
            if (brandIdObj != null) {
                Integer brandId = Integer.parseInt(brandIdObj.toString());
                boolQueryBuilder.must(termQuery("brandId", brandId));
            }
            //根据店铺ID搜索
            Object shopIdObj = condition.get("shopId");
            if (shopIdObj != null) {
                Integer shopId = Integer.parseInt(shopIdObj.toString());
                boolQueryBuilder.must(termQuery("shopId", shopId));
            }
            //根据商品价格最小值查询
            Object minSalesPriceObj = condition.get("minSalesPrice");
            if (minSalesPriceObj != null) {
                BigDecimal minSalesPrice = new BigDecimal(minSalesPriceObj.toString());
                boolQueryBuilder.must(rangeQuery("minSalesPrice").gte(minSalesPrice));
            }
            //根据商品价格最大值查询
            Object maxSalesPriceObj = condition.get("maxSalesPrice");
            if (maxSalesPriceObj != null) {
                BigDecimal maxSalesPrice = new BigDecimal(maxSalesPriceObj.toString());
                boolQueryBuilder.must(rangeQuery("minSalesPrice").lte(maxSalesPrice));
            }
            //根据商品类目ID搜索
            Object categoryIdObj = condition.get("categoryId");
            if (categoryIdObj != null) {
                Integer categoryId = Integer.parseInt(categoryIdObj.toString());
                boolQueryBuilder.must(QueryBuilders.constantScoreQuery(termQuery("categoryIds", categoryId)));
            }
            //根据店铺类目ID搜索
            Object shopCategoryIdObj = condition.get("shopCategoryId");
            if (shopCategoryIdObj != null) {
                Integer shopCategoryId = Integer.parseInt(shopCategoryIdObj.toString());
                boolQueryBuilder.must(QueryBuilders.constantScoreQuery(termQuery("shopCategoryIds", shopCategoryId)));
            }
            //根据类目属性ID查询
            Object categoryAttributeObj = condition.get("categoryAttributeIds");
            if (categoryAttributeObj != null) {
                String categoryAttributeIdsStr = categoryAttributeObj.toString();
                String[] array = categoryAttributeIdsStr.split(",");
                List<Integer> categoryAttributeIds = new ArrayList<>();
                for (String idStr : array) {
                    categoryAttributeIds.add(Integer.parseInt(idStr));
                }
                if (categoryAttributeIds.size() > 0) {
                    for (Integer categoryAttributeId : categoryAttributeIds) {
                        if (categoryAttributeId != null) {
                            boolQueryBuilder.must(QueryBuilders.constantScoreQuery(termQuery("categoryAttributeIds", categoryAttributeId)));
                        }
                    }
                }
            }
            //根据类目属性值ID查询
            Object categoryAttributeValueObj = condition.get("categoryAttributeValueIds");
            if (categoryAttributeValueObj != null) {
                String categoryAttributeValueIdsStr = categoryAttributeValueObj.toString();
                String[] array = categoryAttributeValueIdsStr.split(",");
                List<Integer> categoryAttributeValueIds = new ArrayList<>();
                for (String idStr : array) {
                    categoryAttributeValueIds.add(Integer.parseInt(idStr));
                }
                if (categoryAttributeValueIds.size() > 0) {
                    for (Integer categoryAttributeValueId : categoryAttributeValueIds) {
                        if (categoryAttributeValueId != null) {
                            boolQueryBuilder.must(QueryBuilders.constantScoreQuery(termQuery("categoryAttributeValueIds", categoryAttributeValueId)));
                        }
                    }
                }
            }
        }
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(boolQueryBuilder).withPageable(pageable).build();
        return elasticsearchTemplate.queryForPage(searchQuery, Product.class);
    }

    @Override
    @Transactional
    public void addProduct(Event event) {
        if (event == null) {
            log.error("event为空");
            return;
        }
        Integer eventId = event.getId();
        String payload = event.getPayload();
        if (StringUtils.isEmpty(payload)) {
            log.warn("payload为空,eventId={}", eventId);
            return;
        }
        JSONObject jsonProduct = JSON.parseObject(payload);
        Product product = new Product();
        Integer id = jsonProduct.getInteger("id");
        if (id == null) {
            log.error("商品ID为空,eventId={}", eventId);
            return;
        }
        product.setId(id);
        product.setProductName(jsonProduct.getString("productName"));
        product.setBrandName(jsonProduct.getString("brandName"));
        product.setImageUrl(jsonProduct.getString("imageUrl"));
        product.setMinSalesPrice(jsonProduct.getBigDecimal("minSalePrice"));
        product.setMaxSalesPrice(jsonProduct.getBigDecimal("maxSalePrice"));
        product.setShopId(jsonProduct.getInteger("shopId"));
        product.setUpdateTime(jsonProduct.getDate("updateTime"));
        //添加品牌信息
        Object brandObj = jsonProduct.get("brand");
        if (brandObj != null) {
            JSONObject jsonBrand = (JSONObject) brandObj;
            product.setBrandId(jsonBrand.getInteger("id"));
            product.setBrandLogoUrl(jsonBrand.getString("logoUrl"));
        } else {
            log.warn("商品品牌信息不存在,productId={}", id);
        }
        //添加销量信息
        Object productExternalInfoObj = jsonProduct.get("productExternalInfo");
        if (productExternalInfoObj != null) {
            JSONObject jsonProductExternalInfo = (JSONObject) productExternalInfoObj;
            Integer salesCount = jsonProductExternalInfo.getInteger("salesCount");
            if (salesCount != null) {
                product.setSalesCount(salesCount);
            } else {
                product.setSalesCount(0);
            }
        } else {
            product.setSalesCount(0);
        }

        //添加商品类目信息
        Object allCategoryListObj = jsonProduct.get("allCategoryList");
        if (allCategoryListObj != null) {
            List<JSONObject> allCategoryList = (List<JSONObject>) allCategoryListObj;
            List<Integer> categoryIds = new ArrayList<>();
            for (JSONObject jsonObject : allCategoryList) {
                Integer categoryId = jsonObject.getInteger("id");
                if (categoryId != null) {
                    categoryIds.add(categoryId);
                }
            }
            product.setCategoryIds(categoryIds);
        } else {
            log.warn("商品类目信息不存在,productId={}", id);
        }
        //添加店铺类目信息
        Object allShopCategoryListObj = jsonProduct.get("allShopCategoryList");
        if (allShopCategoryListObj != null) {
            List<JSONObject> allShopCategoryList = (List<JSONObject>) allShopCategoryListObj;
            List<Integer> shopCategoryIds = new ArrayList<>();
            for (JSONObject jsonObject : allShopCategoryList) {
                Integer shopCategoryId = jsonObject.getInteger("id");
                if (shopCategoryId != null) {
                    shopCategoryIds.add(shopCategoryId);
                }
            }
            product.setShopCategoryIds(shopCategoryIds);
        } else {
            log.warn("商品店铺类目信息不存在，productId={}", id);
        }
        //添加类目属性与类目属性值信息
        Object productCategoryAttributeValueListObj = jsonProduct.get("productCategoryAttributeValueList");
        if (productCategoryAttributeValueListObj != null) {
            List<JSONObject> productCategoryAttributeValueList = (List<JSONObject>) productCategoryAttributeValueListObj;
            List<String> attributes = new ArrayList<>();
            List<Integer> categoryAttributeIdList = new ArrayList<>();
            List<Integer> categoryAttributeValueIdList = new ArrayList<>();
            for (JSONObject jsonObject : productCategoryAttributeValueList) {
                String attributeValue = jsonObject.getString("attributeValue");
                if (StringUtils.isNotBlank(attributeValue)) {
                    attributes.add(attributeValue);
                }
                //商品类目属性
                Object categoryAttributeObj = jsonObject.get("categoryAttribute");
                if (categoryAttributeObj != null) {
                    JSONObject jsonCategoryAttribute = (JSONObject) categoryAttributeObj;
                    Integer categoryAttributeId = jsonCategoryAttribute.getInteger("id");
                    if (categoryAttributeId != null) {
                        categoryAttributeIdList.add(categoryAttributeId);
                    }
                }
                //商品类目属性值
                Object categoryAttributeValueObj = jsonObject.get("categoryAttributeValue");
                if (categoryAttributeValueObj != null) {
                    JSONObject jsonCategoryAttributeValue = (JSONObject) categoryAttributeValueObj;
                    Integer categoryAttributeValueId = jsonCategoryAttributeValue.getInteger("id");
                    if (categoryAttributeValueId != null) {
                        categoryAttributeValueIdList.add(categoryAttributeValueId);
                    }
                }
            }
            product.setAttributes(attributes);
            product.setCategoryAttributeIds(categoryAttributeIdList);
            product.setCategoryAttributeValueIds(categoryAttributeValueIdList);
        } else {
            log.warn("商品类目属性与类目属性值信息不存在，productId={}", id);
        }
        productRepository.save(product);

    }

    @Override
    @Transactional
    public void deleteProduct(Event event) {
        if (event == null) {
            log.error("event信息不存在");
            return;
        }
        String payload = event.getPayload();
        if (StringUtils.isEmpty(payload)) {
            log.warn("payload为空,eventId={}", event.getId());
            return;
        }
        Integer productId = Integer.parseInt(payload);
        productRepository.delete(productId);
    }

    @Override
    public List<Map<String, Object>> findBrandList(String productName, Integer categoryId) {
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        if (StringUtils.isNotBlank(productName)) {
            boolQueryBuilder.must(matchQuery("productName", productName).operator(MatchQueryBuilder.Operator.AND));
        }
        if (categoryId != null) {
            boolQueryBuilder.must(QueryBuilders.constantScoreQuery(termQuery("categoryIds", categoryId)));
        }
        // TODO 优化逻辑
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(boolQueryBuilder).build();
        List<Product> productList = elasticsearchTemplate.queryForList(searchQuery, Product.class);
        Set<Product> set = new TreeSet<>(new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                //字符串,则按照asicc码升序排列
                return o1.getBrandId().compareTo(o2.getBrandId());
            }
        });
        set.addAll(productList);
        List<Product> newProductList = new ArrayList<>(set);
        List<Map<String, Object>> mapList = new ArrayList<>();
        for (Product product : newProductList) {
            Map<String, Object> map = new HashMap<>(10);
            map.put("id", product.getBrandId());
            map.put("name", product.getBrandName());
            map.put("logoUrl", product.getBrandLogoUrl());
            mapList.add(map);
        }
        return mapList;
    }


}
