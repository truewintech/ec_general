/*
 * Copy Right@
 */
package com.es.service.ec;

import com.es.entity.ec.Product;
import com.framework.entity.Event;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface ProductService {

    /**
     * 根据条件分页查询产品
     *
     * @param condition 查询条件
     * @param pageable  分页
     * @return 产品列表
     */
    Page<Product> findProductPage(Map<String, Object> condition, Pageable pageable);

    /**
     * 添加商品信息
     *
     * @param event 实体
     */
    void addProduct(Event event);

    /**
     * 删除商品信息
     *
     * @param event 实体
     */
    void deleteProduct(Event event);

    /**
     * 根据传入的商品名称和分类id获取品牌信息
     *
     * @param productName 商品名称
     * @param categoryId  分类id
     * @return 品牌信息
     */
    List<Map<String, Object>> findBrandList(String productName, Integer categoryId);
}
