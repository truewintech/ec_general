/*
 * Copy Right@
 */
package com.es.service.ec.impl;

import com.es.entity.ec.Order;
import com.es.repository.ec.OrderRepository;
import com.es.service.ec.OrderService;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    /**
     * 订单仓库
     */
    @Autowired
    private OrderRepository orderRepository;

    /**
     * 初始化是否执行中
     */
    private Boolean running = false;

    @Override
    public void initOrders(int orderNum) {
        synchronized (running) {
            if (running) {
                return;
            }
            running = true;
            int size = 10;
            DateFormat df = new SimpleDateFormat("yyyyMMdd");
            // 线程池
            ExecutorService executor = new ThreadPoolExecutor(size, size, 0,
                    TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(size * 2),
                    new ThreadFactoryBuilder().setNameFormat("initOrders-pool-%d").build());
            for (int group = 0; group < size; group++) {
                final int g = group;
                final int interval = orderNum / 10;
                executor.execute(() -> {
                    List<Order> orders = new ArrayList<>();
                    for (int i = 1 + g * interval; i < (g + 1) * interval + 1; i++) {
                        Order order = orderRepository.findOne(i);
                        if (order != null) {
                            continue;
                        }

                        order = new Order();
                        order.setId(i);
                        Date date = new Date();
                        order.setOrderId(df.format(date) + String.format("%06d", i));
                        order.setOrderTime(date);
                        order.setPrice(new BigDecimal(i));
                        order.setBuyerId(1);
                        order.setSellerId(2);
                        order.setStatus("paid");
                        orders.add(order);
                        if (orders.size() % 20 == 0) {
                            orderRepository.save(orders);
                            orders.clear();
                            log.info("批量添加订单成功：{}", i);
                        }
                    }
                    if (!orders.isEmpty()) {
                        orderRepository.save(orders);
                        orders.clear();
                        log.info("批量添加订单成完成");
                    }
                });
            }

            executor.shutdown();
            try {
                boolean stop;
                do {
                    //等待所有任务完成
                    stop = executor.awaitTermination(1, TimeUnit.SECONDS);
                } while (!stop);
            } catch (Exception e) {
            }

            running = false;
        }


    }

    @Override
    public Page<Order> findByOrderIdLike(String orderId, Pageable pageable) {
        return orderRepository.findByOrderIdLike(orderId, pageable);
    }

}
