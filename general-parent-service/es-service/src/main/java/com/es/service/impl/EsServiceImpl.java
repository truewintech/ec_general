/*
 * Copy Right@
 */
package com.es.service.impl;

import com.es.service.EsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class EsServiceImpl implements EsService {

    /**
     * es工具类
     */
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Override
    public List find(String queryString, Class clz) {
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryStringQuery(queryString)).build();
        return elasticsearchTemplate.queryForList(searchQuery, clz);
    }
}
