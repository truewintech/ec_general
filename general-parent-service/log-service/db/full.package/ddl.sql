SET FOREIGN_KEY_CHECKS=0;

drop table if exists behavior_log;

drop table if exists login_log;

/*==============================================================*/
/* Table: behavior_log                                          */
/*==============================================================*/
create table behavior_log
(
   id                   int not null auto_increment comment '主键',
   account_id           int comment '账户标识',
   account              varchar(32) comment '账户',
   ip                   varchar(32) comment '访问ip',
   domain               varchar(256) comment '域名',
   url                  varchar(512) comment '地址',
   referrer             varchar(512) comment '来源地址',
   user_agent           varchar(256) comment '访问设备',
   params               text comment '页面参数',
   visitor_id           varchar(64) comment '访客标识',
   visit_time           datetime not null comment '访问时间',
   visit_duration       int comment '访问时长',
   primary key (id)
);

alter table behavior_log comment '行为日志';

/*==============================================================*/
/* Table: login_log                                             */
/*==============================================================*/
create table login_log
(
   id                   int not null auto_increment comment '主键',
   account_id           int comment '账户ID',
   account              varchar(32) comment '账户',
   ip                   varchar(32) comment '登录ip',
   login_time           datetime not null comment '登录时间',
   mobile_model         varchar(256) comment '手机型号',
   mobile_system        varchar(256) comment '手机系统',
   mobile_system_version varchar(256) comment '手机系统版本',
   mobile_sn            varchar(256) comment '手机唯一号',
   province             varchar(32) comment '省',
   city                 varchar(32) comment '市',
   district             varchar(32) comment '区县',
   address              varchar(256) comment '详细地址',
   soft_version         varchar(32) comment '软件版本',
   primary key (id)
);

alter table login_log comment '登录日志';

SET FOREIGN_KEY_CHECKS=1;