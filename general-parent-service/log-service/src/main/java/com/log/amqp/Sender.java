/*
 * Copy Right@
 */
package com.log.amqp;

import com.common.amqp.AmqpConfig;
import com.log.entity.BehaviorLog;
import com.log.entity.LoginLog;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class Sender {

    /**
     * 消息模板
     */
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 新增用户行为
     *
     * @param behaviorLog 行为日志
     */
    public void addBehaviorLog(BehaviorLog behaviorLog) {
        rabbitTemplate.convertAndSend(AmqpConfig.LOG_BEHAVIOR_QUEUE_NAME, behaviorLog);
    }

    /**
     * 新增登录日志
     *
     * @param loginLog 登录日志
     */
    public void addLoginLog(LoginLog loginLog) {
        rabbitTemplate.convertAndSend(AmqpConfig.LOG_LOGIN_QUEUE_NAME, loginLog);
    }

}
