/*
 * Copy Right@
 */
package com.log.amqp;

import com.common.amqp.AmqpConfig;
import com.common.constant.DbConst;
import com.framework.amqp.EventReceiver;
import com.framework.entity.Event;
import com.log.entity.BehaviorLog;
import com.log.entity.LoginLog;
import com.log.repository.BehaviorLogRepository;
import com.log.repository.LoginLogRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
@Slf4j
public class Receiver extends EventReceiver {

    /**
     * 接收服务事件
     *
     * @param event 事件
     * @return 处理是否成功
     */
    @RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = DbConst.Event.SenderOrReceiver.LOG_SERVICE)
    @RabbitHandler
    public boolean receiveEvent(Event event) {
        return saveEvent(event);
    }

    /**
     * 行为日志服务
     */
    @Autowired
    private BehaviorLogRepository behaviorLogRepository;

    /**
     * 登录日志服务
     */
    @Autowired
    private LoginLogRepository loginLogRepository;

    /**
     * 接收新增用户行为
     *
     * @param behaviorLog 消息信息
     */
    @RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = AmqpConfig.LOG_BEHAVIOR_QUEUE_NAME)
    @RabbitHandler
    public void receiveBehaviorLog(BehaviorLog behaviorLog) {
        try {
            behaviorLogRepository.save(behaviorLog);
        } catch (Exception ex) {
            log.warn("接收新增用户行为失败", ex);
        }
    }

    /**
     * 接收新增登录日志
     *
     * @param loginLog 消息信息
     */
    @RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = AmqpConfig.LOG_LOGIN_QUEUE_NAME)
    @RabbitHandler
    public void receiveLoginLog(Map<String, String> loginLog) {
        try {
            LoginLog log = new LoginLog();
            log.setAccount(loginLog.get("account"));
            log.setAccountId(loginLog.get("accountId"));
            log.setIp(loginLog.get("ip"));
            loginLogRepository.save(log);
        } catch (Exception ex) {
            log.warn("接收新增登录日志失败", ex);
        }
    }
}
