/*
 * Copy Right@
 */
package com.log.repository;

import com.log.entity.BehaviorLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface BehaviorLogRepository extends JpaRepository<BehaviorLog, Integer>,
        JpaSpecificationExecutor<BehaviorLog> {
}