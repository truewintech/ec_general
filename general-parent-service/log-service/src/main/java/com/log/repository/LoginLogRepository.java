/*
 * Copy Right@
 */
package com.log.repository;

import com.log.entity.LoginLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 * <pre>
 * </pre>
 *
 * @author ChenChao
 * @date 2017年09月04日
 */
public interface LoginLogRepository extends JpaRepository<LoginLog, Integer>,
        JpaSpecificationExecutor<LoginLog> {

    /**
     * 根据用户ID获取上次登录记录
     *
     * @param userId 用户ID
     * @return 上次登录记录
     */
    @Query(value = "SELECT * FROM login_log where account_id =?1 order by login_time DESC limit 1 OFFSET 1", nativeQuery = true)
    LoginLog findLastLoginLogByUserId(Integer userId);
}