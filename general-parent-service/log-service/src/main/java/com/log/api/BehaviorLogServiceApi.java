/*
 * Copy Right@
 */
package com.log.api;

import com.common.constant.Const;
import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.framework.util.ServletUtil;
import com.log.amqp.Sender;
import com.log.entity.BehaviorLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("behaviorLog")
@Api(value = "行为日志服务API")
@Slf4j
public class BehaviorLogServiceApi extends BaseApi {

    /**
     * 发送工具
     */
    @Autowired
    private Sender sender;

    /**
     * 新增一个行为日志
     *
     * @param request http请求
     * @return 是否成功
     * @throws ParseException 异常
     */
    @ApiOperation(value = "新增一个行为日志")
    @GetMapping("addBehaviorLog")
    public Result addBehaviorLog(HttpServletRequest request) throws ParseException {
        String userAgent = ServletUtil.getUserAgent(request);
        String ip = ServletUtil.getRealIp(request);
        String referrer = ServletUtil.getReferrer(request);
        String domain = ServletUtil.getDomain(request);
        String visitTime = request.getParameter("visitTime");
        String url = request.getParameter("url");
        String visitDuration = request.getParameter("visitDuration");
        String currentUserId = request.getHeader(Const.Header.CURRENT_USER_ID);
        String visitorId = request.getHeader(Const.Header.VISITOR_ID);
        String params = request.getQueryString();

        BehaviorLog behaviorLog = new BehaviorLog();
        behaviorLog.setIp(ip);
        behaviorLog.setUserAgent(userAgent);
        behaviorLog.setDomain(domain);
        behaviorLog.setReferrer(referrer);
        behaviorLog.setUrl(url);
        behaviorLog.setParams(params);
        behaviorLog.setVisitTime(DateUtils.parseDate(visitTime, "yyyy-MM-dd hh:mm:ss"));
        behaviorLog.setAccountId(currentUserId);
        behaviorLog.setVisitorId(visitorId);
        if (StringUtils.isNotBlank(visitDuration)) {
            try {
                behaviorLog.setVisitDuration(Integer.parseInt(visitDuration));
            } catch (Exception e) {
                log.warn("转换操作时间异常", e);
            }
        }

        sender.addBehaviorLog(behaviorLog);
        return success();
    }

}
