/*
 * Copy Right@
 */
package com.log.api;

import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.log.service.LoginLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <pre>
 * </pre>
 *
 * @author ZhangZhenJie
 * @date 2018年03月08日
 */
@RestController
@RequestMapping("loginLog")
@Api(value = "登录日志服务API")
@Slf4j
public class LoginLogServiceApi extends BaseApi {

    /**
     * 日志服务
     */
    @Autowired
    private LoginLogService loginLogService;

    /**
     * 根据用户ID获取上次登录记录
     *
     * @param userId 用户id
     * @return 上次登录记录
     */
    @ApiOperation(value = "根据用户ID获取上次登录记录")
    @GetMapping("findLastLoginLogByUserId")
    public Result findLastLoginLogByUserId(@RequestParam Integer userId) {
        return success(loginLogService.findLastLoginLogByUserId(userId));
    }

}
