/*
 * Copy Right@
 */
package com.log.task;

import com.common.constant.DbConst;
import com.framework.constant.DbConstList;
import com.framework.entity.Event;
import com.framework.task.BaseEventTask;
import com.log.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class EventHandleTask extends BaseEventTask {

    /**
     * 事件服务
     */
    @Autowired
    private EventService eventService;

    @Override
    public List<Event> findEvents(Integer dealGroup) {
        return eventRepository.findTop100ByDealGroupAndStatusAndReceiverOrderById(dealGroup, DbConstList.Event.Status.NEW
                , DbConst.Event.SenderOrReceiver.LOG_SERVICE);
    }

    @Override
    protected boolean dealEvent(Event event) {
        String type = event.getEventType();
        if (DbConst.Event.EventType.ToLog.RECORD_LOGIN_LOG.equals(type)) {
            eventService.addLoginLog(event);
            return true;
        }
        return false;
    }

}
