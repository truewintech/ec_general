/*
 * Copy Right@
 */
package com.log.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Data
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Entity
@Table(name = "behavior_log")
public class BehaviorLog {

    /**
     * 主表主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 账户id
     */
    @Column(name = "account_id")
    private String accountId;

    /**
     * 账户
     */
    @Column(name = "account")
    private String account;

    /**
     * 登录ip
     */
    @Column(name = "ip")
    private String ip;

    /**
     * 域名
     */
    @Column(name = "domain")
    private String domain;

    /**
     * 地址
     */
    @Column(name = "url")
    private String url;

    /**
     * 来源地址
     */
    @Column(name = "referrer")
    private String referrer;

    /**
     * 访问设备
     */
    @Column(name = "user_agent")
    private String userAgent;

    /**
     * 页面参数
     */
    @Column(name = "params")
    private String params;

    /**
     * 访客标识
     */
    @Column(name = "visitor_id")
    private String visitorId;

    /**
     * 访问时间
     */
    @Column(name = "visit_time")
    private Date visitTime;

    /**
     * 访问时长
     */
    @Column(name = "visit_duration")
    private int visitDuration;

}
