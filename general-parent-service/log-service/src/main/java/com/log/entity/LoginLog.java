/*
 * Copy Right@
 */
package com.log.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Data
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Entity
@Table(name = "login_log")
public class LoginLog {

    /**
     * 主表主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 账户id
     */
    @Column(name = "account_id")
    private String accountId;

    /**
     * 账户
     */
    @Column(name = "account")
    private String account;

    /**
     * 登录ip
     */
    @Column(name = "ip")
    private String ip;

    /**
     * 登录时间
     */
    @Column(name = "login_time")
    private Date loginTime;

    /**
     * 手机型号
     */
    @Column(name = "mobile_model")
    private String mobileModel;

    /**
     * 手机系统
     */
    @Column(name = "mobile_system")
    private String mobileSystem;

    /**
     * 手机系统版本
     */
    @Column(name = "mobile_system_version")
    private String mobileSystemVersion;

    /**
     * 手机系统版本
     */
    @Column(name = "mobile_sn")
    private String mobileSn;

    /**
     * 省
     */
    @Column(name = "province")
    private String province;

    /**
     * 市
     */
    @Column(name = "city")
    private String city;

    /**
     * 区
     */
    @Column(name = "district")
    private String district;

    /**
     * 详细地址
     */
    @Column(name = "address")
    private String address;

    /**
     * 软件版本
     */
    @Column(name = "soft_version")
    private String softVersion;

    @PrePersist
    public void prePersist() {
        if (this.loginTime == null) {
            this.loginTime = new Date();
        }
    }
}
