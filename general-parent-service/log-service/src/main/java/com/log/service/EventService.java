/*
 * Copy Right@
 */
package com.log.service;

import com.framework.entity.Event;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface EventService {

    /**
     * 新增登录记录
     *
     * @param event 事件
     */
    void addLoginLog(Event event);
}
