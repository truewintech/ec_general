/*
 * Copy Right@
 */
package com.log.service.impl;

import com.common.util.rest.GeneralRestTemplateUtil;
import com.log.service.RestService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * <pre>
 *
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
public class RestServiceImpl implements RestService {

    /**
     * 数据服务地址
     */
    @Value("${serviceUrl.general.data}")
    private String dataServiceUrl;

    @Override
    @HystrixCommand(fallbackMethod = "getSystemParameterFallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000")
    })
    public String getSystemParameter(String id) {
        return GeneralRestTemplateUtil.Data.getParam(dataServiceUrl, id);
    }

    private String getSystemParameterFallback(String id) {
        return null;
    }
}
