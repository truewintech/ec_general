/*
 * Copy Right@
 */
package com.log.service;

import com.log.entity.LoginLog;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface LoginLogService {

    /**
     * 根据用户ID获取上次登录记录
     *
     * @param userId 用户ID
     * @return 上次登录记录
     */
    LoginLog findLastLoginLogByUserId(Integer userId);

}
