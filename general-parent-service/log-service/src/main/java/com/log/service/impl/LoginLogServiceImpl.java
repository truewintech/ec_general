/*
 * Copy Right@
 */
package com.log.service.impl;

import com.log.entity.LoginLog;
import com.log.repository.LoginLogRepository;
import com.log.service.LoginLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class LoginLogServiceImpl implements LoginLogService {

    @Autowired
    private LoginLogRepository loginLogRepository;

    @Override
    public LoginLog findLastLoginLogByUserId(Integer userId) {
        return loginLogRepository.findLastLoginLogByUserId(userId);
    }
}
