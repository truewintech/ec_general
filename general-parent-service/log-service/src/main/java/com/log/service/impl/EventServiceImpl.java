/*
 * Copy Right@
 */
package com.log.service.impl;

import com.framework.entity.Event;
import com.log.entity.LoginLog;
import com.log.repository.LoginLogRepository;
import com.log.service.EventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class EventServiceImpl implements EventService {

    /**
     * 登录日志数据仓库
     */
    @Autowired
    private LoginLogRepository loginLogRepository;

    @Override
    public void addLoginLog(Event event) {
        // TODO 解析event，封装日志
        LoginLog loginLog = new LoginLog();

        loginLogRepository.save(loginLog);
    }
}
