/*
 * Copy Right@
 */
package com.tcc.task;

import com.alibaba.fastjson.JSON;
import com.common.constant.DbConst;
import com.framework.constant.DbConstList;
import com.framework.entity.Event;
import com.framework.entity.TccEvent;
import com.framework.repository.EventRepository;
import com.framework.task.BaseEventTask;
import com.tcc.service.RestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class EventHandleTask extends BaseEventTask {

    /**
     * 远程调用服务
     */
    @Autowired
    private RestService restService;

    /**
     * 事件数据仓库
     */
    @Autowired
    private EventRepository eventRepository;

    @Override
    public List<Event> findEvents(Integer dealGroup) {
        return eventRepository.findTop100ByDealGroupAndStatusAndReceiverOrderById(dealGroup, DbConstList.Event.Status.NEW
                , DbConst.Event.SenderOrReceiver.TCC_SERVICE);
    }

    @Override
    protected boolean dealEvent(Event event) {
        if (DbConst.Event.EventType.ToTcc.CANCEL_TCC.equals(event.getEventType())) {
            String payload = event.getPayload();
            TccEvent tccEvent = JSON.toJavaObject(JSON.parseObject(payload), TccEvent.class);
            if (restService.cancelTccEvent(tccEvent)) {
                event.setStatus(DbConstList.Event.Status.FINISHED);
                eventRepository.save(event);
                return true;
            }
        }
        return false;
    }

}
