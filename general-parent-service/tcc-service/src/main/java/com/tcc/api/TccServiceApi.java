/*
 * Copy Right@
 */
package com.tcc.api;

import com.framework.entity.TccEvent;
import com.framework.exception.MessageException;
import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.tcc.service.TccService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@Api(value = "tcc事务管理器api")
public class TccServiceApi extends BaseApi {

    /**
     * tcc事务服务
     */
    @Autowired
    private TccService tccService;

    /**
     * 确认tcc事务
     *
     * @param tccEvents 事件列表
     * @return 是否成功
     */
    @ApiOperation(value = "确认tcc事务")
    @PostMapping(value = TccEvent.CONFIRM_URL)
    public Result confirmTcc(@RequestBody List<TccEvent> tccEvents) {
        if (tccService.confirmTcc(tccEvents)) {
            return success();
        }

        throw new MessageException("确认tcc事务失败");
    }

    /**
     * 取消tcc事务
     *
     * @param tccEvents 事件列表
     * @return 是否成功
     */
    @ApiOperation(value = "取消tcc事务")
    @PostMapping(value = TccEvent.CANCEL_URL)
    public Result cancelTcc(@RequestBody List<TccEvent> tccEvents) {
        tccService.cancelTcc(tccEvents);

        return success();
    }

}
