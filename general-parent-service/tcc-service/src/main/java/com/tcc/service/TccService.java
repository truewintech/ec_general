/*
 * Copy Right@
 */
package com.tcc.service;

import com.framework.entity.TccEvent;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface TccService {

    /**
     * 确认tcc事件
     *
     * @param tccEvents tcc事件列表
     * @return 是否成功
     */
    boolean confirmTcc(List<TccEvent> tccEvents);

    /**
     * 取消tcc事件
     *
     * @param tccEvents tcc事件列表
     */
    void cancelTcc(List<TccEvent> tccEvents);
}
