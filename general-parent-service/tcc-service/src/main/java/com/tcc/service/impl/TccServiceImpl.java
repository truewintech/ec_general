/*
 * Copy Right@
 */
package com.tcc.service.impl;

import com.alibaba.fastjson.JSON;
import com.common.constant.DbConst;
import com.framework.entity.Event;
import com.framework.entity.TccEvent;
import com.framework.repository.EventRepository;
import com.tcc.service.RestService;
import com.tcc.service.TccService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class TccServiceImpl implements TccService {

    /**
     * 远程服务
     */
    @Autowired
    private RestService restService;

    /**
     * 事件数据仓库
     */
    @Autowired
    private EventRepository eventRepository;

    @Override
    public boolean confirmTcc(List<TccEvent> tccEvents) {
        if (tccEvents == null || tccEvents.isEmpty()) {
            return true;
        }

        final boolean[] result = {true};
        List<Event> events = new ArrayList<>();
        tccEvents.forEach(tccEvent -> {
            if (result[0]) {
                log.info("------------confirmTccStarted at" + new Date().toString() + "------------");
                result[0] = restService.confirmTccEvent(tccEvent);
                log.info("------------confirmTccEnded at" + new Date().toString() + "------------");
                if (!result[0]) {
                    log.error("tcc事务确认失败,serviceId:{},eventId:{}"
                            , tccEvent.getServiceId(), tccEvent.getId());
                }
            }

            // 存待回滚TccEvent
            Event event = new Event();
            event.setPayload(JSON.toJSONString(tccEvent));
            event.setReceiver(DbConst.Event.SenderOrReceiver.TCC_SERVICE);
            event.setSender(DbConst.Event.SenderOrReceiver.TCC_SERVICE);
            event.setEventType(DbConst.Event.EventType.ToTcc.CANCEL_TCC);
            events.add(event);
        });

        if (!result[0]) {
            // 记录回滚事件
            eventRepository.save(events);
        }

        return result[0];
    }

    @Override
    public void cancelTcc(List<TccEvent> tccEvents) {
        if (tccEvents == null || tccEvents.isEmpty()) {
            return;
        }

        tccEvents.forEach(tccEvent -> {
            if (!restService.cancelTccEvent(tccEvent)) {
                log.error("tcc事务取消失败,serviceId:{},eventId:{}"
                        , tccEvent.getServiceId(), tccEvent.getId());

                // 记录回滚事件
                Event event = new Event();
                event.setPayload(JSON.toJSONString(tccEvent));
                event.setReceiver(DbConst.Event.SenderOrReceiver.TCC_SERVICE);
                event.setSender(DbConst.Event.SenderOrReceiver.TCC_SERVICE);
                event.setEventType(DbConst.Event.EventType.ToTcc.CANCEL_TCC);
                eventRepository.save(event);
            }
        });
    }
}
