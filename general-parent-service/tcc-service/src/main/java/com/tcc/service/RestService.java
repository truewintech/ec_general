/*
 * Copy Right@
 */
package com.tcc.service;

import com.framework.entity.TccEvent;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface RestService {

    /**
     * 确认tcc事件
     *
     * @param tccEvent tcc事件
     * @return 结果
     */
    boolean confirmTccEvent(TccEvent tccEvent);

    /**
     * 取消tcc事件
     *
     * @param tccEvent tcc事件
     * @return 结果
     */
    boolean cancelTccEvent(TccEvent tccEvent);
}
