/*
 * Copy Right@
 */
package com.tcc.service.impl;

import com.framework.entity.TccEvent;
import com.framework.util.RestTemplateUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.tcc.service.RestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <pre>
 *
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class RestServiceImpl implements RestService {

    @Override
    @HystrixCommand(fallbackMethod = "tccEventFallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000")
    })
    public boolean confirmTccEvent(TccEvent tccEvent) {
        return RestTemplateUtil.post(tccEvent.getServiceId() + TccEvent.CONFIRM_URL, tccEvent);
    }

    @Override
    @HystrixCommand(fallbackMethod = "tccEventFallback", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "10000")
    })
    public boolean cancelTccEvent(TccEvent tccEvent) {
        return RestTemplateUtil.post(tccEvent.getServiceId() + TccEvent.CANCEL_URL, tccEvent);
    }

    /**
     * tcc事务熔断
     *
     * @param tccEvent tcc事务
     * @return 失败
     */
    private boolean tccEventFallback(TccEvent tccEvent) {
        log.error("tcc事务熔断，type：{}，serviceId：{}", tccEvent.getTccType(), tccEvent.getServiceId());
        return false;
    }
}
