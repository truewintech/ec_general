SET FOREIGN_KEY_CHECKS=0;

drop table if exists area;

drop table if exists delivery_company;

drop table if exists system_parameter;

drop table if exists dictionary;

/*==============================================================*/
/* Table: area                                                  */
/*==============================================================*/
create table area
(
   id                   int not null auto_increment comment '主键',
   parent_id            int comment '父id',
   name                 varchar(32) comment '名称',
   level                int comment '等级',
   code                 varchar(32) comment '行政区域编码',
   create_user          int comment '创建人',
   create_time          datetime not null comment '创建时间',
   update_user          int comment '更新人',
   update_time          datetime comment '更新时间',
   is_active            bit comment '是否有效',
   is_delete            bit comment '是否删除',
   primary key (id)
)
;

alter table area comment '区域信息';

/*==============================================================*/
/* Index: idx_area_parent_id                                    */
/*==============================================================*/
create index idx_area_parent_id on area
(
   parent_id
);

/*==============================================================*/
/* Index: idx_area_level                                        */
/*==============================================================*/
create index idx_area_level on area
(
   level
);

/*==============================================================*/
/* Index: idx_area_is_active_is_del                             */
/*==============================================================*/
create index idx_area_is_active_is_del on area
(
   is_active,
   is_delete
);



/*==============================================================*/
/* Table: delivery_company                                      */
/*==============================================================*/
create table delivery_company
(
   id                   int not null auto_increment comment '主键',
   code                 varchar(32) not null comment '编码',
   company_name         varchar(32) not null comment '公司名称',
   primary key (id),
   key ak_code (code),
   key ak_company_name (company_name)
);

alter table delivery_company comment '物流公司表';

/*==============================================================*/
/* Index: code                                                  */
/*==============================================================*/
create unique index code on delivery_company
(
   code
);

/*==============================================================*/
/* Index: company_name                                          */
/*==============================================================*/
create unique index company_name on delivery_company
(
   company_name
);

/*==============================================================*/
/* Table: system_parameter                                      */
/*==============================================================*/
create table system_parameter
(
   id                   varchar(64) not null comment '主键',
   value                varchar(256) comment '值',
   detail               varchar(256) comment '详情',
   primary key (id)
)
;

/*==============================================================*/
/* Table: dictionary                                            */
/*==============================================================*/
CREATE TABLE `dictionary` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `type` varchar(50) NOT NULL COMMENT '类型',
  `code` varchar(50) DEFAULT NULL COMMENT '代码',
  `description` varchar(50) DEFAULT NULL COMMENT '描述',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '父ID',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `is_active` bit(1) DEFAULT NULL COMMENT '是否激活',
  PRIMARY KEY (`id`),
  KEY `IDX_D_TYPE` (`type`),
  KEY `IDX_D_SORT` (`sort`),
  KEY `IDX_D_IS_ACTIVE` (`is_active`),
  KEY `IDX_D_PARENT_ID` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='静态数据字典';


alter table system_parameter comment '系统参数';

alter table area add constraint fk_area_r_area foreign key (parent_id)
      references area (id) on delete restrict on update restrict;

SET FOREIGN_KEY_CHECKS=1;