-- 初始化区域表
INSERT INTO `area` VALUES (1, NULL, '中国', 0, '', NULL, NULL, NULL, NULL, 1, 0);
UPDATE area SET is_delete=1 WHERE `level` = 4 or `level` = 5;
ALTER TABLE `area` AUTO_INCREMENT=4000000;

-- 初始化系统参数
INSERT INTO `system_parameter` VALUES ("is_need_product_audit", "false", "false:表示商品不需要审核;true:表示商品需要审核");
INSERT INTO `system_parameter` VALUES ("sellerDefaultGrowth", "0", "卖家默认成长值");
INSERT INTO `system_parameter` VALUES ("sellerDefaultPoint", "0", "卖家默认积分");
INSERT INTO `system_parameter` VALUES ("buyerDefaultGrowth", "0", "买家默认成长值");
INSERT INTO `system_parameter` VALUES ("buyerDefaultPoint", "0", "买家默认积分");

-- 初始化快递公司数据
INSERT INTO `delivery_company`(`code`, `company_name`) VALUES ('ems', 'EMS');
INSERT INTO `delivery_company`(`code`, `company_name`) VALUES ('huitongkuaidi', '汇通快递');
INSERT INTO `delivery_company`(`code`, `company_name`) VALUES ('rufengda', '如风达快递');
INSERT INTO `delivery_company`(`code`, `company_name`) VALUES ('yuantong', '圆通速递');
INSERT INTO `delivery_company`(`code`, `company_name`) VALUES ('yunda', '韵达快运');
INSERT INTO `delivery_company`(`code`, `company_name`) VALUES ('yuntongkuaidi', '运通快递');
INSERT INTO `delivery_company`(`code`, `company_name`) VALUES ('shunfeng', '顺丰速递');
INSERT INTO `delivery_company`(`code`, `company_name`) VALUES ('shentong', '申通快递');
INSERT INTO `delivery_company`(`code`, `company_name`) VALUES ('tiantian', '天天快递');
INSERT INTO `delivery_company`(`code`, `company_name`) VALUES ('lianbangkuaidi', '联邦快递');
ALTER TABLE `delivery_company` AUTO_INCREMENT=100;

INSERT INTO `dictionary` (`id`, `type`, `code`, `description`, `parent_id`, `sort`, `is_active`) VALUES ('MALL_RESOURCE.TB', 'MALL_RESOURCE', 'TB', '淘宝', NULL, '1', '');
INSERT INTO `dictionary` (`id`, `type`, `code`, `description`, `parent_id`, `sort`, `is_active`) VALUES ('MALL_RESOURCE.JD', 'MALL_RESOURCE', 'JD', '京东', NULL, '2', '');
INSERT INTO `dictionary` (`id`, `type`, `code`, `description`, `parent_id`, `sort`, `is_active`) VALUES ('MALL_RESOURCE.WPH', 'MALL_RESOURCE.JD', 'WPH', '唯品会', NULL, '3', '');