/*
 * Copy Right@ 楚文互联
 */
package com.data.repository;

import com.data.entity.Dictionary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author xiayulei
 * @date 2018/10/31
 */
public interface DictionaryRepository extends JpaRepository<Dictionary, String>,
        JpaSpecificationExecutor<Dictionary> {

    /**
     * 获取可用数据字典的所有顶级类型
     *
     * @return 类型列表
     */
    @Query("SELECT d.type FROM Dictionary d WHERE d.isActive = 1 AND d.parentId IS NULL GROUP BY d.type")
    List<String> findAllTypeByIsActiveIsTrue();

    /**
     * 根据parentId获取子集合的所有类型
     *
     * @param parentId 父级
     * @return 类型列表
     */
    @Query("SELECT d.type FROM Dictionary d WHERE d.isActive = 1 AND d.parentId = ?1 GROUP BY d.type")
    List<String> findAllTypeByParentAndIsActiveIsTrue(String parentId);


    /**
     * 根据类型获取数据字典数据
     *
     * @param type     类型
     * @param isActive 是否激活
     * @return 主数据列表
     */
    List<Dictionary> findByTypeAndIsActiveOrderBySort(String type, Boolean isActive);
}
