/*
 * Copy Right@
 */
package com.data.repository;

import com.data.entity.Area;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface AreaRepository extends JpaRepository<Area, Integer> {

    /**
     * 获取启用地址列表
     *
     * @param parentId 父id
     * @return 地址列表
     */
    List<Area> findByParentIdAndIsActiveIsTrue(Integer parentId);

    /**
     * 获取地址列表
     *
     * @param parentId 父id
     * @return 地址列表
     */
    List<Area> findByParentId(Integer parentId);
}
