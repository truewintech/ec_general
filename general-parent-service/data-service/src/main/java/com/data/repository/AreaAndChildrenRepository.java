/*
 * Copy Right@
 */
package com.data.repository;

import com.data.entity.AreaAndChildren;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface AreaAndChildrenRepository extends JpaRepository<AreaAndChildren, Integer> {

    /**
     * 通过父查找所有地址
     * @param parentId 父id
     * @return 地址列表
     */
    List<AreaAndChildren> findAllByParentId(Integer parentId);
}
