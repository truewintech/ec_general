/*
 * Copy Right@
 */
package com.data.repository;

import com.data.entity.SystemParameter;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface SystemParameterRepository extends JpaRepository<SystemParameter, String> {
}
