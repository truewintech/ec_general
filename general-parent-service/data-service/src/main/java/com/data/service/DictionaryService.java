/*
 * Copy Right@ 楚文互联
 */
package com.data.service;

import java.util.Map;

/**
 * @author xiayulei
 * @date 2018/10/31
 */
public interface DictionaryService {

    /**
     * 获取所有主数据列表以及子集合
     *
     * @return 主数据列表
     */
    Map<String, Object> findAll();

    /**
     * 根据类型获取数据
     *
     * @param type 类型
     * @return 数据
     */
    Map<String, Object> findByType(String type);
}
