/*
 * Copy Right@
 */
package com.data.service.impl;

import com.data.entity.Area;
import com.data.entity.AreaAndChildren;
import com.data.repository.AreaAndChildrenRepository;
import com.data.repository.AreaRepository;
import com.data.service.AreaService;
import com.framework.exception.MessageException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
@CacheConfig(cacheNames = "AreaServiceImpl")
public class AreaServiceImpl implements AreaService {

    /**
     * 地址数据仓库
     */
    @Autowired
    private AreaRepository areaRepository;

    /**
     * 地址数据仓库
     */
    @Autowired
    private AreaAndChildrenRepository areaAndChildrenRepository;

    @Cacheable
    @Override
    public List<AreaAndChildren> findAllByParentId(Integer parentId) {
        if (parentId == null) {
            throw new MessageException("查询条件parentId不能为空");
        }
        return areaAndChildrenRepository.findAllByParentId(parentId);
    }

    @Cacheable
    @Override
    public List<Area> findActiveAreaListByParentId(Integer parentId) {
        // 根据父id查询启用地址列表
        if (parentId == null) {
            throw new MessageException("查询条件parentId不能为空");
        }
        return areaRepository.findByParentIdAndIsActiveIsTrue(parentId);
    }

    @Cacheable
    @Override
    public List<Area> findAreaListByParentId(Integer parentId) {
        // 根据父id查询启用地址列表
        if (parentId == null) {
            throw new MessageException("查询条件parentId不能为空");
        }
        return areaRepository.findByParentId(parentId);
    }

    @Override
    public Area findAreaById(Integer id) {
        if (id == null) {
            log.error("查询失败,id为空");
            throw new MessageException("查询失败");
        }
        return areaRepository.findOne(id);
    }

    @CacheEvict(allEntries = true)
    @Override
    public void addArea(Area area) {
        if (area == null) {
            log.error("新增失败,参数为空");
            throw new MessageException("新增失败");
        }
        areaRepository.save(area);
    }

    @CacheEvict(allEntries = true)
    @Override
    public void updateArea(Area area) {
        if (area == null || area.getId() == null) {
            log.error("编辑失败,参数为空");
            throw new MessageException("编辑失败");
        }
        areaRepository.save(area);
    }

    @CacheEvict(allEntries = true)
    @Transactional
    @Override
    public void deleteArea(Integer[] ids) {
        if (ids != null && ids.length > 0) {
            for (Integer id : ids) {
                Area area = areaRepository.findOne(id);
                if (area != null) {
                    area.setIsDelete(true);
                    areaRepository.save(area);
                }
            }
        }
    }

    @CacheEvict(allEntries = true)
    @Transactional
    @Override
    public void setUnActive(Integer[] ids) {
        if (ids != null && ids.length > 0) {
            for (Integer id : ids) {
                Area area = areaRepository.findOne(id);
                if (area != null) {
                    area.setIsActive(false);
                    areaRepository.save(area);
                }
            }
        }
    }

    @CacheEvict(allEntries = true)
    @Transactional
    @Override
    public void setActive(Integer[] ids) {
        if (ids != null && ids.length > 0) {
            for (Integer id : ids) {
                Area area = areaRepository.findOne(id);
                if (area != null) {
                    area.setIsActive(true);
                    areaRepository.save(area);
                }
            }
        }
    }
}
