/*
 * Copy Right@
 */
package com.data.service.impl;

import com.common.constant.RedisKey;
import com.data.entity.SystemParameter;
import com.data.repository.SystemParameterRepository;
import com.data.service.SystemParameterService;
import com.framework.exception.MessageException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
@CacheConfig(cacheNames = "SystemParameterService")
public class SystemParameterServiceImpl implements SystemParameterService {

    /**
     * 系统参数数据仓库
     */
    @Autowired
    SystemParameterRepository systemParameterRepository;

    @Cacheable(key = RedisKey.DataService.SystemParameter.IMPL +
            RedisKey.DataService.SystemParameter.Method.FIND_SYSTEM_PARAMETER + "#id")
    @Override
    public SystemParameter findSystemParameter(String id) {
        return systemParameterRepository.findOne(id);
    }

    @Override
    public Page<SystemParameter> findSystemParameterList(Pageable pageable) {
        return systemParameterRepository.findAll(pageable);
    }

    @Override
    public void addSystemParameter(SystemParameter systemParameter) {
        if (systemParameter == null) {
            log.error("添加失败,参数为空");
            throw new MessageException("添加失败");
        }
        //id为key不能重复
        String id = systemParameter.getId();
        SystemParameter sp = this.findSystemParameter(id);
        if (sp != null) {
            log.error("添加失败,该key已存在！");
            throw new MessageException("添加失败，该key已存在！");
        }
        systemParameterRepository.save(systemParameter);
    }

    @CacheEvict(key = RedisKey.DataService.SystemParameter.IMPL +
            RedisKey.DataService.SystemParameter.Method.FIND_SYSTEM_PARAMETER + "#systemParameter.id")
    @Override
    public void updateSystemParameter(SystemParameter systemParameter) {
        if (systemParameter == null) {
            log.error("编辑失败,参数为空");
            throw new MessageException("编辑失败");
        }
        String id = systemParameter.getId();
        SystemParameter sp = this.findSystemParameter(id);
        if (sp == null) {
            log.error("编辑失败,该key不存在");
            throw new MessageException("编辑失败，该key不存在！");
        }
        systemParameterRepository.save(systemParameter);
    }

}
