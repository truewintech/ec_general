/*
 * Copy Right@
 */
package com.data.service;

import com.data.entity.SystemParameter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface SystemParameterService {

    /**
     * 获取系统参数值
     *
     * @param id 主键
     * @return 系统参数值
     */
    SystemParameter findSystemParameter(String id);

    /**
     * 获取系统参数列表
     *
     * @param pageable 分页
     * @return 系统参数列表
     */
    Page<SystemParameter> findSystemParameterList(Pageable pageable);

    /**
     * 新增系统参数
     *
     * @param systemParameter 系统参数对象
     */
    void addSystemParameter(SystemParameter systemParameter);

    /**
     * 修改系统参数
     *
     * @param systemParameter 系统参数对象
     */
    void updateSystemParameter(SystemParameter systemParameter);

}
