/*
 * Copy Right@ 楚文互联
 */
package com.data.service.impl;

import com.common.constant.RedisKey;
import com.data.entity.Dictionary;
import com.data.repository.DictionaryRepository;
import com.data.service.DictionaryService;
import com.framework.exception.MessageException;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author xiayulei
 * @date 2018/10/31
 */
@Service
@Slf4j
@CacheConfig(cacheNames = "DictionaryServiceImpl")
public class DictionaryServiceImpl implements DictionaryService {

    /**
     * 数据字典仓库
     */
    @Autowired
    private DictionaryRepository dictionaryRepository;

    @Override
    @Cacheable(key = RedisKey.DataService.Dictionary.IMPL + RedisKey.DataService.Dictionary.Method.FIND_ALL)
    public Map<String, Object> findAll() {
        List<String> typeList = dictionaryRepository.findAllTypeByIsActiveIsTrue();
        if (typeList == null || typeList.size() == 0) {
            return null;
        }
        return getDictionaryAndSubDataByTypeList(typeList);
    }

    @Override
    @Cacheable(key = RedisKey.DataService.Dictionary.IMPL + RedisKey.DataService.Dictionary.Method.FIND_BY_TYPE + "#type")
    public Map<String, Object> findByType(String type) {
        if (StringUtils.isBlank(type)) {
            log.error("获取类型失败，type为空");
            throw new MessageException("获取类型失败");
        }
        return getDictionaryAndSubDataByTypeList(Lists.newArrayList(type));
    }

    /**
     * 根据类型列表获取字典包含子集合的数据
     *
     * @param typeList 类型列表
     * @return 字典包含子集合的数据
     */
    private Map<String, Object> getDictionaryAndSubDataByTypeList(List<String> typeList) {
        if (typeList == null || typeList.size() == 0) {
            return null;
        }
        Map<String, Object> result = new HashMap<>(16);
        for (String type : typeList) {
            if (StringUtils.isBlank(type)) {
                continue;
            }
            List<Dictionary> dictionaryList = dictionaryRepository.findByTypeAndIsActiveOrderBySort(type, true);
            if (dictionaryList != null) {
                for (Dictionary dictionary : dictionaryList) {
                    if (dictionary == null) {
                        continue;
                    }
                    String id = dictionary.getId();
                    if (StringUtils.isBlank(id)) {
                        continue;
                    }
                    List<String> subTypeList = dictionaryRepository.findAllTypeByParentAndIsActiveIsTrue(id);
                    if (subTypeList != null && subTypeList.size() > 0) {
                        dictionary.setSubData(getDictionaryAndSubDataByTypeList(subTypeList));
                    }
                }
                result.put(type, dictionaryList);
            }
        }
        return result;
    }
}
