/*
 * Copy Right@
 */
package com.data.service;

import com.data.entity.Area;
import com.data.entity.AreaAndChildren;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface AreaService {

    /**
     * 通过父查找所有地址
     *
     * @param parentId 父id
     * @return 地址列表
     */
    List<AreaAndChildren> findAllByParentId(Integer parentId);

    /**
     * 根据父id获取启用地址列表
     *
     * @param parentId 父id
     * @return 地址列表
     */
    List<Area> findActiveAreaListByParentId(Integer parentId);

    /**
     * 根据父id获取地址列表
     *
     * @param parentId 父id
     * @return 地址列表
     */
    List<Area> findAreaListByParentId(Integer parentId);

    /**
     * 根据id获取地址信息
     *
     * @param id 主键
     * @return 地址
     */
    Area findAreaById(Integer id);

    /**
     * 新增地址
     *
     * @param area 地址对象
     */
    void addArea(Area area);

    /**
     * 编辑地址
     *
     * @param area 地址对象
     */
    void updateArea(Area area);

    /**
     * 删除地址
     *
     * @param ids 将要删除地址id集合
     */
    void deleteArea(Integer[] ids);

    /**
     * 设置为未启用状态
     *
     * @param ids 设置为未启用状态地址id集合
     */
    void setUnActive(Integer[] ids);

    /**
     * 设置为启用状态
     *
     * @param ids 设置为启用状态地址id集合
     */
    void setActive(Integer[] ids);

}
