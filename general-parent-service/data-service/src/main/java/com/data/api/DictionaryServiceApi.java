/*
 * Copy Right@ 楚文互联
 */
package com.data.api;

import com.data.service.DictionaryService;
import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiayulei
 * @date 2018/10/31
 */
@RestController
@RequestMapping("dictionary")
@Api(value = "静态数据字典API")
public class DictionaryServiceApi extends BaseApi {

    /**
     * 数据字典服务
     */
    @Autowired
    private DictionaryService dictionaryService;

    /**
     * 获取所有主数据列表以及子集合
     *
     * @return 所有主数据列表
     */
    @ApiOperation("获取所有主数据列表以及子集合")
    @GetMapping("findAll")
    public Result findAll() {
        return success(dictionaryService.findAll());
    }

    /**
     * 根据类型获取数据
     *
     * @param type 类型
     * @return 数据
     */
    @ApiOperation("根据类型获取数据")
    @GetMapping("findByType")
    public Result findByType(@RequestParam String type) {
        return success(dictionaryService.findByType(type));
    }
}
