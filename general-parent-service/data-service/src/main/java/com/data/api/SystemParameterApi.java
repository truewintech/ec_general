/*
 * Copy Right@
 */
package com.data.api;

import com.data.entity.SystemParameter;
import com.data.service.SystemParameterService;
import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.framework.validator.group.Insert;
import com.framework.validator.group.Update;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("systemParameter")
@Api(value = "系统参数API")
@Slf4j
public class SystemParameterApi extends BaseApi {

    /**
     * 系统参数服务
     */
    @Autowired
    private SystemParameterService systemParameterService;

    /**
     * 获取系统参数值
     *
     * @param id 主键
     * @return 获取系统参数值
     */
    @ApiOperation(value = "获取系统参数值")
    @GetMapping("findSystemParameter")
    public Result findSystemParameter(@RequestParam String id) {
        SystemParameter systemParameter = systemParameterService.findSystemParameter(id);

        return success(systemParameter);
    }

    /**
     * 获取系统参数列表
     *
     * @return 获取系统参数值
     */
    @ApiOperation(value = "获取系统参数列表")
    @GetMapping("findSystemParameterList")
    public Result findSystemParameterList(@PageableDefault Pageable pageable) {
        Page<SystemParameter> systemParameters = systemParameterService.findSystemParameterList(pageable);

        return success(systemParameters);
    }

    /**
     * 新增系统参数
     *
     * @param systemParameter 系统参数对象
     * @return 是否成功
     */
    @ApiOperation(value = "新增系统参数")
    @PostMapping("addSystemParameter")
    public Result addSystemParameter(@Validated({Insert.class}) @RequestBody SystemParameter systemParameter, BindingResult result) {
        if (result.hasErrors()) {
            return validError(result);
        }
        systemParameterService.addSystemParameter(systemParameter);
        return success();
    }

    /**
     * 修改系统参数
     *
     * @param systemParameter 系统参数对象
     * @return 是否成功
     */
    @ApiOperation(value = "修改系统参数")
    @PostMapping("updateSystemParameter")
    public Result updateSystemParameter(@Validated({Update.class}) @RequestBody SystemParameter systemParameter, BindingResult result) {
        if (result.hasErrors()) {
            return validError(result);
        }
        systemParameterService.updateSystemParameter(systemParameter);
        return success();
    }
}
