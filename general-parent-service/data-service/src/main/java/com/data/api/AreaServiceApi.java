/*
 * Copy Right@
 */
package com.data.api;

import com.data.entity.Area;
import com.data.service.AreaService;
import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.framework.validator.group.Insert;
import com.framework.validator.group.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("area")
@Api(value = "地址服务API")
@Slf4j
public class AreaServiceApi extends BaseApi {

    /**
     * 地址信息服务
     */
    @Autowired
    private AreaService areaService;

    /**
     * 根据父id获取所有地址列表
     *
     * @param parentId 父id
     * @return 地址列表
     */
    @ApiOperation("获取所有地址")
    @GetMapping(value = "findAllByParentId")
    public Result findAllByParentId(@RequestParam Integer parentId) {
        return successEager(areaService.findAllByParentId(parentId));
    }

    /**
     * 根据父id获取启用地址列表
     *
     * @param parentId 父id
     * @return 地址列表
     */
    @ApiOperation("获取所有启用地址")
    @GetMapping(value = "findActiveAreaListByParentId")
    public Result findActiveAreaListByParentId(@RequestParam Integer parentId) {
        return success(areaService.findActiveAreaListByParentId(parentId));
    }

    /**
     * 根据父id获取地址列表
     *
     * @param parentId 父id
     * @return 地址列表
     */
    @ApiOperation("获取所有地址")
    @GetMapping(value = "findAreaListByParentId")
    public Result findAreaListByParentId(@RequestParam Integer parentId) {
        return success(areaService.findAreaListByParentId(parentId));
    }

    /**
     * 根据id获取地址对象
     *
     * @param id 主键
     * @return 地址
     */
    @ApiOperation("根据id获取地址对象")
    @GetMapping(value = "findAreaById")
    public Result findAreaById(@RequestParam Integer id) {
        return success(areaService.findAreaById(id));
    }

    /**
     * 新增地址
     *
     * @param area 系统参数对象
     * @return 是否成功
     */
    @ApiOperation(value = "新增地址")
    @PostMapping("addArea")
    public Result addArea(@Validated({Insert.class}) @RequestBody Area area, BindingResult result) {
        if (result.hasErrors()) {
            return validError(result);
        }
        areaService.addArea(area);
        return success();
    }

    /**
     * 编辑地址
     *
     * @param area 地址对象
     * @return 是否成功
     */
    @ApiOperation(value = "编辑地址")
    @PostMapping("updateArea")
    public Result updateArea(@Validated({Update.class}) @RequestBody Area area, BindingResult result) {
        if (result.hasErrors()) {
            return validError(result);
        }
        areaService.updateArea(area);
        return success();
    }

    /**
     * 删除地址
     *
     * @param ids 将要删除地址id集合
     * @return 是否成功
     */
    @ApiOperation(value = "删除地址")
    @PostMapping("deleteArea")
    public Result deleteArea(@RequestBody Integer[] ids) {

        areaService.deleteArea(ids);
        return success();
    }

    /**
     * 设置为未启用状态
     *
     * @param ids 设置为未启用状态地址id集合
     * @return 是否成功
     */
    @ApiOperation(value = "设置为未启用状态")
    @PostMapping("setUnActive")
    public Result setUnActive(@RequestBody Integer[] ids) {

        areaService.setUnActive(ids);
        return success();
    }

    /**
     * 设置为启用状态
     *
     * @param ids 设置为启用状态地址id集合
     * @return 是否成功
     */
    @ApiOperation(value = "设置为启用状态")
    @PostMapping("setActive")
    public Result setActive(@RequestBody Integer[] ids) {

        areaService.setActive(ids);
        return success();
    }
}
