/*
 * Copy Right@
 */
package com.data.entity;

import com.common.constant.Const;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Data
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Entity
@EqualsAndHashCode
@Table(name = "area")
@Where(clause = Const.IsDel.CLAUSE)
public class Area extends AreaBase {
}
