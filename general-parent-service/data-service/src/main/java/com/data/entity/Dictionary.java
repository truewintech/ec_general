/*
 * @(#)DictionaryLookup.java 2017年05月04日
 *
 * Copy Right@ 腾骧趋势
 */
package com.data.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Map;

/**
 * <pre>
 * 修改版本: 1.0
 * 修改日期: 2017年05月04日
 * 修改人 :  Tony
 * 修改说明: 初步完成
 * 复审人 :
 * </pre>
 */
@Data
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Entity
@Table(name = "dictionary")
public class Dictionary {

    /**
     * 主键
     */
    @Id
    @Column(name = "id")
    private String id;

    /**
     * 类型
     */
    @Column(name = "type")
    private String type;

    /**
     * code
     */
    @Column(name = "code")
    private String code;

    /**
     * 父ID
     */
    @Column(name = "parent_id")
    private String parentId;

    /**
     * 描述
     */
    @Column(name = "description")
    private String description;

    /**
     * 排序
     */
    @Column(name = "sort")
    private int sort;

    /**
     * 是否激活
     */
    @Column(name = "is_active")
    private Boolean isActive;

    /**
     * 子集合
     */
    @Transient
    private Map<String, Object> subData;

}