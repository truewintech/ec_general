/*
 * Copy Right@
 */
package com.data.entity;

import com.framework.validator.group.Insert;
import com.framework.validator.group.Update;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Data
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Entity
@Table(name = "system_parameter")
public class SystemParameter {

    /**
     * 主表主键
     */
    @Id
    @Column(name = "id")
    @NotBlank(message = "key不能为空", groups = {Insert.class, Update.class})
    @Length(min = 1, max = 64, message = "key长度只能在1-64之内", groups = {
            Insert.class, Update.class})
    private String id;

    /**
     * 值
     */
    @Column(name = "value")
    @Length(max = 128, message = "值长度只能在128以内", groups = {
            Insert.class, Update.class})
    private String value;

    /**
     * 详情
     */
    @Column(name = "detail")
    @Length(max = 200, message = "详情长度只能在200以内", groups = {
            Insert.class, Update.class})
    private String detail;
}
