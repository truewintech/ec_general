/*
 * Copy Right@
 */
package com.data.entity;

import com.common.constant.Const;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Data
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Entity
@EqualsAndHashCode
@Table(name = "area")
@Where(clause = Const.IsDel.CLAUSE)
public class AreaAndChildren extends AreaBase {

    /**
     * 子组织
     */
    @Where(clause = Const.IsDel.CLAUSE)
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "parentId")
    private List<AreaAndChildren> children;

}
