/*
 * Copy Right@
 */
package com.data.entity;

import com.framework.entity.IsActiveAndIsDeleteEntity;
import com.framework.validator.group.Insert;
import com.framework.validator.group.Update;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * <pre>
 * </pre>
 *
 * @author Chenchao
 * @date 2017年09月04日
 */
@MappedSuperclass
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class AreaBase extends IsActiveAndIsDeleteEntity {

    /**
     * 主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 父id
     */
    @Column(name = "parent_id")
    @NotNull(message="父ID不可为空")
    private Integer parentId;

    /**
     * 名称
     */
    @Column(name = "name")
    @NotBlank(message = "地址名不能为空", groups = {Insert.class, Update.class})
    @Length(min = 1, max = 50, message = "地址名的长度只能在1-50之内", groups = {Insert.class, Update.class})
    private String name;

    /**
     * 等级
     */
    @Column(name = "level")
    private Integer level;

    /**
     * 行政区编码
     */
    @Column(name = "code")
    private String code;

}
