/*
 * Copy Right@
 */
package com.data.task;

import com.common.constant.DbConst;
import com.framework.amqp.EventSender;
import com.framework.constant.DbConstList;
import com.framework.entity.Event;
import com.framework.task.BaseEventTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class EventSendTask extends BaseEventTask {

    /**
     * 消息发送服务
     */
    @Autowired
    private EventSender sender;

    @Override
    public List<Event> findEvents(Integer dealGroup) {
        return eventRepository.findTop100ByDealGroupAndStatusAndReceiverIsNotOrderById(dealGroup, DbConstList.Event.Status.NEW
                , DbConst.Event.SenderOrReceiver.DATA_SERVICE);
    }

    @Override
    protected boolean dealEvent(Event event) {
        return sender.sendEvent(event);
    }

}
