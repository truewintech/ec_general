SET FOREIGN_KEY_CHECKS=0;

drop table if exists message_log;

drop table if exists message_template;

/*==============================================================*/
/* Table: message_log                                           */
/*==============================================================*/
create table message_log
(
   id                   int not null auto_increment comment '主键',
   template_id          int comment '模板id',
   message_type         varchar(32) comment '消息类型',
   title                varchar(32) comment '标题',
   content              varchar(512) comment '内容',
   receiver             varchar(32) comment '接收者',
   send_time            datetime not null comment '发送时间',
   send_status          varchar(32) comment '发送状态',
   primary key (id)
);

alter table message_log comment '消息日志';

/*==============================================================*/
/* Table: message_template                                      */
/*==============================================================*/
create table message_template
(
   id                   int not null auto_increment comment '主键',
   title                varchar(256) comment '标题',
   template_content     varchar(256) comment '内容',
   template_type        varchar(32) comment '类型',
   template_key         varchar(64) comment '关键词',
   primary key (id)
);

alter table message_template comment '消息模板';

alter table message_log add constraint fk_msg_log_r_msg_template foreign key (template_id)
      references message_template (id) on delete restrict on update restrict;

SET FOREIGN_KEY_CHECKS=1;