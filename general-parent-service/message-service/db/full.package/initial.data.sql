-- 短信模板
INSERT INTO `message_template` VALUES (1, NULL, '您的验证码是${code}，有效期10分钟，请勿泄漏验证码', 'SMS', 'SMS_67140610');

-- 微信模板
INSERT INTO `message_template` VALUES (100, NULL, '尊敬的用户，您申请的上门服务，工程师已于{time}服务完毕，请您点击进行服务评价【快益点电器服务连锁】', 'WX', 'WX_SPECIAL_COMPLETE');