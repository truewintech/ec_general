/*
 * Copy Right@
 */
package com.message.amqp;

import com.common.amqp.AmqpConfig;
import com.framework.entity.MsgRequestBean;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
public class Sender {

    /**
     * 消息模板
     */
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送注册码
     *
     * @param msgRequestBean 消息信息
     */
    public void sendMessage(MsgRequestBean msgRequestBean) {
        rabbitTemplate.convertAndSend(AmqpConfig.MESSAGE_QUEUE_NAME, msgRequestBean);
    }

}
