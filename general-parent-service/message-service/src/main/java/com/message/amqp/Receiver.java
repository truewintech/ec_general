/*
 * Copy Right@
 */
package com.message.amqp;

import com.common.amqp.AmqpConfig;
import com.common.constant.DbConst;
import com.framework.amqp.EventReceiver;
import com.framework.entity.Event;
import com.framework.entity.MsgRequestBean;
import com.message.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Component
@Slf4j
public class Receiver extends EventReceiver {

    /**
     * 接收服务事件
     *
     * @param event 事件
     * @return 处理是否成功
     */
    @RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = DbConst.Event.SenderOrReceiver.USER_SERVICE)
    @RabbitHandler
    public boolean receiveEvent(Event event) {
        return saveEvent(event);
    }

    /**
     * 短消息服务
     */
    @Autowired
    private MessageService messageService;

    /**
     * 接收发送短信消息
     *
     * @param msgRequestBean 消息信息
     */
    @RabbitListener(containerFactory = "rabbitListenerContainerFactory", queues = AmqpConfig.MESSAGE_QUEUE_NAME)
    @RabbitHandler
    public void receiveMessage(MsgRequestBean msgRequestBean) {
        try {
            messageService.sendMessage(msgRequestBean);
        } catch (Exception ex) {
            log.warn("接收短信消息失败", ex);
        }
    }

}
