/*
 * Copy Right@
 */
package com.message.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Entity
@Table(name = "message_template")
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Getter
@Setter
public class MessageTemplate {

    /**
     * 主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 标题
     */
    @Column(name = "title")
    private String title;

    /**
     * 内容
     */
    @Column(name = "template_content")
    private String templateContent;

    /**
     * 类型
     */
    @Column(name = "template_type")
    private String templateType;

    /**
     * 关键词
     */
    @Column(name = "template_key")
    private String templateKey;
}
