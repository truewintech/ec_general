/*
 * Copy Right@
 */
package com.message.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Entity
@Table(name = "message_log")
@DynamicUpdate
@DynamicInsert
@NoArgsConstructor
@Getter
@Setter
public class MessageLog {

    /**
     * 主键
     */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    /**
     * 模板
     */
    @ManyToOne
    @JoinColumn(name = "template_id")
    private MessageTemplate messageTemplate;

    /**
     * 类型
     */
    @Column(name = "message_type")
    private String messageType;

    /**
     * 标题
     */
    @Column(name = "title")
    private String title;

    /**
     * 内容
     */
    @Column(name = "content")
    private String content;

    /**
     * 接收者
     */
    @Column(name = "receiver")
    private String receiver;

    /**
     * 发送时间
     */
    @Column(name = "send_time")
    private Date sendTime;

    /**
     * 状态
     */
    @Column(name = "send_status")
    private String sendStatus;

    /**
     * 插入时执行
     */
    @PrePersist
    public void prePersist() {
        this.sendTime = new Date();
    }
}
