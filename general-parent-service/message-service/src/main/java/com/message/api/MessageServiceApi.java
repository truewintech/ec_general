/*
 * Copy Right@
 */
package com.message.api;

import com.common.constant.Const;
import com.common.constant.DbConst;
import com.framework.data.redis.RedisManager;
import com.framework.entity.MsgRequestBean;
import com.framework.exception.MessageException;
import com.framework.mvc.api.BaseApi;
import com.framework.mvc.result.Result;
import com.message.amqp.Sender;
import com.message.service.RestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@RestController
@RequestMapping("message")
@Api(value = "消息API")
@Slf4j
public class MessageServiceApi extends BaseApi {

    /**
     * 短消息服务
     */
    @Autowired
    private Sender sender;

    /**
     * 缓存服务
     */
    @Autowired
    private RedisManager redisManager;

    /**
     * rest服务
     */
    @Autowired
    private RestService restService;

    /**
     * 发送消息
     *
     * @param msgRequestBean 消息对象
     * @return 成功
     */
    @ApiOperation(value = "发送消息")
    @PostMapping("sendMessage")
    public Result sendMessage(@RequestBody MsgRequestBean msgRequestBean) {
        sender.sendMessage(msgRequestBean);
        return success();
    }

    /**
     * 发送注册验证码带图形验证码
     *
     * @param mobile                手机号
     * @param imageVerificationCode 图形验证吗
     * @return 发送是否成功标记
     */
    @ApiOperation(value = "发送注册验证码")
    @GetMapping("sendCustomerRegisterValidCode")
    public Result sendCustomerRegisterValidCode(@RequestParam String mobile, @RequestParam(required = false) String imageVerificationCode) {
        boolean needValid = false;
        try {
            String param = restService.getSystemParameter(DbConst.SystemParameter.Id.SEND_SMS_NEED_VALID);
            needValid = Boolean.valueOf(param);
        } catch (Exception ex) {
            log.warn("获取系统参数{}异常", DbConst.SystemParameter.Id.SEND_SMS_NEED_VALID);
        }
        if (needValid) {
            // 获取当前用户图形验证码正确值
            String currentImageVerificationCode = redisManager.getCache(Const.CacheKey.VERIFICATION_CODE_REGISTER_PRE + mobile);
            if (StringUtils.isBlank(currentImageVerificationCode)) {
                log.info("请求注册时图形验证码已失效");
                throw new MessageException("图形验证码已失效！请检查");
            }
            if (!currentImageVerificationCode.equals(imageVerificationCode)) {
                log.info("请求注册时图形验证码不正确");
                throw new MessageException("图形验证码不正确！请检查");
            }
        }
        MsgRequestBean msgRequestBean = new MsgRequestBean();
        msgRequestBean.setReceiver(mobile);
        msgRequestBean.setMsgTemplateId(DbConst.MessageTemplate.Id.SMS_REGISTER_VALID_ID);
        // 封装验证码参数
        String code = RandomStringUtils.randomNumeric(4);
        msgRequestBean.getMessageParams().put("vcode", code);
        sender.sendMessage(msgRequestBean);
        //删除校验码缓存，防止重复提交
        redisManager.delCache(Const.CacheKey.VERIFICATION_CODE_REGISTER_PRE + mobile);
        return success();
    }

    /**
     * 发送修改手机号验证码带图形验证码
     *
     * @param mobile                手机号
     * @param imageVerificationCode 图形验证吗
     * @return 发送是否成功标记
     */
    @ApiOperation(value = "发送修改手机号验证码")
    @GetMapping("sendUpdateMobileValidCode")
    public Result sendUpdateMobileValidCode(@RequestParam String mobile, @RequestParam(required = false) String imageVerificationCode) {
        boolean needValid = false;
        try {
            String param = restService.getSystemParameter(DbConst.SystemParameter.Id.SEND_SMS_NEED_VALID);
            needValid = Boolean.valueOf(param);
        } catch (Exception ex) {
            log.warn("获取系统参数{}异常", DbConst.SystemParameter.Id.SEND_SMS_NEED_VALID);
        }
        if (needValid) {
            // 获取当前用户图形验证码正确值
            String currentImageVerificationCode = redisManager.getCache(Const.CacheKey.VERIFICATION_CODE_UPDATE_MOBILE_PRE + mobile);
            if (StringUtils.isBlank(currentImageVerificationCode)) {
                log.info("请求修改手机号时图形验证码已失效");
                throw new MessageException("图形验证码已失效！请检查");
            }
            if (!currentImageVerificationCode.equals(imageVerificationCode)) {
                log.info("请求修改手机号图形验证码不正确");
                throw new MessageException("图形验证码不正确！请检查");
            }
        }
        MsgRequestBean msgRequestBean = new MsgRequestBean();
        msgRequestBean.setReceiver(mobile);
        msgRequestBean.setMsgTemplateId(DbConst.MessageTemplate.Id.SMS_UPDATE_MOBILE_VALID_ID);
        // 封装验证码参数
        String code = RandomStringUtils.randomNumeric(4);
        msgRequestBean.getMessageParams().put("vcode", code);
        sender.sendMessage(msgRequestBean);
        //删除校验码缓存，防止重复提交
        redisManager.delCache(Const.CacheKey.VERIFICATION_CODE_UPDATE_MOBILE_PRE + mobile);
        return success();
    }

    /**
     * 发送忘记密码验证码带图形验证码
     *
     * @param mobile                手机号
     * @param imageVerificationCode 图形验证吗
     * @return 发送是否成功标记
     */
    @ApiOperation(value = "发送记密码验证码短信验证码")
    @GetMapping("view/sendForgetPasswordValidCode")
    public Result sendForgetPasswordValidCode(@RequestParam String mobile, @RequestParam(required = false) String imageVerificationCode) {
        boolean needValid = false;
        try {
            String param = restService.getSystemParameter(DbConst.SystemParameter.Id.SEND_SMS_NEED_VALID);
            needValid = Boolean.valueOf(param);
        } catch (Exception ex) {
            log.warn("获取系统参数{}异常", DbConst.SystemParameter.Id.SEND_SMS_NEED_VALID);
        }
        if (needValid) {
            // 获取当前用户图形验证码正确值
            String currentImageVerificationCode = redisManager.getCache(Const.CacheKey.VERIFICATION_CODE_FORGET_PASSWORD_PRE + mobile);
            if (StringUtils.isBlank(currentImageVerificationCode)) {
                log.info("请求修改手机号时图形验证码已失效");
                throw new MessageException("图形验证码已失效！请检查");
            }
            if (!currentImageVerificationCode.equals(imageVerificationCode)) {
                log.info("请求修改手机号图形验证码不正确");
                throw new MessageException("图形验证码不正确！请检查");
            }
        }
        MsgRequestBean msgRequestBean = new MsgRequestBean();
        msgRequestBean.setReceiver(mobile);
        msgRequestBean.setMsgTemplateId(DbConst.MessageTemplate.Id.SMS_FORGET_PASSWORD_VALID_ID);
        // 封装验证码参数
        String code = RandomStringUtils.randomNumeric(4);
        msgRequestBean.getMessageParams().put("vcode", code);
        sender.sendMessage(msgRequestBean);
        //删除校验码缓存，防止重复提交
        redisManager.delCache(Const.CacheKey.VERIFICATION_CODE_FORGET_PASSWORD_PRE + mobile);
        return success();
    }

}
