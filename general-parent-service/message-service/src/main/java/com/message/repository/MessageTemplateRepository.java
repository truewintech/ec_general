/*
 * Copy Right@
 */
package com.message.repository;

import com.message.entity.MessageTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface MessageTemplateRepository extends JpaRepository<MessageTemplate, Integer>,
        JpaSpecificationExecutor<MessageTemplate> {
}