/*
 * Copy Right@
 */
package com.message.service;

import com.framework.entity.MsgRequestBean;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface MessageService {

    /**
     * 发送消息
     *
     * @param msgRequestBean 消息请求体
     * @return 成功返回true
     */
    boolean sendMessage(MsgRequestBean msgRequestBean);

}
