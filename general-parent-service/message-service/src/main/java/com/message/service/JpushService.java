/*
 * Copy Right@
 */
package com.message.service;

import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface JpushService {

    /**
     * 发送jpush消息
     *
     * @param title  标题
     * @param extras 附加信息
     * @param ids    接收者ID
     * @return 发送结果
     */
    boolean sendMessage(String title, Map<String, String> extras, String... ids);

}
