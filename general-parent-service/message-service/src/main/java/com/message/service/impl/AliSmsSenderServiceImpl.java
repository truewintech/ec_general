/*
 * Copy Right@
 */
package com.message.service.impl;

import com.framework.sms.SmsUtil;
import com.message.service.SmsSenderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class AliSmsSenderServiceImpl implements SmsSenderService {

    @Override
    public void send(String templateCode, Map<String, String> messageSenderParams, String phoneNo) {
        SmsUtil.sendSMS(templateCode, messageSenderParams, phoneNo);
    }
}
