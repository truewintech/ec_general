/*
 * Copy Right@
 */
package com.message.service;

import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
public interface SmsSenderService {

    /**
     * 发送短信
     *
     * @param templateCode 模板
     * @param params       参数
     * @param phoneNo      手机号
     */
    void send(String templateCode, Map<String, String> params, String phoneNo);
}
