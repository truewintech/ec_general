/*
 * Copy Right@
 */
package com.message.service.impl;

import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.PushPayload;
import com.framework.jpush.JpushConfig;
import com.message.service.JpushService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class JpushServiceImpl implements JpushService {

    /**
     * 消息模板数据仓库
     */
    @Autowired
    private JPushClient jPushClient;

    @Override
    public boolean sendMessage(String title, Map<String, String> extras, String... ids) {
        try {
            PushPayload pushPayload = JpushConfig.buildPushObjectByIds(title, extras, ids);
            PushResult result = jPushClient.sendPush(pushPayload);
            log.info("发送jPush消息{}成功", result.msg_id);
            return true;
        } catch (APIConnectionException e) {
            log.error("发送jPush消息失败，连接失败");
        } catch (APIRequestException e) {
            log.error("接收jPush消息失败请求失败", e);
        }

        return false;
    }
}
