/*
 * Copy Right@
 */
package com.message.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.common.constant.Const;
import com.common.constant.DbConst;
import com.framework.data.redis.RedisManager;
import com.framework.entity.MsgRequestBean;
import com.framework.exception.MessageException;
import com.framework.util.WeChatUtil;
import com.message.entity.MessageLog;
import com.message.entity.MessageTemplate;
import com.message.repository.MessageLogRepository;
import com.message.repository.MessageTemplateRepository;
import com.message.service.JpushService;
import com.message.service.MessageService;
import com.message.service.SmsSenderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * </pre>
 *
 * @author xiongfei
 * @date 2017年09月04日
 */
@Service
@Slf4j
public class MessageServiceImpl implements MessageService {

    /**
     * 消息模板数据仓库
     */
    @Autowired
    private MessageTemplateRepository messageTemplateRepository;

    /**
     * 消息数据仓库
     */
    @Autowired
    private MessageLogRepository messageLogRepository;

    /**
     * 短信发送服务
     */
    @Autowired
    @Qualifier("aliSmsSenderServiceImpl")
    private SmsSenderService smsSenderService;

    /**
     * JPush服务
     */
    @Autowired
    private JpushService jPushService;

    /**
     * 缓存服务
     */
    @Autowired
    private RedisManager redisManager;

    /**
     * 微信公众号
     */
    @Value("${weChat.appConfig}")
    private String weChatAppConfig;

    @Override
    public boolean sendMessage(MsgRequestBean msgRequestBean) {
        String receiver = msgRequestBean.getReceiver();
        if (StringUtils.isBlank(receiver)) {
            log.error("接收对象不能为空");
            throw new MessageException("接收对象不能为空");
        }

        Integer msgTemplateId = msgRequestBean.getMsgTemplateId();
        if (msgTemplateId == null) {
            log.error("消息模板templateId不能为空");
            throw new MessageException("消息模板templateId不能为空");
        }

        MessageTemplate messageTemplate = messageTemplateRepository.findOne(msgTemplateId);
        if (messageTemplate == null) {
            log.error("messageTemplate不能为空");
            throw new MessageException("消息模板不能为空");
        }
        String templateType = messageTemplate.getTemplateType();
        String msgTemplateKey = messageTemplate.getTemplateKey();

        Map<String, String> messageParams = msgRequestBean.getMessageParams();
        String key = "";
        if (DbConst.MessageTemplate.Id.SMS_REGISTER_VALID_ID.equals(msgTemplateId)) {
            // 判断是否重复发送
            key = Const.CacheKey.SMS_VERIFICATION_CODE_REGISTER_PRE + receiver;
        } else if (DbConst.MessageTemplate.Id.SMS_UPDATE_MOBILE_VALID_ID.equals(msgTemplateId)) {
            // 判断是否重复发送
            key = Const.CacheKey.SMS_VERIFICATION_CODE_UPDATE_MOBILE_PRE + receiver;
        } else if (DbConst.MessageTemplate.Id.SMS_FORGET_PASSWORD_VALID_ID.equals(msgTemplateId)) {
            key = Const.CacheKey.SMS_VERIFICATION_CODE_FORGET_PASSWORD_PRE + receiver;
        }
        if (StringUtils.isBlank(key)) {
            log.error("参数错误，无法通过短信模板Id获得缓存key");
            throw new MessageException("短信发送失败，请稍后重试");
        }
        Map<String, Object> params = redisManager.getCache(key);
        if (params != null) {
            Date sendTime = (Date) params.get(Const.RegistrationCode.SEND_TIME_KEY);
            if (sendTime == null || System.currentTimeMillis() - sendTime.getTime() < Const.RegistrationCode.FORBIDDEN_TIME) {
                log.warn("手机{}重复发送验证", receiver);
                throw new MessageException("验证码已发送，请勿重复尝试发送验证码");
            }
        }

        // 保存验证码
        String code = messageParams.get("vcode");
        params = new HashMap<>(20);
        params.put(Const.RegistrationCode.VALID_CODE_KEY, code);
        params.put(Const.RegistrationCode.SEND_TIME_KEY, new Date());

        boolean success = redisManager.setCache(key, params, Const.RegistrationCode.OVERTIME);
        if (!success) {
            log.error("保存手机{}短信验证码{}失败", receiver, code);
            throw new MessageException("保存手机短信验证码失败");
        }


        String content = messageTemplate.getTemplateContent();
        if (StringUtils.isBlank(content)) {
            log.error("templateContent为空");
            throw new MessageException("消息模板不能为空");
        }

        final String[] templateContent = {content};
        if (messageParams.size() > 0) {
            messageParams.forEach((String k, String v) -> {
                if (v == null) {
                    v = "";
                    log.warn("参数{}的值为null", k);
                }
                templateContent[0] = templateContent[0].replaceAll("\\$?\\{" + k + "\\}", v);
            });
        }


        // 记录消息发送日志
        String title = messageTemplate.getTitle();
        if (StringUtils.isBlank(title)) {
            title = msgRequestBean.getTitle();
        }
        String linkUrl = msgRequestBean.getLinkUrl();
        MessageLog messageLog = new MessageLog();
        messageLog.setMessageType(templateType);
        messageLog.setContent(templateContent[0]);
        messageLog.setReceiver(receiver);
        messageLog.setTitle(title);
        messageLogRepository.save(messageLog);
        log.info("消息发送成功，消息内容为：" + templateContent[0]);

        if (StringUtils.equals(DbConst.MessageTemplate.TemplateType.SMS, templateType)) {
            //通过网关发送短信
            smsSenderService.send(msgTemplateKey, messageParams, receiver);
        } else if (DbConst.MessageTemplate.TemplateType.APP.equals(templateType)) {
            //JPush到APP
            String appReceiver = redisManager.getCache(Const.CacheKey.AJPUSH_REGISTRATION_ID_PRE + receiver);
            if (StringUtils.isBlank(appReceiver)) {
                // 找不到APP消息接收者
                log.error("APP消息没有接收者");
                throw new MessageException("APP消息没有接收者");
            }
            Map<String, String> extras = new HashMap<>(20);
            extras.put("linkUrl", linkUrl);
            jPushService.sendMessage(title, extras, appReceiver);
        } else if (DbConst.MessageTemplate.TemplateType.WX.equals(templateType)) {
            JSONObject jsonObject = JSON.parseObject(receiver);
            String openId = jsonObject.getString("openId");
            String appId = jsonObject.getString("appId");
            if (StringUtils.isBlank(openId) || StringUtils.isBlank(appId)) {
                log.error("接收对象openId微信公众号appId不能为空");
                throw new MessageException("接收对象不能为空");
            }

            //依据appId和模板key从配置文件中读取模板ID
            String templateId = getTemplateIdByKey(appId, msgTemplateKey);

            //获取缓存中的accessToken
            String accessToken = redisManager.getCache(Const.CacheKey.WECHAT_ACCESS_TOKEN_PRE + appId);
            if (StringUtils.isBlank(accessToken)) {
                log.error("获取微信公众号的accessToken失败，appId为：{}", appId);
                throw new MessageException("获取微信公众号的accessToken失败");
            }
            //发送微信消息
            WeChatUtil.sendWeChatMessage(appId, openId, accessToken, templateId, msgRequestBean.getLinkUrl(), messageParams);
        }

        return true;
    }

    /**
     * 依据微信模板类型获取模板id
     *
     * @param appId          微信ID
     * @param msgTemplateKey 模板key
     * @return 模板ID
     */
    private String getTemplateIdByKey(String appId, String msgTemplateKey) {
        if (StringUtils.isBlank(appId) || StringUtils.isBlank(msgTemplateKey)) {
            log.error("依据微信模板类型获取模板id失败，appId或模板key为空");
            throw new MessageException("依据微信模板类型获取模板id失败，appId或模板key为空");
        }

        JSONArray jsonArray = JSON.parseArray(weChatAppConfig);
        if (jsonArray == null || jsonArray.size() <= 0) {
            log.error("依据微信模板类型获取模板id失败，微信配置为空");
            throw new MessageException("依据微信模板类型获取模板id失败，微信配置为空");
        }

        String templateId = null;
        for (int i = 0; i < jsonArray.size(); i++) {
            com.alibaba.fastjson.JSONObject config = jsonArray.getJSONObject(i);
            if (config != null) {
                String configAppId = config.getString("appId");
                if (StringUtils.equals(appId, configAppId)) {
                    String msgTemplate = config.getString("msgTemplate");
                    com.alibaba.fastjson.JSONObject msgTemplateConfig = JSON.parseObject(msgTemplate);
                    templateId = msgTemplateConfig.getString(msgTemplateKey);
                    break;
                }
            }
        }
        return templateId;
    }

}
